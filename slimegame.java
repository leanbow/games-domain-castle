import jagex.object3d;
import jagex.pixmap;
import jagex.tools;
import jagex.world3d;

public class slimegame extends module {
	int anInt930 = 7;
	int anInt931 = anInt930 + 1;
	gameclient anApplet_Sub1_Sub1_Sub1_932;
	pixmap aClass6_933;
	world3d aClass10_934;
	world3d aClass10_935;
	int anInt936;
	object3d aClass7_937;
	object3d aClass7_938;
	object3d aClass7_939;
	object3d[] aClass7Array940 = new object3d[anInt930 * anInt930];
	int anInt941;
	int anInt942 = 512;
	int anInt943 = 896;
	int anInt944;
	int anInt945;
	int anInt946;
	int anInt947;
	boolean aBoolean948 = false;
	boolean aBoolean949 = false;
	boolean aBoolean950 = false;
	boolean aBoolean951 = false;
	int anInt952;
	int anInt953 = 64;
	int anInt954 = -1;
	int anInt955 = -1;
	int anInt956 = 100;
	int anInt957;
	int anInt958 = 60;
	int anInt959;
	int anInt960;
	boolean aBoolean961 = true;
	int[][] anIntArrayArray962 = new int[anInt930][anInt930];
	int[][] anIntArrayArray963 = new int[anInt930][anInt930];
	int[][] anIntArrayArray964 = new int[anInt930][anInt930];

	public void onInit(gameclient applet_sub1_sub1_sub1, int i) {
		anApplet_Sub1_Sub1_Sub1_932 = applet_sub1_sub1_sub1;
		anInt936 = i;
		aClass6_933 = applet_sub1_sub1_sub1.pixelMap;
		aClass10_934 = applet_sub1_sub1_sub1.aClass10_1086;
		aClass10_935 = applet_sub1_sub1_sub1.aClass10_1084;
		System.out.println("getting 2d");
		try {
			aClass6_933.method249(applet_sub1_sub1_sub1.aByteArray1083, tools.findFile("2dslime.tga", (applet_sub1_sub1_sub1.aByteArray1083)), i, true, 1, false);
		} catch (Exception exception) {
			System.out.println("Error");
		}
		System.out.println("getting 3d");
		try {
			aClass7_939 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("slime.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
			aClass7_938 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("board.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		} catch (Exception exception) {
			System.out.println("Error");
		}
		System.out.println("Creating board");
		aClass7_937 = new object3d(anInt931 * anInt931, anInt930 * anInt930);
		for (int i_0_ = 0; i_0_ < anInt931; i_0_++) {
			for (int i_1_ = 0; i_1_ < anInt931; i_1_++)
				aClass7_937.addVertex((i_1_ + i_1_ - anInt930) * 8 * anInt930, 0, (i_0_ + i_0_ - anInt930) * 8 * anInt930);
		}
		for (int i_2_ = 0; i_2_ < anInt930; i_2_++) {
			for (int i_3_ = 0; i_3_ < anInt930; i_3_++) {
				int[] is = new int[4];
				is[0] = i_2_ * anInt931 + i_3_;
				is[1] = i_2_ * anInt931 + i_3_ + 1;
				is[2] = i_2_ * anInt931 + i_3_ + anInt931 + 1;
				is[3] = i_2_ * anInt931 + i_3_ + anInt931;
				aClass7_937.addPolygon(4, is, 16, 16);
			}
		}
		aClass7_937.method279(50);
		aClass7_938.method280(true, 52, 32, -50, -10, -50);
		aClass7_937.method280(true, 52, 32, -50, -10, -50);
		aClass7_939.method280(true, 52, 32, -50, -10, -50);
		System.out.println("Copying pieces");
		for (int i_4_ = 0; i_4_ < anInt930 * anInt930; i_4_++)
			aClass7Array940[i_4_] = aClass7_939.copy();
	}

	public void method113(int i) {
		anInt960 = i;
		anInt942 = 0;
		aClass10_934.clearObjects();
		aClass10_935.clearObjects();
		aClass10_934.method362(256, 152, 256, 152, 512, 9);
		aClass10_934.maxRenderZ = 3000;
		aClass10_934.anInt464 = 3000;
		aClass10_934.anInt465 = 20;
		aClass10_934.anInt466 = 5000;
		aClass10_935.method385(aClass10_934);
		aClass10_934.clearObjects();
		aClass10_935.clearObjects();
		aClass10_934.addObject(aClass7_938);
		aClass10_934.addObject(aClass7_937);
		for (int i_5_ = 0; i_5_ < anInt930; i_5_++) {
			for (int i_6_ = 0; i_6_ < anInt930; i_6_++)
				anIntArrayArray962[i_5_][i_6_] = 0;
		}
		anIntArrayArray962[0][0] = 1;
		anIntArrayArray962[0][anInt930 - 1] = 2;
		anIntArrayArray962[anInt930 - 1][0] = 2;
		anIntArrayArray962[anInt930 - 1][anInt930 - 1] = 1;
		for (int i_7_ = 0; i_7_ < 32; i_7_++)
			method173();
		aBoolean961 = true;
		anInt952 = 0;
		aBoolean949 = false;
		aBoolean950 = false;
		anInt956 = 100;
	}

	public void method121() {
		aClass10_934.clearObjects();
		aClass10_935.clearObjects();
		aClass10_934.addObject(aClass7_938);
		aClass10_934.addObject(aClass7_937);
	}

	public void method173() {
		aBoolean951 = false;
		for (int i = 0; i < anInt930; i++) {
			for (int i_8_ = 0; i_8_ < anInt930; i_8_++) {
				int i_9_ = 64;
				int i_10_ = 0;
				int i_11_ = method178(i, i_8_);
				if (method178(i - 1, i_8_) == i_11_)
					i_9_ += 24;
				if (method178(i, i_8_ - 1) == i_11_)
					i_9_ += 24;
				if (method178(i + 1, i_8_) == i_11_)
					i_9_ += 24;
				if (method178(i, i_8_ + 1) == i_11_)
					i_9_ += 24;
				if (anIntArrayArray962[i][i_8_] == 0)
					i_9_ = 0;
				else if (anIntArrayArray962[i][i_8_] == 1)
					i_10_ = 0;
				else if (anIntArrayArray962[i][i_8_] == 2)
					i_10_ = 256;
				if (anIntArrayArray964[i][i_8_] == 0)
					anIntArrayArray963[i][i_8_] = i_10_;
				if (anIntArrayArray964[i][i_8_] < i_9_) {
					anIntArrayArray964[i][i_8_] += 4;
					aBoolean951 = true;
				} else if (anIntArrayArray964[i][i_8_] > i_9_) {
					anIntArrayArray964[i][i_8_] -= 4;
					aBoolean951 = true;
				}
				if (anIntArrayArray963[i][i_8_] < i_10_) {
					anIntArrayArray963[i][i_8_] += 4;
					aBoolean951 = true;
				} else if (anIntArrayArray963[i][i_8_] > i_10_) {
					anIntArrayArray963[i][i_8_] -= 4;
					aBoolean951 = true;
				}
			}
		}
	}

	public boolean method114() {
		if (aBoolean951)
			return false;
		return true;
	}

	public void method103(int i, int i_12_, byte[] is) {
		if (i == 255) {
			anInt957 = is[1] & 0xff;
			anInt958 = is[2] & 0xff;
			anInt956 = 100;
			if (anInt957 != anInt960) {
				aBoolean961 = false;
				anInt954 = -1;
			}
		} else if (i == 254) {
			int i_13_ = 1;
			boolean bool = false;
			for (int i_14_ = 0; i_14_ < anInt930; i_14_++) {
				for (int i_15_ = 0; i_15_ < anInt930; i_15_++) {
					int i_16_ = is[i_13_++] & 0xff;
					if (anIntArrayArray962[i_14_][i_15_] != i_16_)
						bool = true;
					anIntArrayArray962[i_14_][i_15_] = i_16_;
				}
			}
			anInt957 = is[i_13_++] & 0xff;
			anInt958 = is[i_13_++] & 0xff;
			anInt956 = 100;
			aBoolean961 = false;
			anApplet_Sub1_Sub1_Sub1_932.aBoolean604 = false;
			anInt954 = -1;
			if (bool && aBoolean142)
				anApplet_Sub1_Sub1_Sub1_932.xmPlayer.method85(0, 6, 8000, 63);
		}
	}

	public void onLoop() {
		anInt959++;
		if (anApplet_Sub1_Sub1_Sub1_932.mouseDown == 2 && !aBoolean948) {
			if (anInt946 == 0) {
				anInt946 = anApplet_Sub1_Sub1_Sub1_932.mouseDown;
				anInt944 = anApplet_Sub1_Sub1_Sub1_932.mouseX;
				anInt945 = anApplet_Sub1_Sub1_Sub1_932.mouseY;
			}
			int i = anApplet_Sub1_Sub1_Sub1_932.mouseX - anInt944;
			int i_17_ = anApplet_Sub1_Sub1_Sub1_932.mouseY - anInt945;
			anInt942 = anInt942 + i * 2 + 1024 & 0x3ff;
			anInt943 = anInt943 - i_17_ * 2 + 1024 & 0x3ff;
			if (anInt943 > 256 && anInt943 < 600)
				anInt943 = 600;
			if (anInt943 > 960 || anInt943 < 256)
				anInt943 = 960;
			anInt944 = anApplet_Sub1_Sub1_Sub1_932.mouseX;
			anInt945 = anApplet_Sub1_Sub1_Sub1_932.mouseY;
		} else
			anInt946 = 0;
		if (anApplet_Sub1_Sub1_Sub1_932.mouseClick == 1)
			anInt947 = 1;
		method173();
		anInt956--;
		if (anInt956 <= 0 && anInt958 > 10) {
			anInt956 = 50;
			anInt958--;
		}
	}

	public void method116(int i, int i_18_, int i_19_) {
		method177();
		method174();
		aClass10_934.setCamera(0, 100, 0, i, i_18_, 0, i_19_);
		aClass10_934.method367();
		aClass10_935.setCamera(0, 100, 0, i, i_18_, 0, i_19_);
		aClass10_935.method367();
	}

	public void onDraw() {
		aClass6_933.method241(0, 0, 512, 384, 0, 4210752);
		if (aBoolean948)
			method176();
		else {
			int i = 1400;
			aClass10_934.method358(anApplet_Sub1_Sub1_Sub1_932.mouseX, anApplet_Sub1_Sub1_Sub1_932.mouseY, 50, 50);
			method116(anInt943, anInt942, i);
		}
		if (aBoolean961)
			aClass6_933.method268((anApplet_Sub1_Sub1_Sub1_932.generalGameModules[0]), 7, 12, 1, 16777215);
		else if (anInt957 == anInt960) {
			aClass6_933.method268((anApplet_Sub1_Sub1_Sub1_932.generalGameModules[1]), 7, 12, 1, 16777215);
			if (anInt954 == -1)
				aClass6_933.method268((anApplet_Sub1_Sub1_Sub1_932.game7Modules[anInt960]), 7, 23, 1, 16777215);
			else
				aClass6_933.method268((anApplet_Sub1_Sub1_Sub1_932.game7Modules[2]), 7, 23, 1, 16777215);
		} else
			aClass6_933.method268((anApplet_Sub1_Sub1_Sub1_932.generalGameModules[2]), 7, 12, 1, 16777215);
		aClass6_933.method268((anApplet_Sub1_Sub1_Sub1_932.generalGameModules[3] + anInt958), 7, 34, 1, 16777215);
		int i = 460;
		int i_20_ = 4;
		anApplet_Sub1_Sub1_Sub1_932.aClass8_1094.method321(i, i_20_, 46, 28);
		aClass6_933.method267(anApplet_Sub1_Sub1_Sub1_932.generalGameModules[4], i + 22, i_20_ + 13, 1, 0);
		if (aBoolean948)
			aClass6_933.method267((anApplet_Sub1_Sub1_Sub1_932.generalGameModules[5]), i + 22, i_20_ + 23, 1, 0);
		else
			aClass6_933.method267((anApplet_Sub1_Sub1_Sub1_932.generalGameModules[6]), i + 22, i_20_ + 23, 1, 0);
		i = 460;
		i_20_ = 34;
		anApplet_Sub1_Sub1_Sub1_932.aClass8_1094.method321(i, i_20_, 46, 28);
		aClass6_933.method267(anApplet_Sub1_Sub1_Sub1_932.generalGameModules[7], i + 22, i_20_ + 18, 1, 0);
		i = 460;
		i_20_ = 64;
		anApplet_Sub1_Sub1_Sub1_932.aClass8_1094.method321(i, i_20_, 46, 28);
		if (anApplet_Sub1_Sub1_Sub1_932.aBoolean604)
			aClass6_933.method267((anApplet_Sub1_Sub1_Sub1_932.generalGameModules[8]), i + 22, i_20_ + 13, 1, 0);
		else
			aClass6_933.method267((anApplet_Sub1_Sub1_Sub1_932.generalGameModules[9]), i + 22, i_20_ + 13, 1, 0);
		aClass6_933.method267(anApplet_Sub1_Sub1_Sub1_932.generalGameModules[10], i + 22, i_20_ + 23, 1, 0);
		if (aBoolean949) {
			method175(anApplet_Sub1_Sub1_Sub1_932.generalGameModules[11], anApplet_Sub1_Sub1_Sub1_932.generalGameModules[12]);
			if (anApplet_Sub1_Sub1_Sub1_932.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_932.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_932.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_932.mouseY < 188 && anInt947 == 1) {
				anApplet_Sub1_Sub1_Sub1_932.resign();
				aBoolean949 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_932.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_932.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_932.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_932.mouseY < 188 && anInt947 == 1)
				aBoolean949 = false;
			anInt954 = -1;
			anInt947 = 0;
		} else if (aBoolean950) {
			if (anApplet_Sub1_Sub1_Sub1_932.aBoolean604)
				method175(anApplet_Sub1_Sub1_Sub1_932.generalGameModules[13], anApplet_Sub1_Sub1_Sub1_932.generalGameModules[14]);
			else
				method175(anApplet_Sub1_Sub1_Sub1_932.generalGameModules[15], anApplet_Sub1_Sub1_Sub1_932.generalGameModules[16]);
			if (anApplet_Sub1_Sub1_Sub1_932.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_932.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_932.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_932.mouseY < 188 && anInt947 == 1) {
				anApplet_Sub1_Sub1_Sub1_932.offerDraw();
				aBoolean950 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_932.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_932.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_932.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_932.mouseY < 188 && anInt947 == 1)
				aBoolean950 = false;
			anInt954 = -1;
			anInt947 = 0;
		} else {
			i = 460;
			i_20_ = 4;
			if (anInt947 == 1 && anApplet_Sub1_Sub1_Sub1_932.mouseX > i && anApplet_Sub1_Sub1_Sub1_932.mouseY > i_20_ && anApplet_Sub1_Sub1_Sub1_932.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_932.mouseY < i_20_ + 29) {
				aBoolean948 = !aBoolean948;
				anInt947 = 0;
			}
			i = 460;
			i_20_ = 34;
			if (anInt947 == 1 && anApplet_Sub1_Sub1_Sub1_932.mouseX > i && anApplet_Sub1_Sub1_Sub1_932.mouseY > i_20_ && anApplet_Sub1_Sub1_Sub1_932.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_932.mouseY < i_20_ + 29) {
				aBoolean949 = true;
				anInt947 = 0;
			}
			i = 460;
			i_20_ = 64;
			if (anInt947 == 1 && anApplet_Sub1_Sub1_Sub1_932.mouseX > i && anApplet_Sub1_Sub1_Sub1_932.mouseY > i_20_ && anApplet_Sub1_Sub1_Sub1_932.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_932.mouseY < i_20_ + 29) {
				aBoolean950 = true;
				anInt947 = 0;
			}
			int i_21_ = -1;
			int i_22_ = -1;
			if (anInt957 == anInt960 && !aBoolean961) {
				if (aBoolean948) {
					if (anApplet_Sub1_Sub1_Sub1_932.mouseX > 109 && anApplet_Sub1_Sub1_Sub1_932.mouseY > 4 && anApplet_Sub1_Sub1_Sub1_932.mouseX < 403 && anApplet_Sub1_Sub1_Sub1_932.mouseY < 298) {
						i_21_ = (anApplet_Sub1_Sub1_Sub1_932.mouseX - 109) / 42;
						i_22_ = 6 - ((anApplet_Sub1_Sub1_Sub1_932.mouseY - 4) / 42);
						if (i_21_ < 0)
							i_21_ = 0;
						else if (i_21_ > 6)
							i_21_ = 6;
						if (i_22_ < 0)
							i_22_ = 0;
						else if (i_22_ > 6)
							i_22_ = 6;
					}
				} else {
					int i_23_ = aClass10_934.method360();
					if (i_23_ >= 0 && i_23_ < anInt930 * anInt930) {
						i_21_ = i_23_ % anInt930;
						i_22_ = i_23_ / anInt930;
					}
				}
			}
			if (anInt954 == -1 && i_21_ != -1 && anInt947 == 1) {
				if (anIntArrayArray962[i_21_][i_22_] == anInt960 + 1) {
					anInt954 = i_21_;
					anInt955 = i_22_;
				}
			} else if (anInt954 != -1 && i_21_ != -1 && anInt947 == 1) {
				int i_24_ = i_21_ - anInt954;
				int i_25_ = i_22_ - anInt955;
				if (anIntArrayArray962[i_21_][i_22_] == 0 && i_24_ >= -2 && i_24_ <= 2 && i_25_ >= -2 && i_25_ <= 2) {
					anApplet_Sub1_Sub1_Sub1_932.aClass2_582.putPacketId(255);
					anApplet_Sub1_Sub1_Sub1_932.aClass2_582.p1(anInt954);
					anApplet_Sub1_Sub1_Sub1_932.aClass2_582.p1(anInt955);
					anApplet_Sub1_Sub1_Sub1_932.aClass2_582.p1(i_21_);
					anApplet_Sub1_Sub1_Sub1_932.aClass2_582.p1(i_22_);
					anApplet_Sub1_Sub1_Sub1_932.aClass2_582.flushNoException();
					if (aBoolean142)
						anApplet_Sub1_Sub1_Sub1_932.xmPlayer.method85(0, 6, 8000, 63);
					anIntArrayArray962[i_21_][i_22_] = anInt960 + 1;
					if (i_24_ == -2 || i_24_ == 2 || i_25_ == -2 || i_25_ == 2)
						anIntArrayArray962[anInt954][anInt955] = 0;
					aBoolean961 = true;
					for (int i_26_ = i_21_ - 1; i_26_ <= i_21_ + 1; i_26_++) {
						for (int i_27_ = i_22_ - 1; i_27_ <= i_22_ + 1; i_27_++) {
							if (method178(i_26_, i_27_) == 2 - anInt960)
								anIntArrayArray962[i_26_][i_27_] = anInt960 + 1;
						}
					}
				}
				anInt954 = -1;
				anInt955 = -1;
			}
			anInt947 = 0;
		}
	}

	public void method174() {
		for (int i = 0; i < anInt930; i++) {
			for (int i_28_ = 0; i_28_ < anInt930; i_28_++) {
				int i_29_ = i_28_ * anInt930 + i;
				aClass7_937.method283(i_29_, 16);
				aClass7_937.method284(i_29_, 16);
			}
		}
		if (anInt954 != -1) {
			int i = anInt955 * anInt930 + anInt954;
			aClass7_937.method283(i, -24577);
			aClass7_937.method284(i, -24577);
		}
	}

	public void method175(String string, String string_30_) {
		int i = 160;
		int i_31_ = 80;
		aClass6_933.method240(256 - i / 2 + 10, 150 - i_31_ / 2 + 10, i, i_31_, 0, 128);
		aClass6_933.method242(256 - i / 2, 150 - i_31_ / 2, i, i_31_, pixmap.method247(96, 96, 121));
		aClass6_933.method267(string, 256, 130, 4, 16777215);
		aClass6_933.method267(string_30_, 256, 150, 4, 16777215);
		int i_32_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_932.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_932.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_932.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_932.mouseY < 188)
			i_32_ = 16711680;
		aClass6_933.method267(anApplet_Sub1_Sub1_Sub1_932.generalGameModules[17], 226, 178, 4, i_32_);
		i_32_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_932.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_932.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_932.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_932.mouseY < 188)
			i_32_ = 16711680;
		aClass6_933.method267(anApplet_Sub1_Sub1_Sub1_932.generalGameModules[18], 286, 178, 4, i_32_);
	}

	public void method176() {
		int i = 109;
		int i_33_ = 256;
		for (int i_34_ = 0; i_34_ < 7; i_34_++) {
			for (int i_35_ = 0; i_35_ < 7; i_35_++) {
				if (anInt954 == i_34_ && anInt955 == i_35_)
					aClass6_933.method242(i_34_ * 42 + i, i_33_ - i_35_ * 42, 42, 42, pixmap.method247(192, 0, 0));
				else
					aClass6_933.method255(i_34_ * 42 + i, i_33_ - i_35_ * 42, anInt936);
			}
		}
		for (int i_36_ = 0; i_36_ < 7; i_36_++) {
			for (int i_37_ = 0; i_37_ < 7; i_37_++) {
				int i_38_ = anIntArrayArray964[i_36_][i_37_];
				int i_39_ = anIntArrayArray963[i_36_][i_37_];
				if (i_38_ > 0) {
					if (i_39_ > 255)
						i_39_ = 255;
					int i_40_ = pixmap.method247(0, 255 - i_39_, i_39_);
					aClass6_933.method239(i_36_ * 42 + i + 21, i_33_ - i_37_ * 42 + 21, i_38_ / 8, i_40_, 256);
				}
			}
		}
	}

	public void method177() {
		aClass10_935.clearObjects();
		anInt941 = 0;
		for (int i = 0; i < anInt930; i++) {
			for (int i_41_ = 0; i_41_ < anInt930; i_41_++) {
				int i_42_ = anIntArrayArray964[i][i_41_];
				int i_43_ = anIntArrayArray963[i][i_41_];
				if (i_42_ > 0) {
					object3d class7 = aClass7Array940[anInt941++];
					class7.requestScale(i_42_, i_42_, i_42_);
					int i_44_ = -1 - (i_43_ - 1) / 8 - (255 - i_43_) / 8 * 32;
					for (int i_45_ = 0; i_45_ < class7.polygonCount; i_45_++)
						class7.method283(i_45_, i_44_);
					class7.requestSetTranslate((i + i - anInt930 + 1) * 8 * anInt930, 0, ((i_41_ + i_41_ - anInt930 + 1) * 8 * anInt930));
					aClass10_935.addObject(class7);
				}
			}
		}
	}

	public int method178(int i, int i_46_) {
		if (i < 0 || i_46_ < 0 || i >= anInt930 || i_46_ >= anInt930)
			return 0;
		return anIntArrayArray962[i][i_46_];
	}
}
