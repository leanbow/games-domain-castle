import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import jagex.client;
import jagex.gui;
import jagex.megastream;
import jagex.pixmap;
import jagex.tools;
import jagex.world3d;
import jagex.xmplayer;

public class gameclient extends client {
	public static int gameCount = 8;
	public static boolean codebaseError;
	public int anInt1079 = 3;
	public String[] gameNameModules = { "Tetralink", "Warships", "Checkers", "Chess", "Reversi", "Go-Mad", "Bgammon", "SlimeWar" };
	public pixmap pixelMap;
	public byte[] aByteArray1082;
	public byte[] aByteArray1083;
	public world3d aClass10_1084;
	public world3d aClass10_1085;
	public world3d aClass10_1086;
	public world3d aClass10_1087;
	public boolean aBoolean1088 = false;
	public Graphics aGraphics1089;
	public int moduleId;
	public int oldModuleId = -1;
	public int anInt1092;
	public module[] modules = new module[20];
	gui aClass8_1094;
	int anInt1095;
	int anInt1096;
	xmplayer xmPlayer;
	String[] loginModules = new String[22];
	String[] chalroomModules = new String[39];
	String[] loadingModules = new String[6];
	String[] generalGameModules = new String[21];
	String[] warshipGameModules = new String[9];
	String[] chessGameModules = new String[1];
	String[] gomadGameModules = new String[4];
	String[] backgammonGameModules = new String[9];
	String[] game7Modules = new String[3];

	public static void main(String[] strings) {
		gameclient applet_sub1_sub1_sub1 = new gameclient();
		applet_sub1_sub1_sub1.startApplication(512, 384, "Jagex Software - Multiplayer Games", false);
	}

	public void mainInit() {
		connectionPort = 43592;
		client.aBoolean567 = true;
		minDelay = 10;
		if (this.isUsingApplet() && gameCount == 8) {
			String host = this.getDocumentBase().getHost().toLowerCase();
			if (!host.equals("") && !host.endsWith("runescape.com") && !host.endsWith("jagex.com") && !host.endsWith("jagex.co.uk") && !host.endsWith("gameon.it") && !host.endsWith("vcsy.com") && !host.endsWith("funbug.com") && !host.endsWith("local") && !host.endsWith("127.0.0.1")) {
				codebaseError = true;
				return;
			}
		}
		this.setFps(50);
		aGraphics1089 = this.getGraphics();
		pixelMap = new pixmap(512, 384, 800, this);
		loadTranslate();
		tools.loadingText = loadingModules[1];
		tools.unpackingText = loadingModules[2];
		world3d.texturesLoadingText = loadingModules[5];
		pixmap.aString252 = client.clientModules[34];
		if (gameCount == 5)
			gameNameModules[4] = gameNameModules[6];
		try {
			aByteArray1083 = tools.getFile("media.jag", loadingModules[4], 25, this);
		} catch (java.io.IOException ioexception) {
			System.out.println("Error loading media");
		}
		pixelMap.method249(aByteArray1083, tools.findFile("scrollbar.tga", aByteArray1083), 0, true, 2, false);
		pixelMap.method249(aByteArray1083, tools.findFile("corners.tga", aByteArray1083), 2, true, 4, false);
		gui.anInt435 = 110;
		gui.anInt436 = 110;
		gui.anInt437 = 155;
		gui.aBoolean433 = false;
		aClass10_1084 = new world3d(pixelMap, 2050, 2050, 100);
		aClass10_1085 = new world3d(pixelMap, 1400, 1400, 100);
		aClass10_1086 = new world3d(pixelMap, 200, 200, 100);
		aClass10_1087 = new world3d(pixelMap, 100, 100, 100);
		aClass10_1084.method362(256, 152, 256, 152, 512, 9);
		aClass10_1084.loadTextures("textures.jag", 4, 4, 50, this);
		aClass10_1085.copyTextureData(aClass10_1084);
		aClass10_1086.copyTextureData(aClass10_1084);
		aClass10_1087.copyTextureData(aClass10_1084);
		try {
			aByteArray1082 = tools.getFile("objects.jag", loadingModules[3], 75, this);
		} catch (java.io.IOException ioexception) {
			System.out.println("Error loading objects");
		}
		this.drawLoadingBar2(90, loadingModules[0]);
		modules[0] = new login();
		modules[0].onInit(this, 50);
		modules[1] = new lobby2();
		modules[1].onInit(this, 100);
		modules[2] = new chalroom();
		modules[2].onInit(this, 150);
		modules[3] = new gameroom();
		modules[3].onInit(this, 200);
		modules[4] = new tetragame();
		modules[4].onInit(this, 400);
		modules[5] = new wargame();
		modules[5].onInit(this, 350);
		modules[6] = new checkgame();
		modules[6].onInit(this, 250);
		modules[7] = new chessgame();
		modules[7].onInit(this, 300);
		if (gameCount > 5) {
			modules[8] = new revgame();
			modules[8].onInit(this, 450);
			modules[9] = new gogame();
			modules[9].onInit(this, 500);
			modules[10] = new bggame();
			modules[10].onInit(this, 550);
		} else {
			modules[8] = new bggame();
			modules[8].onInit(this, 550);
		}
		if (gameCount == 8) {
			modules[11] = new slimegame();
			modules[11].onInit(this, 600);
		}
		String string = "small";
		if (this.isUsingApplet())
			string = this.getParameter("chatsize");
		aClass8_1094 = new gui(pixelMap, 50);
		aClass8_1094.createButton(3, 304, 505, 78);
		aClass8_1094.aBoolean418 = false;
		if (string != null && string.equalsIgnoreCase("big")) {
			anInt1095 = aClass8_1094.method340(6, 304, 502, 64, 3, 20, true);
			anInt1096 = aClass8_1094.method342(8, 372, 498, 18, 3, 80, false, true);
			aClass8_1094.createHorizontalLine(0, 365, 512);
		} else {
			anInt1095 = aClass8_1094.method340(6, 304, 502, 64, 0, 20, true);
			anInt1096 = aClass8_1094.method342(8, 374, 498, 14, 0, 80, false, true);
			aClass8_1094.createHorizontalLine(0, 368, 512);
		}
		aClass8_1094.setInputComponentId(anInt1096);
		byte[] is = tools.getFile("sounds.xm", 1000, aByteArray1083);
		xmPlayer = new xmplayer(is);
		xmPlayer.start();
		moduleId = 0;
	}

	public synchronized void mainLoop() {
		if (!codebaseError) {
			if (moduleId != oldModuleId) {
				aBoolean1088 = true;
				modules[moduleId].onModuleChange();
				oldModuleId = moduleId;
				pixelMap.clear();
			}
			modules[moduleId].aBoolean142 = true;
			modules[moduleId].onLoop();
			if (moduleId > 0) {
				this.method34();
				if (mouseY > 304 && (mouseClick != 0 || mouseDown != 0))
					aBoolean1088 = true;
				aClass8_1094.process(mouseX, mouseY, mouseClick, mouseDown);
				if (aClass8_1094.isComponentClicked(anInt1096)) {
					String string = aClass8_1094.getText(anInt1096);
					aClass8_1094.setText(anInt1096, "");
					if (string.equalsIgnoreCase("simlostcon"))
						aClass2_582 = new megastream();
					this.method38(string);
					aBoolean1088 = true;
				}
			}
			mouseClick = 0;
		}
	}

	public synchronized void mainRedraw() {
		if (codebaseError) {
			Graphics graphics = this.getGraphics();
			graphics.setColor(Color.black);
			graphics.fillRect(0, 0, 512, 384);
			graphics.setFont(new Font("Helvetica", 1, 20));
			graphics.setColor(Color.white);
			graphics.drawString("Error - Unable to load game!", 50, 50);
			graphics.drawString("To play this game make sure you play from", 50, 100);
			graphics.drawString("an authorised website", 50, 150);
			this.setFps(1);
		} else {
			if (moduleId != oldModuleId) {
				aBoolean1088 = true;
				modules[moduleId].onModuleChange();
				oldModuleId = moduleId;
				pixelMap.clear();
			}
			modules[moduleId].onDraw();
			if (moduleId > 0 && aBoolean1088)
				aClass8_1094.draw();
			if (aBoolean1088 || moduleId == 0) {
				aBoolean1088 = false;
				pixelMap.drawImage(aGraphics1089, 0, 0);
			} else
				pixelMap.drawImage(aGraphics1089, 0, 0, 512, 304);
		}
	}

	public void mainDestroy() {
		this.method31();
		if (xmPlayer != null) {
			xmPlayer.stop();
			xmPlayer = null;
		}
	}

	public void mainKeyDown(int i) {
		modules[moduleId].onKeyDown(i);
		if (moduleId > 0) {
			aClass8_1094.processInput(i);
			aBoolean1088 = true;
		}
	}

	public void method63(String string) {
		aClass8_1094.method341(anInt1095, string, true);
		aBoolean1088 = true;
	}

	public void toLoginScreen() {
		moduleId = 0;
	}

	public void method54() {
		moduleId = 1;
		oldModuleId = -1;
	}

	public void method77(int i) {
		mouseClick = 0;
		mouseDown = 0;
		anInt1092 = i;
		if (i >= gameCount)
			i = 0;
		for (int i_0_ = 0; i_0_ < 20; i_0_++) {
			pixelMap.method246();
			pixelMap.drawImage(aGraphics1089, 0, 0);
			this.resetOldTimes();
		}
		this.sendModuleChosenPacket(i + 1);
		int i_1_ = modules[i + 4].id;
		if (i_1_ == 0)
			moduleId = 2;
		else
			moduleId = 3;
		modules[moduleId].name = gameNameModules[i];
		modules[moduleId].id = i;
	}

	public void method78() {
		this.sendModuleChosenPacket(0);
		moduleId = 1;
	}

	public void method68(int i) {
		aBoolean604 = false;
		moduleId = anInt1092 + 4;
		modules[moduleId].method113(i);
	}

	public void method71(String string, String string_2_, String string_3_) {
		int i = modules[anInt1092 + 4].id;
		if (i == 0)
			moduleId = 2;
		else
			moduleId = 3;
		oldModuleId = moduleId;
		pixelMap.clear();
		aBoolean1088 = true;
		modules[moduleId].method115(string, string_2_, string_3_);
	}

	public void method75(int i) {
		if (i >= 0 && i <= gameCount && moduleId > 3)
			method71(generalGameModules[19], "", generalGameModules[20]);
		if (i == 0)
			method78();
	}

	public boolean method70() {
		return modules[moduleId].method114();
	}

	public void method57(int i, int i_4_, byte[] is) {
		modules[moduleId].method103(i, i_4_, is);
	}

	public void method58(int i, int i_5_, byte[] is, int i_6_) {
		modules[moduleId].method118(i, i_5_, is, i_6_);
	}

	public void method59(int i) {
		modules[moduleId].method119(i);
	}

	public void method60(int i, String string, String string_7_) {
		modules[moduleId].method120(i, string, string_7_);
	}

	public void method52(String string, String string_8_) {
		modules[moduleId].setServerResponseText(string, string_8_);
	}

	public void method56() {
		modules[moduleId].loginAfterCreation();
	}

	public void method61(int i, int[] is, int[] is_9_, String[] strings) {
		modules[moduleId].method106(i, is, is_9_, strings);
	}

	public void method62(int i, String[] strings, String[] strings_10_) {
		modules[moduleId].method107(i, strings, strings_10_);
	}

	public void method64(String string) {
		modules[moduleId].method108(string);
	}

	public void method65(String string, String string_11_) {
		modules[moduleId].method109(string, string_11_);
	}

	public void method66(String string, String string_12_) {
		modules[moduleId].method110(string, string_12_);
	}

	public void method67(String string, String string_13_, String string_14_, String string_15_) {
		modules[moduleId].method111(string, string_13_, string_14_, string_15_);
	}

	public void method69(int i, String[] strings, int[] is, int[] is_16_, int[] is_17_) {
		modules[moduleId].method112(i, strings, is, is_16_, is_17_);
	}

	public void loadTranslate() {
		try {
			megastream megastream = new megastream("translate1.txt");
			for (int id = 0; id < 8; id++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != id)
					throw new RuntimeException("syncerror0 " + id);
				gameNameModules[id] = megastream.getStringValue();
			}
			for (int i = 0; i < 6; i++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != i)
					throw new RuntimeException("syncerror1 " + i);
				loadingModules[i] = megastream.getStringValue();
			}
			for (int i = 0; i < 22; i++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != i)
					throw new RuntimeException("syncerror2 " + i);
				loginModules[i] = megastream.getStringValue();
			}
			for (int i = 0; i < 39; i++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != i)
					throw new RuntimeException("syncerror3 " + i);
				chalroomModules[i] = megastream.getStringValue();
			}
			for (int i = 0; i < 53; i++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != i)
					throw new RuntimeException("syncerror4 " + i);
				client.clientModules[i] = megastream.getStringValue();
			}
			for (int i = 0; i < 21; i++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != i)
					throw new RuntimeException("syncerror5 " + i);
				generalGameModules[i] = megastream.getStringValue();
			}
			for (int i = 0; i < 9; i++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != i)
					throw new RuntimeException("syncerror6 " + i);
				warshipGameModules[i] = megastream.getStringValue();
			}
			for (int i = 0; i < 1; i++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != i)
					throw new RuntimeException("syncerror7 " + i);
				chessGameModules[i] = megastream.getStringValue();
			}
			for (int i = 0; i < 4; i++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != i)
					throw new RuntimeException("syncerror8 " + i);
				gomadGameModules[i] = megastream.getStringValue();
			}
			for (int i = 0; i < 9; i++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != i)
					throw new RuntimeException("syncerror9 " + i);
				backgammonGameModules[i] = megastream.getStringValue();
			}
			for (int id = 0; id < 3; id++) {
				megastream.skipToEquals();
				int textId = megastream.getBase10Value();
				if (textId != id)
					throw new RuntimeException("syncerror10 " + id);
				game7Modules[id] = megastream.getStringValue();
			}
		} catch (Exception exception) {
			System.out.println("Error in trans routine: " + exception);
		}
	}
}
