import jagex.object3d;
import jagex.pixmap;
import jagex.tools;
import jagex.world3d;

public class checkgame extends module {
	gameclient anApplet_Sub1_Sub1_Sub1_1025;
	pixmap aClass6_1026;
	world3d aClass10_1027;
	world3d aClass10_1028;
	world3d aClass10_1029;
	int anInt1030;
	object3d aClass7_1031;
	object3d aClass7_1032;
	object3d aClass7_1033;
	object3d aClass7_1034;
	object3d aClass7_1035;
	object3d aClass7_1036;
	object3d[] aClass7Array1037 = new object3d[32];
	object3d[] aClass7Array1038 = new object3d[32];
	object3d[] aClass7Array1039 = new object3d[32];
	object3d[] aClass7Array1040 = new object3d[32];
	int anInt1041;
	int anInt1042 = 896;
	int anInt1043;
	int anInt1044;
	int anInt1045;
	int anInt1046;
	int anInt1047;
	int anInt1048;
	int anInt1049;
	int anInt1050;
	boolean aBoolean1051 = false;
	boolean aBoolean1052 = false;
	boolean aBoolean1053 = false;
	int anInt1054 = 100;
	int anInt1055;
	int anInt1056 = 60;
	int anInt1057;
	int anInt1058;
	boolean aBoolean1059 = true;
	int[][] anIntArrayArray1060 = new int[8][8];
	int[][] anIntArrayArray1061 = new int[8][8];
	int anInt1062;
	int[] anIntArray1063 = new int[64];
	int anInt1064 = -1;
	int anInt1065 = -1;
	int anInt1066 = -1;
	int anInt1067 = -1;
	int anInt1068 = -1;
	boolean aBoolean1069 = false;
	int anInt1070 = -1;
	int anInt1071 = -1;
	int anInt1072 = -1;
	int anInt1073 = -1;
	int anInt1074 = -1;
	int anInt1075 = -1;
	int anInt1076 = 32;

	public void onInit(gameclient applet_sub1_sub1_sub1, int i) {
		anApplet_Sub1_Sub1_Sub1_1025 = applet_sub1_sub1_sub1;
		anInt1030 = i;
		aClass6_1026 = applet_sub1_sub1_sub1.pixelMap;
		aClass10_1027 = applet_sub1_sub1_sub1.aClass10_1086;
		aClass10_1028 = applet_sub1_sub1_sub1.aClass10_1084;
		aClass10_1029 = applet_sub1_sub1_sub1.aClass10_1085;
		aClass6_1026.method249(applet_sub1_sub1_sub1.aByteArray1083, tools.findFile("2dcheckers.tga", (applet_sub1_sub1_sub1.aByteArray1083)), i, true, 6, false);
		aClass7_1031 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("board.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_1034 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("redbit.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_1033 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("whitebit.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_1035 = aClass7_1033.copy();
		aClass7_1035.requestSetTranslate(0, 32, 0);
		aClass7_1035.applyChanges();
		aClass7_1036 = aClass7_1034.copy();
		aClass7_1036.requestSetTranslate(0, 32, 0);
		aClass7_1036.applyChanges();
		aClass7_1033.requestScale(224, 224, 224);
		aClass7_1034.requestScale(224, 224, 224);
		aClass7_1035.requestScale(224, 224, 224);
		aClass7_1036.requestScale(224, 224, 224);
		aClass7_1032 = new object3d(81, 64);
		for (int i_0_ = 0; i_0_ < 9; i_0_++) {
			for (int i_1_ = 0; i_1_ < 9; i_1_++)
				aClass7_1032.addVertex(i_1_ * 96 - 384, 0, i_0_ * 96 - 384);
		}
		for (int i_2_ = 0; i_2_ < 8; i_2_++) {
			for (int i_3_ = 0; i_3_ < 8; i_3_++) {
				int[] is = new int[4];
				is[0] = i_2_ * 9 + i_3_;
				is[1] = i_2_ * 9 + i_3_ + 1;
				is[2] = i_2_ * 9 + i_3_ + 10;
				is[3] = i_2_ * 9 + i_3_ + 9;
				if ((i_3_ & 0x1) == (i_2_ & 0x1))
					aClass7_1032.addPolygon(4, is, 20, 20);
				else
					aClass7_1032.addPolygon(4, is, 10, 10);
			}
		}
		aClass7_1032.method279(50);
		aClass7_1032.method280(true, 52, 32, -50, -10, -50);
		aClass7_1032.aBoolean331 = true;
		aClass7_1031.method280(true, 52, 32, -50, -10, -50);
		aClass7_1033.method280(true, 52, 32, -50, -10, -50);
		aClass7_1034.method280(true, 52, 32, -50, -10, -50);
		aClass7_1035.method280(true, 42, 10, -50, -10, -50);
		aClass7_1036.method280(true, 52, 10, -50, -10, -50);
		for (int i_4_ = 0; i_4_ < 32; i_4_++) {
			aClass7Array1037[i_4_] = aClass7_1033.copy();
			aClass7Array1038[i_4_] = aClass7_1034.copy();
			aClass7Array1039[i_4_] = aClass7_1035.copy();
			aClass7Array1040[i_4_] = aClass7_1036.copy();
		}
	}

	public void method113(int i) {
		anInt1058 = i;
		if (i == 0)
			anInt1041 = 0;
		else
			anInt1041 = 512;
		aClass10_1027.clearObjects();
		aClass10_1028.clearObjects();
		aClass10_1029.clearObjects();
		aClass10_1027.method362(256, 152, 256, 152, 512, 9);
		aClass10_1027.maxRenderZ = 3000;
		aClass10_1027.anInt464 = 3000;
		aClass10_1027.anInt465 = 20;
		aClass10_1027.anInt466 = 5000;
		aClass10_1028.method385(aClass10_1027);
		aClass10_1029.method385(aClass10_1027);
		aClass10_1027.addObject(aClass7_1031);
		aClass10_1027.addObject(aClass7_1032);
		for (int i_5_ = 0; i_5_ < 8; i_5_++) {
			for (int i_6_ = 0; i_6_ < 8; i_6_++)
				anIntArrayArray1060[i_6_][i_5_] = -1;
		}
		for (int i_7_ = 0; i_7_ < 8; i_7_++) {
			for (int i_8_ = 0; i_8_ < 8; i_8_++) {
				if (i_7_ <= 2 && (i_8_ & 0x1) == (i_7_ & 0x1))
					anIntArrayArray1060[i_8_][i_7_] = 0;
				if (i_7_ >= 5 && (i_8_ & 0x1) == (i_7_ & 0x1))
					anIntArrayArray1060[i_8_][i_7_] = 1;
			}
		}
		anInt1066 = -1;
		anInt1067 = -1;
		anInt1068 = -1;
		aBoolean1069 = false;
		aBoolean1059 = true;
		anInt1070 = -1;
		anInt1071 = -1;
		anInt1072 = -1;
		anInt1073 = -1;
		anInt1074 = -1;
		anInt1075 = -1;
		aBoolean1052 = false;
		aBoolean1053 = false;
		anInt1054 = 100;
		method195();
	}

	public void method121() {
		aClass10_1027.clearObjects();
		aClass10_1028.clearObjects();
		aClass10_1029.clearObjects();
		aClass10_1027.addObject(aClass7_1031);
		aClass10_1027.addObject(aClass7_1032);
	}

	public boolean method114() {
		if (aBoolean1069)
			return false;
		return true;
	}

	public void method103(int i, int i_9_, byte[] is) {
		if (i == 255) {
			anInt1055 = is[1] & 0xff;
			anInt1056 = is[2] & 0xff;
			anInt1064 = is[3];
			anInt1065 = is[4];
			anInt1054 = 100;
			if (anInt1055 == anInt1058 && anInt1062 == 0 && !aBoolean1059)
				method195();
			if (anInt1055 != anInt1058)
				aBoolean1059 = false;
		}
		if (i == 254) {
			int i_10_ = 1;
			for (int i_11_ = 0; i_11_ < 8; i_11_++) {
				for (int i_12_ = 0; i_12_ < 8; i_12_++)
					anIntArrayArray1061[i_11_][i_12_] = anIntArrayArray1060[i_11_][i_12_] = is[i_10_++];
			}
			anInt1055 = is[i_10_++] & 0xff;
			anInt1056 = is[i_10_++] & 0xff;
			anInt1054 = 100;
			anInt1064 = is[i_10_++];
			anInt1065 = is[i_10_++];
			if (anInt1055 == anInt1058 && anInt1062 == 0)
				method195();
			aBoolean1059 = false;
		}
		if (i == 253) {
			int i_13_ = is[1] & 0xff;
			int i_14_ = is[2] & 0xff;
			int i_15_ = is[3] & 0xff;
			int i_16_ = is[4] & 0xff;
			if (i_13_ != anInt1070 || i_14_ != anInt1071 || i_15_ != anInt1072 || i_16_ != anInt1073)
				method191(i_13_, i_14_, i_15_, i_16_, false);
			anIntArrayArray1061[i_15_][i_16_] = anIntArrayArray1061[i_13_][i_14_];
			anIntArrayArray1061[i_13_][i_14_] = -1;
			if (i_15_ - i_13_ == 2 || i_13_ - i_15_ == 2)
				anIntArrayArray1061[(i_13_ + i_15_) / 2][(i_14_ + i_16_) / 2] = -1;
			if (i_16_ == 7 && anIntArrayArray1061[i_15_][i_16_] == 0)
				anIntArrayArray1061[i_15_][i_16_] = 2;
			if (i_16_ == 0 && anIntArrayArray1061[i_15_][i_16_] == 1)
				anIntArrayArray1061[i_15_][i_16_] = 3;
			if (!aBoolean1069) {
				for (int i_17_ = 0; i_17_ < 8; i_17_++) {
					for (int i_18_ = 0; i_18_ < 8; i_18_++) {
						if (anIntArrayArray1061[i_17_][i_18_] != anIntArrayArray1060[i_17_][i_18_])
							anIntArrayArray1060[i_17_][i_18_] = anIntArrayArray1061[i_17_][i_18_];
					}
				}
			}
			aBoolean1059 = false;
		}
	}

	public void onLoop() {
		anInt1057++;
		if (anApplet_Sub1_Sub1_Sub1_1025.mouseDown == 2 && !aBoolean1051) {
			if (anInt1045 == 0) {
				anInt1045 = anApplet_Sub1_Sub1_Sub1_1025.mouseDown;
				anInt1043 = anApplet_Sub1_Sub1_Sub1_1025.mouseX;
				anInt1044 = anApplet_Sub1_Sub1_Sub1_1025.mouseY;
			}
			int i = anApplet_Sub1_Sub1_Sub1_1025.mouseX - anInt1043;
			int i_19_ = anApplet_Sub1_Sub1_Sub1_1025.mouseY - anInt1044;
			anInt1041 = anInt1041 + i * 2 + 1024 & 0x3ff;
			anInt1042 = anInt1042 - i_19_ * 2 + 1024 & 0x3ff;
			if (anInt1042 > 256 && anInt1042 < 600)
				anInt1042 = 600;
			if (anInt1042 > 960 || anInt1042 < 256)
				anInt1042 = 960;
			anInt1043 = anApplet_Sub1_Sub1_Sub1_1025.mouseX;
			anInt1044 = anApplet_Sub1_Sub1_Sub1_1025.mouseY;
		} else
			anInt1045 = 0;
		if (anApplet_Sub1_Sub1_Sub1_1025.mouseClick == 1) {
			anInt1046 = 1;
			aClass10_1027.method358(anApplet_Sub1_Sub1_Sub1_1025.mouseX, anApplet_Sub1_Sub1_Sub1_1025.mouseY, 50, 50);
		}
		if (aBoolean1069) {
			anInt1075++;
			if (anInt1075 == anInt1076)
				method190();
		}
		anInt1054--;
		if (anInt1054 <= 0 && anInt1056 > 10) {
			anInt1054 = 50;
			anInt1056--;
		}
	}

	public void method116(int i, int i_20_, int i_21_) {
		method193();
		method192();
		if (aBoolean1069)
			method189();
		aClass10_1029.setCamera(0, 100, 0, i, i_20_, 0, i_21_);
		aClass10_1029.method367();
		aClass10_1027.setCamera(0, 100, 0, i, i_20_, 0, i_21_);
		aClass10_1027.method367();
		aClass10_1028.setCamera(0, 100, 0, i, i_20_, 0, i_21_);
		aClass10_1028.method367();
	}

	public void onDraw() {
		aClass6_1026.method241(0, 0, 512, 384, 0, 4210752);
		if (aBoolean1051) {
			method187();
			if (aBoolean1069)
				method188();
		} else {
			int i = 1200;
			method116(anInt1042, anInt1041, i);
		}
		if (aBoolean1069 || aBoolean1059)
			aClass6_1026.method268((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[0]), 7, 12, 1, 16777215);
		else if (anInt1055 == anInt1058)
			aClass6_1026.method268((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[1]), 7, 12, 1, 16777215);
		else
			aClass6_1026.method268((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[2]), 7, 12, 1, 16777215);
		aClass6_1026.method268((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[3]) + anInt1056, 7, 23, 1, 16777215);
		int i = 460;
		int i_22_ = 4;
		anApplet_Sub1_Sub1_Sub1_1025.aClass8_1094.method321(i, i_22_, 46, 28);
		aClass6_1026.method267((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[4]), i + 22, i_22_ + 13, 1, 0);
		if (aBoolean1051)
			aClass6_1026.method267((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[5]), i + 22, i_22_ + 23, 1, 0);
		else
			aClass6_1026.method267((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[6]), i + 22, i_22_ + 23, 1, 0);
		i = 460;
		i_22_ = 34;
		anApplet_Sub1_Sub1_Sub1_1025.aClass8_1094.method321(i, i_22_, 46, 28);
		aClass6_1026.method267((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[7]), i + 22, i_22_ + 18, 1, 0);
		i = 460;
		i_22_ = 64;
		anApplet_Sub1_Sub1_Sub1_1025.aClass8_1094.method321(i, i_22_, 46, 28);
		if (anApplet_Sub1_Sub1_Sub1_1025.aBoolean604)
			aClass6_1026.method267((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[8]), i + 22, i_22_ + 13, 1, 0);
		else
			aClass6_1026.method267((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[9]), i + 22, i_22_ + 13, 1, 0);
		aClass6_1026.method267((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[10]), i + 22, i_22_ + 23, 1, 0);
		if (aBoolean1052) {
			method186(anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[11], anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[12]);
			if (anApplet_Sub1_Sub1_Sub1_1025.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_1025.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_1025.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_1025.mouseY < 188 && anInt1046 == 1) {
				anApplet_Sub1_Sub1_Sub1_1025.resign();
				aBoolean1052 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_1025.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_1025.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_1025.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_1025.mouseY < 188 && anInt1046 == 1)
				aBoolean1052 = false;
			anInt1046 = 0;
		} else if (aBoolean1053) {
			if (anApplet_Sub1_Sub1_Sub1_1025.aBoolean604)
				method186(anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[13], anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[14]);
			else
				method186(anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[15], anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[16]);
			if (anApplet_Sub1_Sub1_Sub1_1025.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_1025.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_1025.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_1025.mouseY < 188 && anInt1046 == 1) {
				anApplet_Sub1_Sub1_Sub1_1025.offerDraw();
				aBoolean1053 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_1025.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_1025.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_1025.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_1025.mouseY < 188 && anInt1046 == 1)
				aBoolean1053 = false;
			anInt1046 = 0;
		} else {
			i = 460;
			i_22_ = 4;
			if (anInt1046 == 1 && anApplet_Sub1_Sub1_Sub1_1025.mouseX > i && anApplet_Sub1_Sub1_Sub1_1025.mouseY > i_22_ && anApplet_Sub1_Sub1_Sub1_1025.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_1025.mouseY < i_22_ + 29) {
				aBoolean1051 = !aBoolean1051;
				anInt1046 = 0;
			}
			i = 460;
			i_22_ = 34;
			if (anInt1046 == 1 && anApplet_Sub1_Sub1_Sub1_1025.mouseX > i && anApplet_Sub1_Sub1_Sub1_1025.mouseY > i_22_ && anApplet_Sub1_Sub1_Sub1_1025.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_1025.mouseY < i_22_ + 29) {
				aBoolean1052 = true;
				anInt1046 = 0;
			}
			i = 460;
			i_22_ = 64;
			if (anInt1046 == 1 && anApplet_Sub1_Sub1_Sub1_1025.mouseX > i && anApplet_Sub1_Sub1_Sub1_1025.mouseY > i_22_ && anApplet_Sub1_Sub1_Sub1_1025.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_1025.mouseY < i_22_ + 29) {
				aBoolean1053 = true;
				anInt1046 = 0;
			}
			if (!aBoolean1069 && anInt1046 == 1 && anInt1055 == anInt1058 && !aBoolean1059) {
				int i_23_ = -1;
				if (aBoolean1051) {
					int i_24_ = (anApplet_Sub1_Sub1_Sub1_1025.mouseX - 108) / 37;
					int i_25_ = 7 - (anApplet_Sub1_Sub1_Sub1_1025.mouseY - 4) / 37;
					if (anInt1058 == 1) {
						i_24_ = 7 - i_24_;
						i_25_ = 7 - i_25_;
					}
					i_23_ = i_24_ + i_25_ * 8;
				} else {
					i_23_ = aClass10_1027.method360();
					object3d class7 = aClass10_1027.method361();
					if (class7 != aClass7_1032)
						i_23_ = -1;
				}
				if (anInt1068 == -1) {
					for (int i_26_ = 0; i_26_ < anInt1062; i_26_++) {
						if (anIntArray1063[i_26_] == i_23_) {
							anInt1066 = i_23_ % 8;
							anInt1067 = i_23_ / 8;
							anInt1068 = anIntArrayArray1060[anInt1066][anInt1067];
							method194();
							break;
						}
					}
				} else {
					for (int i_27_ = 0; i_27_ < anInt1062; i_27_++) {
						if (anIntArray1063[i_27_] == i_23_) {
							int i_28_ = i_23_ % 8;
							int i_29_ = i_23_ / 8;
							method191(anInt1066, anInt1067, i_28_, i_29_, true);
							break;
						}
						if (i_27_ == anInt1062 - 1) {
							anInt1068 = -1;
							method195();
							break;
						}
					}
				}
			}
			anInt1046 = 0;
		}
	}

	public void method186(String string, String string_30_) {
		int i = 160;
		int i_31_ = 80;
		aClass6_1026.method240(256 - i / 2 + 10, 150 - i_31_ / 2 + 10, i, i_31_, 0, 128);
		aClass6_1026.method242(256 - i / 2, 150 - i_31_ / 2, i, i_31_, pixmap.method247(96, 96, 121));
		aClass6_1026.method267(string, 256, 130, 4, 16777215);
		aClass6_1026.method267(string_30_, 256, 150, 4, 16777215);
		int i_32_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_1025.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_1025.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_1025.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_1025.mouseY < 188)
			i_32_ = 16711680;
		aClass6_1026.method267((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[17]), 226, 178, 4, i_32_);
		i_32_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_1025.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_1025.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_1025.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_1025.mouseY < 188)
			i_32_ = 16711680;
		aClass6_1026.method267((anApplet_Sub1_Sub1_Sub1_1025.generalGameModules[18]), 286, 178, 4, i_32_);
	}

	public void method187() {
		int i = 108;
		int i_33_ = 263;
		for (int i_34_ = 0; i_34_ < 8; i_34_++) {
			for (int i_35_ = 0; i_35_ < 8; i_35_++) {
				int i_36_;
				if ((i_34_ & 0x1) == (i_35_ & 0x1))
					i_36_ = anInt1030;
				else
					i_36_ = anInt1030 + 1;
				aClass6_1026.method255(i_34_ * 37 + i, i_33_ - i_35_ * 37, i_36_);
			}
		}
		if ((anInt1057 / 8 & 0x1) == 1 && !aBoolean1069 && !aBoolean1059 && anInt1055 == anInt1058) {
			for (int i_37_ = 0; i_37_ < anInt1062; i_37_++) {
				int i_38_ = anIntArray1063[i_37_];
				int i_39_ = i_38_ % 8;
				int i_40_ = i_38_ / 8;
				if (anInt1058 == 1) {
					i_39_ = 7 - i_39_;
					i_40_ = 7 - i_40_;
				}
				if (anInt1068 == -1)
					aClass6_1026.method242(i_39_ * 37 + i, i_33_ - i_40_ * 37, 37, 37, pixmap.method247(192, 0, 0));
				else
					aClass6_1026.method242(i_39_ * 37 + i, i_33_ - i_40_ * 37, 37, 37, pixmap.method247(192, 128, 0));
			}
		}
		for (int i_41_ = 0; i_41_ < 8; i_41_++) {
			for (int i_42_ = 0; i_42_ < 8; i_42_++) {
				int i_43_ = anIntArrayArray1060[i_41_][i_42_];
				if (anInt1058 == 1)
					i_43_ = anIntArrayArray1060[7 - i_41_][7 - i_42_];
				if (i_43_ == 0)
					aClass6_1026.method255(i_41_ * 37 + i, i_33_ - i_42_ * 37, anInt1030 + 3);
				if (i_43_ == 1)
					aClass6_1026.method255(i_41_ * 37 + i, i_33_ - i_42_ * 37, anInt1030 + 2);
				if (i_43_ == 2)
					aClass6_1026.method255(i_41_ * 37 + i, i_33_ - i_42_ * 37, anInt1030 + 5);
				if (i_43_ == 3)
					aClass6_1026.method255(i_41_ * 37 + i, i_33_ - i_42_ * 37, anInt1030 + 4);
			}
		}
	}

	public void method188() {
		int i = 108;
		int i_44_ = 263;
		int i_45_ = ((anInt1072 * 37 * anInt1075 + anInt1070 * 37 * (anInt1076 - anInt1075)) / anInt1076);
		int i_46_ = ((anInt1073 * 37 * anInt1075 + anInt1071 * 37 * (anInt1076 - anInt1075)) / anInt1076);
		if (anInt1058 == 1) {
			i_45_ = 259 - i_45_;
			i_46_ = 259 - i_46_;
		}
		if (anInt1074 == 0)
			aClass6_1026.method255(i_45_ + i, i_44_ - i_46_, anInt1030 + 3);
		if (anInt1074 == 1)
			aClass6_1026.method255(i_45_ + i, i_44_ - i_46_, anInt1030 + 2);
		if (anInt1074 == 2)
			aClass6_1026.method255(i_45_ + i, i_44_ - i_46_, anInt1030 + 5);
		if (anInt1074 == 3)
			aClass6_1026.method255(i_45_ + i, i_44_ - i_46_, anInt1030 + 4);
	}

	public void method189() {
		int i = ((anInt1072 * 96 * anInt1075 + anInt1070 * 96 * (anInt1076 - anInt1075)) / anInt1076);
		int i_47_ = ((anInt1073 * 96 * anInt1075 + anInt1071 * 96 * (anInt1076 - anInt1075)) / anInt1076);
		int i_48_ = 0;
		if (anInt1072 - anInt1070 == 2 || anInt1070 - anInt1072 == 2)
			i_48_ = (int) (Math.sin((double) anInt1075 * 3.142 / (double) anInt1076) * 80.0);
		if (anInt1074 == 0 || anInt1074 == 2) {
			object3d class7 = aClass7Array1038[anInt1047++];
			aClass10_1028.addObject(class7);
			class7.requestSetTranslate(i - 336, -i_48_, i_47_ - 336);
			object3d class7_49_ = aClass7Array1040[anInt1048++];
			aClass10_1029.addObject(class7_49_);
			class7_49_.requestSetTranslate(i - 336, i_48_, i_47_ - 336);
			if (anInt1074 == 2) {
				object3d class7_50_ = aClass7Array1038[anInt1047++];
				aClass10_1028.addObject(class7_50_);
				class7_50_.requestSetTranslate(i - 336, -i_48_ - 27, i_47_ - 336);
				object3d class7_51_ = aClass7Array1040[anInt1048++];
				aClass10_1029.addObject(class7_51_);
				class7_51_.requestSetTranslate(i - 336, i_48_ + 27, i_47_ - 336);
			}
		}
		if (anInt1074 == 1 || anInt1074 == 3) {
			object3d class7 = aClass7Array1037[anInt1049++];
			aClass10_1028.addObject(class7);
			class7.requestSetTranslate(i - 336, -i_48_, i_47_ - 336);
			object3d class7_52_ = aClass7Array1039[anInt1050++];
			aClass10_1029.addObject(class7_52_);
			class7_52_.requestSetTranslate(i - 336, i_48_, i_47_ - 336);
			if (anInt1074 == 3) {
				object3d class7_53_ = aClass7Array1037[anInt1049++];
				aClass10_1028.addObject(class7_53_);
				class7_53_.requestSetTranslate(i - 336, -i_48_ - 27, i_47_ - 336);
				object3d class7_54_ = aClass7Array1039[anInt1050++];
				aClass10_1029.addObject(class7_54_);
				class7_54_.requestSetTranslate(i - 336, i_48_ + 27, i_47_ - 336);
			}
		}
	}

	public void method190() {
		if (aBoolean142)
			anApplet_Sub1_Sub1_Sub1_1025.xmPlayer.method85(0, 2, 8000, 63);
		anIntArrayArray1060[anInt1072][anInt1073] = anInt1074;
		if (anInt1072 - anInt1070 == 2 || anInt1070 - anInt1072 == 2)
			anIntArrayArray1060[(anInt1070 + anInt1072) / 2][(anInt1071 + anInt1073) / 2] = -1;
		if (anInt1073 == 7 && anInt1074 == 0) {
			anInt1074 = 2;
			anIntArrayArray1060[anInt1072][anInt1073] = anInt1074;
		}
		if (anInt1073 == 0 && anInt1074 == 1) {
			anInt1074 = 3;
			anIntArrayArray1060[anInt1072][anInt1073] = anInt1074;
		}
		aBoolean1069 = false;
		anInt1062 = 0;
		anInt1068 = -1;
		anInt1064 = -1;
		anInt1065 = -1;
		anApplet_Sub1_Sub1_Sub1_1025.aBoolean604 = false;
	}

	public void method191(int i, int i_55_, int i_56_, int i_57_, boolean bool) {
		aBoolean1069 = true;
		anInt1070 = i;
		anInt1071 = i_55_;
		anInt1072 = i_56_;
		anInt1073 = i_57_;
		anInt1074 = anIntArrayArray1060[i][i_55_];
		anInt1075 = 0;
		anIntArrayArray1060[i][i_55_] = -1;
		if (bool) {
			aBoolean1059 = true;
			anApplet_Sub1_Sub1_Sub1_1025.aClass2_582.putPacketId(255);
			anApplet_Sub1_Sub1_Sub1_1025.aClass2_582.p1(i);
			anApplet_Sub1_Sub1_Sub1_1025.aClass2_582.p1(i_55_);
			anApplet_Sub1_Sub1_Sub1_1025.aClass2_582.p1(i_56_);
			anApplet_Sub1_Sub1_Sub1_1025.aClass2_582.p1(i_57_);
			anApplet_Sub1_Sub1_Sub1_1025.aClass2_582.flushNoException();
		}
	}

	public void method192() {
		if ((anInt1057 / 8 & 0x1) == 0 || aBoolean1069 || aBoolean1059 || anInt1055 != anInt1058) {
			for (int i = 0; i < 8; i++) {
				for (int i_58_ = 0; i_58_ < 8; i_58_++) {
					int i_59_;
					if ((i & 0x1) == (i_58_ & 0x1))
						i_59_ = 20;
					else
						i_59_ = 10;
					aClass7_1032.method283(i + i_58_ * 8, i_59_);
					aClass7_1032.method284(i + i_58_ * 8, i_59_);
				}
			}
		} else {
			for (int i = 0; i < anInt1062; i++) {
				int i_60_ = anIntArray1063[i];
				int i_61_;
				if (anInt1068 == -1)
					i_61_ = -31745;
				else
					i_61_ = -32385;
				aClass7_1032.method283(i_60_, i_61_);
				aClass7_1032.method284(i_60_, i_61_);
			}
		}
	}

	public void method193() {
		aClass10_1028.clearObjects();
		aClass10_1029.clearObjects();
		anInt1047 = 0;
		anInt1048 = 0;
		anInt1049 = 0;
		anInt1050 = 0;
		for (int i = 0; i < 8; i++) {
			for (int i_62_ = 0; i_62_ < 8; i_62_++) {
				if (anIntArrayArray1060[i][i_62_] == 0 || anIntArrayArray1060[i][i_62_] == 2) {
					object3d class7 = aClass7Array1038[anInt1047++];
					aClass10_1028.addObject(class7);
					class7.requestSetTranslate(i * 96 - 336, 0, i_62_ * 96 - 336);
					object3d class7_63_ = aClass7Array1040[anInt1048++];
					aClass10_1029.addObject(class7_63_);
					class7_63_.requestSetTranslate(i * 96 - 336, 0, i_62_ * 96 - 336);
					if (anIntArrayArray1060[i][i_62_] == 2) {
						object3d class7_64_ = aClass7Array1038[anInt1047++];
						aClass10_1028.addObject(class7_64_);
						class7_64_.requestSetTranslate(i * 96 - 336, -27, i_62_ * 96 - 336);
						object3d class7_65_ = aClass7Array1040[anInt1048++];
						aClass10_1029.addObject(class7_65_);
						class7_65_.requestSetTranslate(i * 96 - 336, 27, i_62_ * 96 - 336);
					}
				}
				if (anIntArrayArray1060[i][i_62_] == 1 || anIntArrayArray1060[i][i_62_] == 3) {
					object3d class7 = aClass7Array1037[anInt1049++];
					aClass10_1028.addObject(class7);
					class7.requestSetTranslate(i * 96 - 336, 0, i_62_ * 96 - 336);
					object3d class7_66_ = aClass7Array1039[anInt1050++];
					aClass10_1029.addObject(class7_66_);
					class7_66_.requestSetTranslate(i * 96 - 336, 0, i_62_ * 96 - 336);
					if (anIntArrayArray1060[i][i_62_] == 3) {
						object3d class7_67_ = aClass7Array1037[anInt1049++];
						aClass10_1028.addObject(class7_67_);
						class7_67_.requestSetTranslate(i * 96 - 336, -27, i_62_ * 96 - 336);
						object3d class7_68_ = aClass7Array1039[anInt1050++];
						aClass10_1029.addObject(class7_68_);
						class7_68_.requestSetTranslate(i * 96 - 336, 27, i_62_ * 96 - 336);
					}
				}
			}
		}
	}

	public void method194() {
		int i = anInt1066;
		int i_69_ = anInt1067;
		anInt1062 = 0;
		if (anInt1068 != 0) {
			if (i > 1 && i_69_ > 1 && anIntArrayArray1060[i - 2][i_69_ - 2] == -1 && (anIntArrayArray1060[i - 1][i_69_ - 1] == 1 - anInt1058 || anIntArrayArray1060[i - 1][i_69_ - 1] == 3 - anInt1058))
				anIntArray1063[anInt1062++] = i - 2 + (i_69_ - 2) * 8;
			if (i < 6 && i_69_ > 1 && anIntArrayArray1060[i + 2][i_69_ - 2] == -1 && (anIntArrayArray1060[i + 1][i_69_ - 1] == 1 - anInt1058 || anIntArrayArray1060[i + 1][i_69_ - 1] == 3 - anInt1058))
				anIntArray1063[anInt1062++] = i + 2 + (i_69_ - 2) * 8;
		}
		if (anInt1068 != 1) {
			if (i > 1 && i_69_ < 6 && anIntArrayArray1060[i - 2][i_69_ + 2] == -1 && (anIntArrayArray1060[i - 1][i_69_ + 1] == 1 - anInt1058 || anIntArrayArray1060[i - 1][i_69_ + 1] == 3 - anInt1058))
				anIntArray1063[anInt1062++] = i - 2 + (i_69_ + 2) * 8;
			if (i < 6 && i_69_ < 6 && anIntArrayArray1060[i + 2][i_69_ + 2] == -1 && (anIntArrayArray1060[i + 1][i_69_ + 1] == 1 - anInt1058 || anIntArrayArray1060[i + 1][i_69_ + 1] == 3 - anInt1058))
				anIntArray1063[anInt1062++] = i + 2 + (i_69_ + 2) * 8;
		}
		if (anInt1062 == 0) {
			if (anInt1068 != 0) {
				if (i > 0 && i_69_ > 0 && anIntArrayArray1060[i - 1][i_69_ - 1] == -1)
					anIntArray1063[anInt1062++] = i - 1 + (i_69_ - 1) * 8;
				if (i < 7 && i_69_ > 0 && anIntArrayArray1060[i + 1][i_69_ - 1] == -1)
					anIntArray1063[anInt1062++] = i + 1 + (i_69_ - 1) * 8;
			}
			if (anInt1068 != 1) {
				if (i > 0 && i_69_ < 7 && anIntArrayArray1060[i - 1][i_69_ + 1] == -1)
					anIntArray1063[anInt1062++] = i - 1 + (i_69_ + 1) * 8;
				if (i < 7 && i_69_ < 7 && anIntArrayArray1060[i + 1][i_69_ + 1] == -1)
					anIntArray1063[anInt1062++] = i + 1 + (i_69_ + 1) * 8;
			}
		}
	}

	public void method195() {
		anInt1062 = 0;
		for (int i = 0; i < 8; i++) {
			for (int i_70_ = 0; i_70_ < 8; i_70_++) {
				if (anInt1064 != -1) {
					if (anInt1062 > 0)
						return;
					i = anInt1064;
					i_70_ = anInt1065;
				}
				if (anIntArrayArray1060[i][i_70_] == anInt1058 || anIntArrayArray1060[i][i_70_] == anInt1058 + 2) {
					if (anIntArrayArray1060[i][i_70_] != 0) {
						if (i > 1 && i_70_ > 1 && anIntArrayArray1060[i - 2][i_70_ - 2] == -1 && ((anIntArrayArray1060[i - 1][i_70_ - 1] == 1 - anInt1058) || (anIntArrayArray1060[i - 1][i_70_ - 1] == 3 - anInt1058))) {
							anIntArray1063[anInt1062++] = i + i_70_ * 8;
							continue;
						}
						if (i < 6 && i_70_ > 1 && anIntArrayArray1060[i + 2][i_70_ - 2] == -1 && ((anIntArrayArray1060[i + 1][i_70_ - 1] == 1 - anInt1058) || (anIntArrayArray1060[i + 1][i_70_ - 1] == 3 - anInt1058))) {
							anIntArray1063[anInt1062++] = i + i_70_ * 8;
							continue;
						}
					}
					if (anIntArrayArray1060[i][i_70_] != 1) {
						if (i > 1 && i_70_ < 6 && anIntArrayArray1060[i - 2][i_70_ + 2] == -1 && ((anIntArrayArray1060[i - 1][i_70_ + 1] == 1 - anInt1058) || (anIntArrayArray1060[i - 1][i_70_ + 1] == 3 - anInt1058))) {
							anIntArray1063[anInt1062++] = i + i_70_ * 8;
							continue;
						}
						if (i < 6 && i_70_ < 6 && anIntArrayArray1060[i + 2][i_70_ + 2] == -1 && ((anIntArrayArray1060[i + 1][i_70_ + 1] == 1 - anInt1058) || (anIntArrayArray1060[i + 1][i_70_ + 1] == 3 - anInt1058))) {
							anIntArray1063[anInt1062++] = i + i_70_ * 8;
							continue;
						}
					}
				}
				if (anInt1064 != -1)
					return;
			}
		}
		if (anInt1062 == 0) {
			for (int i = 0; i < 8; i++) {
				for (int i_71_ = 0; i_71_ < 8; i_71_++) {
					if (anIntArrayArray1060[i][i_71_] == anInt1058 || anIntArrayArray1060[i][i_71_] == anInt1058 + 2) {
						if (anIntArrayArray1060[i][i_71_] != 0) {
							if (i > 0 && i_71_ > 0 && (anIntArrayArray1060[i - 1][i_71_ - 1] == -1)) {
								anIntArray1063[anInt1062++] = i + i_71_ * 8;
								continue;
							}
							if (i < 7 && i_71_ > 0 && (anIntArrayArray1060[i + 1][i_71_ - 1] == -1)) {
								anIntArray1063[anInt1062++] = i + i_71_ * 8;
								continue;
							}
						}
						if (anIntArrayArray1060[i][i_71_] != 1) {
							if (i > 0 && i_71_ < 7 && anIntArrayArray1060[i - 1][i_71_ + 1] == -1)
								anIntArray1063[anInt1062++] = i + i_71_ * 8;
							else if (i < 7 && i_71_ < 7 && (anIntArrayArray1060[i + 1][i_71_ + 1] == -1))
								anIntArray1063[anInt1062++] = i + i_71_ * 8;
						}
					}
				}
			}
		}
	}
}
