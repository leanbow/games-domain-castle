import jagex.object3d;
import jagex.pixmap;
import jagex.tools;
import jagex.world3d;

public class revgame extends module {
	gameclient anApplet_Sub1_Sub1_Sub1_812;
	pixmap aClass6_813;
	world3d aClass10_814;
	world3d aClass10_815;
	int anInt816;
	object3d aClass7_817;
	object3d aClass7_818;
	object3d aClass7_819;
	object3d aClass7_820;
	object3d[] aClass7Array821 = new object3d[64];
	int anInt822;
	int anInt823 = 512;
	int anInt824 = 896;
	int anInt825;
	int anInt826;
	int anInt827;
	int anInt828;
	boolean aBoolean829 = false;
	boolean aBoolean830 = false;
	boolean aBoolean831 = false;
	boolean aBoolean832 = false;
	int anInt833 = -1;
	int anInt834 = -1;
	int anInt835;
	int anInt836 = 64;
	int anInt837 = 100;
	int anInt838;
	int anInt839 = 60;
	int anInt840;
	int anInt841;
	boolean aBoolean842 = true;
	int[][] anIntArrayArray843 = new int[8][8];

	public void onInit(gameclient applet_sub1_sub1_sub1, int i) {
		anApplet_Sub1_Sub1_Sub1_812 = applet_sub1_sub1_sub1;
		anInt816 = i;
		aClass6_813 = applet_sub1_sub1_sub1.pixelMap;
		aClass10_814 = applet_sub1_sub1_sub1.aClass10_1086;
		aClass10_815 = applet_sub1_sub1_sub1.aClass10_1084;
		aClass6_813.method249(applet_sub1_sub1_sub1.aByteArray1083, tools.findFile("2dreversi.tga", (applet_sub1_sub1_sub1.aByteArray1083)), i, true, 3, false);
		aClass7_819 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("star.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_820 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("board.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_819.requestScale(192, 128, 192);
		aClass7_817 = new object3d(81, 64);
		for (int i_0_ = 0; i_0_ < 9; i_0_++) {
			for (int i_1_ = 0; i_1_ < 9; i_1_++)
				aClass7_817.addVertex(i_1_ * 96 - 384, 0, i_0_ * 96 - 384);
		}
		for (int i_2_ = 0; i_2_ < 8; i_2_++) {
			for (int i_3_ = 0; i_3_ < 8; i_3_++) {
				int[] is = new int[4];
				is[0] = i_2_ * 9 + i_3_;
				is[1] = i_2_ * 9 + i_3_ + 1;
				is[2] = i_2_ * 9 + i_3_ + 10;
				is[3] = i_2_ * 9 + i_3_ + 9;
				aClass7_817.addPolygon(4, is, 14, 14);
			}
		}
		aClass7_817.method279(50);
		aClass7_820.method280(true, 52, 32, -50, -10, -50);
		aClass7_817.method280(true, 52, 32, -50, -10, -50);
		aClass7_819.method280(true, 52, 32, -50, -10, -50);
		for (int i_4_ = 0; i_4_ < 64; i_4_++)
			aClass7Array821[i_4_] = aClass7_819.copy();
	}

	public void method113(int i) {
		anInt841 = i;
		anInt823 = 0;
		aClass10_814.clearObjects();
		aClass10_815.clearObjects();
		aClass10_814.method362(256, 152, 256, 152, 512, 9);
		aClass10_814.maxRenderZ = 3000;
		aClass10_814.anInt464 = 3000;
		aClass10_814.anInt465 = 20;
		aClass10_814.anInt466 = 5000;
		aClass10_815.method385(aClass10_814);
		aClass10_814.clearObjects();
		aClass10_815.clearObjects();
		aClass10_814.addObject(aClass7_820);
		aClass10_814.addObject(aClass7_817);
		for (int i_5_ = 0; i_5_ < 8; i_5_++) {
			for (int i_6_ = 0; i_6_ < 8; i_6_++)
				anIntArrayArray843[i_5_][i_6_] = 0;
		}
		anIntArrayArray843[3][3] = 1;
		anIntArrayArray843[4][3] = 2;
		anIntArrayArray843[3][4] = 2;
		anIntArrayArray843[4][4] = 1;
		aBoolean830 = false;
		aBoolean842 = true;
		anInt835 = 0;
		aBoolean831 = false;
		aBoolean832 = false;
		anInt837 = 100;
	}

	public void method121() {
		aClass10_814.clearObjects();
		aClass10_815.clearObjects();
		aClass10_814.addObject(aClass7_820);
		aClass10_814.addObject(aClass7_817);
	}

	public boolean method114() {
		if (aBoolean830)
			return false;
		return true;
	}

	public void method103(int i, int i_7_, byte[] is) {
		if (i == 255) {
			anInt838 = is[1] & 0xff;
			anInt839 = is[2] & 0xff;
			anInt837 = 100;
			if (anInt838 != anInt841)
				aBoolean842 = false;
		}
		if (i == 254) {
			int i_8_ = 1;
			for (int i_9_ = 0; i_9_ < 8; i_9_++) {
				for (int i_10_ = 0; i_10_ < 8; i_10_++) {
					int i_11_ = is[i_8_++];
					int i_12_ = anIntArrayArray843[i_9_][i_10_];
					if (i_11_ == 1 && i_12_ == 2) {
						anIntArrayArray843[i_9_][i_10_] = 100;
						anInt835 = anInt836;
						if (aBoolean142 && !aBoolean830)
							anApplet_Sub1_Sub1_Sub1_812.xmPlayer.method85(0, 0, 8000, 63);
						aBoolean830 = true;
					} else if (i_11_ == 2 && i_12_ == 1) {
						anIntArrayArray843[i_9_][i_10_] = 200;
						anInt835 = anInt836;
						if (aBoolean142 && !aBoolean830)
							anApplet_Sub1_Sub1_Sub1_812.xmPlayer.method85(0, 0, 8000, 63);
						aBoolean830 = true;
					} else
						anIntArrayArray843[i_9_][i_10_] = i_11_;
				}
			}
			anInt838 = is[i_8_++] & 0xff;
			anInt839 = is[i_8_++] & 0xff;
			anInt837 = 100;
			aBoolean842 = false;
			anApplet_Sub1_Sub1_Sub1_812.aBoolean604 = false;
		}
	}

	public void onLoop() {
		anInt840++;
		if (anApplet_Sub1_Sub1_Sub1_812.mouseDown == 2 && !aBoolean829) {
			if (anInt827 == 0) {
				anInt827 = anApplet_Sub1_Sub1_Sub1_812.mouseDown;
				anInt825 = anApplet_Sub1_Sub1_Sub1_812.mouseX;
				anInt826 = anApplet_Sub1_Sub1_Sub1_812.mouseY;
			}
			int i = anApplet_Sub1_Sub1_Sub1_812.mouseX - anInt825;
			int i_13_ = anApplet_Sub1_Sub1_Sub1_812.mouseY - anInt826;
			anInt823 = anInt823 + i * 2 + 1024 & 0x3ff;
			anInt824 = anInt824 - i_13_ * 2 + 1024 & 0x3ff;
			if (anInt824 > 256 && anInt824 < 600)
				anInt824 = 600;
			if (anInt824 > 960 || anInt824 < 256)
				anInt824 = 960;
			anInt825 = anApplet_Sub1_Sub1_Sub1_812.mouseX;
			anInt826 = anApplet_Sub1_Sub1_Sub1_812.mouseY;
		} else
			anInt827 = 0;
		if (anInt835 > 0)
			anInt835--;
		else
			aBoolean830 = false;
		for (int i = 0; i < 8; i++) {
			for (int i_14_ = 0; i_14_ < 8; i_14_++) {
				if (anIntArrayArray843[i][i_14_] >= 100) {
					anIntArrayArray843[i][i_14_]++;
					if (anIntArrayArray843[i][i_14_] == 100 + anInt836)
						anIntArrayArray843[i][i_14_] = 1;
					if (anIntArrayArray843[i][i_14_] == 200 + anInt836)
						anIntArrayArray843[i][i_14_] = 2;
				}
			}
		}
		if (anApplet_Sub1_Sub1_Sub1_812.mouseClick == 1)
			anInt828 = 1;
		anInt837--;
		if (anInt837 <= 0 && anInt839 > 10) {
			anInt837 = 50;
			anInt839--;
		}
	}

	public void method116(int i, int i_15_, int i_16_) {
		method159();
		method156();
		aClass10_814.setCamera(0, 100, 0, i, i_15_, 0, i_16_);
		aClass10_814.method367();
		aClass10_815.setCamera(0, 100, 0, i, i_15_, 0, i_16_);
		aClass10_815.method367();
	}

	public void onDraw() {
		aClass6_813.method241(0, 0, 512, 384, 0, 4210752);
		if (aBoolean829)
			method158();
		else {
			int i = 1200;
			aClass10_814.method358(anApplet_Sub1_Sub1_Sub1_812.mouseX, anApplet_Sub1_Sub1_Sub1_812.mouseY, 50, 50);
			method116(anInt824, anInt823, i);
		}
		if (aBoolean830 || aBoolean842)
			aClass6_813.method268((anApplet_Sub1_Sub1_Sub1_812.generalGameModules[0]), 7, 12, 1, 16777215);
		else if (anInt838 == anInt841)
			aClass6_813.method268((anApplet_Sub1_Sub1_Sub1_812.generalGameModules[1]), 7, 12, 1, 16777215);
		else
			aClass6_813.method268((anApplet_Sub1_Sub1_Sub1_812.generalGameModules[2]), 7, 12, 1, 16777215);
		aClass6_813.method268((anApplet_Sub1_Sub1_Sub1_812.generalGameModules[3] + anInt839), 7, 23, 1, 16777215);
		int i = 460;
		int i_17_ = 4;
		anApplet_Sub1_Sub1_Sub1_812.aClass8_1094.method321(i, i_17_, 46, 28);
		aClass6_813.method267(anApplet_Sub1_Sub1_Sub1_812.generalGameModules[4], i + 22, i_17_ + 13, 1, 0);
		if (aBoolean829)
			aClass6_813.method267((anApplet_Sub1_Sub1_Sub1_812.generalGameModules[5]), i + 22, i_17_ + 23, 1, 0);
		else
			aClass6_813.method267((anApplet_Sub1_Sub1_Sub1_812.generalGameModules[6]), i + 22, i_17_ + 23, 1, 0);
		i = 460;
		i_17_ = 34;
		anApplet_Sub1_Sub1_Sub1_812.aClass8_1094.method321(i, i_17_, 46, 28);
		aClass6_813.method267(anApplet_Sub1_Sub1_Sub1_812.generalGameModules[7], i + 22, i_17_ + 18, 1, 0);
		i = 460;
		i_17_ = 64;
		anApplet_Sub1_Sub1_Sub1_812.aClass8_1094.method321(i, i_17_, 46, 28);
		if (anApplet_Sub1_Sub1_Sub1_812.aBoolean604)
			aClass6_813.method267((anApplet_Sub1_Sub1_Sub1_812.generalGameModules[8]), i + 22, i_17_ + 13, 1, 0);
		else
			aClass6_813.method267((anApplet_Sub1_Sub1_Sub1_812.generalGameModules[9]), i + 22, i_17_ + 13, 1, 0);
		aClass6_813.method267(anApplet_Sub1_Sub1_Sub1_812.generalGameModules[10], i + 22, i_17_ + 23, 1, 0);
		if (aBoolean831) {
			method157(anApplet_Sub1_Sub1_Sub1_812.generalGameModules[11], anApplet_Sub1_Sub1_Sub1_812.generalGameModules[12]);
			if (anApplet_Sub1_Sub1_Sub1_812.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_812.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_812.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_812.mouseY < 188 && anInt828 == 1) {
				anApplet_Sub1_Sub1_Sub1_812.resign();
				aBoolean831 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_812.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_812.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_812.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_812.mouseY < 188 && anInt828 == 1)
				aBoolean831 = false;
			anInt833 = -1;
			anInt834 = -1;
			anInt828 = 0;
		} else if (aBoolean832) {
			if (anApplet_Sub1_Sub1_Sub1_812.aBoolean604)
				method157(anApplet_Sub1_Sub1_Sub1_812.generalGameModules[13], anApplet_Sub1_Sub1_Sub1_812.generalGameModules[14]);
			else
				method157(anApplet_Sub1_Sub1_Sub1_812.generalGameModules[15], anApplet_Sub1_Sub1_Sub1_812.generalGameModules[16]);
			if (anApplet_Sub1_Sub1_Sub1_812.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_812.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_812.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_812.mouseY < 188 && anInt828 == 1) {
				anApplet_Sub1_Sub1_Sub1_812.offerDraw();
				aBoolean832 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_812.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_812.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_812.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_812.mouseY < 188 && anInt828 == 1)
				aBoolean832 = false;
			anInt833 = -1;
			anInt834 = -1;
			anInt828 = 0;
		} else {
			i = 460;
			i_17_ = 4;
			if (anInt828 == 1 && anApplet_Sub1_Sub1_Sub1_812.mouseX > i && anApplet_Sub1_Sub1_Sub1_812.mouseY > i_17_ && anApplet_Sub1_Sub1_Sub1_812.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_812.mouseY < i_17_ + 29) {
				aBoolean829 = !aBoolean829;
				anInt828 = 0;
			}
			i = 460;
			i_17_ = 34;
			if (anInt828 == 1 && anApplet_Sub1_Sub1_Sub1_812.mouseX > i && anApplet_Sub1_Sub1_Sub1_812.mouseY > i_17_ && anApplet_Sub1_Sub1_Sub1_812.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_812.mouseY < i_17_ + 29) {
				aBoolean831 = true;
				anInt828 = 0;
			}
			i = 460;
			i_17_ = 64;
			if (anInt828 == 1 && anApplet_Sub1_Sub1_Sub1_812.mouseX > i && anApplet_Sub1_Sub1_Sub1_812.mouseY > i_17_ && anApplet_Sub1_Sub1_Sub1_812.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_812.mouseY < i_17_ + 29) {
				aBoolean832 = true;
				anInt828 = 0;
			}
			int i_18_ = -1;
			int i_19_ = -1;
			if (aBoolean829) {
				if (anApplet_Sub1_Sub1_Sub1_812.mouseX > 108 && anApplet_Sub1_Sub1_Sub1_812.mouseY > 4 && anApplet_Sub1_Sub1_Sub1_812.mouseX < 404 && anApplet_Sub1_Sub1_Sub1_812.mouseY < 300) {
					i_18_ = (anApplet_Sub1_Sub1_Sub1_812.mouseX - 108) / 37;
					i_19_ = 7 - (anApplet_Sub1_Sub1_Sub1_812.mouseY - 4) / 37;
				}
			} else {
				int i_20_ = aClass10_814.method360();
				if (i_20_ >= 0) {
					i_18_ = i_20_ % 8;
					i_19_ = i_20_ / 8;
				}
			}
			if (!aBoolean830 && anInt838 == anInt841 && !aBoolean842 && i_18_ != -1 && method160(i_18_, i_19_)) {
				anInt833 = i_18_;
				anInt834 = i_19_;
				if (anInt828 == 1) {
					anIntArrayArray843[i_18_][i_19_] = anInt841 + 1;
					aBoolean842 = true;
					anApplet_Sub1_Sub1_Sub1_812.aClass2_582.putPacketId(255);
					anApplet_Sub1_Sub1_Sub1_812.aClass2_582.p1(i_18_);
					anApplet_Sub1_Sub1_Sub1_812.aClass2_582.p1(i_19_);
					anApplet_Sub1_Sub1_Sub1_812.aClass2_582.flushNoException();
				}
			} else {
				anInt833 = -1;
				anInt834 = -1;
			}
			anInt828 = 0;
		}
	}

	public void method156() {
		for (int i = 0; i < 8; i++) {
			for (int i_21_ = 0; i_21_ < 8; i_21_++) {
				int i_22_ = i_21_ * 8 + i;
				aClass7_817.method283(i_22_, 14);
				aClass7_817.method284(i_22_, 14);
			}
		}
		if (anInt833 != -1) {
			int i = anInt834 * 8 + anInt833;
			aClass7_817.method283(i, -24577);
			aClass7_817.method284(i, -24577);
		}
	}

	public void method157(String string, String string_23_) {
		int i = 160;
		int i_24_ = 80;
		aClass6_813.method240(256 - i / 2 + 10, 150 - i_24_ / 2 + 10, i, i_24_, 0, 128);
		aClass6_813.method242(256 - i / 2, 150 - i_24_ / 2, i, i_24_, pixmap.method247(96, 96, 121));
		aClass6_813.method267(string, 256, 130, 4, 16777215);
		aClass6_813.method267(string_23_, 256, 150, 4, 16777215);
		int i_25_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_812.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_812.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_812.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_812.mouseY < 188)
			i_25_ = 16711680;
		aClass6_813.method267(anApplet_Sub1_Sub1_Sub1_812.generalGameModules[17], 226, 178, 4, i_25_);
		i_25_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_812.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_812.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_812.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_812.mouseY < 188)
			i_25_ = 16711680;
		aClass6_813.method267(anApplet_Sub1_Sub1_Sub1_812.generalGameModules[18], 286, 178, 4, i_25_);
	}

	public void method158() {
		int i = 108;
		int i_26_ = 263;
		for (int i_27_ = 0; i_27_ < 8; i_27_++) {
			for (int i_28_ = 0; i_28_ < 8; i_28_++)
				aClass6_813.method255(i_27_ * 37 + i, i_26_ - i_28_ * 37, anInt816);
		}
		if (anInt833 != -1 && anInt834 != -1)
			aClass6_813.method242(anInt833 * 37 + i, i_26_ - anInt834 * 37, 37, 37, pixmap.method247(192, 0, 0));
		for (int i_29_ = 0; i_29_ < 8; i_29_++) {
			for (int i_30_ = 0; i_30_ < 8; i_30_++) {
				if (anIntArrayArray843[i_29_][i_30_] == 1)
					aClass6_813.method255(i_29_ * 37 + i, i_26_ - i_30_ * 37, anInt816 + 1);
				if (anIntArrayArray843[i_29_][i_30_] == 2)
					aClass6_813.method255(i_29_ * 37 + i, i_26_ - i_30_ * 37, anInt816 + 2);
				if (anIntArrayArray843[i_29_][i_30_] >= 100)
					aClass6_813.method255(i_29_ * 37 + i, i_26_ - i_30_ * 37, anInt816 + 1 + (anInt840 / 5 & 0x1));
			}
		}
	}

	public void method159() {
		aClass10_815.clearObjects();
		anInt822 = 0;
		for (int i = 0; i < 8; i++) {
			for (int i_31_ = 0; i_31_ < 8; i_31_++) {
				if (anIntArrayArray843[i][i_31_] == 1) {
					object3d class7 = aClass7Array821[anInt822++];
					aClass10_815.addObject(class7);
					class7.requestSetTranslate(i * 96 - 336, 0, i_31_ * 96 - 336);
					class7.requestSetRotate(0, 0, 0);
				}
				if (anIntArrayArray843[i][i_31_] == 2) {
					object3d class7 = aClass7Array821[anInt822++];
					aClass10_815.addObject(class7);
					class7.requestSetTranslate(i * 96 - 336, 0, i_31_ * 96 - 336);
					class7.requestSetRotate(128, 0, 0);
				}
				if (anIntArrayArray843[i][i_31_] >= 100 && anIntArrayArray843[i][i_31_] < 200) {
					int i_32_ = ((anIntArrayArray843[i][i_31_] - 100) * 128 / anInt836);
					object3d class7 = aClass7Array821[anInt822++];
					aClass10_815.addObject(class7);
					class7.requestSetTranslate(i * 96 - 336, -(int) (Math.sin((double) i_32_ * 0.02454) * 32.0), i_31_ * 96 - 336);
					class7.requestSetRotate(128 - i_32_, 0, 0);
				}
				if (anIntArrayArray843[i][i_31_] >= 200 && anIntArrayArray843[i][i_31_] < 300) {
					int i_33_ = ((anIntArrayArray843[i][i_31_] - 200) * 128 / anInt836);
					object3d class7 = aClass7Array821[anInt822++];
					aClass10_815.addObject(class7);
					class7.requestSetTranslate(i * 96 - 336, -(int) (Math.sin((double) i_33_ * 0.02454) * 32.0), i_31_ * 96 - 336);
					class7.requestSetRotate(i_33_, 0, 0);
				}
			}
		}
	}

	public boolean method160(int i, int i_34_) {
		if (i < 0 || i_34_ < 0 || i > 7 || i_34_ > 7)
			return false;
		if (anIntArrayArray843[i][i_34_] != 0)
			return false;
		if (method161(i, i_34_, -1, -1))
			return true;
		if (method161(i, i_34_, 0, -1))
			return true;
		if (method161(i, i_34_, 1, -1))
			return true;
		if (method161(i, i_34_, -1, 0))
			return true;
		if (method161(i, i_34_, 1, 0))
			return true;
		if (method161(i, i_34_, -1, 1))
			return true;
		if (method161(i, i_34_, 0, 1))
			return true;
		if (method161(i, i_34_, 1, 1))
			return true;
		return false;
	}

	public boolean method161(int i, int i_35_, int i_36_, int i_37_) {
		int i_38_ = 2 - anInt841;
		int i_39_ = i + i_36_;
		int i_40_ = i_35_ + i_37_;
		int i_41_ = 0;
		for (/**/; method162(i_39_, i_40_) == i_38_; i_40_ += i_37_) {
			i_41_++;
			i_39_ += i_36_;
		}
		if (method162(i_39_, i_40_) != anInt841 + 1 || i_41_ == 0)
			return false;
		return true;
	}

	public int method162(int i, int i_42_) {
		if (i < 0 || i_42_ < 0 || i > 7 || i_42_ > 7)
			return 0;
		return anIntArrayArray843[i][i_42_];
	}
}
