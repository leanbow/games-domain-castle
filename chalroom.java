import jagex.gui;
import jagex.pixmap;

public class chalroom extends module {
	pixmap aClass6_669;
	gameclient anApplet_Sub1_Sub1_Sub1_670;
	gui aClass8_671;
	int anInt672;
	int anInt673;
	int anInt674;
	int anInt675;
	int anInt676;
	int anInt677;
	int anInt678;
	int anInt679;
	int anInt680;
	int anInt681;
	int anInt682;
	int anInt683;
	int anInt684;
	int anInt685;
	int anInt686;
	int anInt687;
	int anInt688;
	int anInt689;
	int anInt690;
	int anInt691;
	String aString692 = "";
	boolean aBoolean693 = false;
	int anInt694;
	String aString695;
	String aString696;
	int anInt697;
	byte[] aByteArray698 = new byte[5000];
	byte[] aByteArray699 = new byte[1000];
	int[] anIntArray700 = { 900, 1200, 1200, 1200, 1200, 1130, 1400, 1200 };

	public void onInit(gameclient applet_sub1_sub1_sub1, int i) {
		anApplet_Sub1_Sub1_Sub1_670 = applet_sub1_sub1_sub1;
		aClass6_669 = applet_sub1_sub1_sub1.pixelMap;
		aClass8_671 = new gui(aClass6_669, 50);
		aClass8_671.aBoolean431 = false;
		int i_0_ = 303;
		aClass8_671.createRectangle(3 + i_0_, 2, 202, 167);
		anInt691 = aClass8_671.createText(104 + i_0_, 12, (applet_sub1_sub1_sub1.chalroomModules[0] + " " + name + " " + (applet_sub1_sub1_sub1.chalroomModules[1])), 5, true);
		aClass8_671.createText(104 + i_0_, 26, applet_sub1_sub1_sub1.chalroomModules[2], 2, true);
		anInt674 = aClass8_671.method340(5 + i_0_, 32, 200, 128, 2, 100, true);
		aClass8_671.createRectangle(3 + i_0_, 172, 202, 130);
		anInt675 = aClass8_671.method331(9 + i_0_, 184, applet_sub1_sub1_sub1.chalroomModules[37], 1, true);
		anInt673 = aClass8_671.method347(7 + i_0_, 192, 198, 100, 2, 200, true);
		i_0_ = -96;
		i_0_ += 7;
		aClass8_671.createButton(209 + i_0_ + 7, 2, 175, 156);
		anInt680 = aClass8_671.createText(304 + i_0_, 18, applet_sub1_sub1_sub1.chalroomModules[4], 5, false);
		anInt681 = aClass8_671.createText(304 + i_0_, 18, applet_sub1_sub1_sub1.chalroomModules[4], 5, true);
		aClass8_671.disableComponent(anInt681);
		anInt682 = aClass8_671.createText(304 + i_0_, 44, applet_sub1_sub1_sub1.chalroomModules[5], 3, false);
		anInt683 = aClass8_671.createText(304 + i_0_, 61, applet_sub1_sub1_sub1.chalroomModules[6], 3, false);
		anInt684 = aClass8_671.createText(304 + i_0_, 78, applet_sub1_sub1_sub1.chalroomModules[7], 3, false);
		anInt685 = aClass8_671.createText(304 + i_0_, 103, applet_sub1_sub1_sub1.chalroomModules[8], 4, false);
		String[] strings = { applet_sub1_sub1_sub1.chalroomModules[9], applet_sub1_sub1_sub1.chalroomModules[10] };
		anInt676 = aClass8_671.method344(304 + i_0_, 116, strings, 2, false);
		anInt686 = aClass8_671.createText(304 + i_0_, 132, applet_sub1_sub1_sub1.chalroomModules[11], 4, false);
		String[] strings_1_ = { applet_sub1_sub1_sub1.chalroomModules[12], applet_sub1_sub1_sub1.chalroomModules[13], applet_sub1_sub1_sub1.chalroomModules[14], applet_sub1_sub1_sub1.chalroomModules[15] };
		anInt677 = aClass8_671.method344(304 + i_0_, 146, strings_1_, 2, false);
		aClass8_671.setSelectedIndex(anInt677, 1);
		String[] strings_2_ = { applet_sub1_sub1_sub1.chalroomModules[16], applet_sub1_sub1_sub1.chalroomModules[17] };
		anInt687 = aClass8_671.method345(304 + i_0_, 126, strings_2_, 4, false);
		aClass8_671.disableComponent(anInt687);
		String[] strings_3_ = { applet_sub1_sub1_sub1.chalroomModules[18] };
		anInt688 = aClass8_671.method344(304 + i_0_, 116, strings_3_, 4, false);
		aClass8_671.disableComponent(anInt688);
		String[] strings_4_ = { applet_sub1_sub1_sub1.chalroomModules[19] };
		anInt689 = aClass8_671.method345(304 + i_0_, 116, strings_4_, 4, false);
		aClass8_671.disableComponent(anInt689);
		aClass8_671.createButton(209 + i_0_ + 7, 161, 175, 111);
		aClass8_671.createText(304 + i_0_, 169, applet_sub1_sub1_sub1.chalroomModules[20], 4, false);
		anInt679 = aClass8_671.createText(304 + i_0_, 185, "-", 4, false);
		aClass8_671.createHorizontalLine(209 + i_0_ + 7, 194, 175);
		aClass8_671.createText(302 + i_0_, 204, applet_sub1_sub1_sub1.chalroomModules[21], 1, false);
		String[] strings_5_ = { applet_sub1_sub1_sub1.chalroomModules[22], applet_sub1_sub1_sub1.chalroomModules[23], applet_sub1_sub1_sub1.chalroomModules[24], applet_sub1_sub1_sub1.chalroomModules[25], applet_sub1_sub1_sub1.chalroomModules[26], applet_sub1_sub1_sub1.chalroomModules[27] };
		anInt678 = aClass8_671.method346(300 + i_0_, 241, strings_5_, 2, false, 43);
		aClass8_671.createButton(209 + i_0_ + 7, 275, 175, 27);
		aClass8_671.createText(303 + i_0_, 288, applet_sub1_sub1_sub1.chalroomModules[3], 5, false);
		anInt690 = aClass8_671.createClickableBox(209 + i_0_ + 7, 275, 175, 27);
		i_0_ -= 7;
		i_0_ = -399;
		aClass8_671.createRectangle(402 + i_0_, 2, 120, 20);
		aClass8_671.createText(455 + i_0_ + 7, 12, applet_sub1_sub1_sub1.chalroomModules[28], 1, true);
		aClass8_671.createRectangle(402 + i_0_, 24, 120, 277);
		anInt672 = aClass8_671.method347(405 + i_0_, 24, 120, 277, 2, 200, true);
		aClass8_671.method348(anInt672);
	}

	public void onModuleChange() {
		aBoolean693 = false;
		anInt697 = 0;
		method138();
		aClass8_671.setText(anInt691, (anApplet_Sub1_Sub1_Sub1_670.chalroomModules[0] + " " + name + " " + (anApplet_Sub1_Sub1_Sub1_670.chalroomModules[1])));
		aClass8_671.method308();
		int i = aClass8_671.getSelectedIndex(anInt676);
		int i_6_ = aClass8_671.getSelectedIndex(anInt677);
		int i_7_ = aClass8_671.getSelectedIndex(anInt678);
		anApplet_Sub1_Sub1_Sub1_670.method37(i, i_6_, i_7_);
	}

	public void onLoop() {
		aClass8_671.process(anApplet_Sub1_Sub1_Sub1_670.mouseX, anApplet_Sub1_Sub1_Sub1_670.mouseY, anApplet_Sub1_Sub1_Sub1_670.mouseClick, anApplet_Sub1_Sub1_Sub1_670.mouseDown);
		if (aClass8_671.isComponentClicked(anInt676) || aClass8_671.isComponentClicked(anInt677) || aClass8_671.isComponentClicked(anInt678)) {
			int i = aClass8_671.getSelectedIndex(anInt676);
			int i_8_ = aClass8_671.getSelectedIndex(anInt677);
			int i_9_ = aClass8_671.getSelectedIndex(anInt678);
			anApplet_Sub1_Sub1_Sub1_670.method37(i, i_8_, i_9_);
		}
		if (aClass8_671.isComponentClicked(anInt672)) {
			int i = aClass8_671.getSelectedIndex(anInt672);
			anApplet_Sub1_Sub1_Sub1_670.challengePlayer(i);
		}
		if (aClass8_671.isComponentClicked(anInt687)) {
			int i = aClass8_671.getSelectedIndex(anInt687);
			if (i == 0)
				anApplet_Sub1_Sub1_Sub1_670.acceptChallenge();
			else {
				anApplet_Sub1_Sub1_Sub1_670.declineChallenge();
				method138();
			}
		}
		if (aClass8_671.isComponentClicked(anInt688)) {
			int i = aClass8_671.getSelectedIndex(anInt688);
			if (i == 0) {
				anApplet_Sub1_Sub1_Sub1_670.declineChallenge();
				method138();
			}
		}
		if (aClass8_671.isComponentClicked(anInt689))
			method138();
		if (aClass8_671.isComponentClicked(anInt690))
			anApplet_Sub1_Sub1_Sub1_670.method78();
		if (!aBoolean693 && aClass8_671.isComponentClicked(anInt673)) {
			int i = aClass8_671.getSelectedIndex(anInt673);
			anApplet_Sub1_Sub1_Sub1_670.method35(i);
		}
		if (aBoolean693) {
			anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].aBoolean142 = false;
			anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].onLoop();
			if (anInt694 > 0) {
				anInt694--;
				if (anInt694 == 0) {
					aBoolean693 = false;
					anInt697 = 0;
				}
			}
			int i = 0;
			while (i < anInt697) {
				int i_10_ = ((aByteArray698[i] & 0xff) * 256 + (aByteArray698[i + 1] & 0xff));
				int i_11_ = aByteArray698[i + 3] & 0xff;
				if (anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].method114()) {
					for (int i_12_ = 0; i_12_ < i_10_; i_12_++)
						aByteArray699[i_12_] = aByteArray698[i_12_ + i + 4];
					anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].method103(i_11_, i_10_, aByteArray699);
					anInt697 -= 4 + i_10_;
					for (int i_13_ = i; i_13_ < anInt697; i_13_++)
						aByteArray698[i_13_] = aByteArray698[i_13_ + 4 + i_10_];
				} else
					i += 4 + i_10_;
			}
			int i_14_ = 306;
			int i_15_ = 172;
			if (anApplet_Sub1_Sub1_Sub1_670.mouseX > i_14_ && anApplet_Sub1_Sub1_Sub1_670.mouseY > i_15_ && anApplet_Sub1_Sub1_Sub1_670.mouseX < i_14_ + 202 && anApplet_Sub1_Sub1_Sub1_670.mouseY < i_15_ + 130 && anApplet_Sub1_Sub1_Sub1_670.mouseClick == 1) {
				aBoolean693 = false;
				anInt697 = 0;
				anApplet_Sub1_Sub1_Sub1_670.method36();
				anApplet_Sub1_Sub1_Sub1_670.mouseClick = 0;
				anApplet_Sub1_Sub1_Sub1_670.mouseDown = 0;
				aClass8_671.method310();
				aClass8_671.isComponentClicked(anInt673);
			}
		}
	}

	public void method138() {
		aClass8_671.enableComponent(anInt680);
		aClass8_671.disableComponent(anInt681);
		aClass8_671.setText(anInt682, (anApplet_Sub1_Sub1_Sub1_670.chalroomModules[29]));
		aClass8_671.setText(anInt683, (anApplet_Sub1_Sub1_Sub1_670.chalroomModules[30]));
		aClass8_671.setText(anInt684, (anApplet_Sub1_Sub1_Sub1_670.chalroomModules[31]));
		aClass8_671.enableComponent(anInt685);
		aClass8_671.enableComponent(anInt686);
		aClass8_671.enableComponent(anInt676);
		aClass8_671.enableComponent(anInt677);
		aClass8_671.disableComponent(anInt687);
		aClass8_671.disableComponent(anInt688);
		aClass8_671.disableComponent(anInt689);
		aClass8_671.setSelectedIndex(anInt689, -1);
		aClass8_671.method314(209, 2, 398, 156);
	}

	public void method111(String string, String string_16_, String string_17_, String string_18_) {
		aClass8_671.disableComponent(anInt680);
		aClass8_671.enableComponent(anInt681);
		aClass8_671.setText(anInt681, string);
		aClass8_671.setText(anInt682, string_16_);
		aClass8_671.setText(anInt683, string_17_);
		aClass8_671.setText(anInt684, string_18_);
		aClass8_671.disableComponent(anInt685);
		aClass8_671.disableComponent(anInt686);
		aClass8_671.disableComponent(anInt676);
		aClass8_671.disableComponent(anInt677);
		aClass8_671.disableComponent(anInt687);
		aClass8_671.disableComponent(anInt688);
		aClass8_671.enableComponent(anInt689);
		aClass8_671.method314(209, 2, 398, 156);
		aClass8_671.setSelectedIndex(anInt689, -1);
	}

	public void method109(String string, String string_19_) {
		anApplet_Sub1_Sub1_Sub1_670.xmPlayer.method85(0, 3, 8000, 63);
		method111(anApplet_Sub1_Sub1_Sub1_670.chalroomModules[32], string, anApplet_Sub1_Sub1_Sub1_670.chalroomModules[33], string_19_);
		aClass8_671.enableComponent(anInt687);
		aClass8_671.disableComponent(anInt689);
		aClass8_671.setSelectedIndex(anInt687, -1);
	}

	public void method110(String string, String string_20_) {
		anApplet_Sub1_Sub1_Sub1_670.xmPlayer.method85(0, 3, 8000, 63);
		method111(anApplet_Sub1_Sub1_Sub1_670.chalroomModules[34], anApplet_Sub1_Sub1_Sub1_670.chalroomModules[35], string, string_20_);
		aClass8_671.enableComponent(anInt688);
		aClass8_671.disableComponent(anInt689);
		aClass8_671.setSelectedIndex(anInt688, -1);
	}

	public void method106(int i, int[] is, int[] is_21_, String[] strings) {
		aClass8_671.method348(anInt672);
		if (i > 200)
			i = 200;
		for (int i_22_ = 0; i_22_ < i; i_22_++) {
			String string = strings[i_22_] + " (" + is[i_22_] / 10 + ")";
			if (is_21_[i_22_] == 0)
				string = "@red@" + string;
			aClass8_671.method349(anInt672, i_22_, string);
		}
	}

	public void method107(int i, String[] strings, String[] strings_23_) {
		aClass8_671.method348(anInt673);
		if (i > 200)
			i = 200;
		for (int i_24_ = 0; i_24_ < i; i_24_++) {
			String string = (strings[i_24_] + " " + anApplet_Sub1_Sub1_Sub1_670.chalroomModules[38] + " " + strings_23_[i_24_]);
			aClass8_671.method349(anInt673, i_24_, string);
		}
		aClass8_671.setText(anInt675, (anApplet_Sub1_Sub1_Sub1_670.chalroomModules[37] + " (" + i + ")"));
	}

	public void method112(int i, String[] strings, int[] is, int[] is_25_, int[] is_26_) {
		aClass8_671.method348(anInt674);
		for (int i_27_ = 0; i_27_ < 50; i_27_++) {
			if (!strings[i_27_].equalsIgnoreCase("-"))
				aClass8_671.method341(anInt674, (String.valueOf(i_27_ + 1) + ":~329~" + strings[i_27_] + "~410~" + is[i_27_] + "~445~" + is_25_[i_27_] + "~471~" + is_26_[i_27_]), false);
		}
	}

	public void method108(String string) {
		aClass8_671.setText(anInt679, string);
		aClass8_671.method314(209, 159, 398, 301);
	}

	public void onDraw() {
		aClass8_671.draw();
		if (aBoolean693) {
			int i = 407;
			int i_28_ = 237;
			int i_29_ = 101;
			int i_30_ = 65;
			anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].method121();
			anApplet_Sub1_Sub1_Sub1_670.aClass10_1084.method362(i, i_28_, i_29_, i_30_, 512, 8);
			anApplet_Sub1_Sub1_Sub1_670.aClass10_1085.method362(i, i_28_, i_29_, i_30_, 512, 8);
			anApplet_Sub1_Sub1_Sub1_670.aClass10_1086.method362(i, i_28_, i_29_, i_30_, 512, 8);
			anApplet_Sub1_Sub1_Sub1_670.aClass10_1087.method362(i, i_28_, i_29_, i_30_, 512, 8);
			aClass6_669.method241(i - i_29_, i_28_ - i_30_, i_29_ * 2, i_30_ * 2, 0, 4210752);
			int i_31_ = (int) ((double) anIntArray700[id] * 1.3);
			if (id == 0)
				anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].method116(896, 512, i_31_);
			else
				anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].method116(912, 0, i_31_);
			aClass6_669.method243(i - i_29_, i_28_ - i_30_, i_29_ * 2, i_30_ * 2, 12632256);
			aClass6_669.method267((aString695 + " " + (anApplet_Sub1_Sub1_Sub1_670.chalroomModules[38]) + " " + aString696), i, i_28_ + i_30_ - 3, 2, 16777215);
		}
	}

	public void method115(String string, String string_32_, String string_33_) {
		anApplet_Sub1_Sub1_Sub1_670.xmPlayer.method85(0, 4, 8000, 63);
		method111(string, "", string_32_, string_33_);
	}

	public void method118(int i, int i_34_, byte[] is, int i_35_) {
		if (i_35_ == id + 1 && aBoolean693 != false) {
			if (anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].method114())
				anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].method103(i, i_34_, is);
			else if (anInt697 + i_34_ + 4 < 5000) {
				aByteArray698[anInt697++] = (byte) (i_34_ / 256);
				aByteArray698[anInt697++] = (byte) (i_34_ & 0xff);
				aByteArray698[anInt697++] = (byte) i_35_;
				aByteArray698[anInt697++] = (byte) i;
				for (int i_36_ = 0; i_36_ < i_34_; i_36_++)
					aByteArray698[anInt697++] = is[i_36_];
			}
		}
	}

	public void method119(int i) {
		if (i == id + 1 && aBoolean693 != false) {
			if (anInt694 == 0)
				anInt694 = 150;
		}
	}

	public void method120(int i, String string, String string_37_) {
		if (i == id + 1) {
			aBoolean693 = true;
			anInt697 = 0;
			aString695 = string.trim();
			aString696 = string_37_.trim();
			anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].method113(0);
			anApplet_Sub1_Sub1_Sub1_670.modules[id + 4].method117();
		}
	}
}
