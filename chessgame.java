import jagex.object3d;
import jagex.pixmap;
import jagex.tools;
import jagex.world3d;

public class chessgame extends module {
	gameclient anApplet_Sub1_Sub1_Sub1_616;
	pixmap aClass6_617;
	world3d aClass10_618;
	world3d aClass10_619;
	world3d aClass10_620;
	int anInt621;
	object3d aClass7_622;
	object3d aClass7_623;
	object3d[][] aClass7ArrayArray624 = new object3d[12][10];
	object3d[][] aClass7ArrayArray625 = new object3d[12][10];
	int[] anIntArray626 = new int[12];
	int anInt627;
	int anInt628 = 912;
	int anInt629;
	int anInt630;
	int anInt631;
	int anInt632;
	boolean aBoolean633 = false;
	boolean aBoolean634 = false;
	boolean aBoolean635 = false;
	int anInt636 = 100;
	int anInt637;
	int anInt638 = 60;
	int anInt639;
	int anInt640;
	boolean aBoolean641 = true;
	boolean aBoolean642 = false;
	boolean aBoolean643 = false;
	boolean aBoolean644 = true;
	boolean aBoolean645 = true;
	int anInt646 = -1;
	int anInt647;
	boolean aBoolean648 = false;
	int[][] anIntArrayArray649 = new int[8][8];
	int[][] anIntArrayArray650 = new int[8][8];
	int anInt651;
	int[] anIntArray652 = new int[64];
	int anInt653 = -1;
	int anInt654 = -1;
	int anInt655 = -1;
	int anInt656;
	int[] anIntArray657 = new int[250];
	int[] anIntArray658 = new int[250];
	int[] anIntArray659 = new int[250];
	int[] anIntArray660 = new int[250];
	boolean aBoolean661 = false;
	int anInt662 = -1;
	int anInt663 = -1;
	int anInt664 = -1;
	int anInt665 = -1;
	int anInt666 = -1;
	int anInt667 = -1;
	int anInt668 = 32;

	public void onInit(gameclient applet_sub1_sub1_sub1, int i) {
		anApplet_Sub1_Sub1_Sub1_616 = applet_sub1_sub1_sub1;
		anInt621 = i;
		aClass6_617 = applet_sub1_sub1_sub1.pixelMap;
		aClass10_618 = applet_sub1_sub1_sub1.aClass10_1086;
		aClass10_619 = applet_sub1_sub1_sub1.aClass10_1084;
		aClass10_620 = applet_sub1_sub1_sub1.aClass10_1085;
		aClass6_617.method249(applet_sub1_sub1_sub1.aByteArray1083, tools.findFile("2dchess.tga", (applet_sub1_sub1_sub1.aByteArray1083)), i, true, 8, false);
		for (int i_0_ = i + 2; i_0_ < i + 8; i_0_++) {
			aClass6_617.method252(i_0_, i_0_ + 6);
			aClass6_617.method253(i_0_ + 6, 16579836, 13304065);
		}
		aClass7_622 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("board.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		String[] strings = { "king", "queen", "rook", "knight", "bishop", "pawn" };
		for (int i_1_ = 0; i_1_ < 6; i_1_++) {
			int i_2_ = i_1_ + 6;
			aClass7ArrayArray624[i_1_][0] = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile(strings[i_1_] + ".ob2", applet_sub1_sub1_sub1.aByteArray1082));
			aClass7ArrayArray624[i_1_][0].method281(50, 40, -32, -32, 0);
			aClass7ArrayArray624[i_1_][0].requestScale(320, 320, 320);
			aClass7ArrayArray624[i_1_][0].applyChanges();
			for (int i_3_ = 1; i_3_ < 10; i_3_++)
				aClass7ArrayArray624[i_1_][i_3_] = aClass7ArrayArray624[i_1_][0].copy();
			aClass7ArrayArray625[i_1_][0] = aClass7ArrayArray624[i_1_][0].copy();
			aClass7ArrayArray625[i_1_][0].method281(48, 12, -32, -32, 0);
			aClass7ArrayArray625[i_1_][0].requestSetRotate(128, 192, 0);
			aClass7ArrayArray625[i_1_][0].applyChanges();
			for (int i_4_ = 1; i_4_ < 10; i_4_++)
				aClass7ArrayArray625[i_1_][i_4_] = aClass7ArrayArray625[i_1_][0].copy();
			aClass7ArrayArray624[i_2_][0] = aClass7ArrayArray624[i_1_][0].copy();
			for (int i_5_ = 0; i_5_ < aClass7ArrayArray624[i_2_][0].polygonCount; i_5_++)
				aClass7ArrayArray624[i_2_][0].method283(i_5_, -22529);
			for (int i_6_ = 1; i_6_ < 10; i_6_++)
				aClass7ArrayArray624[i_2_][i_6_] = aClass7ArrayArray624[i_2_][0].copy();
			aClass7ArrayArray625[i_2_][0] = aClass7ArrayArray624[i_2_][0].copy();
			aClass7ArrayArray625[i_2_][0].method281(52, 12, -32, -32, 0);
			aClass7ArrayArray625[i_2_][0].requestSetRotate(128, 192, 0);
			aClass7ArrayArray625[i_2_][0].applyChanges();
			for (int i_7_ = 1; i_7_ < 10; i_7_++)
				aClass7ArrayArray625[i_2_][i_7_] = aClass7ArrayArray625[i_2_][0].copy();
		}
		aClass7_623 = new object3d(81, 64);
		for (int i_8_ = 0; i_8_ < 9; i_8_++) {
			for (int i_9_ = 0; i_9_ < 9; i_9_++)
				aClass7_623.addVertex(i_9_ * 96 - 384, 0, i_8_ * 96 - 384);
		}
		for (int i_10_ = 0; i_10_ < 8; i_10_++) {
			for (int i_11_ = 0; i_11_ < 8; i_11_++) {
				int[] is = new int[4];
				is[0] = i_10_ * 9 + i_11_;
				is[1] = i_10_ * 9 + i_11_ + 1;
				is[2] = i_10_ * 9 + i_11_ + 10;
				is[3] = i_10_ * 9 + i_11_ + 9;
				if ((i_11_ & 0x1) == (i_10_ & 0x1))
					aClass7_623.addPolygon(4, is, 20, 20);
				else
					aClass7_623.addPolygon(4, is, 10, 10);
			}
		}
		aClass7_623.method279(50);
		aClass7_623.method280(true, 52, 32, -50, -10, -50);
		aClass7_623.aBoolean331 = true;
		aClass7_622.method280(true, 52, 32, -50, -10, -50);
	}

	public void method113(int i) {
		anInt640 = i;
		if (i == 0)
			anInt627 = 0;
		else
			anInt627 = 512;
		aClass10_618.clearObjects();
		aClass10_619.clearObjects();
		aClass10_620.clearObjects();
		aClass10_618.method362(256, 152, 256, 152, 512, 9);
		aClass10_618.maxRenderZ = 3000;
		aClass10_618.anInt464 = 3000;
		aClass10_618.anInt465 = 20;
		aClass10_618.anInt466 = 5000;
		aClass10_619.method385(aClass10_618);
		aClass10_620.method385(aClass10_618);
		aClass10_618.addObject(aClass7_622);
		aClass10_618.addObject(aClass7_623);
		for (int i_12_ = 0; i_12_ < 8; i_12_++) {
			for (int i_13_ = 0; i_13_ < 8; i_13_++) {
				if (i_13_ == 0) {
					if (i_12_ == 0 || i_12_ == 7)
						anIntArrayArray649[i_12_][i_13_] = 3;
					if (i_12_ == 1 || i_12_ == 6)
						anIntArrayArray649[i_12_][i_13_] = 4;
					if (i_12_ == 2 || i_12_ == 5)
						anIntArrayArray649[i_12_][i_13_] = 5;
					if (i_12_ == 3)
						anIntArrayArray649[i_12_][i_13_] = 2;
					if (i_12_ == 4)
						anIntArrayArray649[i_12_][i_13_] = 1;
				} else if (i_13_ == 1)
					anIntArrayArray649[i_12_][i_13_] = 6;
				else if (i_13_ == 6)
					anIntArrayArray649[i_12_][i_13_] = 12;
				else if (i_13_ == 7) {
					if (i_12_ == 0 || i_12_ == 7)
						anIntArrayArray649[i_12_][i_13_] = 9;
					if (i_12_ == 1 || i_12_ == 6)
						anIntArrayArray649[i_12_][i_13_] = 10;
					if (i_12_ == 2 || i_12_ == 5)
						anIntArrayArray649[i_12_][i_13_] = 11;
					if (i_12_ == 3)
						anIntArrayArray649[i_12_][i_13_] = 8;
					if (i_12_ == 4)
						anIntArrayArray649[i_12_][i_13_] = 7;
				} else
					anIntArrayArray649[i_12_][i_13_] = 0;
				anIntArrayArray650[i_12_][i_13_] = anIntArrayArray649[i_12_][i_13_];
			}
		}
		anInt653 = -1;
		anInt654 = -1;
		anInt655 = -1;
		aBoolean661 = false;
		aBoolean641 = true;
		anInt662 = -1;
		anInt663 = -1;
		anInt664 = -1;
		anInt665 = -1;
		anInt666 = -1;
		anInt667 = -1;
		aBoolean634 = false;
		aBoolean635 = false;
		anInt636 = 100;
		method131();
	}

	public void method121() {
		aClass10_618.clearObjects();
		aClass10_619.clearObjects();
		aClass10_620.clearObjects();
		aClass10_618.addObject(aClass7_622);
		aClass10_618.addObject(aClass7_623);
	}

	public boolean method114() {
		if (aBoolean661)
			return false;
		return true;
	}

	public void method103(int i, int i_14_, byte[] is) {
		if (i == 255) {
			anInt637 = is[1] & 0xff;
			anInt638 = is[2] & 0xff;
			anInt636 = 100;
			if (anInt637 == anInt640) {
				int i_15_ = is[3] & 0xff;
				if ((i_15_ & 0x1) == 1)
					aBoolean644 = true;
				else
					aBoolean644 = false;
				if ((i_15_ & 0x2) == 2)
					aBoolean645 = true;
				else
					aBoolean645 = false;
				anInt646 = is[4];
				anInt647 = is[5];
			}
			if (anInt637 == anInt640 && anInt651 == 0 && !aBoolean641)
				method131();
			if (anInt637 != anInt640)
				aBoolean641 = false;
		} else if (i == 254) {
			int i_16_ = 1;
			for (int i_17_ = 0; i_17_ < 8; i_17_++) {
				for (int i_18_ = 0; i_18_ < 8; i_18_++)
					anIntArrayArray650[i_17_][i_18_] = anIntArrayArray649[i_17_][i_18_] = is[i_16_++];
			}
			anInt637 = is[i_16_++] & 0xff;
			anInt638 = is[i_16_++] & 0xff;
			anInt636 = 100;
			if (anInt637 == anInt640) {
				int i_19_ = is[i_16_++] & 0xff;
				if ((i_19_ & 0x1) == 1)
					aBoolean644 = true;
				else
					aBoolean644 = false;
				if ((i_19_ & 0x2) == 2)
					aBoolean645 = true;
				else
					aBoolean645 = false;
				anInt646 = is[i_16_++];
				anInt647 = is[i_16_++];
			}
			if (anInt637 == anInt640 && anInt651 == 0 && !aBoolean641)
				method131();
			aBoolean641 = false;
			aBoolean648 = false;
		} else if (i == 253) {
			int i_20_ = is[1] & 0xff;
			int i_21_ = is[2] & 0xff;
			int i_22_ = is[3] & 0xff;
			int i_23_ = is[4] & 0xff;
			if (i_20_ != anInt662 || i_21_ != anInt663 || i_22_ != anInt664 || i_23_ != anInt665)
				method127(i_20_, i_21_, i_22_, i_23_, false);
			anIntArrayArray650[i_22_][i_23_] = anIntArrayArray650[i_20_][i_21_];
			anIntArrayArray650[i_20_][i_21_] = 0;
			if (!aBoolean661) {
				for (int i_24_ = 0; i_24_ < 8; i_24_++) {
					for (int i_25_ = 0; i_25_ < 8; i_25_++) {
						if (anIntArrayArray650[i_24_][i_25_] != anIntArrayArray649[i_24_][i_25_])
							anIntArrayArray649[i_24_][i_25_] = anIntArrayArray650[i_24_][i_25_];
					}
				}
			}
		} else if (i == 252) {
			aBoolean648 = true;
			aBoolean641 = false;
		}
	}

	public void onLoop() {
		anInt639++;
		if (anApplet_Sub1_Sub1_Sub1_616.mouseDown == 2) {
			if (anInt631 == 0) {
				anInt631 = anApplet_Sub1_Sub1_Sub1_616.mouseDown;
				anInt629 = anApplet_Sub1_Sub1_Sub1_616.mouseX;
				anInt630 = anApplet_Sub1_Sub1_Sub1_616.mouseY;
			}
			int i = anApplet_Sub1_Sub1_Sub1_616.mouseX - anInt629;
			int i_26_ = anApplet_Sub1_Sub1_Sub1_616.mouseY - anInt630;
			anInt627 = anInt627 + i * 2 + 1024 & 0x3ff;
			anInt628 = anInt628 - i_26_ * 2 + 1024 & 0x3ff;
			if (anInt628 > 256 && anInt628 < 600)
				anInt628 = 600;
			if (anInt628 > 960 || anInt628 < 256)
				anInt628 = 960;
			anInt629 = anApplet_Sub1_Sub1_Sub1_616.mouseX;
			anInt630 = anApplet_Sub1_Sub1_Sub1_616.mouseY;
		} else
			anInt631 = 0;
		if (anApplet_Sub1_Sub1_Sub1_616.mouseClick == 1) {
			anInt632 = 1;
			aClass10_618.method358(anApplet_Sub1_Sub1_Sub1_616.mouseX, anApplet_Sub1_Sub1_Sub1_616.mouseY, 50, 50);
		}
		if (aBoolean661) {
			anInt667++;
			if (anInt667 == anInt668)
				method126();
		}
		anInt636--;
		if (anInt636 <= 0 && anInt638 > 10) {
			anInt636 = 50;
			anInt638--;
		}
	}

	public void method116(int i, int i_27_, int i_28_) {
		method129();
		method128();
		if (aBoolean661)
			method125();
		aClass10_620.setCamera(0, 80, 0, i, i_27_, 0, i_28_);
		aClass10_620.method367();
		aClass10_618.setCamera(0, 80, 0, i, i_27_, 0, i_28_);
		aClass10_618.method367();
		aClass10_619.setCamera(0, 80, 0, i, i_27_, 0, i_28_);
		aClass10_619.method367();
	}

	public void onDraw() {
		aClass6_617.method241(0, 0, 512, 384, 0, 4210752);
		if (aBoolean633) {
			method123();
			if (aBoolean661)
				method124();
		} else {
			int i = 1200;
			method116(anInt628, anInt627, i);
		}
		if (aBoolean661 || aBoolean641)
			aClass6_617.method268((anApplet_Sub1_Sub1_Sub1_616.generalGameModules[0]), 7, 12, 1, 16777215);
		else if (anInt637 == anInt640)
			aClass6_617.method268((anApplet_Sub1_Sub1_Sub1_616.generalGameModules[1]), 7, 12, 1, 16777215);
		else
			aClass6_617.method268((anApplet_Sub1_Sub1_Sub1_616.generalGameModules[2]), 7, 12, 1, 16777215);
		aClass6_617.method268((anApplet_Sub1_Sub1_Sub1_616.generalGameModules[3] + anInt638), 7, 23, 1, 16777215);
		int i = 460;
		int i_29_ = 4;
		anApplet_Sub1_Sub1_Sub1_616.aClass8_1094.method321(i, i_29_, 46, 28);
		aClass6_617.method267(anApplet_Sub1_Sub1_Sub1_616.generalGameModules[4], i + 22, i_29_ + 13, 1, 0);
		if (aBoolean633)
			aClass6_617.method267((anApplet_Sub1_Sub1_Sub1_616.generalGameModules[5]), i + 22, i_29_ + 23, 1, 0);
		else
			aClass6_617.method267((anApplet_Sub1_Sub1_Sub1_616.generalGameModules[6]), i + 22, i_29_ + 23, 1, 0);
		i = 460;
		i_29_ = 34;
		anApplet_Sub1_Sub1_Sub1_616.aClass8_1094.method321(i, i_29_, 46, 28);
		aClass6_617.method267(anApplet_Sub1_Sub1_Sub1_616.generalGameModules[7], i + 22, i_29_ + 18, 1, 0);
		i = 460;
		i_29_ = 64;
		anApplet_Sub1_Sub1_Sub1_616.aClass8_1094.method321(i, i_29_, 46, 28);
		if (anApplet_Sub1_Sub1_Sub1_616.aBoolean604)
			aClass6_617.method267((anApplet_Sub1_Sub1_Sub1_616.generalGameModules[8]), i + 22, i_29_ + 13, 1, 0);
		else
			aClass6_617.method267((anApplet_Sub1_Sub1_Sub1_616.generalGameModules[9]), i + 22, i_29_ + 13, 1, 0);
		aClass6_617.method267(anApplet_Sub1_Sub1_Sub1_616.generalGameModules[10], i + 22, i_29_ + 23, 1, 0);
		if (aBoolean634) {
			method122(anApplet_Sub1_Sub1_Sub1_616.generalGameModules[11], anApplet_Sub1_Sub1_Sub1_616.generalGameModules[12]);
			if (anApplet_Sub1_Sub1_Sub1_616.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_616.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_616.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_616.mouseY < 188 && anInt632 == 1) {
				anApplet_Sub1_Sub1_Sub1_616.resign();
				aBoolean634 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_616.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_616.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_616.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_616.mouseY < 188 && anInt632 == 1)
				aBoolean634 = false;
			anInt632 = 0;
		} else if (aBoolean635) {
			if (anApplet_Sub1_Sub1_Sub1_616.aBoolean604)
				method122(anApplet_Sub1_Sub1_Sub1_616.generalGameModules[13], anApplet_Sub1_Sub1_Sub1_616.generalGameModules[14]);
			else
				method122(anApplet_Sub1_Sub1_Sub1_616.generalGameModules[15], anApplet_Sub1_Sub1_Sub1_616.generalGameModules[16]);
			if (anApplet_Sub1_Sub1_Sub1_616.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_616.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_616.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_616.mouseY < 188 && anInt632 == 1) {
				anApplet_Sub1_Sub1_Sub1_616.offerDraw();
				aBoolean635 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_616.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_616.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_616.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_616.mouseY < 188 && anInt632 == 1)
				aBoolean635 = false;
			anInt632 = 0;
		} else {
			i = 460;
			i_29_ = 4;
			if (anInt632 == 1 && anApplet_Sub1_Sub1_Sub1_616.mouseX > i && anApplet_Sub1_Sub1_Sub1_616.mouseY > i_29_ && anApplet_Sub1_Sub1_Sub1_616.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_616.mouseY < i_29_ + 29) {
				aBoolean633 = !aBoolean633;
				anInt632 = 0;
			}
			i = 460;
			i_29_ = 34;
			if (anInt632 == 1 && anApplet_Sub1_Sub1_Sub1_616.mouseX > i && anApplet_Sub1_Sub1_Sub1_616.mouseY > i_29_ && anApplet_Sub1_Sub1_Sub1_616.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_616.mouseY < i_29_ + 29) {
				aBoolean634 = true;
				anInt632 = 0;
			}
			i = 460;
			i_29_ = 64;
			if (anInt632 == 1 && anApplet_Sub1_Sub1_Sub1_616.mouseX > i && anApplet_Sub1_Sub1_Sub1_616.mouseY > i_29_ && anApplet_Sub1_Sub1_Sub1_616.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_616.mouseY < i_29_ + 29) {
				aBoolean635 = true;
				anInt632 = 0;
			}
			if (aBoolean648) {
				aClass6_617.method242(176, 110, 160, 70, 0);
				aClass6_617.method243(176, 110, 160, 70, 16777215);
				int i_30_ = 179;
				int i_31_ = 135;
				aClass6_617.method262(i_30_, i_31_, 37, 37, anInt621 + 3 + anInt640 * 6);
				aClass6_617.method262(i_30_ + 40, i_31_, 37, 37, anInt621 + 4 + anInt640 * 6);
				aClass6_617.method262(i_30_ + 80, i_31_, 37, 37, anInt621 + 5 + anInt640 * 6);
				aClass6_617.method262(i_30_ + 120, i_31_, 37, 37, anInt621 + 6 + anInt640 * 6);
				aClass6_617.method267((anApplet_Sub1_Sub1_Sub1_616.chessGameModules[0]), 256, 128, 1, 16777215);
				if (anInt632 == 1) {
					if (anApplet_Sub1_Sub1_Sub1_616.mouseX > i_30_ && anApplet_Sub1_Sub1_Sub1_616.mouseY > i_31_ && anApplet_Sub1_Sub1_Sub1_616.mouseX < i_30_ + 40 && anApplet_Sub1_Sub1_Sub1_616.mouseY < i_31_ + 40) {
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.putPacketId(254);
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.p1(2);
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.flushNoException();
					}
					if (anApplet_Sub1_Sub1_Sub1_616.mouseX > i_30_ + 40 && anApplet_Sub1_Sub1_Sub1_616.mouseY > i_31_ && anApplet_Sub1_Sub1_Sub1_616.mouseX < i_30_ + 80 && anApplet_Sub1_Sub1_Sub1_616.mouseY < i_31_ + 40) {
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.putPacketId(254);
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.p1(3);
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.flushNoException();
					}
					if (anApplet_Sub1_Sub1_Sub1_616.mouseX > i_30_ + 80 && anApplet_Sub1_Sub1_Sub1_616.mouseY > i_31_ && anApplet_Sub1_Sub1_Sub1_616.mouseX < i_30_ + 120 && anApplet_Sub1_Sub1_Sub1_616.mouseY < i_31_ + 40) {
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.putPacketId(254);
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.p1(4);
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.flushNoException();
					}
					if (anApplet_Sub1_Sub1_Sub1_616.mouseX > i_30_ + 120 && anApplet_Sub1_Sub1_Sub1_616.mouseY > i_31_ && anApplet_Sub1_Sub1_Sub1_616.mouseX < i_30_ + 160 && anApplet_Sub1_Sub1_Sub1_616.mouseY < i_31_ + 40) {
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.putPacketId(254);
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.p1(5);
						anApplet_Sub1_Sub1_Sub1_616.aClass2_582.flushNoException();
					}
				}
			}
			if (!aBoolean661 && anInt632 == 1 && anInt637 == anInt640 && !aBoolean641 && !aBoolean648) {
				int i_32_ = -1;
				if (aBoolean633) {
					int i_33_ = (anApplet_Sub1_Sub1_Sub1_616.mouseX - 108) / 37;
					int i_34_ = 7 - (anApplet_Sub1_Sub1_Sub1_616.mouseY - 4) / 37;
					if (anInt640 == 1) {
						i_33_ = 7 - i_33_;
						i_34_ = 7 - i_34_;
					}
					i_32_ = i_33_ + i_34_ * 8;
				} else {
					i_32_ = aClass10_618.method360();
					object3d class7 = aClass10_618.method361();
					if (class7 != aClass7_623)
						i_32_ = -1;
				}
				if (anInt655 == -1) {
					for (int i_35_ = 0; i_35_ < anInt651; i_35_++) {
						if (anIntArray652[i_35_] == i_32_) {
							anInt653 = i_32_ % 8;
							anInt654 = i_32_ / 8;
							anInt655 = anIntArrayArray649[anInt653][anInt654];
							method130();
							break;
						}
					}
				} else {
					for (int i_36_ = 0; i_36_ < anInt651; i_36_++) {
						if (anIntArray652[i_36_] == i_32_) {
							int i_37_ = i_32_ % 8;
							int i_38_ = i_32_ / 8;
							method127(anInt653, anInt654, i_37_, i_38_, true);
							break;
						}
						if (i_36_ == anInt651 - 1) {
							anInt655 = -1;
							method131();
							break;
						}
					}
				}
			}
			anInt632 = 0;
		}
	}

	public void method122(String string, String string_39_) {
		int i = 160;
		int i_40_ = 80;
		aClass6_617.method240(256 - i / 2 + 10, 150 - i_40_ / 2 + 10, i, i_40_, 0, 128);
		aClass6_617.method242(256 - i / 2, 150 - i_40_ / 2, i, i_40_, pixmap.method247(96, 96, 121));
		aClass6_617.method267(string, 256, 130, 4, 16777215);
		aClass6_617.method267(string_39_, 256, 150, 4, 16777215);
		int i_41_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_616.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_616.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_616.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_616.mouseY < 188)
			i_41_ = 16711680;
		aClass6_617.method267(anApplet_Sub1_Sub1_Sub1_616.generalGameModules[17], 226, 178, 4, i_41_);
		i_41_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_616.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_616.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_616.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_616.mouseY < 188)
			i_41_ = 16711680;
		aClass6_617.method267(anApplet_Sub1_Sub1_Sub1_616.generalGameModules[18], 286, 178, 4, i_41_);
	}

	public void method123() {
		int i = 108;
		int i_42_ = 263;
		for (int i_43_ = 0; i_43_ < 8; i_43_++) {
			for (int i_44_ = 0; i_44_ < 8; i_44_++) {
				int i_45_;
				if ((i_43_ & 0x1) == (i_44_ & 0x1))
					i_45_ = anInt621;
				else
					i_45_ = anInt621 + 1;
				aClass6_617.method255(i_43_ * 37 + i, i_42_ - i_44_ * 37, i_45_);
			}
		}
		if ((anInt639 / 12 & 0x1) == 1 && !aBoolean661 && !aBoolean641 && anInt637 == anInt640 && !aBoolean648) {
			for (int i_46_ = 0; i_46_ < anInt651; i_46_++) {
				int i_47_ = anIntArray652[i_46_];
				int i_48_ = i_47_ % 8;
				int i_49_ = i_47_ / 8;
				if (anInt640 == 1) {
					i_48_ = 7 - i_48_;
					i_49_ = 7 - i_49_;
				}
				if (anInt655 == -1)
					aClass6_617.method242(i_48_ * 37 + i, i_42_ - i_49_ * 37, 37, 37, pixmap.method247(192, 0, 0));
				else
					aClass6_617.method242(i_48_ * 37 + i, i_42_ - i_49_ * 37, 37, 37, pixmap.method247(192, 128, 0));
			}
		}
		for (int i_50_ = 0; i_50_ < 8; i_50_++) {
			for (int i_51_ = 0; i_51_ < 8; i_51_++) {
				int i_52_ = anIntArrayArray649[i_50_][i_51_];
				if (anInt640 == 1)
					i_52_ = anIntArrayArray649[7 - i_50_][7 - i_51_];
				if (i_52_ != 0)
					aClass6_617.method255(i_50_ * 37 + i, i_42_ - i_51_ * 37, i_52_ + anInt621 + 1);
			}
		}
	}

	public void method124() {
		int i = 108;
		int i_53_ = 263;
		int i_54_ = ((anInt664 * 37 * anInt667 + anInt662 * 37 * (anInt668 - anInt667)) / anInt668);
		int i_55_ = ((anInt665 * 37 * anInt667 + anInt663 * 37 * (anInt668 - anInt667)) / anInt668);
		if (anInt640 == 1) {
			i_54_ = 259 - i_54_;
			i_55_ = 259 - i_55_;
		}
		aClass6_617.method255(i_54_ + i, i_53_ - i_55_, anInt621 + 1 + anInt666);
	}

	public void method125() {
		int i = ((anInt664 * 96 * anInt667 + anInt662 * 96 * (anInt668 - anInt667)) / anInt668);
		int i_56_ = ((anInt665 * 96 * anInt667 + anInt663 * 96 * (anInt668 - anInt667)) / anInt668);
		int i_57_ = anInt666;
		if (i_57_ != 0) {
			object3d class7 = aClass7ArrayArray624[i_57_ - 1][anIntArray626[i_57_ - 1]];
			object3d class7_58_ = aClass7ArrayArray625[i_57_ - 1][anIntArray626[i_57_ - 1]];
			anIntArray626[i_57_ - 1]++;
			aClass10_619.addObject(class7);
			class7.requestSetTranslate(i - 336, 0, i_56_ - 336);
			aClass10_620.addObject(class7_58_);
			class7_58_.requestSetTranslate(i - 336, 0, i_56_ - 336);
		}
	}

	public void method126() {
		if (aBoolean142)
			anApplet_Sub1_Sub1_Sub1_616.xmPlayer.method85(0, 2, 8000, 63);
		anIntArrayArray649[anInt664][anInt665] = anInt666;
		aBoolean661 = false;
		anInt651 = 0;
		anInt655 = -1;
		anApplet_Sub1_Sub1_Sub1_616.aBoolean604 = false;
	}

	public void method127(int i, int i_59_, int i_60_, int i_61_, boolean bool) {
		aBoolean661 = true;
		anInt662 = i;
		anInt663 = i_59_;
		anInt664 = i_60_;
		anInt665 = i_61_;
		anInt666 = anIntArrayArray649[i][i_59_];
		anInt667 = 0;
		int i_62_ = i_60_ - i;
		int i_63_ = i_61_ - i_59_;
		anInt668 = (int) (Math.sqrt((double) (i_62_ * i_62_ + i_63_ * i_63_)) * 24.0);
		anIntArrayArray649[i][i_59_] = 0;
		if (bool) {
			aBoolean641 = true;
			anApplet_Sub1_Sub1_Sub1_616.aClass2_582.putPacketId(255);
			anApplet_Sub1_Sub1_Sub1_616.aClass2_582.p1(i);
			anApplet_Sub1_Sub1_Sub1_616.aClass2_582.p1(i_59_);
			anApplet_Sub1_Sub1_Sub1_616.aClass2_582.p1(i_60_);
			anApplet_Sub1_Sub1_Sub1_616.aClass2_582.p1(i_61_);
			anApplet_Sub1_Sub1_Sub1_616.aClass2_582.flushNoException();
		}
	}

	public void method128() {
		if ((anInt639 / 12 & 0x1) == 0 || aBoolean661 || aBoolean641 || anInt637 != anInt640 || aBoolean648) {
			for (int i = 0; i < 8; i++) {
				for (int i_64_ = 0; i_64_ < 8; i_64_++) {
					int i_65_;
					if ((i & 0x1) == (i_64_ & 0x1))
						i_65_ = 20;
					else
						i_65_ = 10;
					aClass7_623.method283(i + i_64_ * 8, i_65_);
					aClass7_623.method284(i + i_64_ * 8, i_65_);
				}
			}
		} else {
			for (int i = 0; i < anInt651; i++) {
				int i_66_ = anIntArray652[i];
				int i_67_;
				if (anInt655 == -1)
					i_67_ = -31745;
				else
					i_67_ = -32385;
				aClass7_623.method283(i_66_, i_67_);
				aClass7_623.method284(i_66_, i_67_);
			}
		}
	}

	public void method129() {
		aClass10_619.clearObjects();
		aClass10_620.clearObjects();
		for (int i = 0; i < 12; i++)
			anIntArray626[i] = 0;
		for (int i = 0; i < 8; i++) {
			for (int i_68_ = 0; i_68_ < 8; i_68_++) {
				int i_69_ = anIntArrayArray649[i][i_68_];
				if (i_69_ != 0) {
					object3d class7 = (aClass7ArrayArray624[i_69_ - 1][anIntArray626[i_69_ - 1]]);
					object3d class7_70_ = (aClass7ArrayArray625[i_69_ - 1][anIntArray626[i_69_ - 1]]);
					anIntArray626[i_69_ - 1]++;
					aClass10_619.addObject(class7);
					class7.requestSetTranslate(i * 96 - 336, 0, i_68_ * 96 - 336);
					aClass10_620.addObject(class7_70_);
					class7_70_.requestSetTranslate(i * 96 - 336, 0, i_68_ * 96 - 336);
				}
			}
		}
	}

	public void method130() {
		int i = anInt653;
		int i_71_ = anInt654;
		anInt651 = 0;
		for (int i_72_ = 0; i_72_ < anInt656; i_72_++) {
			if (i == anIntArray657[i_72_] && i_71_ == anIntArray658[i_72_])
				anIntArray652[anInt651++] = anIntArray659[i_72_] + anIntArray660[i_72_] * 8;
		}
	}

	public void method131() {
		aBoolean643 = false;
		aBoolean642 = false;
		method132(anInt640);
		anInt651 = 0;
		for (int i = 0; i < anInt656; i++) {
			int i_73_ = anIntArray657[i] + anIntArray658[i] * 8;
			for (int i_74_ = 0; i_74_ < anInt651; i_74_++) {
				if (anIntArray652[i_74_] == i_73_)
					break;
				if (i_74_ == anInt651 - 1) {
					anIntArray652[anInt651++] = i_73_;
					break;
				}
			}
			if (anInt651 == 0)
				anIntArray652[anInt651++] = i_73_;
		}
	}

	public void method132(int i) {
		if (!aBoolean643)
			anInt656 = 0;
		int i_75_ = i * 6;
		for (int i_76_ = 0; i_76_ < 8; i_76_++) {
			for (int i_77_ = 0; i_77_ < 8; i_77_++) {
				if (anIntArrayArray649[i_76_][i_77_] != 0) {
					if (anIntArrayArray649[i_76_][i_77_] == 6 + i_75_) {
						if (i == 0) {
							if (method134(i_76_, i_77_ + 1))
								method136(i_76_, i_77_, i_76_, i_77_ + 1);
							if (i_77_ == 1 && method134(i_76_, i_77_ + 1) && method134(i_76_, i_77_ + 2))
								method136(i_76_, i_77_, i_76_, i_77_ + 2);
							if (method135(i_76_ - 1, i_77_ + 1, i))
								method136(i_76_, i_77_, i_76_ - 1, i_77_ + 1);
							if (method135(i_76_ + 1, i_77_ + 1, i))
								method136(i_76_, i_77_, i_76_ + 1, i_77_ + 1);
							if (anInt646 == i_76_ - 1 && anInt647 == i_77_ && method134(i_76_ - 1, i_77_ + 1) && method135(i_76_ - 1, i_77_, i))
								method136(i_76_, i_77_, i_76_ - 1, i_77_ + 1);
							if (anInt646 == i_76_ + 1 && anInt647 == i_77_ && method134(i_76_ + 1, i_77_ + 1) && method135(i_76_ + 1, i_77_, i))
								method136(i_76_, i_77_, i_76_ + 1, i_77_ + 1);
						} else if (i == 1) {
							if (method134(i_76_, i_77_ - 1))
								method136(i_76_, i_77_, i_76_, i_77_ - 1);
							if (i_77_ == 6 && method134(i_76_, i_77_ - 1) && method134(i_76_, i_77_ - 2))
								method136(i_76_, i_77_, i_76_, i_77_ - 2);
							if (method135(i_76_ - 1, i_77_ - 1, i))
								method136(i_76_, i_77_, i_76_ - 1, i_77_ - 1);
							if (method135(i_76_ + 1, i_77_ - 1, i))
								method136(i_76_, i_77_, i_76_ + 1, i_77_ - 1);
							if (anInt646 == i_76_ - 1 && anInt647 == i_77_ && method134(i_76_ - 1, i_77_ - 1) && method135(i_76_ - 1, i_77_, i))
								method136(i_76_, i_77_, i_76_ - 1, i_77_ - 1);
							if (anInt646 == i_76_ + 1 && anInt647 == i_77_ && method134(i_76_ + 1, i_77_ - 1) && method135(i_76_ + 1, i_77_, i))
								method136(i_76_, i_77_, i_76_ + 1, i_77_ - 1);
						}
					} else if (anIntArrayArray649[i_76_][i_77_] == 5 + i_75_ || (anIntArrayArray649[i_76_][i_77_] == 2 + i_75_)) {
						int i_78_ = i_76_ - 1;
						int i_79_;
						for (i_79_ = i_77_ - 1; method134(i_78_, i_79_); i_79_--) {
							method136(i_76_, i_77_, i_78_, i_79_);
							i_78_--;
						}
						if (method135(i_78_, i_79_, i))
							method136(i_76_, i_77_, i_78_, i_79_);
						i_78_ = i_76_ + 1;
						for (i_79_ = i_77_ - 1; method134(i_78_, i_79_); i_79_--) {
							method136(i_76_, i_77_, i_78_, i_79_);
							i_78_++;
						}
						if (method135(i_78_, i_79_, i))
							method136(i_76_, i_77_, i_78_, i_79_);
						i_78_ = i_76_ - 1;
						for (i_79_ = i_77_ + 1; method134(i_78_, i_79_); i_79_++) {
							method136(i_76_, i_77_, i_78_, i_79_);
							i_78_--;
						}
						if (method135(i_78_, i_79_, i))
							method136(i_76_, i_77_, i_78_, i_79_);
						i_78_ = i_76_ + 1;
						for (i_79_ = i_77_ + 1; method134(i_78_, i_79_); i_79_++) {
							method136(i_76_, i_77_, i_78_, i_79_);
							i_78_++;
						}
						if (method135(i_78_, i_79_, i))
							method136(i_76_, i_77_, i_78_, i_79_);
					} else if (anIntArrayArray649[i_76_][i_77_] == 4 + i_75_) {
						if (method133(i_76_ - 1, i_77_ + 2, i))
							method136(i_76_, i_77_, i_76_ - 1, i_77_ + 2);
						if (method133(i_76_ + 1, i_77_ + 2, i))
							method136(i_76_, i_77_, i_76_ + 1, i_77_ + 2);
						if (method133(i_76_ - 1, i_77_ - 2, i))
							method136(i_76_, i_77_, i_76_ - 1, i_77_ - 2);
						if (method133(i_76_ + 1, i_77_ - 2, i))
							method136(i_76_, i_77_, i_76_ + 1, i_77_ - 2);
						if (method133(i_76_ + 2, i_77_ - 1, i))
							method136(i_76_, i_77_, i_76_ + 2, i_77_ - 1);
						if (method133(i_76_ + 2, i_77_ + 1, i))
							method136(i_76_, i_77_, i_76_ + 2, i_77_ + 1);
						if (method133(i_76_ - 2, i_77_ - 1, i))
							method136(i_76_, i_77_, i_76_ - 2, i_77_ - 1);
						if (method133(i_76_ - 2, i_77_ + 1, i))
							method136(i_76_, i_77_, i_76_ - 2, i_77_ + 1);
					}
					if (anIntArrayArray649[i_76_][i_77_] == 3 + i_75_ || anIntArrayArray649[i_76_][i_77_] == 2 + i_75_) {
						int i_80_ = i_76_ - 1;
						int i_81_;
						for (i_81_ = i_77_; method134(i_80_, i_81_); i_80_--)
							method136(i_76_, i_77_, i_80_, i_81_);
						if (method135(i_80_, i_81_, i))
							method136(i_76_, i_77_, i_80_, i_81_);
						i_80_ = i_76_ + 1;
						for (i_81_ = i_77_; method134(i_80_, i_81_); i_80_++)
							method136(i_76_, i_77_, i_80_, i_81_);
						if (method135(i_80_, i_81_, i))
							method136(i_76_, i_77_, i_80_, i_81_);
						i_80_ = i_76_;
						for (i_81_ = i_77_ - 1; method134(i_80_, i_81_); i_81_--)
							method136(i_76_, i_77_, i_80_, i_81_);
						if (method135(i_80_, i_81_, i))
							method136(i_76_, i_77_, i_80_, i_81_);
						i_80_ = i_76_;
						for (i_81_ = i_77_ + 1; method134(i_80_, i_81_); i_81_++)
							method136(i_76_, i_77_, i_80_, i_81_);
						if (method135(i_80_, i_81_, i))
							method136(i_76_, i_77_, i_80_, i_81_);
					} else if (anIntArrayArray649[i_76_][i_77_] == 1 + i_75_) {
						if (method133(i_76_ - 1, i_77_ - 1, i))
							method136(i_76_, i_77_, i_76_ - 1, i_77_ - 1);
						if (method133(i_76_, i_77_ - 1, i))
							method136(i_76_, i_77_, i_76_, i_77_ - 1);
						if (method133(i_76_ + 1, i_77_ - 1, i))
							method136(i_76_, i_77_, i_76_ + 1, i_77_ - 1);
						if (method133(i_76_ - 1, i_77_, i))
							method136(i_76_, i_77_, i_76_ - 1, i_77_);
						if (method133(i_76_ + 1, i_77_, i))
							method136(i_76_, i_77_, i_76_ + 1, i_77_);
						if (method133(i_76_ - 1, i_77_ + 1, i))
							method136(i_76_, i_77_, i_76_ - 1, i_77_ + 1);
						if (method133(i_76_, i_77_ + 1, i))
							method136(i_76_, i_77_, i_76_, i_77_ + 1);
						if (method133(i_76_ + 1, i_77_ + 1, i))
							method136(i_76_, i_77_, i_76_ + 1, i_77_ + 1);
						if (aBoolean643 == false) {
							if (aBoolean644 == true && method134(i_76_ - 1, i_77_) && method134(i_76_ - 2, i_77_) && method134(i_76_ - 3, i_77_) && !method137(0, 0, 0, 0) && !method137(i_76_, i_77_, i_76_ - 1, i_77_))
								method136(i_76_, i_77_, i_76_ - 2, i_77_);
							if (aBoolean645 == true && method134(i_76_ + 1, i_77_) && method134(i_76_ + 2, i_77_) && !method137(0, 0, 0, 0) && !method137(i_76_, i_77_, i_76_ + 1, i_77_))
								method136(i_76_, i_77_, i_76_ + 2, i_77_);
						}
					}
				}
			}
		}
	}

	public boolean method133(int i, int i_82_, int i_83_) {
		if (method134(i, i_82_) || method135(i, i_82_, i_83_))
			return true;
		return false;
	}

	public boolean method134(int i, int i_84_) {
		if (i < 0 || i_84_ < 0 || i >= 8 || i_84_ >= 8)
			return false;
		if (anIntArrayArray649[i][i_84_] == 0)
			return true;
		return false;
	}

	public boolean method135(int i, int i_85_, int i_86_) {
		if (i < 0 || i_85_ < 0 || i >= 8 || i_85_ >= 8)
			return false;
		if (anIntArrayArray649[i][i_85_] == 0)
			return false;
		if ((anIntArrayArray649[i][i_85_] - 1) / 6 == i_86_)
			return false;
		return true;
	}

	public void method136(int i, int i_87_, int i_88_, int i_89_) {
		if (anIntArrayArray649[i_88_][i_89_] == 1 || anIntArrayArray649[i_88_][i_89_] == 7)
			aBoolean642 = true;
		if (aBoolean643 == false) {
			if (!method137(i, i_87_, i_88_, i_89_)) {
				anIntArray657[anInt656] = i;
				anIntArray658[anInt656] = i_87_;
				anIntArray659[anInt656] = i_88_;
				anIntArray660[anInt656++] = i_89_;
			}
		}
	}

	public boolean method137(int i, int i_90_, int i_91_, int i_92_) {
		int i_93_ = anIntArrayArray649[i][i_90_];
		int i_94_ = anIntArrayArray649[i_91_][i_92_];
		anIntArrayArray649[i][i_90_] = 0;
		anIntArrayArray649[i_91_][i_92_] = i_93_;
		aBoolean642 = false;
		aBoolean643 = true;
		method132(1 - anInt640);
		aBoolean643 = false;
		anIntArrayArray649[i][i_90_] = i_93_;
		anIntArrayArray649[i_91_][i_92_] = i_94_;
		return aBoolean642;
	}
}
