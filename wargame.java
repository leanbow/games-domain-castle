import jagex.object3d;
import jagex.pixmap;
import jagex.tools;
import jagex.world3d;

public class wargame extends module {
	gameclient anApplet_Sub1_Sub1_Sub1_844;
	pixmap aClass6_845;
	world3d aClass10_846;
	world3d aClass10_847;
	world3d aClass10_848;
	world3d aClass10_849;
	int anInt850;
	object3d aClass7_851;
	object3d aClass7_852;
	object3d aClass7_853;
	object3d[] aClass7Array854 = new object3d[5];
	int anInt855;
	int anInt856 = 896;
	int anInt857;
	int anInt858;
	int anInt859;
	int anInt860 = -1;
	int anInt861 = -1;
	boolean aBoolean862 = false;
	boolean aBoolean863 = false;
	boolean aBoolean864 = false;
	int anInt865 = 100;
	int[][] anIntArrayArray866 = new int[10][10];
	int[][] anIntArrayArray867 = new int[10][10];
	int[][] anIntArrayArray868 = new int[10][10];
	int[] anIntArray869 = new int[6];
	int anInt870 = -1;
	int anInt871 = -1;
	int anInt872;
	int anInt873;
	int anInt874 = -1;
	int anInt875 = -1;
	int anInt876 = -1;
	int anInt877 = -1;
	int anInt878;
	int anInt879;
	int anInt880;
	int timeLeft = 60;
	int anInt882;
	int anInt883;
	boolean aBoolean884 = false;
	boolean aBoolean885 = false;
	boolean aBoolean886 = true;
	int[][] anIntArrayArray887 = new int[9][9];
	int[][] anIntArrayArray888 = new int[9][9];
	int[][] anIntArrayArray889 = new int[9][9];
	int[] anIntArray890 = { 2, 2, 3, 3, 4, 5 };
	int[] anIntArray891 = { 2, 2, 3, 3, 4, 5 };

	public void onInit(gameclient applet_sub1_sub1_sub1, int i) {
		anApplet_Sub1_Sub1_Sub1_844 = applet_sub1_sub1_sub1;
		anInt850 = i;
		aClass6_845 = applet_sub1_sub1_sub1.pixelMap;
		aClass10_846 = applet_sub1_sub1_sub1.aClass10_1086;
		aClass10_847 = applet_sub1_sub1_sub1.aClass10_1084;
		aClass10_848 = applet_sub1_sub1_sub1.aClass10_1085;
		aClass10_849 = applet_sub1_sub1_sub1.aClass10_1087;
		aClass6_845.method249(applet_sub1_sub1_sub1.aByteArray1083, tools.findFile("2dwar2.tga", applet_sub1_sub1_sub1.aByteArray1083), i, true, 4, false);
		aClass6_845.method250(applet_sub1_sub1_sub1.aByteArray1083, tools.findFile("2dwar1.tga", applet_sub1_sub1_sub1.aByteArray1083), i + 10, true, 1, 4, false);
		for (int i_0_ = i + 10; i_0_ < i + 14; i_0_++)
			aClass6_845.method254(i_0_, i_0_ + 10);
		aClass7_851 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("board.ob2", applet_sub1_sub1_sub1.aByteArray1082));
		aClass7Array854[0] = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("2ship.ob2", applet_sub1_sub1_sub1.aByteArray1082));
		aClass7Array854[1] = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("3ship.ob2", applet_sub1_sub1_sub1.aByteArray1082));
		aClass7Array854[2] = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("4ship.ob2", applet_sub1_sub1_sub1.aByteArray1082));
		aClass7Array854[3] = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("5ship.ob2", applet_sub1_sub1_sub1.aByteArray1082));
		aClass7Array854[0].requestScale(300, 300, 300);
		aClass7Array854[1].requestScale(300, 300, 300);
		aClass7Array854[2].requestScale(320, 320, 320);
		aClass7Array854[3].requestScale(320, 320, 320);
		aClass7_853 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("bomb.ob2", applet_sub1_sub1_sub1.aByteArray1082));
		aClass7_852 = new object3d(100, 81);
		for (int i_1_ = 0; i_1_ < 10; i_1_++) {
			for (int i_2_ = 0; i_2_ < 10; i_2_++)
				aClass7_852.addVertex(i_2_ * 85 - 382, 0, i_1_ * 85 - 382);
		}
		for (int i_3_ = 0; i_3_ < 9; i_3_++) {
			for (int i_4_ = 0; i_4_ < 9; i_4_++) {
				int[] is = new int[4];
				is[0] = i_3_ * 10 + i_4_;
				is[1] = i_3_ * 10 + i_4_ + 1;
				is[2] = i_3_ * 10 + i_4_ + 11;
				is[3] = i_3_ * 10 + i_4_ + 10;
				if ((i_4_ & 0x1) == (i_3_ & 0x1))
					aClass7_852.addPolygon(4, is, -26, -26);
				else
					aClass7_852.addPolygon(4, is, -20, -20);
			}
		}
		aClass7_852.method279(50);
		aClass7_852.method280(true, 32, 64, -50, -10, -50);
		aClass7_852.aBoolean332 = true;
		aClass7_852.aBoolean331 = true;
		aClass7_851.method280(true, 52, 32, -50, -10, -50);
		for (int i_5_ = 0; i_5_ < 10; i_5_++) {
			for (int i_6_ = 0; i_6_ < 10; i_6_++) {
				anIntArrayArray866[i_5_][i_6_] = (int) (Math.random() * 256.0);
				anIntArrayArray867[i_5_][i_6_] = (int) (Math.random() * 9.0) + 4;
				anIntArrayArray868[i_5_][i_6_] = (int) (Math.random() * 7.0) + 3;
			}
		}
	}

	public void method113(int i) {
		anInt883 = i;
		aClass10_846.clearObjects();
		aClass10_847.clearObjects();
		aClass10_848.clearObjects();
		aClass10_846.method362(256, 152, 256, 152, 512, 9);
		aClass10_846.maxRenderZ = 3000;
		aClass10_846.anInt464 = 3000;
		aClass10_846.anInt465 = 20;
		aClass10_846.anInt466 = 5000;
		aClass10_847.method385(aClass10_846);
		aClass10_848.method385(aClass10_846);
		aClass10_849.method385(aClass10_846);
		aClass10_846.addObject(aClass7_851);
		aClass10_846.addObject(aClass7_852);
		method163();
		aBoolean886 = true;
		aBoolean885 = false;
		anInt873 = 0;
		anInt879 = 0;
		anInt872 = 0;
		anInt865 = 100;
	}

	public void method121() {
		aClass10_846.clearObjects();
		aClass10_847.clearObjects();
		aClass10_848.clearObjects();
		aClass10_846.addObject(aClass7_851);
		aClass10_846.addObject(aClass7_852);
	}

	public void method117() {
		method166(1, 1, 1, 2);
		method166(2, 3, 0, 4);
		method166(7, 2, 1, 5);
		method166(3, 6, 0, 3);
	}

	public void method163() {
		for (int i = 0; i < 9; i++) {
			for (int i_7_ = 0; i_7_ < 9; i_7_++)
				anIntArrayArray889[i][i_7_] = anIntArrayArray888[i][i_7_] = anIntArrayArray887[i][i_7_] = -1;
		}
		for (int i = 0; i < 6; i++)
			anIntArray891[i] = anIntArray890[i];
	}

	public void method164() {
		for (int i = 0; i < 9; i++) {
			for (int i_8_ = 0; i_8_ < 9; i_8_++)
				anIntArrayArray887[i][i_8_] = -1;
		}
		for (int i = 0; i < 6; i++)
			anIntArray891[i] = anIntArray890[i];
	}

	public boolean placeShip(int i, int i_9_, int i_10_, int size) {
		if (i < 0 || i_9_ < 0)
			return false;
		int i_12_ = i;
		int i_13_ = i_9_;
		for (int i_14_ = 0; i_14_ < size; i_14_++) {
			if (i_12_ >= 9 || i_13_ >= 9 || anIntArrayArray887[i_12_][i_13_] != -1)
				return false;
			if (i_10_ == 0)
				i_12_++;
			else
				i_13_++;
		}
		int i_15_;
		for (i_15_ = 0; i_15_ < 6; i_15_++) {
			if (anIntArray891[i_15_] == size) {
				anIntArray891[i_15_] = 0;
				break;
			}
			if (i_15_ == 5)
				return false;
		}
		i_12_ = i;
		i_13_ = i_9_;
		for (int i_16_ = 0; i_16_ < size; i_16_++) {
			anIntArrayArray887[i_12_][i_13_] = i_15_ + 64;
			if (i_10_ == 0)
				i_12_++;
			else
				i_13_++;
		}
		anIntArrayArray887[i][i_9_] = i_15_ + i_10_ * 32;
		anApplet_Sub1_Sub1_Sub1_844.aClass2_582.putPacketId(255);
		anApplet_Sub1_Sub1_Sub1_844.aClass2_582.p2(size);
		anApplet_Sub1_Sub1_Sub1_844.aClass2_582.p2(i);
		anApplet_Sub1_Sub1_Sub1_844.aClass2_582.p2(i_9_);
		anApplet_Sub1_Sub1_Sub1_844.aClass2_582.p2(i_10_);
		anApplet_Sub1_Sub1_Sub1_844.aClass2_582.flushNoException();
		return true;
	}

	public void method166(int i, int i_17_, int i_18_, int i_19_) {
		int i_20_;
		for (i_20_ = 0; i_20_ < 6; i_20_++) {
			if (anIntArray891[i_20_] == i_19_) {
				anIntArray891[i_20_] = 0;
				break;
			}
			if (i_20_ == 5)
				return;
		}
		int i_21_ = i;
		int i_22_ = i_17_;
		for (int i_23_ = 0; i_23_ < i_19_; i_23_++) {
			anIntArrayArray887[i_21_][i_22_] = i_20_ + 64;
			if (i_18_ == 0)
				i_21_++;
			else
				i_22_++;
		}
		anIntArrayArray887[i][i_17_] = i_20_ + i_18_ * 32;
	}

	public void method167(int i, int i_24_) {
		if (i >= 0 && i_24_ >= 0 && i < 9 && i_24_ < 9 && anIntArrayArray887[i][i_24_] != -1) {
			int i_25_ = anIntArrayArray887[i][i_24_] & 0xf;
			anIntArray891[i_25_] = anIntArray890[i_25_];
			for (int i_26_ = 0; i_26_ < 9; i_26_++) {
				for (int i_27_ = 0; i_27_ < 9; i_27_++) {
					if ((anIntArrayArray887[i_26_][i_27_] & 0xf) == i_25_)
						anIntArrayArray887[i_26_][i_27_] = -1;
				}
			}
			anApplet_Sub1_Sub1_Sub1_844.aClass2_582.putPacketId(254);
			anApplet_Sub1_Sub1_Sub1_844.aClass2_582.p2(i_25_);
			anApplet_Sub1_Sub1_Sub1_844.aClass2_582.flushNoException();
		}
	}

	public boolean method114() {
		if (anInt872 > 0)
			return false;
		if (anInt873 > 0)
			return false;
		return true;
	}

	public void method103(int i, int i_28_, byte[] is) {
		if (i == 255) {
			aBoolean885 = false;
			aBoolean886 = true;
			timeLeft = is[1] & 0xff;
			anInt865 = 100;
		} else if (i == 254) {
			method163();
			int i_29_ = 1;
			for (int i_30_ = 0; i_30_ < 6; i_30_++) {
				int i_31_ = is[i_29_++];
				int i_32_ = is[i_29_++];
				int i_33_ = is[i_29_++];
				int i_34_ = is[i_29_++];
				placeShip(i_31_, i_32_, i_34_, anIntArray890[i_33_]);
			}
			aBoolean885 = false;
		} else if (i == 253) {
			aBoolean886 = false;
			timeLeft = is[1] & 0xff;
			anInt880 = is[2];
			anInt865 = 100;
			if (anInt880 != anInt883)
				aBoolean885 = false;
		} else if (i == 252) {
			int i_35_ = is[1];
			int i_36_ = is[2];
			int i_37_ = is[3];
			int i_38_ = is[4];
			if (anInt870 != i_35_ + i_36_ * 9 || anInt871 != i_37_) {
				anInt871 = i_37_;
				anInt872 = 80;
				anInt870 = i_35_ + i_36_ * 9;
			}
			anInt874 = i_35_;
			anInt875 = i_36_;
			anInt876 = i_38_;
			anInt877 = i_37_;
			anInt873 = 50;
			anApplet_Sub1_Sub1_Sub1_844.aBoolean604 = false;
		} else if (i == 251)
			anInt873 = 100;
		else if (i == 250) {
			anInt878 = is[1];
			anInt879 = 100;
			if (anInt878 < 0 || anInt878 >= 6)
				anInt878 = 0;
			anInt873 = 125;
			anApplet_Sub1_Sub1_Sub1_844.aBoolean604 = false;
		} else if (i == 249) {
			method164();
			int i_39_ = 1;
			for (int i_40_ = 0; i_40_ < 6; i_40_++) {
				int i_41_ = is[i_39_++];
				int i_42_ = is[i_39_++];
				int i_43_ = is[i_39_++];
				int i_44_ = is[i_39_++];
				placeShip(i_41_, i_42_, i_44_, anIntArray890[i_43_]);
			}
			anInt880 = is[i_39_++];
			timeLeft = is[i_39_++] & 0xff;
			anInt865 = 100;
			int i_45_ = is[i_39_++];
			if (i_45_ == 1)
				aBoolean886 = true;
			else
				aBoolean886 = false;
			for (int i_46_ = 0; i_46_ < 9; i_46_++) {
				for (int i_47_ = 0; i_47_ < 9; i_47_++) {
					int i_48_ = is[i_39_++];
					if (i_48_ == -1 && anIntArrayArray887[i_46_][i_47_] != -1)
						anIntArrayArray889[i_46_][i_47_] = 1;
				}
			}
			aBoolean885 = false;
		}
	}

	public void onLoop() {
		anInt882++;
		if (anInt872 > 0)
			anInt872--;
		if (anInt872 <= 0 && anInt874 != -1) {
			if (anInt877 == anInt883 && anIntArrayArray889[anInt874][anInt875] == -1)
				anIntArrayArray889[anInt874][anInt875] = anInt876;
			else if (anInt877 != anInt883 && anIntArrayArray888[anInt874][anInt875] == -1)
				anIntArrayArray888[anInt874][anInt875] = anInt876;
			anInt874 = -1;
			if (anInt876 == 1 && aBoolean142)
				anApplet_Sub1_Sub1_Sub1_844.xmPlayer.method85(0, 5, 8000, 63);
		} else if (anInt872 <= 0 && anInt873 > 0) {
			anInt873--;
			if (anInt873 == 0)
				aBoolean886 = false;
		}
		if (anInt879 > 0)
			anInt879--;
		if (aBoolean863) {
			if (anApplet_Sub1_Sub1_Sub1_844.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_844.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_844.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_844.mouseY < 188 && anApplet_Sub1_Sub1_Sub1_844.mouseClick == 1) {
				anApplet_Sub1_Sub1_Sub1_844.resign();
				aBoolean863 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_844.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_844.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_844.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_844.mouseY < 188 && anApplet_Sub1_Sub1_Sub1_844.mouseClick == 1)
				aBoolean863 = false;
			anApplet_Sub1_Sub1_Sub1_844.mouseClick = 0;
			anApplet_Sub1_Sub1_Sub1_844.mouseDown = 0;
		} else if (aBoolean864) {
			if (anApplet_Sub1_Sub1_Sub1_844.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_844.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_844.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_844.mouseY < 188 && anApplet_Sub1_Sub1_Sub1_844.mouseClick == 1) {
				anApplet_Sub1_Sub1_Sub1_844.offerDraw();
				aBoolean864 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_844.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_844.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_844.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_844.mouseY < 188 && anApplet_Sub1_Sub1_Sub1_844.mouseClick == 1)
				aBoolean864 = false;
			anApplet_Sub1_Sub1_Sub1_844.mouseClick = 0;
			anApplet_Sub1_Sub1_Sub1_844.mouseDown = 0;
		} else {
			int i = 460;
			int i_49_ = 4;
			if (anApplet_Sub1_Sub1_Sub1_844.mouseClick == 1 && anApplet_Sub1_Sub1_Sub1_844.mouseX > i && anApplet_Sub1_Sub1_Sub1_844.mouseY > i_49_ && anApplet_Sub1_Sub1_Sub1_844.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_844.mouseY < i_49_ + 29) {
				aBoolean862 = !aBoolean862;
				anApplet_Sub1_Sub1_Sub1_844.mouseClick = 0;
				anApplet_Sub1_Sub1_Sub1_844.mouseDown = 0;
			}
			i = 460;
			i_49_ = 34;
			if (anApplet_Sub1_Sub1_Sub1_844.mouseClick == 1 && anApplet_Sub1_Sub1_Sub1_844.mouseX > i && anApplet_Sub1_Sub1_Sub1_844.mouseY > i_49_ && anApplet_Sub1_Sub1_Sub1_844.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_844.mouseY < i_49_ + 29) {
				aBoolean863 = true;
				anApplet_Sub1_Sub1_Sub1_844.mouseClick = 0;
				anApplet_Sub1_Sub1_Sub1_844.mouseDown = 0;
			}
			i = 460;
			i_49_ = 64;
			if (anApplet_Sub1_Sub1_Sub1_844.mouseClick == 1 && anApplet_Sub1_Sub1_Sub1_844.mouseX > i && anApplet_Sub1_Sub1_Sub1_844.mouseY > i_49_ && anApplet_Sub1_Sub1_Sub1_844.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_844.mouseY < i_49_ + 29) {
				aBoolean864 = true;
				anApplet_Sub1_Sub1_Sub1_844.mouseClick = 0;
				anApplet_Sub1_Sub1_Sub1_844.mouseDown = 0;
			}
			if (aBoolean886 && anApplet_Sub1_Sub1_Sub1_844.mouseClick == 1 && anApplet_Sub1_Sub1_Sub1_844.mouseX > 177 && anApplet_Sub1_Sub1_Sub1_844.mouseY > 11 && anApplet_Sub1_Sub1_Sub1_844.mouseX < 377 && anApplet_Sub1_Sub1_Sub1_844.mouseY < 27) {
				method163();
				anApplet_Sub1_Sub1_Sub1_844.aClass2_582.putPacketId(253);
				anApplet_Sub1_Sub1_Sub1_844.aClass2_582.flushNoException();
			} else if (!aBoolean886 && anInt880 == anInt883 && anApplet_Sub1_Sub1_Sub1_844.mouseClick == 1 && anInt860 != -1 && anInt872 == 0 && anInt873 == 0 && !aBoolean885) {
				aBoolean885 = true;
				anInt870 = anInt860;
				anInt872 = 80;
				anInt871 = 1 - anInt880;
				anApplet_Sub1_Sub1_Sub1_844.aClass2_582.putPacketId(252);
				anApplet_Sub1_Sub1_Sub1_844.aClass2_582.p1(anInt860);
				anApplet_Sub1_Sub1_Sub1_844.aClass2_582.flushNoException();
			} else if (aBoolean886 && anApplet_Sub1_Sub1_Sub1_844.mouseDown == 1) {
				if (anInt859 != 1) {
					anInt859 = anApplet_Sub1_Sub1_Sub1_844.mouseDown;
					anInt861 = anInt860;
				}
			} else if (anApplet_Sub1_Sub1_Sub1_844.mouseDown == 2 && !aBoolean862) {
				if (anInt859 != 2) {
					anInt859 = anApplet_Sub1_Sub1_Sub1_844.mouseDown;
					anInt857 = anApplet_Sub1_Sub1_Sub1_844.mouseX;
					anInt858 = anApplet_Sub1_Sub1_Sub1_844.mouseY;
				}
				int i_50_ = anApplet_Sub1_Sub1_Sub1_844.mouseX - anInt857;
				int i_51_ = anApplet_Sub1_Sub1_Sub1_844.mouseY - anInt858;
				anInt855 = anInt855 + i_50_ * 2 + 1024 & 0x3ff;
				anInt856 = anInt856 - i_51_ * 2 + 1024 & 0x3ff;
				if (anInt856 > 256 && anInt856 < 600)
					anInt856 = 600;
				if (anInt856 > 944 || anInt856 < 256)
					anInt856 = 944;
				anInt857 = anApplet_Sub1_Sub1_Sub1_844.mouseX;
				anInt858 = anApplet_Sub1_Sub1_Sub1_844.mouseY;
			} else {
				if (aBoolean886 && anInt861 != -1) {
					int i_53_ = anInt861 % 9;
					int i_54_ = anInt861 / 9;
					int i_55_ = anInt860 % 9;
					int i_56_ = anInt860 / 9;
					method167(i_53_, i_54_);
					int i_57_ = i_55_ - i_53_;
					if (i_57_ < 0)
						i_57_ = -i_57_;
					int i_58_ = i_56_ - i_54_;
					if (i_58_ < 0)
						i_58_ = -i_58_;
					int i_59_;
					int i_60_;
					if (i_57_ > i_58_) {
						i_59_ = i_57_ + 1;
						i_60_ = 0;
						if (i_55_ < i_53_)
							i_53_ = i_55_;
					} else {
						i_59_ = i_58_ + 1;
						i_60_ = 1;
						if (i_56_ < i_54_)
							i_54_ = i_56_;
					}
					placeShip(i_53_, i_54_, i_60_, i_59_);
				}
				anInt859 = 0;
				anInt861 = -1;
			}
			for (int i_61_ = 0; i_61_ < 10; i_61_++) {
				for (int i_62_ = 0; i_62_ < 10; i_62_++) {
					int i_63_ = (int) (Math.sin((double) (anIntArrayArray866[i_61_][i_62_]) / 80.0) * (double) anIntArrayArray867[i_61_][i_62_]);
					anIntArrayArray866[i_61_][i_62_] += anIntArrayArray868[i_61_][i_62_];
					aClass7_852.method282(i_61_ + i_62_ * 10, i_61_ * 85 - 382, i_63_ + 16, i_62_ * 85 - 382);
				}
			}
			anInt865--;
			if (anInt865 <= 0 && timeLeft > 10) {
				anInt865 = 50;
				timeLeft--;
			}
		}
	}

	public void method168(String string, String string_64_) {
		int i = 160;
		int i_65_ = 80;
		aClass6_845.method240(256 - i / 2 + 10, 150 - i_65_ / 2 + 10, i, i_65_, 0, 128);
		aClass6_845.method242(256 - i / 2, 150 - i_65_ / 2, i, i_65_, pixmap.method247(96, 96, 121));
		aClass6_845.method267(string, 256, 130, 4, 16777215);
		aClass6_845.method267(string_64_, 256, 150, 4, 16777215);
		int i_66_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_844.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_844.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_844.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_844.mouseY < 188)
			i_66_ = 16711680;
		aClass6_845.method267(anApplet_Sub1_Sub1_Sub1_844.generalGameModules[17], 226, 178, 4, i_66_);
		i_66_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_844.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_844.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_844.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_844.mouseY < 188)
			i_66_ = 16711680;
		aClass6_845.method267(anApplet_Sub1_Sub1_Sub1_844.generalGameModules[18], 286, 178, 4, i_66_);
	}

	public void method116(int i, int i_67_, int i_68_) {
		aClass10_847.clearObjects();
		aClass10_848.clearObjects();
		aClass10_849.clear();
		if (aBoolean886 || anInt880 != anInt883) {
			method171();
			method172();
		}
		method170();
		if (anInt872 > 0) {
			int i_69_ = anInt870 % 9;
			int i_70_ = anInt870 / 9;
			object3d class7 = aClass7_853.copy();
			aClass10_847.addObject(class7);
			class7.requestAdjustTranslate(i_69_ * 85 - 340, -anInt872 * 5 + 16, i_70_ * 85 - 340);
		}
		aClass10_848.setCamera(0, 100, 0, i, i_67_, 0, i_68_);
		aClass10_848.method367();
		aClass10_846.setCamera(0, 100, 0, i, i_67_, 0, i_68_);
		aClass10_846.method367();
		aClass10_847.setCamera(0, 100, 0, i, i_67_, 0, i_68_);
		aClass10_847.method367();
		aClass10_849.setCamera(0, 100, 0, i, i_67_, 0, i_68_);
		aClass10_849.method367();
	}

	public void onDraw() {
		aClass6_845.method241(0, 0, 512, 384, 0, 4210752);
		if (aBoolean862) {
			aClass6_845.method242(105, 9, 303, 295, pixmap.method247(93, 52, 27));
			if (anApplet_Sub1_Sub1_Sub1_844.mouseX > 117 && anApplet_Sub1_Sub1_Sub1_844.mouseY > 21 && anApplet_Sub1_Sub1_Sub1_844.mouseX < 396 && anApplet_Sub1_Sub1_Sub1_844.mouseY < 300)
				anInt860 = ((anApplet_Sub1_Sub1_Sub1_844.mouseX - 117) / 31 + ((8 - (anApplet_Sub1_Sub1_Sub1_844.mouseY - 21) / 31) * 9));
			else
				anInt860 = -1;
			method170();
			method169();
			if (aBoolean886 || anInt880 != anInt883)
				method172();
			if (anInt872 > 0) {
				int i = anInt870 % 9;
				int i_71_ = anInt870 / 9;
				int i_72_ = i * 31 + 117;
				int i_73_ = 248 - i_71_ * 31 + 21;
				aClass6_845.method255(i_72_, i_73_ - anInt872 * 2, anInt850 + 3);
			}
		} else {
			int i = 1200;
			aClass10_846.method358(anApplet_Sub1_Sub1_Sub1_844.mouseX, anApplet_Sub1_Sub1_Sub1_844.mouseY, 50, 50);
			method116(anInt856, anInt855, i);
			anInt860 = aClass10_846.method360();
			object3d class7 = aClass10_846.method361();
			if (class7 != aClass7_852)
				anInt860 = -1;
		}
		if (anInt879 > 0) {
			aClass6_845.method240(186, 130, 160, 60, 0, 128);
			aClass6_845.method242(176, 120, 160, 60, pixmap.method247(96, 96, 121));
			int i = anIntArray890[anInt878];
			if (i >= 0) {
				int i_74_ = i * 31;
				aClass6_845.method255(256 - i_74_ / 2, 145, anInt850 + 10 + i - 2);
				aClass6_845.method267(((anApplet_Sub1_Sub1_Sub1_844.warshipGameModules[0]) + " " + i + " " + (anApplet_Sub1_Sub1_Sub1_844.warshipGameModules[1])), 256, 135, 1, 16777215);
			}
		}
		if (aBoolean886) {
			aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.warshipGameModules[2]), 7, 12, 1, 16777215);
			String string = "";
			for (int i = 0; i < 6; i++) {
				if (anIntArray891[i] > 0) {
					if (string.length() > 0)
						string += " - ";
					string += anIntArray891[i];
				}
			}
			if (string.length() == 0)
				aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.warshipGameModules[3]), 7, 23, 1, 16777215);
			else
				aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.warshipGameModules[4]) + string, 7, 23, 1, 16777215);
			int i = 16777215;
			if (anApplet_Sub1_Sub1_Sub1_844.mouseX > 177 && anApplet_Sub1_Sub1_Sub1_844.mouseY > 11 && anApplet_Sub1_Sub1_Sub1_844.mouseX < 377 && anApplet_Sub1_Sub1_Sub1_844.mouseY < 27)
				i = 16711680;
			aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.warshipGameModules[5]), 177, 23, 1, i);
			aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.generalGameModules[3]) + timeLeft, 7, 45, 1, 16777215);
		} else {
			if (aBoolean884 || aBoolean885 || anInt873 > 0 || anInt872 > 0)
				aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.generalGameModules[0]), 7, 12, 1, 16777215);
			else if (anInt880 == anInt883)
				aClass6_845.method268(((anApplet_Sub1_Sub1_Sub1_844.generalGameModules[1]) + " - " + (anApplet_Sub1_Sub1_Sub1_844.warshipGameModules[6])), 7, 12, 1, 16777215);
			else
				aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.generalGameModules[2]), 7, 12, 1, 16777215);
			if (anInt880 == anInt883)
				aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.warshipGameModules[7]), 7, 23, 1, 16777215);
			else
				aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.warshipGameModules[8]), 7, 23, 1, 16777215);
			aClass6_845.method268((anApplet_Sub1_Sub1_Sub1_844.generalGameModules[3]) + timeLeft, 7, 34, 1, 16777215);
		}
		int i = 460;
		int i_75_ = 4;
		anApplet_Sub1_Sub1_Sub1_844.aClass8_1094.method321(i, i_75_, 46, 28);
		aClass6_845.method267(anApplet_Sub1_Sub1_Sub1_844.generalGameModules[4], i + 22, i_75_ + 13, 1, 0);
		if (aBoolean862)
			aClass6_845.method267((anApplet_Sub1_Sub1_Sub1_844.generalGameModules[5]), i + 22, i_75_ + 23, 1, 0);
		else
			aClass6_845.method267((anApplet_Sub1_Sub1_Sub1_844.generalGameModules[6]), i + 22, i_75_ + 23, 1, 0);
		i = 460;
		i_75_ = 34;
		anApplet_Sub1_Sub1_Sub1_844.aClass8_1094.method321(i, i_75_, 46, 28);
		aClass6_845.method267(anApplet_Sub1_Sub1_Sub1_844.generalGameModules[7], i + 22, i_75_ + 18, 1, 0);
		i = 460;
		i_75_ = 64;
		anApplet_Sub1_Sub1_Sub1_844.aClass8_1094.method321(i, i_75_, 46, 28);
		if (anApplet_Sub1_Sub1_Sub1_844.aBoolean604)
			aClass6_845.method267((anApplet_Sub1_Sub1_Sub1_844.generalGameModules[8]), i + 22, i_75_ + 13, 1, 0);
		else
			aClass6_845.method267((anApplet_Sub1_Sub1_Sub1_844.generalGameModules[9]), i + 22, i_75_ + 13, 1, 0);
		aClass6_845.method267(anApplet_Sub1_Sub1_Sub1_844.generalGameModules[10], i + 22, i_75_ + 23, 1, 0);
		if (aBoolean863)
			method168(anApplet_Sub1_Sub1_Sub1_844.generalGameModules[11], anApplet_Sub1_Sub1_Sub1_844.generalGameModules[12]);
		if (aBoolean864) {
			if (anApplet_Sub1_Sub1_Sub1_844.aBoolean604)
				method168(anApplet_Sub1_Sub1_Sub1_844.generalGameModules[13], anApplet_Sub1_Sub1_Sub1_844.generalGameModules[14]);
			else
				method168(anApplet_Sub1_Sub1_Sub1_844.generalGameModules[15], anApplet_Sub1_Sub1_Sub1_844.generalGameModules[16]);
		}
	}

	public void method169() {
		if (aBoolean886 || anInt880 != anInt883) {
			for (int i = 0; i < 9; i++) {
				for (int i_76_ = 0; i_76_ < 9; i_76_++) {
					if (anIntArrayArray887[i][i_76_] >= 0 && anIntArrayArray887[i][i_76_] < 64) {
						int i_77_ = i * 31 + 117;
						int i_78_ = 248 - i_76_ * 31 + 21;
						int i_79_ = anIntArrayArray887[i][i_76_] & 0x1f;
						int i_80_ = anIntArray890[i_79_];
						int i_81_ = anIntArrayArray887[i][i_76_] / 32;
						if (i_81_ == 0)
							aClass6_845.method255(i_77_, i_78_, anInt850 + 10 + i_80_ - 2);
						else
							aClass6_845.method255(i_77_, i_78_ - 124, anInt850 + 20 + i_80_ - 2);
					}
				}
			}
		}
	}

	public void method170() {
		int i = -24577;
		int i_82_;
		int i_83_;
		int i_84_;
		int i_85_;
		if (anInt861 == -1 || !aBoolean886) {
			i_82_ = anInt860 % 9;
			i_83_ = anInt860 / 9;
			i_84_ = anInt860 % 9;
			i_85_ = anInt860 / 9;
			i = -24961;
		} else {
			i_82_ = anInt861 % 9;
			i_83_ = anInt861 / 9;
			i_84_ = anInt860 % 9;
			i_85_ = anInt860 / 9;
			int i_86_ = i_84_ - i_82_;
			if (i_86_ < 0)
				i_86_ = -i_86_;
			int i_87_ = i_85_ - i_83_;
			if (i_87_ < 0)
				i_87_ = -i_87_;
			if (i_86_ > i_87_)
				i_85_ = i_83_;
			else
				i_84_ = i_82_;
			if (i_82_ > i_84_) {
				int i_88_ = i_82_;
				i_82_ = i_84_;
				i_84_ = i_88_;
			}
			if (i_83_ > i_85_) {
				int i_89_ = i_83_;
				i_83_ = i_85_;
				i_85_ = i_89_;
			}
		}
		if (anInt872 > 0) {
			int i_90_ = anInt870 % 9;
			int i_91_ = anInt870 / 9;
			i_82_ = i_90_;
			i_83_ = i_91_;
			i_84_ = i_90_;
			i_85_ = i_91_;
			i = -24577;
		}
		for (int i_92_ = 0; i_92_ < 9; i_92_++) {
			for (int i_93_ = 0; i_93_ < 9; i_93_++) {
				int i_94_ = i_92_ * 9 + i_93_;
				int i_95_;
				if ((i_93_ & 0x1) == (i_92_ & 0x1))
					i_95_ = -26;
				else
					i_95_ = -20;
				if (!aBoolean886 && anInt880 == anInt883 && anIntArrayArray888[i_93_][i_92_] == 0 && i_95_ == -26)
					i_95_ = 13;
				else if (!aBoolean886 && anInt880 == anInt883 && anIntArrayArray888[i_93_][i_92_] == 0 && i_95_ == -20)
					i_95_ = 23;
				else if (!aBoolean886 && anInt880 == anInt883 && anIntArrayArray888[i_93_][i_92_] == 1 && i_95_ == -26)
					i_95_ = 24;
				else if (!aBoolean886 && anInt880 == anInt883 && anIntArrayArray888[i_93_][i_92_] == 1 && i_95_ == -20)
					i_95_ = 25;
				if (i_93_ >= i_82_ && i_92_ >= i_83_ && i_93_ <= i_84_ && i_92_ <= i_85_ && (anInt872 > 0 || anInt873 == 0 && (anInt880 == anInt883 || aBoolean886)))
					i_95_ = i;
				if (!aBoolean862) {
					aClass7_852.method283(i_94_, i_95_);
					aClass7_852.method284(i_94_, i_95_);
				} else {
					int i_96_ = i_93_ * 31 + 117;
					int i_97_ = 248 - i_92_ * 31 + 21;
					int i_98_ = 0;
					if (i_95_ == -26 || i_95_ == 13 || i_95_ == 24)
						i_98_ = pixmap.method247(24, 24, 189);
					else if (i_95_ == -20 || i_95_ == 23 || i_95_ == 25)
						i_98_ = pixmap.method247(27, 30, 151);
					else if (i_95_ == -24577)
						i_98_ = pixmap.method247(192, 0, 0);
					else if (i_95_ == -24961)
						i_98_ = pixmap.method247(192, 96, 0);
					aClass6_845.method242(i_96_, i_97_, 31, 31, i_98_);
					if (i_95_ >= 0) {
						if (i_95_ == 13 || i_95_ == 23)
							i_95_ = pixmap.method247(208, 208, 208);
						else
							i_95_ = pixmap.method247(208, 0, 0);
						for (int i_99_ = 0; i_99_ < 31; i_99_++) {
							aClass6_845.method244(i_96_ + i_99_ - 1, i_97_ + i_99_, 3, i_95_);
							aClass6_845.method244(i_96_ + 29 - i_99_, i_97_ + i_99_, 3, i_95_);
						}
					}
				}
			}
		}
	}

	public void method171() {
		for (int i = 0; i < 9; i++) {
			for (int i_100_ = 0; i_100_ < 9; i_100_++) {
				if (anIntArrayArray887[i][i_100_] >= 0 && anIntArrayArray887[i][i_100_] < 64) {
					int i_101_ = anIntArrayArray887[i][i_100_] & 0x1f;
					int i_102_ = anIntArray890[i_101_];
					int i_103_ = anIntArrayArray887[i][i_100_] / 32;
					int i_104_ = i_103_;
					int i_105_ = 1;
					int i_106_ = 0;
					if (i_103_ == 1) {
						i_104_ = 1;
						i_105_ = i_103_;
					}
					for (int i_107_ = i; i_107_ <= i + i_104_; i_107_++) {
						for (int i_108_ = i_100_; i_108_ <= i_100_ + i_105_; i_108_++)
							i_106_ += (int) (Math.sin((double) (anIntArrayArray866[i][i_100_]) / 80.0) * (double) (anIntArrayArray867[i][i_100_]));
					}
					i_106_ /= (i_105_ + 1) * (i_104_ + 1);
					i_106_ /= 1.5;
					anIntArray869[i_101_] = i_106_;
					object3d class7 = aClass7Array854[i_102_ - 2].copy();
					if (i_103_ == 1)
						class7.requestSetRotate(0, 192, 0);
					aClass10_847.addObject(class7);
					class7.requestAdjustTranslate(i * 85 - 340, i_106_ + 16, i_100_ * 85 - 340);
					object3d class7_109_ = aClass7Array854[i_102_ - 2].copy();
					if (i_103_ == 1)
						class7_109_.requestSetRotate(128, 192, 0);
					else
						class7_109_.requestSetRotate(128, 0, 0);
					aClass10_848.addObject(class7_109_);
					class7_109_.requestAdjustTranslate(i * 85 - 340, i_106_ + 16, i_100_ * 85 - 340);
				}
			}
		}
	}

	public void method172() {
		for (int i = 0; i < 9; i++) {
			for (int i_110_ = 0; i_110_ < 9; i_110_++) {
				if (anIntArrayArray889[i][i_110_] == 1) {
					int i_111_ = anIntArrayArray887[i][i_110_];
					if (i_111_ < 0 || i_111_ >= 6)
						i_111_ = 0;
					if (!aBoolean862)
						aClass10_849.method355(anInt850 + anInt882 / 4 % 3, i * 85 - 340, anIntArray869[i_111_], i_110_ * 85 - 340, 64, 64);
					else {
						int i_112_ = i * 31 + 117;
						int i_113_ = 248 - i_110_ * 31 + 21;
						aClass6_845.method255(i_112_, i_113_, anInt850 + anInt882 / 4 % 3);
					}
				}
			}
		}
	}
}
