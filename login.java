import jagex.client;
import jagex.gui;
import jagex.pixmap;

public class login extends module {
	pixmap pixelMap;
	gameclient gameClient;
	int guiRenderId;
	String user = "";
	String pass = "";
	String nickname;
	boolean autoLogin = false;
	boolean loggingIn = false;
	gui titleScreenGui;
	gui newUserGui;
	gui existingUserGui;
	int newUserId;
	int existingUserId;
	int newUserInformationLine1Id;
	int newUserInformationLine2Id;
	int anInt911;
	int anInt912;
	int anInt913;
	int anInt914;
	int anInt915;
	int cancelBoxId;
	int submitBoxId;
	int usernameInputBoxId;
	int emailInputBoxId;
	int passwordInputBoxId;
	int informationLine1Id;
	int informationLine2Id;
	int loginUsernameInputBoxId;
	int loginPasswordInputBoxId;
	int okBoxId;
	int loginCancelBoxId;

	public void onInit(gameclient gameclient, int i) {
		loggingIn = false;
		gameClient = gameclient;
		pixelMap = gameclient.pixelMap;
		int maxCharacters = 12;
		titleScreenGui = new gui(pixelMap, 50);
		int yOffset = 200;
		titleScreenGui.createCenteredRectangle(256, yOffset, 130, 200);
		titleScreenGui.createText(256, yOffset, gameclient.loginModules[0], 5, true);
		yOffset += 40;
		titleScreenGui.createCenteredButton(166, yOffset - 80, 120, 35);
		titleScreenGui.createCenteredButton(346, yOffset, 120, 35);
		titleScreenGui.createText(166, yOffset - 80, gameclient.loginModules[1], 5, false);
		titleScreenGui.createText(346, yOffset, gameclient.loginModules[2], 5, false);
		newUserId = titleScreenGui.createCenteredClickableBox(156, yOffset - 80, 120, 35);
		existingUserId = titleScreenGui.createCenteredClickableBox(356, yOffset, 120, 35);
		
		yOffset = 35;
		int xOffset = 110;
		boolean bool = false;
		newUserGui = new gui(pixelMap, 50);
		yOffset += 80;
		newUserGui.createCenteredButton(256, yOffset + 17, 420, 34);
		newUserGui.createText(256, yOffset + 8, gameclient.loginModules[3], 4, bool);
		usernameInputBoxId = newUserGui.createInputBox(256, yOffset + 25, 200, 40, 4, maxCharacters, false, bool);
		yOffset += 40;
		newUserGui.createCenteredButton(256, yOffset + 17, 420, 34);
		newUserGui.createText(256, yOffset + 8, gameclient.loginModules[4], 4, bool);
		passwordInputBoxId = newUserGui.createInputBox(256, yOffset + 25, 200, 40, 4, maxCharacters, true, bool);
		yOffset += 40;
		newUserGui.createCenteredButton(256, yOffset + 17, 420, 34);
		newUserGui.createText(256, yOffset + 8, gameclient.loginModules[5], 4, bool);
		emailInputBoxId = newUserGui.createInputBox(256, yOffset + 25, 200, 40, 4, 40, false, bool);
		yOffset += 40;
		newUserGui.createCenteredButton(256 - xOffset + 50 - 15, yOffset + 17, 270, 34);
		newUserInformationLine1Id = newUserGui.createText(256 - xOffset + 50 - 15, yOffset + 8, gameclient.loginModules[6], 4, true);
		newUserInformationLine2Id = newUserGui.createText(256 - xOffset + 50 - 15, yOffset + 25, gameclient.loginModules[7], 4, true);
		newUserGui.createCenteredButton(356, yOffset + 17, 70, 34);
		newUserGui.createText(356, yOffset + 17, gameclient.loginModules[8], 5, bool);
		submitBoxId = newUserGui.createCenteredClickableBox(341, yOffset + 17, 100, 34);
		newUserGui.createCenteredButton(431, yOffset + 17, 70, 34);
		newUserGui.createText(431, yOffset + 17, gameclient.loginModules[9], 5, bool);
		cancelBoxId = newUserGui.createCenteredClickableBox(431, yOffset + 17, 100, 34);
		newUserGui.setInputComponentId(usernameInputBoxId);
		
		existingUserGui = new gui(pixelMap, 50);
		existingUserGui.createCenteredRectangle(256, 192, 280, 256);
		yOffset = 108;
		informationLine1Id = existingUserGui.createText(256, yOffset - 10, gameclient.loginModules[10], 5, true);
		informationLine2Id = existingUserGui.createText(256, yOffset + 10, gameclient.loginModules[11], 5, true);
		yOffset += 60;
		existingUserGui.createCenteredButton(256, yOffset, 200, 40);
		existingUserGui.createText(256, yOffset - 10, gameclient.loginModules[12], 4, bool);
		loginUsernameInputBoxId = existingUserGui.createInputBox(256, yOffset + 10, 200, 40, 4, maxCharacters, false, bool);
		yOffset += 60;
		existingUserGui.createCenteredButton(256, yOffset, 200, 40);
		existingUserGui.createText(256, yOffset - 10, gameclient.loginModules[13], 4, bool);
		loginPasswordInputBoxId = existingUserGui.createInputBox(256, yOffset + 10, 200, 40, 4, maxCharacters, true, bool);
		yOffset += 60;
		xOffset = 70;
		existingUserGui.createCenteredButton(256 - xOffset, yOffset, 120, 40);
		existingUserGui.createText(256 - xOffset, yOffset, gameclient.loginModules[14], 4, bool);
		okBoxId = existingUserGui.createCenteredClickableBox(256 - xOffset, yOffset, 120, 40);
		xOffset = -70;
		existingUserGui.createCenteredButton(256 - xOffset, yOffset, 120, 40);
		existingUserGui.createText(256 - xOffset, yOffset, gameclient.loginModules[9], 4, bool);
		loginCancelBoxId = existingUserGui.createCenteredClickableBox(256 - xOffset, yOffset, 120, 40);
		existingUserGui.setInputComponentId(loginUsernameInputBoxId);
	}

	public void onModuleChange() {
		guiRenderId = 0;
		user = "";
		pass = "";
		autoLogin = false;
		if (gameClient.isUsingApplet()) {
			nickname = gameClient.getParameter("nickname");
			user = gameClient.getParameter("user");
			pass = gameClient.getParameter("pass");
			if (user == null || pass == null || user.length() > 22 || pass.length() > 20) {
				user = "";
				pass = "";
			} else
				autoLogin = true;
		}
	}

	public void method99() {
		/* empty */
	}

	public void method103(int i, int i_3_, byte[] is) {
		/* empty */
	}

	public void onLoop() {
		if (autoLogin && !loggingIn) {
			gameClient.mainRedraw();
			gameClient.resetOldTimes();
			gameClient.createAccount(user, pass, "null", "null", "null", 0, 0, 0);
			if (nickname == null || nickname.length() == 0)
				gameClient.login(user, pass);
			else
				gameClient.login(user, pass, nickname);
			loggingIn = true;
		} else if (guiRenderId == 0) {
			titleScreenGui.process(gameClient.mouseX, gameClient.mouseY, gameClient.mouseClick, gameClient.mouseDown);
			if (titleScreenGui.isComponentClicked(newUserId)) {
				guiRenderId = 1;
				newUserGui.setText(newUserInformationLine1Id, gameClient.loginModules[15]);
				newUserGui.setText(newUserInformationLine2Id, gameClient.loginModules[16]);
			}
			if (titleScreenGui.isComponentClicked(existingUserId)) {
				guiRenderId = 2;
				existingUserGui.setText(informationLine1Id, gameClient.loginModules[10]);
				existingUserGui.setText(informationLine2Id, gameClient.loginModules[11]);
			}
		} else if (guiRenderId == 1) {
			newUserGui.process(gameClient.mouseX, gameClient.mouseY, gameClient.mouseClick, gameClient.mouseDown);
			if (newUserGui.isComponentClicked(usernameInputBoxId))
				newUserGui.setInputComponentId(passwordInputBoxId);
			if (newUserGui.isComponentClicked(passwordInputBoxId))
				newUserGui.setInputComponentId(emailInputBoxId);
			if (newUserGui.isComponentClicked(emailInputBoxId))
				newUserGui.setInputComponentId(anInt911);
			if (newUserGui.isComponentClicked(anInt911))
				newUserGui.setInputComponentId(anInt912);
			if (newUserGui.isComponentClicked(anInt912))
				newUserGui.setInputComponentId(anInt913);
			if (newUserGui.isComponentClicked(anInt913))
				newUserGui.setInputComponentId(usernameInputBoxId);
			if (newUserGui.isComponentClicked(cancelBoxId))
				guiRenderId = 0;
			if (newUserGui.isComponentClicked(submitBoxId)) {
				if ((newUserGui.getText(anInt911) != null && newUserGui.getText(anInt911).length() == 0) || (newUserGui.getText(anInt912) != null && newUserGui.getText(anInt912).length() == 0) || (newUserGui.getText(anInt913) != null && newUserGui.getText(anInt913).length() == 0) || (newUserGui.getText(usernameInputBoxId) != null && newUserGui.getText(usernameInputBoxId).length() == 0) || (newUserGui.getText(emailInputBoxId) != null && newUserGui.getText(emailInputBoxId).length() == 0) || newUserGui.getSelectedIndex(anInt915) == -1) {
					newUserGui.setText(newUserInformationLine1Id, gameClient.loginModules[17]);
					newUserGui.setText(newUserInformationLine2Id, gameClient.loginModules[18]);
				} else {
					newUserGui.setText(newUserInformationLine1Id, gameClient.loginModules[19]);
					newUserGui.setText(newUserInformationLine2Id, gameClient.loginModules[20]);
					gameClient.mainRedraw();
					gameClient.resetOldTimes();
					String string = newUserGui.getText(anInt911);
					String string_4_ = newUserGui.getText(anInt912);
					String string_5_ = newUserGui.getText(usernameInputBoxId);
					String string_6_ = newUserGui.getText(passwordInputBoxId);
					String string_7_ = newUserGui.getText(emailInputBoxId);
					int i = newUserGui.getSelectedIndex(anInt915);
					int i_8_ = newUserGui.getSelectedIndex(anInt914);
					int i_9_ = 0;
					String string_10_ = newUserGui.getText(anInt913);
					try {
						i_9_ = Integer.parseInt(string_10_);
					} catch (Exception exception) {
						/* empty */
					}
					gameClient.createAccount(string_5_, string_6_, string, string_4_, string_7_, i, i_9_, i_8_);
				}
			}
		} else if (guiRenderId == 2) {
			existingUserGui.process(gameClient.mouseX, gameClient.mouseY, gameClient.mouseClick, gameClient.mouseDown);
			if (existingUserGui.isComponentClicked(loginCancelBoxId))
				guiRenderId = 0;
			if (existingUserGui.isComponentClicked(loginUsernameInputBoxId))
				existingUserGui.setInputComponentId(loginPasswordInputBoxId);
			if (existingUserGui.isComponentClicked(loginPasswordInputBoxId) || existingUserGui.isComponentClicked(okBoxId)) {
				user = existingUserGui.getText(loginUsernameInputBoxId);
				pass = existingUserGui.getText(loginPasswordInputBoxId);
				gameClient.login(user, pass);
			}
		}
	}

	public void onDraw() {
		pixelMap.clear();
		if (autoLogin) {
			if (loggingIn) {
				pixelMap.method267(client.clientModules[15], 256, 184, 5, 16777215);
				pixelMap.method267(client.clientModules[12], 256, 200, 5, 16777215);
			} else
				pixelMap.method267(gameClient.loginModules[21], 256, 192, 5, 16777215);
		} else {
			if (guiRenderId == 0)
				titleScreenGui.draw();
			if (guiRenderId == 1)
				newUserGui.draw();
			if (guiRenderId == 2)
				existingUserGui.draw();
		}
	}

	public void onKeyDown(int key) {
		loggingIn = false;
		if (!autoLogin) {
			if (guiRenderId == 0)
				titleScreenGui.processInput(key);
			if (guiRenderId == 1)
				newUserGui.processInput(key);
			if (guiRenderId == 2)
				existingUserGui.processInput(key);
		}
	}

	public void setServerResponseText(String line1, String line2) {
		if (guiRenderId == 1) {
			newUserGui.setText(newUserInformationLine1Id, line1);
			newUserGui.setText(newUserInformationLine2Id, line2);
		}
		if (guiRenderId == 2) {
			existingUserGui.setText(informationLine1Id, line1);
			existingUserGui.setText(informationLine2Id, line2);
		}
		gameClient.mainRedraw();
		gameClient.resetOldTimes();
	}

	public void loginAfterCreation() {
		String username = newUserGui.getText(usernameInputBoxId);
		String password = newUserGui.getText(passwordInputBoxId);
		guiRenderId = 2;
		existingUserGui.setText(informationLine1Id, gameClient.loginModules[10]);
		existingUserGui.setText(informationLine2Id, gameClient.loginModules[11]);
		existingUserGui.setText(loginUsernameInputBoxId, username);
		existingUserGui.setText(loginPasswordInputBoxId, password);
		gameClient.mainRedraw();
		gameClient.resetOldTimes();
		gameClient.login(username, password);
	}
}
