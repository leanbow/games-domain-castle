import jagex.object3d;
import jagex.pixmap;
import jagex.tools;
import jagex.world3d;

public class gogame extends module {
	gameclient anApplet_Sub1_Sub1_Sub1_701;
	pixmap aClass6_702;
	world3d aClass10_703;
	world3d aClass10_704;
	int anInt705;
	object3d aClass7_706;
	object3d aClass7_707;
	object3d aClass7_708;
	object3d aClass7_709;
	object3d[] aClass7Array710 = new object3d[169];
	int anInt711;
	object3d[] aClass7Array712 = new object3d[169];
	int anInt713;
	int anInt714;
	int anInt715 = 896;
	int anInt716;
	int anInt717;
	int anInt718;
	int anInt719;
	boolean aBoolean720 = false;
	boolean aBoolean721 = false;
	boolean aBoolean722 = false;
	int anInt723 = -1;
	int anInt724 = -1;
	int anInt725;
	int anInt726 = 64;
	int anInt727 = 13;
	int anInt728 = anInt727 + 1;
	int anInt729;
	int anInt730;
	int anInt731 = 100;
	int anInt732;
	int anInt733 = 60;
	int anInt734;
	int anInt735;
	boolean aBoolean736 = true;
	int[][] anIntArrayArray737 = new int[anInt727][anInt727];
	int[][] anIntArrayArray738 = new int[anInt727][anInt727];
	int[][] anIntArrayArray739 = new int[anInt727][anInt727];
	int[] anIntArray740 = new int[anInt727 * anInt727];
	int[] anIntArray741 = new int[anInt727 * anInt727];
	int[] anIntArray742 = new int[1000];
	int[] anIntArray743 = new int[1000];
	int anInt744 = -1;
	int anInt745 = -1;
	int anInt746 = -1;

	public void onInit(gameclient applet_sub1_sub1_sub1, int i) {
		anApplet_Sub1_Sub1_Sub1_701 = applet_sub1_sub1_sub1;
		anInt705 = i;
		aClass6_702 = applet_sub1_sub1_sub1.pixelMap;
		aClass10_703 = applet_sub1_sub1_sub1.aClass10_1086;
		aClass10_704 = applet_sub1_sub1_sub1.aClass10_1084;
		aClass6_702.method249(applet_sub1_sub1_sub1.aByteArray1083, tools.findFile("2dgo.tga", (applet_sub1_sub1_sub1.aByteArray1083)), i, true, 3, false);
		aClass7_707 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("board.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_707.requestScale(256 * anInt727 / 13, 256, 256 * anInt727 / 13);
		aClass7_708 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("counter.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_708.requestScale(128, 160, 128);
		aClass7_706 = new object3d(anInt728 * anInt728, anInt727 * anInt727);
		for (int i_0_ = 0; i_0_ < anInt728; i_0_++) {
			for (int i_1_ = 0; i_1_ < anInt728; i_1_++)
				aClass7_706.addVertex(i_1_ * 59 - anInt727 * 59 / 2, 0, i_0_ * 59 - anInt727 * 59 / 2);
		}
		for (int i_2_ = 0; i_2_ < anInt727; i_2_++) {
			for (int i_3_ = 0; i_3_ < anInt727; i_3_++) {
				int[] is = new int[4];
				is[0] = i_2_ * anInt728 + i_3_;
				is[1] = i_2_ * anInt728 + i_3_ + 1;
				is[2] = i_2_ * anInt728 + i_3_ + anInt728 + 1;
				is[3] = i_2_ * anInt728 + i_3_ + anInt728;
				aClass7_706.addPolygon(4, is, 15, 15);
			}
		}
		aClass7_706.method279(50);
		aClass7_707.method280(true, 52, 32, -50, -10, -50);
		aClass7_706.method280(true, 52, 32, -50, -10, -50);
		aClass7_708.method280(true, 52, 32, -50, -10, -50);
		aClass7_709 = aClass7_708.copy();
		for (int i_4_ = 0; i_4_ < aClass7_709.polygonCount; i_4_++)
			aClass7_709.method283(i_4_, -5286);
		for (int i_5_ = 0; i_5_ < 169; i_5_++)
			aClass7Array710[i_5_] = aClass7_708.copy();
		for (int i_6_ = 0; i_6_ < 169; i_6_++)
			aClass7Array712[i_6_] = aClass7_709.copy();
	}

	public void method121() {
		aClass10_703.clearObjects();
		aClass10_704.clearObjects();
		aClass10_703.addObject(aClass7_707);
		aClass10_703.addObject(aClass7_706);
	}

	public void method113(int i) {
		anInt735 = i;
		anInt714 = 0;
		aClass10_703.clearObjects();
		aClass10_704.clearObjects();
		aClass10_703.method362(256, 152, 256, 152, 512, 9);
		aClass10_703.maxRenderZ = 3000;
		aClass10_703.anInt464 = 3000;
		aClass10_703.anInt465 = 20;
		aClass10_703.anInt466 = 5000;
		aClass10_704.method385(aClass10_703);
		aClass10_703.clearObjects();
		aClass10_704.clearObjects();
		aClass10_703.addObject(aClass7_707);
		aClass10_703.addObject(aClass7_706);
		for (int i_7_ = 0; i_7_ < anInt727; i_7_++) {
			for (int i_8_ = 0; i_8_ < anInt727; i_8_++)
				anIntArrayArray738[i_7_][i_8_] = anIntArrayArray737[i_7_][i_8_] = 0;
		}
		aBoolean721 = false;
		aBoolean736 = true;
		anInt725 = 0;
		aBoolean722 = false;
		anInt744 = -1;
		anInt745 = -1;
		anInt746 = -1;
		anInt729 = 0;
		anInt730 = 0;
		anInt731 = 100;
	}

	public void method117() {
		anIntArrayArray737[5][5] = 1;
		anIntArrayArray737[5][6] = 1;
		anIntArrayArray737[5][7] = 1;
		anIntArrayArray737[6][5] = 1;
		anIntArrayArray737[7][5] = 1;
		anIntArrayArray737[4][5] = 2;
		anIntArrayArray737[4][6] = 2;
		anIntArrayArray737[4][7] = 2;
		anIntArrayArray737[4][8] = 2;
	}

	public boolean method114() {
		if (aBoolean721)
			return false;
		return true;
	}

	public void method103(int i, int i_9_, byte[] is) {
		if (i == 255) {
			anInt732 = is[1] & 0xff;
			anInt733 = is[2] & 0xff;
			anInt731 = 100;
			if (anInt732 != anInt735)
				aBoolean736 = false;
		}
		if (i == 254) {
			int i_10_ = 1;
			for (int i_11_ = 0; i_11_ < anInt727; i_11_++) {
				for (int i_12_ = 0; i_12_ < anInt727; i_12_++)
					anIntArrayArray738[i_11_][i_12_] = anIntArrayArray737[i_11_][i_12_] = is[i_10_++];
			}
			anInt732 = is[i_10_++] & 0xff;
			anInt733 = is[i_10_++] & 0xff;
			anInt731 = 100;
			aBoolean736 = false;
		}
		if (i == 253) {
			int i_13_ = is[1];
			int i_14_ = is[2];
			int i_15_ = is[3];
			anInt744 = i_13_;
			anInt745 = i_14_;
			anInt746 = i_15_ - 1;
			anIntArrayArray737[i_13_][i_14_] = i_15_;
			anIntArrayArray738[i_13_][i_14_] = i_15_;
			if (aBoolean142 && anInt746 != anInt735)
				anApplet_Sub1_Sub1_Sub1_701.xmPlayer.method85(0, 0, 8000, 63);
			for (int i_16_ = 0; i_16_ < anInt727; i_16_++) {
				for (int i_17_ = 0; i_17_ < anInt727; i_17_++)
					anIntArrayArray737[i_16_][i_17_] = anIntArrayArray738[i_16_][i_17_];
			}
			method145();
		}
	}

	public void onLoop() {
		anInt734++;
		if (anApplet_Sub1_Sub1_Sub1_701.mouseDown == 2 && !aBoolean720) {
			if (anInt718 == 0) {
				anInt718 = anApplet_Sub1_Sub1_Sub1_701.mouseDown;
				anInt716 = anApplet_Sub1_Sub1_Sub1_701.mouseX;
				anInt717 = anApplet_Sub1_Sub1_Sub1_701.mouseY;
			}
			int i = anApplet_Sub1_Sub1_Sub1_701.mouseX - anInt716;
			int i_18_ = anApplet_Sub1_Sub1_Sub1_701.mouseY - anInt717;
			anInt714 = anInt714 + i * 2 + 1024 & 0x3ff;
			anInt715 = anInt715 - i_18_ * 2 + 1024 & 0x3ff;
			if (anInt715 > 256 && anInt715 < 600)
				anInt715 = 600;
			if (anInt715 > 960 || anInt715 < 256)
				anInt715 = 960;
			anInt716 = anApplet_Sub1_Sub1_Sub1_701.mouseX;
			anInt717 = anApplet_Sub1_Sub1_Sub1_701.mouseY;
		} else
			anInt718 = 0;
		if (anApplet_Sub1_Sub1_Sub1_701.mouseClick == 1)
			anInt719 = 1;
		anInt731--;
		if (anInt731 <= 0 && anInt733 > 10) {
			anInt731 = 50;
			anInt733--;
		}
	}

	public void method116(int i, int i_19_, int i_20_) {
		method142();
		method139();
		aClass10_703.setCamera(0, 100, 0, i, i_19_, 0, i_20_);
		aClass10_703.method367();
		aClass10_704.setCamera(0, 100, 0, i, i_19_, 0, i_20_);
		aClass10_704.method367();
	}

	public void onDraw() {
		aClass6_702.method241(0, 0, 512, 384, 0, 4210752);
		if (aBoolean720)
			method141();
		else {
			int i = 1200;
			aClass10_703.method358(anApplet_Sub1_Sub1_Sub1_701.mouseX, anApplet_Sub1_Sub1_Sub1_701.mouseY, 50, 50);
			method116(anInt715, anInt714, i);
		}
		if (aBoolean721 || aBoolean736)
			aClass6_702.method268((anApplet_Sub1_Sub1_Sub1_701.generalGameModules[0]), 7, 12, 1, 16777215);
		else if (anInt732 == anInt735)
			aClass6_702.method268((anApplet_Sub1_Sub1_Sub1_701.generalGameModules[1]), 7, 12, 1, 16777215);
		else
			aClass6_702.method268((anApplet_Sub1_Sub1_Sub1_701.generalGameModules[2]), 7, 12, 1, 16777215);
		aClass6_702.method268((anApplet_Sub1_Sub1_Sub1_701.generalGameModules[3] + anInt733), 7, 23, 1, 16777215);
		aClass6_702.method268((anApplet_Sub1_Sub1_Sub1_701.gomadGameModules[0] + anInt729), 7, 45, 1, 16777215);
		aClass6_702.method268((anApplet_Sub1_Sub1_Sub1_701.gomadGameModules[1] + anInt730), 7, 56, 1, 16777215);
		int i = 460;
		int i_21_ = 4;
		anApplet_Sub1_Sub1_Sub1_701.aClass8_1094.method321(i, i_21_, 46, 28);
		aClass6_702.method267(anApplet_Sub1_Sub1_Sub1_701.generalGameModules[4], i + 22, i_21_ + 13, 1, 0);
		if (aBoolean720)
			aClass6_702.method267((anApplet_Sub1_Sub1_Sub1_701.generalGameModules[5]), i + 22, i_21_ + 23, 1, 0);
		else
			aClass6_702.method267((anApplet_Sub1_Sub1_Sub1_701.generalGameModules[6]), i + 22, i_21_ + 23, 1, 0);
		i = 460;
		i_21_ = 34;
		anApplet_Sub1_Sub1_Sub1_701.aClass8_1094.method321(i, i_21_, 46, 28);
		aClass6_702.method267(anApplet_Sub1_Sub1_Sub1_701.generalGameModules[7], i + 22, i_21_ + 18, 1, 0);
		i = 460;
		i_21_ = 64;
		anApplet_Sub1_Sub1_Sub1_701.aClass8_1094.method321(i, i_21_, 46, 28);
		aClass6_702.method267(anApplet_Sub1_Sub1_Sub1_701.gomadGameModules[2], i + 22, i_21_ + 13, 1, 0);
		aClass6_702.method267(anApplet_Sub1_Sub1_Sub1_701.gomadGameModules[3], i + 22, i_21_ + 23, 1, 0);
		if (aBoolean722) {
			method140(anApplet_Sub1_Sub1_Sub1_701.generalGameModules[11], anApplet_Sub1_Sub1_Sub1_701.generalGameModules[12]);
			if (anApplet_Sub1_Sub1_Sub1_701.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_701.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_701.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_701.mouseY < 188 && anInt719 == 1) {
				anApplet_Sub1_Sub1_Sub1_701.resign();
				aBoolean722 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_701.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_701.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_701.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_701.mouseY < 188 && anInt719 == 1)
				aBoolean722 = false;
			anInt723 = -1;
			anInt724 = -1;
			anInt719 = 0;
		} else {
			i = 460;
			i_21_ = 4;
			if (anInt719 == 1 && anApplet_Sub1_Sub1_Sub1_701.mouseX > i && anApplet_Sub1_Sub1_Sub1_701.mouseY > i_21_ && anApplet_Sub1_Sub1_Sub1_701.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_701.mouseY < i_21_ + 29) {
				aBoolean720 = !aBoolean720;
				anInt719 = 0;
			}
			i = 460;
			i_21_ = 34;
			if (anInt719 == 1 && anApplet_Sub1_Sub1_Sub1_701.mouseX > i && anApplet_Sub1_Sub1_Sub1_701.mouseY > i_21_ && anApplet_Sub1_Sub1_Sub1_701.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_701.mouseY < i_21_ + 29) {
				aBoolean722 = true;
				anInt719 = 0;
			}
			i = 460;
			i_21_ = 64;
			if (anInt719 == 1 && anApplet_Sub1_Sub1_Sub1_701.mouseX > i && anApplet_Sub1_Sub1_Sub1_701.mouseY > i_21_ && anApplet_Sub1_Sub1_Sub1_701.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_701.mouseY < i_21_ + 29) {
				if (aBoolean142)
					anApplet_Sub1_Sub1_Sub1_701.xmPlayer.method85(0, 0, 8000, 63);
				anApplet_Sub1_Sub1_Sub1_701.aClass2_582.putPacketId(254);
				anApplet_Sub1_Sub1_Sub1_701.aClass2_582.flushNoException();
				anInt719 = 0;
				aBoolean736 = true;
			}
			int i_22_ = -1;
			int i_23_ = -1;
			if (aBoolean720) {
				int i_24_ = 256 - anInt727 * 23 / 2;
				int i_25_ = 152 - anInt727 * 23 / 2;
				if (anApplet_Sub1_Sub1_Sub1_701.mouseX > i_24_ && anApplet_Sub1_Sub1_Sub1_701.mouseY > i_25_ && (anApplet_Sub1_Sub1_Sub1_701.mouseX < i_24_ + anInt727 * 23) && (anApplet_Sub1_Sub1_Sub1_701.mouseY < i_25_ + anInt727 * 23)) {
					i_22_ = (anApplet_Sub1_Sub1_Sub1_701.mouseX - i_24_) / 23;
					i_23_ = anInt727 - 1 - (anApplet_Sub1_Sub1_Sub1_701.mouseY - i_25_) / 23;
				}
			} else {
				int i_26_ = aClass10_703.method360();
				object3d class7 = aClass10_703.method361();
				if (i_26_ >= 0 && class7 == aClass7_706) {
					i_22_ = i_26_ % anInt727;
					i_23_ = i_26_ / anInt727;
				}
			}
			if (!aBoolean721 && anInt732 == anInt735 && !aBoolean736 && i_22_ != -1 && i_23_ != -1 && anIntArrayArray737[i_22_][i_23_] == 0) {
				anInt723 = i_22_;
				anInt724 = i_23_;
				if (anInt719 == 1 && method144(i_22_, i_23_)) {
					anIntArrayArray737[i_22_][i_23_] = anInt732 + 1;
					if (aBoolean142)
						anApplet_Sub1_Sub1_Sub1_701.xmPlayer.method85(0, 0, 8000, 63);
					anApplet_Sub1_Sub1_Sub1_701.aClass2_582.putPacketId(255);
					anApplet_Sub1_Sub1_Sub1_701.aClass2_582.p1(i_22_);
					anApplet_Sub1_Sub1_Sub1_701.aClass2_582.p1(i_23_);
					anApplet_Sub1_Sub1_Sub1_701.aClass2_582.flushNoException();
					aBoolean736 = true;
				}
			} else {
				anInt723 = -1;
				anInt724 = -1;
			}
			anInt719 = 0;
		}
	}

	public void method139() {
		for (int i = 0; i < anInt727; i++) {
			for (int i_27_ = 0; i_27_ < anInt727; i_27_++) {
				int i_28_ = i_27_ * anInt727 + i;
				aClass7_706.method283(i_28_, 15);
				aClass7_706.method284(i_28_, 15);
			}
		}
		if (anInt744 != -1 && anInt746 != anInt735) {
			int i = anInt745 * anInt727 + anInt744;
			aClass7_706.method283(i, -25);
			aClass7_706.method284(i, -25);
		}
		if (anInt723 != -1) {
			int i = anInt724 * anInt727 + anInt723;
			aClass7_706.method283(i, -24577);
			aClass7_706.method284(i, -24577);
		}
	}

	public void method140(String string, String string_29_) {
		int i = 160;
		int i_30_ = 80;
		aClass6_702.method240(256 - i / 2 + 10, 150 - i_30_ / 2 + 10, i, i_30_, 0, 128);
		aClass6_702.method242(256 - i / 2, 150 - i_30_ / 2, i, i_30_, pixmap.method247(96, 96, 121));
		aClass6_702.method267(string, 256, 130, 4, 16777215);
		aClass6_702.method267(string_29_, 256, 150, 4, 16777215);
		int i_31_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_701.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_701.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_701.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_701.mouseY < 188)
			i_31_ = 16711680;
		aClass6_702.method267(anApplet_Sub1_Sub1_Sub1_701.generalGameModules[17], 226, 178, 4, i_31_);
		i_31_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_701.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_701.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_701.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_701.mouseY < 188)
			i_31_ = 16711680;
		aClass6_702.method267(anApplet_Sub1_Sub1_Sub1_701.generalGameModules[18], 286, 178, 4, i_31_);
	}

	public void method141() {
		int i = 256 - anInt727 * 23 / 2;
		int i_32_ = 152 - anInt727 * 23 / 2 + (anInt727 - 1) * 23;
		for (int i_33_ = 0; i_33_ < anInt727; i_33_++) {
			for (int i_34_ = 0; i_34_ < anInt727; i_34_++)
				aClass6_702.method255(i_33_ * 23 + i, i_32_ - i_34_ * 23, anInt705);
		}
		if (anInt744 != -1 && anInt746 != -anInt735)
			aClass6_702.method242(anInt744 * 23 + i, i_32_ - anInt745 * 23, 23, 23, pixmap.method247(0, 0, 200));
		if (anInt723 != -1 && anInt724 != -1)
			aClass6_702.method242(anInt723 * 23 + i, i_32_ - anInt724 * 23, 23, 23, pixmap.method247(192, 0, 0));
		for (int i_35_ = 0; i_35_ < anInt727; i_35_++) {
			for (int i_36_ = 0; i_36_ < anInt727; i_36_++) {
				if (anIntArrayArray737[i_35_][i_36_] == 1)
					aClass6_702.method255(i_35_ * 23 + i, i_32_ - i_36_ * 23, anInt705 + 1);
				if (anIntArrayArray737[i_35_][i_36_] == 2)
					aClass6_702.method255(i_35_ * 23 + i, i_32_ - i_36_ * 23, anInt705 + 2);
			}
		}
	}

	public void method142() {
		aClass10_704.clearObjects();
		anInt711 = 0;
		anInt713 = 0;
		for (int i = 0; i < anInt727; i++) {
			for (int i_37_ = 0; i_37_ < anInt727; i_37_++) {
				if (anIntArrayArray737[i][i_37_] == 1) {
					object3d class7 = aClass7Array710[anInt711++];
					aClass10_704.addObject(class7);
					class7.requestSetTranslate(i * 59 - (anInt727 - 1) * 59 / 2, 0, i_37_ * 59 - (anInt727 - 1) * 59 / 2);
				}
				if (anIntArrayArray737[i][i_37_] == 2) {
					object3d class7 = aClass7Array712[anInt713++];
					aClass10_704.addObject(class7);
					class7.requestSetTranslate(i * 59 - (anInt727 - 1) * 59 / 2, 0, i_37_ * 59 - (anInt727 - 1) * 59 / 2);
				}
			}
		}
	}

	public boolean method143(int i, int i_38_, int i_39_, int i_40_) {
		if (anIntArrayArray739[i][i_38_] != i_39_)
			return false;
		int i_41_ = 0;
		int i_42_ = 0;
		anIntArray740[i_41_] = i;
		anIntArray741[i_41_++] = i_38_;
		anIntArrayArray739[i][i_38_] = i_40_;
		while (i_42_ < i_41_) {
			i = anIntArray740[i_42_];
			i_38_ = anIntArray741[i_42_++];
			if (i > 0) {
				int i_43_ = anIntArrayArray739[i - 1][i_38_];
				if (i_43_ == 0 || i_43_ > i_40_)
					return false;
				if (i_39_ == i_43_) {
					anIntArray740[i_41_] = i - 1;
					anIntArray741[i_41_++] = i_38_;
					anIntArrayArray739[i - 1][i_38_] = i_40_;
				}
			}
			if (i_38_ > 0) {
				int i_44_ = anIntArrayArray739[i][i_38_ - 1];
				if (i_44_ == 0 || i_44_ > i_40_)
					return false;
				if (i_39_ == i_44_) {
					anIntArray740[i_41_] = i;
					anIntArray741[i_41_++] = i_38_ - 1;
					anIntArrayArray739[i][i_38_ - 1] = i_40_;
				}
			}
			if (i < anInt727 - 1) {
				int i_45_ = anIntArrayArray739[i + 1][i_38_];
				if (i_45_ == 0 || i_45_ > i_40_)
					return false;
				if (i_39_ == i_45_) {
					anIntArray740[i_41_] = i + 1;
					anIntArray741[i_41_++] = i_38_;
					anIntArrayArray739[i + 1][i_38_] = i_40_;
				}
			}
			if (i_38_ < anInt727 - 1) {
				int i_46_ = anIntArrayArray739[i][i_38_ + 1];
				if (i_46_ == 0 || i_46_ > i_40_)
					return false;
				if (i_39_ == i_46_) {
					anIntArray740[i_41_] = i;
					anIntArray741[i_41_++] = i_38_ + 1;
					anIntArrayArray739[i][i_38_ + 1] = i_40_;
				}
			}
		}
		return true;
	}

	public boolean method144(int i, int i_47_) {
		if (i < 0 || i_47_ < 0 || i >= anInt727 || i_47_ >= anInt727)
			return false;
		if (anIntArrayArray737[i][i_47_] != 0)
			return false;
		anIntArrayArray737[i][i_47_] = anInt732 + 1;
		for (int i_48_ = 0; i_48_ < anInt727; i_48_++) {
			for (int i_49_ = 0; i_49_ < anInt727; i_49_++)
				anIntArrayArray739[i_48_][i_49_] = anIntArrayArray737[i_48_][i_49_];
		}
		boolean bool = false;
		if (!bool && i > 0)
			bool = method143(i - 1, i_47_, 2 - anInt732, 99);
		if (!bool && i_47_ > 0)
			bool = method143(i, i_47_ - 1, 2 - anInt732, 98);
		if (!bool && i < anInt727 - 1)
			bool = method143(i + 1, i_47_, 2 - anInt732, 97);
		if (!bool && i_47_ < anInt727 - 1)
			bool = method143(i, i_47_ + 1, 2 - anInt732, 96);
		if (!bool)
			bool = !method143(i, i_47_, anInt732 + 1, 100);
		anIntArrayArray737[i][i_47_] = 0;
		return bool;
	}

	public void method145() {
		for (int i = 0; i < anInt727; i++) {
			for (int i_50_ = 0; i_50_ < anInt727; i_50_++)
				anIntArrayArray739[i][i_50_] = anIntArrayArray737[i][i_50_];
		}
		for (int i = 0; i < 1000; i++) {
			anIntArray742[i] = 0;
			anIntArray743[i] = 0;
		}
		int i = 100;
		int i_51_ = 500;
		for (int i_52_ = 0; i_52_ < anInt727; i_52_++) {
			for (int i_53_ = 0; i_53_ < anInt727; i_53_++) {
				if (anIntArrayArray739[i_52_][i_53_] == 1)
					method146(i_52_, i_53_, i++, 1);
				else if (anIntArrayArray739[i_52_][i_53_] == 2)
					method146(i_52_, i_53_, i_51_++, 2);
			}
		}
		for (int i_54_ = 0; i_54_ < anInt727; i_54_++) {
			for (int i_55_ = 0; i_55_ < anInt727; i_55_++) {
				if (anIntArrayArray739[i_54_][i_55_] > 0 && anIntArray743[anIntArrayArray739[i_54_][i_55_]] < 6)
					anIntArrayArray739[i_54_][i_55_] = 0;
			}
		}
		for (int i_56_ = 0; i_56_ < anInt727; i_56_++) {
			for (int i_57_ = 0; i_57_ < anInt727; i_57_++) {
				if (anIntArrayArray739[i_56_][i_57_] == 0)
					method147(i_56_, i_57_);
			}
		}
		anInt729 = 0;
		anInt730 = 0;
		for (int i_58_ = 100; i_58_ < 499; i_58_++) {
			if (anIntArray742[i_58_] >= 2)
				anInt729 += anIntArray743[i_58_];
		}
		for (int i_59_ = 500; i_59_ < 999; i_59_++) {
			if (anIntArray742[i_59_] >= 2)
				anInt730 += anIntArray743[i_59_];
		}
		int i_60_ = anInt727 * anInt727 / 2;
		if (anInt729 > i_60_ || anInt730 > i_60_) {
			anApplet_Sub1_Sub1_Sub1_701.aClass2_582.putPacketId(253);
			anApplet_Sub1_Sub1_Sub1_701.aClass2_582.flushNoException();
		}
	}

	public void method146(int i, int i_61_, int i_62_, int i_63_) {
		int i_64_ = 0;
		int i_65_ = 0;
		anIntArray740[i_64_] = i;
		anIntArray741[i_64_++] = i_61_;
		anIntArrayArray739[i][i_61_] = i_62_;
		anIntArray743[i_62_]++;
		while (i_65_ < i_64_) {
			i = anIntArray740[i_65_];
			i_61_ = anIntArray741[i_65_++];
			if (i > 0 && anIntArrayArray739[i - 1][i_61_] == i_63_) {
				anIntArray740[i_64_] = i - 1;
				anIntArray741[i_64_++] = i_61_;
				anIntArrayArray739[i - 1][i_61_] = i_62_;
				anIntArray743[i_62_]++;
			}
			if (i_61_ > 0 && anIntArrayArray739[i][i_61_ - 1] == i_63_) {
				anIntArray740[i_64_] = i;
				anIntArray741[i_64_++] = i_61_ - 1;
				anIntArrayArray739[i][i_61_ - 1] = i_62_;
				anIntArray743[i_62_]++;
			}
			if (i < anInt727 - 1 && anIntArrayArray739[i + 1][i_61_] == i_63_) {
				anIntArray740[i_64_] = i + 1;
				anIntArray741[i_64_++] = i_61_;
				anIntArrayArray739[i + 1][i_61_] = i_62_;
				anIntArray743[i_62_]++;
			}
			if (i_61_ < anInt727 - 1 && anIntArrayArray739[i][i_61_ + 1] == i_63_) {
				anIntArray740[i_64_] = i;
				anIntArray741[i_64_++] = i_61_ + 1;
				anIntArrayArray739[i][i_61_ + 1] = i_62_;
				anIntArray743[i_62_]++;
			}
		}
	}

	public void method147(int i, int i_66_) {
		int i_67_ = 0;
		int i_68_ = 0;
		int i_69_ = -1;
		boolean bool = false;
		anIntArray740[i_67_] = i;
		anIntArray741[i_67_++] = i_66_;
		anIntArrayArray739[i][i_66_] = 1000;
		while (i_68_ < i_67_) {
			i = anIntArray740[i_68_];
			i_66_ = anIntArray741[i_68_++];
			if (!bool && method148(i - 1, i_66_) && method148(i - 1, i_66_ - 1) && method148(i, i_66_ - 1) && method148(i + 1, i_66_ - 1) && method148(i + 1, i_66_) && method148(i + 1, i_66_ + 1) && method148(i, i_66_ + 1) && method148(i - 1, i_66_ + 1))
				bool = true;
			if (i > 0) {
				int i_70_ = anIntArrayArray739[i - 1][i_66_];
				if (i_70_ == 0) {
					anIntArray740[i_67_] = i - 1;
					anIntArray741[i_67_++] = i_66_;
					anIntArrayArray739[i - 1][i_66_] = 1000;
				} else if (i_70_ != 1000 && i_69_ == -1)
					i_69_ = i_70_;
				else if (i_70_ != 1000 && i_70_ != i_69_)
					bool = true;
			}
			if (i_66_ > 0) {
				int i_71_ = anIntArrayArray739[i][i_66_ - 1];
				if (i_71_ == 0) {
					anIntArray740[i_67_] = i;
					anIntArray741[i_67_++] = i_66_ - 1;
					anIntArrayArray739[i][i_66_ - 1] = 1000;
				} else if (i_71_ != 1000 && i_69_ == -1)
					i_69_ = i_71_;
				else if (i_71_ != 1000 && i_71_ != i_69_)
					bool = true;
			}
			if (i < anInt727 - 1) {
				int i_72_ = anIntArrayArray739[i + 1][i_66_];
				if (i_72_ == 0) {
					anIntArray740[i_67_] = i + 1;
					anIntArray741[i_67_++] = i_66_;
					anIntArrayArray739[i + 1][i_66_] = 1000;
				} else if (i_72_ != 1000 && i_69_ == -1)
					i_69_ = i_72_;
				else if (i_72_ != 1000 && i_72_ != i_69_)
					bool = true;
			}
			if (i_66_ < anInt727 - 1) {
				int i_73_ = anIntArrayArray739[i][i_66_ + 1];
				if (i_73_ == 0) {
					anIntArray740[i_67_] = i;
					anIntArray741[i_67_++] = i_66_ + 1;
					anIntArrayArray739[i][i_66_ + 1] = 1000;
				} else if (i_73_ != 1000 && i_69_ == -1)
					i_69_ = i_73_;
				else if (i_73_ != 1000 && i_73_ != i_69_)
					bool = true;
			}
		}
		if (!bool && i_69_ != -1) {
			anIntArray742[i_69_]++;
			anIntArray743[i_69_] += i_67_;
		}
	}

	public boolean method148(int i, int i_74_) {
		if (i < 0 || i_74_ < 0 || i >= anInt727 || i_74_ >= anInt727)
			return true;
		if (anIntArrayArray739[i][i_74_] == 0 || anIntArrayArray739[i][i_74_] == 1000)
			return true;
		return false;
	}
}
