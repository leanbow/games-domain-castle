package jagex;

import java.applet.Applet;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class megastream implements Runnable {
	public static int[] packetAmountSent = new int[256];
	public static int[] packetLengthSent = new int[256];
	final int anInt145 = 61;
	final int anInt146 = 59;
	final int anInt147 = 42;
	final int anInt148 = 43;
	final int anInt149 = 44;
	final int anInt150 = 45;
	final int anInt151 = 46;
	final int anInt152 = 47;
	final int anInt153 = 92;
	final int anInt154 = 32;
	final int anInt155 = 124;
	final int anInt156 = 34;
	static char[] aCharArray157 = new char[256];
	public InputStream inputStream;
	public OutputStream outputStream;
	public Socket socket;
	boolean noConnection = false;
	byte[] inputData;
	int inputPos;
	public int outputBufferSize = 5000;
	private byte[] outputBuffer;
	private int anInt166;
	private int outputBufferPos;
	private boolean aBoolean168 = false;
	public boolean exceptionCaught = false;
	public String exceptionMessage = "";
	public boolean aBoolean171 = false;
	private int threadId;
	public static int networkWriterThreadsCount;
	private static int anInt174;
	private static boolean networkWriterThreadsCreated;
	private static Thread[] networkWriterThreads;
	private static megastream[] megastreams;
	private static int megastreamsPos;
	private static int megastreamsPollPos;
	public static boolean aBoolean180;
	public static long aLong181;
	public int pos = 3;
	public byte[] data;

	public megastream() {
		noConnection = true;
	}

	public megastream(InputStream inputstream) {
		inputStream = inputstream;
	}

	public megastream(OutputStream outputstream) {
		outputStream = outputstream;
	}

	public megastream(Socket socket) throws IOException {
		this.socket = socket;
		inputStream = socket.getInputStream();
		outputStream = socket.getOutputStream();
	}

	public megastream(String string) throws IOException {
		inputStream = tools.openInputStream(string);
	}

	public megastream(byte[] is) {
		inputData = is;
		inputPos = 0;
		noConnection = true;
	}

	public static megastream create(String ip, Applet applet, int port) throws IOException {
		Socket socket;
		if (applet != null)
			socket = new Socket(InetAddress.getByName(applet.getCodeBase().getHost()), port);
		else
			socket = new Socket(InetAddress.getByName(ip), port);
		socket.setSoTimeout(10000);
		return new megastream(socket);
	}

	public void close() {
		if (!noConnection) {
			if (outputStream != null) {
				try {
					outputStream.flush();
				} catch (IOException ioexception) {
					/* empty */
				}
			}
			try {
				if (socket != null)
					socket.close();
				if (inputStream != null)
					inputStream.close();
				if (outputStream != null)
					outputStream.close();
				outputBuffer = null;
				data = null;
			} catch (IOException ioexception) {
				System.out.println("Error closing stream");
			}
		}
	}

	public int read() throws IOException {
		if (inputData != null)
			return inputData[inputPos++];
		if (noConnection)
			return 0;
		return inputStream.read();
	}

	public int readShort() throws IOException {
		int i = read();
		int i_0_ = read();
		return i * 256 + i_0_;
	}

	public int available() throws IOException {
		if (noConnection)
			return 0;
		return inputStream.available();
	}

	public void read(byte[] buffer, int len) throws IOException {
		if (!noConnection) {
			int bytesRead;
			for (int off = 0; off < len; off += bytesRead) {
				if ((bytesRead = inputStream.read(buffer, off, len - off)) <= 0)
					throw new IOException("EOF");
			}
		}
	}

	public static int unsigned(byte value) {
		return value & 0xff;
	}

	public static int g2(byte[] is, int i) {
		return unsigned(is[i]) * 256 + unsigned(is[i + 1]);
	}

	public static int g3(byte[] is, int i) {
		return (unsigned(is[i]) * 16777216 + unsigned(is[i + 1]) * 65536 + unsigned(is[i + 2]) * 256 + unsigned(is[i + 3]));
	}

	public void skipToEquals() throws IOException {
		for (int i = read(); i != 61 && i != -1; i = read()) {
			/* empty */
		}
	}

	public int getBase10Value() throws IOException {
		int value = 0;
		boolean negate = false;
		int character = read();
		while (character < 48 || character > 57) {
			if (character == 45)
				negate = true;
			character = read();
			if (character == -1)
				throw new IOException("Eof!");
		}
		for (/**/; character >= 48 && character <= 57; character = read())
			value = value * 10 + character - 48;
		if (negate)
			value = -value;
		return value;
	}

	public String getStringValue() throws IOException {
		String value = "";
		boolean quoteStarted = false;
		int character = read();
		while (character < 32 || character == 44 || character == 59 || character == 61) {//,;=
			character = read();
			if (character == -1)
				throw new IOException("Eof!");
		}
		if (character == 34) {//"
			quoteStarted = true;
			character = read();
		}
		for (/**/; character != -1; character = read()) {
			if (!quoteStarted && (character == 44 || character == 61 || character == 59) || quoteStarted && character == 34)
				break;
			value += aCharArray157[character];
		}
		return value;
	}

	public int getBase16Value() throws IOException {
		int value = 0;
		int character = read();
		while ((character < 48 || character > 57) && (character < 97 || character > 102) && (character < 65 || character > 70)) {
			character = read();
			if (character == -1)
				throw new IOException("Eof!");
		}
		for (;;) {
			if (character >= 48 && character <= 57)
				value = value * 16 + character - 48;
			else if (character >= 97 && character <= 102)
				value = value * 16 + character + 10 - 97;
			else {
				if (character < 65 || character > 70)
					break;
				value = value * 16 + character + 10 - 65;
			}
			character = read();
		}
		return value;
	}

	public void write(byte[] buffer, int off, int length) throws IOException {
		if (!noConnection) {
			outputStream.write(buffer, off, length);
		}
	}

	public void method211(byte[] buf, int off, int len, boolean flush) throws IOException {
		if (!noConnection) {
			if (!networkWriterThreadsCreated) {
				System.out.println("Creating " + networkWriterThreadsCount + " network writer threads");
				networkWriterThreadsCreated = true;
				networkWriterThreads = new Thread[networkWriterThreadsCount];
				for (int id = 0; id < networkWriterThreadsCount; id++) {
					megastream megastream = new megastream();
					megastream.threadId = id;
					networkWriterThreads[id] = new Thread(megastream);
					networkWriterThreads[id].setDaemon(true);
					networkWriterThreads[id].start();
				}
			}
			if (outputBuffer == null)
				outputBuffer = new byte[outputBufferSize];
			for (int id = 0; id < len; id++) {
				outputBuffer[outputBufferPos] = buf[id + off];
				outputBufferPos = (outputBufferPos + 1) % outputBufferSize;
				if (outputBufferPos == (anInt166 + outputBufferSize - 100) % outputBufferSize) {
					exceptionMessage = "Write buffer full! " + len;
					exceptionCaught = true;
					flush = false;
					break;
				}
			}
			if (flush)
				addMegastream();
			if (exceptionCaught)
				throw new IOException(exceptionMessage);
		}
	}

	public void addMegastream() {
		if (!noConnection) {
			synchronized (megastreams) {
				megastreams[megastreamsPos] = this;
				megastreamsPos = (megastreamsPos + 1) % 2000;
				megastreams.notify();
			}
		}
	}

	public void run() {
		while_0_: for (;;) {
			megastream megastream;
			synchronized (megastreams) {
				anInt174++;
				while (megastreams[megastreamsPollPos] == null) {
					try {
						megastreams.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
				megastream = megastreams[megastreamsPollPos];
				megastreams[megastreamsPollPos] = null;
				megastreamsPollPos = (megastreamsPollPos + 1) % 2000;
				anInt174--;
			}
			boolean bool = false;
			synchronized (megastream) {
				if (megastream.aBoolean171)
					bool = true;
				else
					megastream.aBoolean171 = true;
			}
			if (!bool) {
				try {
					for (;;) {
						int offset;
						int length;
						synchronized (megastream) {
							offset = megastream.anInt166;
							length = megastream.outputBufferPos - offset;
							if (length < 0)
								length = outputBufferSize - offset;
							if (length == 0) {
								megastream.aBoolean171 = false;
								break;
							}
						}
						if (aBoolean180) {
							long l = aLong181;
							while (aLong181 - l < 5L && length > 0) {
								int i_12_ = length;
								if (i_12_ > 100)
									i_12_ = 100;
								megastream.outputStream.write(megastream.outputBuffer, offset, i_12_);
								offset += i_12_;
								length -= i_12_;
								megastream.anInt166 = (megastream.anInt166 + i_12_) % outputBufferSize;
							}
							if (anInt174 == 0) {
								synchronized (megastream) {
									megastream.aBoolean171 = false;
								}
								megastream.addMegastream();
								continue while_0_;
							}
						} else {
							megastream.outputStream.write(megastream.outputBuffer, offset, length);
							megastream.anInt166 = (megastream.anInt166 + length) % outputBufferSize;
						}
					}
					megastream.outputStream.flush();
					if (megastream.aBoolean168)
						megastream.close();
				} catch (Exception exception) {
					megastream.exceptionMessage = "Twriter IOEx:" + exception;
					megastream.exceptionCaught = true;
				}
			}
		}
	}

	public void putPacketId(int i) {
		if (data == null)
			data = new byte[4000];
		data[2] = (byte) i;
		pos = 3;
	}

	public void p1(int i) {
		data[pos++] = (byte) i;
	}

	public void p2(int i) {
		data[pos++] = (byte) ((i & 0xff00) >> 8);
		data[pos++] = (byte) (i & 0xff);
	}

	public void p4(int i) {
		data[pos++] = (byte) (i / 16777216);
		data[pos++] = (byte) (i / 65536);
		data[pos++] = (byte) (i / 256);
		data[pos++] = (byte) i;
	}

	public void putString(String string) {
		string.getBytes(0, string.length(), data, pos);
		pos += string.length();
	}

	public void flush() throws IOException {
		data[0] = (byte) ((pos - 2) / 256);
		data[1] = (byte) (pos - 2 & 0xff);
		method211(data, 0, pos, true);
		int packetId = data[2] & 0xff;
		packetAmountSent[packetId]++;
		packetLengthSent[packetId] += pos;
	}

	public void flushNoException() {
		data[0] = (byte) ((pos - 2) / 256);
		data[1] = (byte) (pos - 2 & 0xff);
		try {
			method211(data, 0, pos, true);
		} catch (IOException ioexception) {
			/* empty */
		}
		int packetId = data[2] & 0xff;
		packetAmountSent[packetId]++;
		packetLengthSent[packetId] += pos;
	}

	static {
		for (int i = 0; i < 256; i++)
			aCharArray157[i] = (char) i;
		aCharArray157[61] = '=';
		aCharArray157[59] = ';';
		aCharArray157[42] = '*';
		aCharArray157[43] = '+';
		aCharArray157[44] = ',';
		aCharArray157[45] = '-';
		aCharArray157[46] = '.';
		aCharArray157[47] = '/';
		aCharArray157[92] = '\\';
		aCharArray157[124] = '|';
		aCharArray157[33] = '!';
		aCharArray157[34] = '\"';
		networkWriterThreadsCount = 1;
		megastreams = new megastream[2000];
	}
}
