package jagex;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class client extends gameshell {
	public static String[] clientModules = new String[53];
	public static boolean aBoolean566 = true;
	public static boolean aBoolean567;
	public static int cyclesBeforeTimeout = 1500;
	public static int timoutCycle;
	public static int version = 1;//??
	public String connectionIp = "127.0.0.1";
	public String connectionIpOnError = "127.0.0.1";//64.23.81.81
	public int connectionPort = 43594;
	String username = "";
	String password = "";
	String chatNickname = "";
	String nickname;
	int selfPlayerIndex;
	int selfPlayerRating;
	int selfPlayerRatingId;
	public megastream aClass2_582;
	int packetSize;
	int packetType;
	byte[] inputStream = new byte[5000];
	long aLong586;
	long aLong587;
	int anInt588 = 1000;
	int anInt589;
	String[] aStringArray590 = new String[anInt588];
	int[] anIntArray591 = new int[anInt588];
	int[] anIntArray592 = new int[anInt588];
	int[] anIntArray593 = new int[anInt588];
	String[] aStringArray594 = new String[anInt588];
	int[] anIntArray595 = new int[anInt588];
	int anInt596 = 1000;
	int anInt597;
	int[] anIntArray598 = new int[anInt596];
	String[] aStringArray599 = new String[anInt596];
	String[] aStringArray600 = new String[anInt596];
	int anInt601 = 1;
	String[] aStringArray602 = { clientModules[0], clientModules[1], clientModules[2], clientModules[3], clientModules[4] };
	String[] aStringArray603 = { clientModules[5], clientModules[6] };
	public boolean aBoolean604 = false;
	String[] aStringArray605 = new String[50];
	int[] anIntArray606 = new int[50];
	int[] anIntArray607 = new int[50];
	int[] anIntArray608 = new int[50];
	String aString609;
	static char[] aCharArray610;
	static char[] aCharArray611;
	static String aString612;
	static String aString613;
	static String aString614;
	static String aString615;

	public String formatName(String input) {
		String output = "";
		for (int id = 0; id < 12; id++) {
			char c = input.charAt(id);
			if (c == '_')
				output += " ";
			else
				output += c;
		}
		return output;
	}

	public void method26(String user, String string_1_) {
		username = user;
		user = tools.makeString(user, 22);
		password = string_1_;
		string_1_ = tools.makeString(string_1_, 20);
		if (user.length() == 0 || string_1_.length() == 0)
			method52(clientModules[7], clientModules[8]);
		else {
			if (nickname == null)
				chatNickname = formatName(user);
			else
				chatNickname = formatName(nickname + "            ");
			method27(clientModules[46], clientModules[47]);
			long l = System.currentTimeMillis();
			while (System.currentTimeMillis() - l < 60000L) {
				try {
					if (this.isUsingApplet())
						aClass2_582 = megastream.create(connectionIp, this, connectionPort);
					else
						aClass2_582 = megastream.create(connectionIp, null, connectionPort);
					if (nickname == null) {
						aClass2_582.putPacketId(19);
						aClass2_582.putString(user);
						aClass2_582.putString(string_1_);
						aClass2_582.p2(version);
						aClass2_582.flush();
					} else {
						String string_2_ = tools.makeString(nickname, 20);
						aClass2_582.putPacketId(21);
						aClass2_582.putString(user);
						aClass2_582.putString(string_1_);
						aClass2_582.p2(version);
						aClass2_582.putString(string_2_);
						aClass2_582.flush();
					}
					aClass2_582.readShort();
					int i = aClass2_582.read();
					if (i == 0) {
						method33();
						method53();
						return;
					}
					if (i >= 1 && i <= 4) {
						toLoginScreen();
						return;
					}
					method27(clientModules[46], clientModules[47]);
				} catch (Exception exception) {
					method27(clientModules[46], clientModules[47]);
				}
				try {
					Thread.sleep(5000L);
				} catch (Exception exception) {
					/* empty */
				}
			}
			toLoginScreen();
		}
	}

	public void method27(String string, String string_3_) {
		Graphics graphics = this.getGraphics();
		Font font = new Font("Helvetica", 1, 15);
		int i = this.width();
		int i_4_ = this.height();
		graphics.setColor(Color.black);
		graphics.fillRect(i / 2 - 140, i_4_ / 2 - 25, 280, 50);
		graphics.setColor(Color.white);
		graphics.drawRect(i / 2 - 140, i_4_ / 2 - 25, 280, 50);
		this.drawString(graphics, string, font, i / 2, i_4_ / 2 - 10);
		this.drawString(graphics, string_3_, font, i / 2, i_4_ / 2 + 10);
	}

	public void login(String user, String pass, String nick) {
		nickname = nick;
		login(user, pass);
	}

	public void login(String user, String pass) {
		try {
			username = user;
			user = tools.makeString(user, 22);
			password = pass;
			pass = tools.makeString(pass, 20);
			if (user.length() == 0 || pass.length() == 0)
				method52(clientModules[7], clientModules[8]);
			else {
				if (nickname == null)
					chatNickname = formatName(user);
				else
					chatNickname = formatName(nickname + "            ");
				method52(clientModules[9], clientModules[10]);
				if (this.isUsingApplet())
					aClass2_582 = megastream.create(connectionIp, this, connectionPort);
				else
					aClass2_582 = megastream.create(connectionIp, null, connectionPort);
				if (nickname == null) {
					aClass2_582.putPacketId(0);
					aClass2_582.putString(user);
					aClass2_582.putString(pass);
					aClass2_582.p2(version);
					aClass2_582.flush();
				} else {
					String nick = tools.makeString(nickname, 20);
					aClass2_582.putPacketId(20);
					aClass2_582.putString(user);
					aClass2_582.putString(pass);
					aClass2_582.p2(version);
					aClass2_582.putString(nick);
					aClass2_582.flush();
				}
				aClass2_582.readShort();
				int response = aClass2_582.read();
				System.out.println("Login response: " + response);
				if (response == 0) {
					method33();
					method54();
				} else if (response == 2)
					method52(clientModules[11], clientModules[12]);
				else if (response == 3)
					method52(clientModules[13], clientModules[14]);
				else if (response == 4)
					method52(clientModules[48], clientModules[49]);
				else
					method52(clientModules[15], clientModules[12]);
			}
		} catch (Exception exception) {
			connectionIp = connectionIpOnError;
			System.out.println(String.valueOf(exception));
			method52(clientModules[15], clientModules[12]);
		}
	}

	public void createAccount(String string, String string_9_, String string_10_, String string_11_, String string_12_, int i, int i_13_, int i_14_) {
		try {
			if (this.isUsingApplet())
				aClass2_582 = megastream.create(connectionIp, this, connectionPort);
			else
				aClass2_582 = megastream.create(connectionIp, null, connectionPort);
			aClass2_582.putPacketId(2);
			string = tools.makeString(string, 22);
			aClass2_582.putString(string);
			string_9_ = tools.makeString(string_9_, 20);
			aClass2_582.putString(string_9_);
			chatNickname = formatName(string);
			for (/**/; string_10_.length() < 20; string_10_ += " ") {
				/* empty */
			}
			aClass2_582.putString(string_10_);
			for (/**/; string_11_.length() < 20; string_11_ += " ") {
				/* empty */
			}
			aClass2_582.putString(string_11_);
			for (/**/; string_12_.length() < 40; string_12_ += " ") {
				/* empty */
			}
			aClass2_582.putString(string_12_);
			aClass2_582.p4(i);
			aClass2_582.p4(i_13_);
			aClass2_582.p4(i_14_);
			aClass2_582.flushNoException();
			aClass2_582.readShort();
			int i_15_ = aClass2_582.read();
			aClass2_582.close();
			System.out.println("Newplayer response: " + i_15_);
			if (i_15_ == 0)
				method56();
			else if (i_15_ == 2)
				method52(clientModules[11], clientModules[12]);
			else if (i_15_ == 3)
				method52(clientModules[16], clientModules[17]);
			else if (i_15_ == 4)
				method52(clientModules[48], clientModules[49]);
			else
				method52(clientModules[15], clientModules[12]);
		} catch (Exception exception) {
			connectionIp = connectionIpOnError;
			System.out.println(String.valueOf(exception));
			method52(clientModules[15], clientModules[12]);
		}
	}

	public void method31() {
		if (aClass2_582 != null) {
			aClass2_582.putPacketId(1);
			aClass2_582.flushNoException();
			toLoginScreen();
		}
	}

	public void method32() {
		System.out.println("Lost connection");
		if (aBoolean567)
			method26(username, password);
		else {
			method31();
			login(username, password);
		}
	}

	public void method33() {
		packetSize = 0;
		packetType = 0;
		timoutCycle = -500;
	}

	public void method34() {
		do {
			try {
				long l = System.currentTimeMillis();
				if (l - aLong587 > 5000L) {
					aLong587 = l;
					aClass2_582.putPacketId(4);
					aClass2_582.flush();
				}
				if (method70()) {
					//timoutCycle++;
					if (timoutCycle > cyclesBeforeTimeout) {
						method33();
						method32();
					} else {
						if (packetSize == 0 && aClass2_582.available() >= 2)
							packetSize = aClass2_582.readShort();
						if (packetSize <= 0 || aClass2_582.available() < packetSize)
							break;
						aClass2_582.read(inputStream, packetSize);
						packetType = megastream.unsigned(inputStream[0]);
						timoutCycle = 0;
						if (packetType == 5) {
							megastream.g2(inputStream, 1);
							selfPlayerIndex = megastream.g2(inputStream, 3);
							int i = (packetSize - 5) / 18;
							for (int i_16_ = 0; i_16_ < i; i_16_++) {
								int index = megastream.g2(inputStream, 5 + i_16_ * 18);
								anIntArray593[i_16_] = index;
								anIntArray591[i_16_] = megastream.g2(inputStream, 7 + i_16_ * 18);
								anIntArray592[i_16_] = megastream.g2(inputStream, 9 + i_16_ * 18);
								aStringArray590[i_16_] = new String(inputStream, 11 + i_16_ * 18, 12).trim();
								aStringArray594[index] = aStringArray590[i_16_];
								anIntArray595[index] = anIntArray591[i_16_];
								if (index == selfPlayerIndex) {
									selfPlayerRating = anIntArray591[i_16_];
									selfPlayerRatingId = method46(selfPlayerRating);
									String string = (String.valueOf(selfPlayerRating / 10) + "." + selfPlayerRating % 10 + " (" + ratingToTitle(selfPlayerRating) + ")");
									method64(string);
								}
							}
							for (int i_18_ = 0; i_18_ < i; i_18_++) {
								if ((anIntArray592[i_18_] & ratingToMask(selfPlayerRatingId)) != 0)
									anIntArray592[i_18_] = 0;
								else
									anIntArray592[i_18_] = 1;
							}
							method61(i, anIntArray591, anIntArray592, aStringArray590);
						} else if (packetType == 7) {
							selfPlayerRating = megastream.g3(inputStream, 1);
							selfPlayerRatingId = method46(selfPlayerRating);
							String string = (String.valueOf(selfPlayerRating / 10) + "." + selfPlayerRating % 10 + " (" + ratingToTitle(selfPlayerRating) + ")");
							method64(string);
							for (int i = 0; i < 50; i++) {
								aStringArray605[i] = new String(inputStream, 5 + i * 20, 12).trim();
								anIntArray606[i] = megastream.g3(inputStream, 17 + i * 20) / 10;
								anIntArray607[i] = megastream.g2(inputStream, 21 + i * 20);
								anIntArray608[i] = megastream.g2(inputStream, 23 + i * 20);
							}
							method69(50, aStringArray605, anIntArray606, anIntArray607, anIntArray608);
						} else if (packetType == 8) {
							String string = new String(inputStream, 1, packetSize - 1);
							if (method51(string) == 1)
								method63(string);
						} else if (packetType == 9) {
							int i = megastream.g2(inputStream, 1);
							int i_19_ = megastream.g2(inputStream, 3);
							int i_20_ = megastream.g2(inputStream, 5);
							method65((aStringArray594[i] + " (" + anIntArray595[i] / 10 + "." + anIntArray595[i] % 10 + ")"), (aStringArray603[i_20_] + " - " + aStringArray602[i_19_] + " " + clientModules[45]));
							anInt601 = 1;
						} else if (packetType == 10) {
							int i = megastream.g2(inputStream, 1);
							int i_21_ = megastream.g2(inputStream, 3);
							int i_22_ = megastream.g2(inputStream, 5);
							method66((aStringArray594[i] + " (" + anIntArray595[i] / 10 + "." + anIntArray595[i] % 10 + ")"), (aStringArray603[i_22_] + " - " + aStringArray602[i_21_] + " " + clientModules[45]));
							anInt601 = 2;
						} else if (packetType == 11) {
							method67(clientModules[18], "", clientModules[19], clientModules[20]);
							anInt601 = 3;
						} else if (packetType == 12 && anInt601 == 2) {
							method67(clientModules[21], "", clientModules[22], clientModules[23]);
							anInt601 = 3;
						} else if (packetType == 12 && anInt601 == 1) {
							method67(clientModules[24], "", clientModules[25], clientModules[26]);
							anInt601 = 3;
						} else if (packetType == 16) {
							selfPlayerIndex = megastream.g2(inputStream, 1);
							method68(selfPlayerIndex);
						} else if (packetType == 17) {
							String string = new String(inputStream, 1, packetSize - 1);
							method74();
							method71("", string, "");
						} else if (packetType == 18) {
							int i = megastream.unsigned(inputStream[1]);
							int i_23_ = megastream.g2(inputStream, 2);
							String string = new String(inputStream, 4, packetSize - 4);
							if (i_23_ != 0) {
								if (i == selfPlayerIndex) {
									method72(true);
									method71(clientModules[27], string, (clientModules[28] + i_23_ / 10 + "." + i_23_ % 10));
								} else {
									method73(true);
									method71(clientModules[29], string, (clientModules[30] + i_23_ / 10 + "." + i_23_ % 10));
								}
							} else if (i == selfPlayerIndex) {
								method72(false);
								method71(clientModules[27], string, clientModules[31]);
							} else {
								method73(false);
								method71(clientModules[29], string, clientModules[31]);
							}
						} else if (packetType == 20) {
							method63(clientModules[32]);
							aBoolean604 = true;
						} else if (packetType == 21) {
							int i = megastream.g2(inputStream, 1);
							method75(i);
						} else if (packetType == 22) {
							int i = inputStream[1] & 0xff;
							packetType = inputStream[2] & 0xff;
							for (int i_24_ = 2; i_24_ < packetSize; i_24_++)
								inputStream[i_24_ - 2] = inputStream[i_24_];
							packetSize -= 2;
							method58(packetType, packetSize, inputStream, i);
						} else if (packetType == 23) {
							int i = inputStream[1] & 0xff;
							method59(i);
						} else if (packetType == 24) {
							int i = inputStream[1] & 0xff;
							String string = new String(inputStream, 2, 12);
							String string_25_ = new String(inputStream, 14, 12);
							method60(i, string, string_25_);
						} else if (packetType == 25) {//Games in progress
							anInt597 = (packetSize - 1) / 26;
							int i = 1;
							for (int i_26_ = 0; i_26_ < anInt597; i_26_++) {
								anIntArray598[i_26_] = megastream.g2(inputStream, i);
								i += 2;
								aStringArray599[i_26_] = new String(inputStream, i, 12).trim();
								i += 12;
								aStringArray600[i_26_] = new String(inputStream, i, 12).trim();
								i += 12;
							}
							method62(anInt597, aStringArray599, aStringArray600);
						} else
							method57(packetType, packetSize, inputStream);
						packetSize = 0;
					}
				}
			} catch (java.io.IOException ioexception) {
				method32();
				break;
			}
			break;
		} while (false);
	}

	public void method35(int i) {
		int i_27_ = anIntArray598[i];
		aClass2_582.putPacketId(25);
		aClass2_582.p2(i_27_);
		aClass2_582.flushNoException();
	}

	public void method36() {
		aClass2_582.putPacketId(25);
		aClass2_582.p2(0);
		aClass2_582.flushNoException();
	}

	public void method37(int i, int i_28_, int i_29_) {
		int i_30_ = 0;
		if (i_29_ == 0)
			i_30_ = 255;
		else if (i_29_ == 1)
			i_30_ = ratingToMask(selfPlayerRatingId);
		else if (i_29_ == 2)
			i_30_ = (ratingToMask(selfPlayerRatingId - 1) + ratingToMask(selfPlayerRatingId) + ratingToMask(selfPlayerRatingId + 1));
		else if (i_29_ == 3) {
			for (int i_31_ = 0; i_31_ <= selfPlayerRatingId; i_31_++)
				i_30_ += ratingToMask(i_31_);
		} else if (i_29_ == 4) {
			for (int i_32_ = selfPlayerRatingId; i_32_ <= 7; i_32_++)
				i_30_ += ratingToMask(i_32_);
		} else if (i_29_ == 5)
			i_30_ = 0;
		aClass2_582.putPacketId(5);
		aClass2_582.p2(i_28_);
		aClass2_582.p2(1 - i);
		aClass2_582.p2(i_30_ ^ 0xffffffff);
		aClass2_582.flushNoException();
		aLong587 = aLong586 = System.currentTimeMillis();
	}

	public boolean method38(String string) {
		if (string.toLowerCase().startsWith(clientModules[34] + " ")) {
			if (aString609 == null) {
				method63(clientModules[50]);
				aString609 = string;
				return true;
			}
			if (!aString609.equals(string)) {
				method63(clientModules[51]);
				aString609 = null;
				return true;
			}
			aString609 = null;
			aClass2_582.putPacketId(3);
			aClass2_582.putString(string);
			aClass2_582.flushNoException();
			method63(clientModules[52]);
			aLong587 = aLong586 = System.currentTimeMillis();
			return true;
		}
		if (method51(string) == 1) {
			aClass2_582.putPacketId(3);
			aClass2_582.putString(string);
			aClass2_582.flushNoException();
			aLong587 = aLong586 = System.currentTimeMillis();
		}
		if (string.toLowerCase().startsWith(clientModules[33] + " ")) {
			int i = 0;
			for (int i_33_ = clientModules[33].length() + 1; i_33_ < string.length(); i_33_++) {
				if (string.charAt(i_33_) == ' ') {
					i = i_33_ + 1;
					break;
				}
			}
			if (i == 0) {
				method63(clientModules[35]);
				return true;
			}
			string = string.replace('_', ' ');
			String string_34_ = string.substring(clientModules[33].length() + 1, i - 1);
			method63("@cha@" + clientModules[36] + " " + string_34_.trim() + ": " + string.substring(i, string.length()));
			return true;
		}
		if (aBoolean566)
			method63("@yel@" + chatNickname.trim() + ": @whi@" + string);
		return false;
	}

	public void challengePlayer(int id) {
		aClass2_582.putPacketId(6);
		aClass2_582.p2(anIntArray593[id]);
		aClass2_582.flushNoException();
	}

	public void acceptChallenge() {
		aClass2_582.putPacketId(7);
		aClass2_582.flushNoException();
	}

	public void declineChallenge() {
		aClass2_582.putPacketId(8);
		aClass2_582.flushNoException();
		anInt601 = 0;
	}

	public void sendModuleChosenPacket(int i) {
		aClass2_582.putPacketId(16);
		aClass2_582.p1(i);
		aClass2_582.flushNoException();
	}

	public void resign() {
		aClass2_582.putPacketId(14);
		aClass2_582.flushNoException();
	}

	public void offerDraw() {
		aClass2_582.putPacketId(18);
		aClass2_582.flushNoException();
	}

	private String ratingToTitle(int i) {
		if (i <= 1000)
			return clientModules[37];
		if (i <= 1200)
			return clientModules[38];
		if (i <= 1400)
			return clientModules[39];
		if (i <= 1700)
			return clientModules[40];
		if (i <= 2000)
			return clientModules[41];
		if (i <= 2500)
			return clientModules[42];
		if (i <= 3000)
			return clientModules[43];
		return clientModules[44];
	}

	private int method46(int i) {
		if (i <= 1000)
			return 0;
		if (i <= 1200)
			return 1;
		if (i <= 1400)
			return 2;
		if (i <= 1700)
			return 3;
		if (i <= 2000)
			return 4;
		if (i <= 2500)
			return 5;
		if (i <= 3000)
			return 6;
		return 7;
	}

	private int ratingToMask(int i) {
		if (i == 0)
			return 1;
		if (i == 1)
			return 2;
		if (i == 2)
			return 4;
		if (i == 3)
			return 8;
		if (i == 4)
			return 16;
		if (i == 5)
			return 32;
		if (i == 6)
			return 64;
		if (i == 7)
			return 128;
		return 0;
	}

	public boolean method48(String string) {
		int i;
		if ((i = aString614.indexOf(string)) != -1) {
			char c = aCharArray610[i];
			char c_36_ = aCharArray610[i + string.length()];
			if ((c == ' ' || c == '*') && (c_36_ == ' ' || c_36_ == '*' || c_36_ == '!'))
				return true;
		}
		return false;
	}

	public boolean method49(String string) {
		int i;
		if ((i = aString614.indexOf(string)) != -1) {
			char c = aCharArray610[i];
			if (c == ' ' || c == '*' || aString613.indexOf(string) != -1)
				return true;
		}
		return false;
	}

	public boolean method50(String string) {
		int i;
		if ((i = aString614.indexOf(string)) != -1) {
			char c = aCharArray610[i];
			if (c == ' ' || c == '*' || aString613.indexOf(string) != -1)
				return true;
		} else if ((i = aString615.indexOf(string)) != -1) {
			char c = aCharArray611[i];
			if (c == ' ' || c == '*' || aString613.indexOf(string) != -1)
				return true;
		}
		return false;
	}

	public int method51(String string) {
		try {
			if (string == null)
				return 1;
			if (string.length() > 79)
				string = string.substring(0, 79);
			aString612 = " " + string.toLowerCase() + " ";
			aString613 = "";
			aString614 = "";
			aString615 = "";
			char c = '~';
			int i = 0;
			int i_37_ = 0;
			int i_38_ = 0;
			for (int i_39_ = 0; i_39_ < aString612.length(); i_39_++) {
				char c_40_ = aString612.charAt(i_39_);
				if (c_40_ == '@' && i_39_ + 4 < aString612.length() && aString612.charAt(i_39_ + 4) == '@')
					i_39_ += 4;
				else {
					if (c_40_ == '@')
						c_40_ = 'a';
					else if (c_40_ == '$')
						c_40_ = 's';
					else if (c_40_ == '!')
						c_40_ = 'i';
					else if (c_40_ == '1')
						c_40_ = 'i';
					else if (c_40_ == '|')
						c_40_ = 'i';
					else if (c_40_ == '0')
						c_40_ = 'o';
					else if (c_40_ == '\u00a3')
						c_40_ = '*';
					else if (c_40_ == '&')
						c_40_ = '*';
					else if (c_40_ == '%')
						c_40_ = '*';
					else if (c_40_ == '#')
						c_40_ = '*';
					else if (c_40_ == '_')
						c_40_ = '*';
					else if (c_40_ == '-')
						c_40_ = '*';
					else if (c_40_ == '.')
						c_40_ = ' ';
					else if (c_40_ == ',')
						c_40_ = ' ';
					else if (c_40_ == '/')
						c_40_ = ' ';
					else if (c_40_ == '\\')
						c_40_ = ' ';
					else if (c_40_ == '<')
						c_40_ = ' ';
					else if (c_40_ == '>')
						c_40_ = ' ';
					else if (c_40_ == '(')
						c_40_ = ' ';
					else if (c_40_ == ')')
						c_40_ = ' ';
					else if (c_40_ == '{')
						c_40_ = ' ';
					else if (c_40_ == '}')
						c_40_ = ' ';
					else if (c_40_ == '3')
						c_40_ = 'e';
					else if (c_40_ == '5')
						c_40_ = 's';
					if (c_40_ != c) {
						c = c_40_;
						aString613 += c_40_;
						i_38_++;
						if (c_40_ != ' ') {
							aString614 += c_40_;
							aCharArray610[i++] = aString613.charAt(i_38_ - 2);
						}
						if (c_40_ >= 'a' && c_40_ <= 'z' || c_40_ >= '0' && c_40_ <= '9') {
							aString615 += c_40_;
							aCharArray611[i_37_++] = aString613.charAt(i_38_ - 2);
						}
					}
				}
			}
			aCharArray610[i++] = ' ';
			aCharArray611[i++] = ' ';
			if (method50("fuck"))
				return 0;
			if (method50("shit"))
				return 0;
			if (method50("wank"))
				return 0;
			if (method49("f4"))
				return 0;
			if (method49("f*4"))
				return 0;
			if (method49("cunt"))
				return 0;
			if (method49("phuck"))
				return 0;
			if (method49("f*ck"))
				return 0;
			if (method49("fu*k"))
				return 0;
			if (method49("fuc*"))
				return 0;
			if (method49("f*k"))
				return 0;
			if (method49("shlt"))
				return 0;
			if (method49("s*it"))
				return 0;
			if (method49("sh*t"))
				return 0;
			if (method49("s*t"))
				return 0;
			if (method49("naked"))
				return 0;
			if (method49("bastard"))
				return 0;
			if (method49("vagina"))
				return 0;
			if (method49("lesbian"))
				return 0;
			if (method49("lesbo"))
				return 0;
			if (method49("lezbo"))
				return 0;
			if (method49("retard"))
				return 0;
			if (method49("spastic"))
				return 0;
			if (method49("shag"))
				return 0;
			if (method49("ashole"))
				return 0;
			if (method49("asshole"))
				return 0;
			if (method49("prostitute"))
				return 0;
			if (method49("slag"))
				return 0;
			if (method49("slut"))
				return 0;
			if (method49("homo"))
				return 0;
			if (method49("hetro"))
				return 0;
			if (method49("bisex"))
				return 0;
			if (method49("fagot"))
				return 0;
			if (method49("faget"))
				return 0;
			if (method49("penis"))
				return 0;
			if (method49("sperm"))
				return 0;
			if (method48("gay"))
				return 0;
			if (method48("bitch"))
				return 0;
			if (method48("bltch"))
				return 0;
			if (method48("whore"))
				return 0;
			if (method48("toser"))
				return 0;
			if (method48("cock"))
				return 0;
			if (method48("clit"))
				return 0;
			if (method48("rape"))
				return 0;
			if (aString613.indexOf(" fu ") != -1)
				return 0;
			if (aString613.indexOf(" fuk ") != -1)
				return 0;
			if (method49("pasword"))
				return 0;
			if (method49("setpas"))
				return 0;
			if (method49("cheat"))
				return 0;
			if (method48("cencor"))
				return 0;
			if (method48("censor"))
				return 0;
			if (aString613.indexOf(" pass ") != -1)
				return 0;
			int i_41_ = 0;
			if (method49("head"))
				i_41_++;
			else if (method49("suck"))
				i_41_++;
			else if (method49("finger"))
				i_41_++;
			else if (method49("lick"))
				i_41_++;
			else if (method49("rub"))
				i_41_++;
			else if (method49("big"))
				i_41_++;
			else if (method49("have"))
				i_41_++;
			else if (method49("wet"))
				i_41_++;
			else if (method49("tease"))
				i_41_++;
			else if (aString612.indexOf(" feel ") != -1)
				i_41_++;
			if (method49("tits"))
				i_41_++;
			if (method49("arse"))
				i_41_++;
			if (method49("pusy"))
				i_41_++;
			if (method49("dick"))
				i_41_++;
			if (method49("cock"))
				i_41_++;
			if (method49("clit"))
				i_41_++;
			if (method48("sex"))
				i_41_++;
			if (i_41_ > 1)
				return 0;
		} catch (Exception exception) {
			return 0;
		}
		return 1;
	}

	public void method52(String string, String string_42_) {
		/* empty */
	}

	public void method53() {
		/* empty */
	}

	public void method54() {
		/* empty */
	}

	public void toLoginScreen() {
		/* empty */
	}

	public void method56() {
		/* empty */
	}

	public void method57(int i, int i_43_, byte[] is) {
		/* empty */
	}

	public void method58(int i, int i_44_, byte[] is, int i_45_) {
		/* empty */
	}

	public void method59(int i) {
		/* empty */
	}

	public void method60(int i, String string, String string_46_) {
		/* empty */
	}

	public void method61(int i, int[] is, int[] is_47_, String[] strings) {
		/* empty */
	}

	public void method62(int i, String[] strings, String[] strings_48_) {
		/* empty */
	}

	public void method63(String string) {
		/* empty */
	}

	public void method64(String string) {
		/* empty */
	}

	public void method65(String string, String string_49_) {
		/* empty */
	}

	public void method66(String string, String string_50_) {
		/* empty */
	}

	public void method67(String string, String string_51_, String string_52_, String string_53_) {
		/* empty */
	}

	public void method68(int i) {
		/* empty */
	}

	public void method69(int i, String[] strings, int[] is, int[] is_54_, int[] is_55_) {
		/* empty */
	}

	public boolean method70() {
		return true;
	}

	public void method71(String string, String string_56_, String string_57_) {
		/* empty */
	}

	public void method72(boolean bool) {
		/* empty */
	}

	public void method73(boolean bool) {
		/* empty */
	}

	public void method74() {
		/* empty */
	}

	public void method75(int i) {
		/* empty */
	}

	static {
		clientModules[0] = "30sec";
		clientModules[1] = "1min";
		clientModules[2] = "2min";
		clientModules[3] = "4min";
		clientModules[4] = "10min";
		clientModules[5] = "Non-rated";
		clientModules[6] = "Rated-game";
		clientModules[7] = "You must enter both a username and a password";
		clientModules[8] = "Please try again";
		clientModules[9] = "Please wait...";
		clientModules[10] = "Connecting to server";
		clientModules[11] = "Sorry! The server is currently full";
		clientModules[12] = "Please try again later";
		clientModules[13] = "Invalid username or password";
		clientModules[14] = "Try again, or create a new account";
		clientModules[15] = "Sorry! Unable to connect to server";
		clientModules[16] = "Username already taken";
		clientModules[17] = "Please choose another username";
		clientModules[18] = "Player busy";
		clientModules[19] = "Other player is busy";
		clientModules[20] = "with another challenge";
		clientModules[21] = "Challenge declined";
		clientModules[22] = "Other player has";
		clientModules[23] = "declined your challenge";
		clientModules[24] = "Challenge aborted";
		clientModules[25] = "Other player has";
		clientModules[26] = "aborted the challenge";
		clientModules[27] = "You won!";
		clientModules[28] = "points gained:";
		clientModules[29] = "You lost!";
		clientModules[30] = "points lost:";
		clientModules[31] = "Non-rated game";
		clientModules[32] = "Your opponent has offered a draw";
		clientModules[33] = "tell";
		clientModules[34] = "setpass";
		clientModules[35] = "You must type a message too!";
		clientModules[36] = "You tell";
		clientModules[37] = "Beginner";
		clientModules[38] = "Novice";
		clientModules[39] = "Apprentice";
		clientModules[40] = "Intermediate";
		clientModules[41] = "Experienced";
		clientModules[42] = "Expert";
		clientModules[43] = "Master";
		clientModules[44] = "Grandmaster";
		clientModules[45] = "move";
		clientModules[46] = "Connection lost! Please wait...";
		clientModules[47] = "Attempting to re-establish";
		clientModules[48] = "That username is already in use";
		clientModules[49] = "Wait 60 seconds then retry";
		clientModules[50] = "Please enter the same command a 2nd time to confirm password change";
		clientModules[51] = "That does not match the 1st code you entered. Please try again";
		clientModules[52] = "Changing password...";
		aCharArray610 = new char[80];
		aCharArray611 = new char[80];
	}
}
