package jagex;

import java.awt.Event;
import java.awt.Frame;
import java.awt.Graphics;

public class viewbox extends Frame {
	int width;
	int height;
	int heightOffset = 28;
	gameshell gameShell;
	Graphics graphics;

	public viewbox(gameshell gs, int w, int h, String title, boolean resizable, boolean usingApplet) {
		width = w;
		height = h;
		gameShell = gs;
		if (usingApplet)
			heightOffset = 48;
		else
			heightOffset = 28;
		this.setTitle(title);
		this.setResizable(resizable);
		this.show();
		this.toFront();
		resize(width, height);
		graphics = getGraphics();
	}

	public Graphics getGraphics() {
		Graphics graphics = super.getGraphics();
		graphics.translate(0, 24);
		return graphics;
	}

	public void resize(int width, int height) {
		super.resize(width, height + heightOffset);
	}

	public int getWidth() {
		return this.size().width;
	}

	public int getHeight() {
		return this.size().height - heightOffset;
	}

	public boolean handleEvent(Event event) {
		if (event.id == 401)
			gameShell.keyDown(event, event.key);
		else if (event.id == 402)
			gameShell.keyUp(event, event.key);
		else if (event.id == 501)
			gameShell.mouseDown(event, event.x, event.y - 24);
		else if (event.id == 506)
			gameShell.mouseDrag(event, event.x, event.y - 24);
		else if (event.id == 502)
			gameShell.mouseUp(event, event.x, event.y - 24);
		else if (event.id == 503)
			gameShell.mouseMove(event, event.x, event.y - 24);
		else if (event.id == 201)
			gameShell.destroy();
		else if (event.id == 1001)
			gameShell.action(event, event.target);
		else if (event.id == 403)
			gameShell.keyDown(event, event.key);
		else if (event.id == 404)
			gameShell.keyUp(event, event.key);
		return true;
	}

	public final void paint(Graphics graphics) {
		gameShell.paint(graphics);
	}
}
