package jagex;

public class gui {
	protected pixmap aClass6_388;
	int componentCount;
	int anInt390;
	boolean[] componentEnabled;
	boolean[] aBooleanArray392;
	boolean[] componentTextCensored;
	boolean[] componentClicked;
	boolean[] aBooleanArray395;
	public int[] anIntArray396;
	public int[] anIntArray397;
	public int[] componentSelectedIndex;
	public int[] anIntArray399;
	boolean[] componentUseSecondaryColor;
	int[] componentX;
	int[] componentY;
	int[] componentType;
	int[] componentWidth;
	int[] componentHeight;
	int[] componentMaxCharacters;
	int[] componentFont;
	String[] componentText;
	String[][] aStringArrayArray409;
	int mouseX;
	int mouseY;
	int anInt412;
	int anInt413;
	int inputComponentId = -1;
	int mouseOffsetX;
	int mouseOffsetY;
	int loopCycle;
	public boolean aBoolean418 = true;
	int anInt419;
	int anInt420;
	int anInt421;
	int anInt422;
	int anInt423;
	int anInt424;
	int anInt425;
	int anInt426;
	int anInt427;
	int anInt428;
	int anInt429;
	int anInt430;
	public boolean aBoolean431 = true;
	public static boolean aBoolean432;
	public static boolean aBoolean433 = true;
	public static int anInt434;
	public static int anInt435 = 114;
	public static int anInt436 = 114;
	public static int anInt437 = 176;
	public static int anInt438;

	public gui(pixmap class6, int i) {
		aClass6_388 = class6;
		anInt390 = i;
		componentEnabled = new boolean[i];
		aBooleanArray392 = new boolean[i];
		componentTextCensored = new boolean[i];
		componentClicked = new boolean[i];
		componentUseSecondaryColor = new boolean[i];
		aBooleanArray395 = new boolean[i];
		anIntArray396 = new int[i];
		anIntArray397 = new int[i];
		componentSelectedIndex = new int[i];
		anIntArray399 = new int[i];
		componentX = new int[i];
		componentY = new int[i];
		componentType = new int[i];
		componentWidth = new int[i];
		componentHeight = new int[i];
		componentMaxCharacters = new int[i];
		componentFont = new int[i];
		componentText = new String[i];
		aStringArrayArray409 = new String[i][];
		anInt419 = method305(114, 114, 176);
		anInt420 = method305(14, 14, 62);
		anInt421 = method305(200, 208, 232);
		anInt422 = method305(96, 129, 184);
		anInt423 = method305(53, 95, 115);
		anInt424 = method305(117, 142, 171);
		anInt425 = method305(98, 122, 158);
		anInt426 = method305(86, 100, 136);
		anInt427 = method305(135, 146, 179);
		anInt428 = method305(97, 112, 151);
		anInt429 = method305(88, 102, 136);
		anInt430 = method305(84, 93, 120);
	}

	public int method305(int i, int i_0_, int i_1_) {
		return pixmap.method247(anInt435 * i / 114, anInt436 * i_0_ / 114, anInt437 * i_1_ / 176);
	}

	public void enableComponent(int i) {
		componentEnabled[i] = true;
	}

	public void disableComponent(int i) {
		componentEnabled[i] = false;
	}

	public void method308() {
		anInt412 = 0;
	}

	public void process(int mx, int my, int i_3_, int i_4_) {
		loopCycle++;
		mouseX = mx - mouseOffsetX;
		mouseY = my - mouseOffsetY;
		anInt413 = i_4_;
		if (i_3_ != 0)
			anInt412 = i_3_;
		if (i_3_ == 1) {
			for (int id = 0; id < componentCount; id++) {
				if (componentEnabled[id] && componentType[id] == 10 && mouseX >= componentX[id] && mouseY >= componentY[id] && mouseX <= componentX[id] + componentWidth[id] && mouseY <= componentY[id] + componentHeight[id])
					componentClicked[id] = true;
			}
		}
	}

	public void method310() {
		anInt412 = 0;
		anInt413 = 0;
	}

	public boolean isComponentClicked(int id) {
		if (componentEnabled[id] && componentClicked[id]) {
			componentClicked[id] = false;
			return true;
		}
		return false;
	}

	public void processInput(int key) {
		if (key != 0) {
			if (inputComponentId != -1 && componentText[inputComponentId] != null && componentEnabled[inputComponentId]) {
				int i_6_ = componentText[inputComponentId].length();
				if (key == 8 && i_6_ > 0)
					componentText[inputComponentId] = componentText[inputComponentId].substring(0, i_6_ - 1);
				if ((key == 10 || key == 13) && i_6_ > 0)
					componentClicked[inputComponentId] = true;
				String string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| ";
				if (i_6_ < componentMaxCharacters[inputComponentId]) {
					for (int i_7_ = 0; i_7_ < string.length(); i_7_++) {
						if (key == string.charAt(i_7_))
							componentText[inputComponentId] += (char) key;
					}
				}
				aBooleanArray395[inputComponentId] = true;
				if (key == 9) {
					do {
						inputComponentId = (inputComponentId + 1) % componentCount;
						if (componentType[inputComponentId] == 5)
							break;
					} while (componentType[inputComponentId] != 6);
					aBooleanArray395[inputComponentId] = true;
				}
			}
		}
	}

	public void setInputComponentId(int id) {
		inputComponentId = id;
	}

	public void method314(int i, int i_8_, int i_9_, int i_10_) {
		if (aBoolean432) {
			aClass6_388.setClip(i, i_8_, i_9_, i_10_);
			for (int i_11_ = 0; i_11_ < componentCount; i_11_++) {
				if (componentEnabled[i_11_]) {
					if (componentType[i_11_] == 0)
						method318(i_11_, componentX[i_11_], componentY[i_11_], componentText[i_11_], componentFont[i_11_]);
					else if (componentType[i_11_] == 1)
						method318(i_11_, (componentX[i_11_] - aClass6_388.method272(componentText[i_11_], componentFont[i_11_]) / 2), componentY[i_11_], componentText[i_11_], componentFont[i_11_]);
					else if (componentType[i_11_] == 2)
						method321(componentX[i_11_], componentY[i_11_], componentWidth[i_11_], componentHeight[i_11_]);
					else if (componentType[i_11_] == 3)
						method324(componentX[i_11_], componentY[i_11_], componentWidth[i_11_]);
					else if (componentType[i_11_] == 11)
						method322(componentX[i_11_], componentY[i_11_], componentWidth[i_11_], componentHeight[i_11_]);
					else if (componentType[i_11_] == 12)
						method323(componentX[i_11_], componentY[i_11_], componentFont[i_11_]);
				}
			}
			aClass6_388.resetClip();
		}
	}

	public void draw() {
		for (int i = 0; i < componentCount; i++) {
			if (componentEnabled[i]) {
				if (!aBoolean432) {
					if (componentType[i] == 0)
						method318(i, componentX[i], componentY[i], componentText[i], componentFont[i]);
					else if (componentType[i] == 1)
						method318(i, (componentX[i] - (aClass6_388.method272(componentText[i], componentFont[i]) / 2)), componentY[i], componentText[i], componentFont[i]);
					else if (componentType[i] == 2)
						method321(componentX[i], componentY[i], componentWidth[i], componentHeight[i]);
					else if (componentType[i] == 3)
						method324(componentX[i], componentY[i], componentWidth[i]);
					else if (componentType[i] == 11)
						method322(componentX[i], componentY[i], componentWidth[i], componentHeight[i]);
					else if (componentType[i] == 12)
						method323(componentX[i], componentY[i], componentFont[i]);
				}
				if (componentType[i] == 4)
					method325(i, componentX[i], componentY[i], componentWidth[i], componentHeight[i], componentFont[i], aStringArrayArray409[i], anIntArray397[i], anIntArray396[i]);
				else if (componentType[i] == 5 || componentType[i] == 6)
					method320(i, componentX[i], componentY[i], componentWidth[i], componentHeight[i], componentText[i], componentFont[i]);
				else if (componentType[i] == 7)
					method327(i, componentX[i], componentY[i], componentFont[i], aStringArrayArray409[i]);
				else if (componentType[i] == 8)
					method328(i, componentX[i], componentY[i], componentFont[i], aStringArrayArray409[i]);
				else if (componentType[i] == 14)
					method329(i, componentX[i], componentY[i], componentFont[i], aStringArrayArray409[i], componentWidth[i]);
				else if (componentType[i] == 9)
					method330(i, componentX[i], componentY[i], componentWidth[i], componentHeight[i], componentFont[i], aStringArrayArray409[i], anIntArray397[i], anIntArray396[i]);
			}
		}
		anInt412 = 0;
	}

	public int getSelectedIndex(int i) {
		return componentSelectedIndex[i];
	}

	public void setSelectedIndex(int i, int i_12_) {
		componentSelectedIndex[i] = i_12_;
	}

	protected void method318(int i, int i_13_, int i_14_, String string, int i_15_) {
		int i_16_ = i_14_ + aClass6_388.method271(i_15_) / 3;
		method319(i, i_13_, i_16_, string, i_15_);
	}

	protected void method319(int i, int i_17_, int i_18_, String string, int i_19_) {
		int i_20_;
		if (componentUseSecondaryColor[i])
			i_20_ = 16777215;
		else
			i_20_ = 0;
		aClass6_388.method268(string, i_17_, i_18_, i_19_, i_20_);
	}

	protected void method320(int i, int i_21_, int i_22_, int i_23_, int i_24_, String string, int i_25_) {
		if (componentTextCensored[i]) {
			int textLength = string.length();
			string = "";
			for (int id = 0; id < textLength; id++)
				string += "X";
		}
		if (componentType[i] == 5) {
			if (anInt412 == 1 && mouseX >= i_21_ && mouseY >= i_22_ - i_24_ / 2 && mouseX <= i_21_ + i_23_ && mouseY <= i_22_ + i_24_ / 2) {
				aBooleanArray395[inputComponentId] = true;
				aBooleanArray395[i] = true;
				inputComponentId = i;
			}
			if (aBooleanArray395[i]) {
				method314(i_21_, i_22_ - i_24_ / 2, i_21_ + i_23_, i_22_ + i_24_ / 2);
				aBooleanArray395[i] = false;
			}
		} else if (componentType[i] == 6) {
			if (anInt412 == 1 && mouseX >= i_21_ - i_23_ / 2 && mouseY >= i_22_ - i_24_ / 2 && mouseX <= i_21_ + i_23_ / 2 && mouseY <= i_22_ + i_24_ / 2) {
				aBooleanArray395[inputComponentId] = true;
				aBooleanArray395[i] = true;
				inputComponentId = i;
			}
			if (aBooleanArray395[i]) {
				method314(i_21_ - i_23_ / 2, i_22_ - i_24_ / 2, i_21_ + i_23_ / 2, i_22_ + i_24_ / 2);
				aBooleanArray395[i] = false;
			}
			i_21_ -= aClass6_388.method272(string, i_25_) / 2;
		}
		if (inputComponentId == i && (loopCycle % 40 < 20 || !aBoolean418))
			string += "*";
		int i_28_ = i_22_ + aClass6_388.method271(i_25_) / 3;
		method319(i, i_21_, i_28_, string, i_25_);
	}

	public void method321(int i, int i_29_, int i_30_, int i_31_) {
		aClass6_388.setClip(i, i_29_, i + i_30_, i_29_ + i_31_);
		aClass6_388.method241(i, i_29_, i_30_, i_31_, anInt430, anInt427);
		if (aBoolean433) {
			for (int i_32_ = i - (i_29_ & 0x3f); i_32_ < i + i_30_; i_32_ += 128) {
				for (int i_33_ = i_29_ - (i_29_ & 0x1f); i_33_ < i_29_ + i_31_; i_33_ += 128)
					aClass6_388.method258(i_32_, i_33_, 6 + anInt434);
			}
		}
		aClass6_388.method244(i, i_29_, i_30_, anInt427);
		aClass6_388.method244(i + 1, i_29_ + 1, i_30_ - 2, anInt427);
		aClass6_388.method244(i + 2, i_29_ + 2, i_30_ - 4, anInt428);
		aClass6_388.method245(i, i_29_, i_31_, anInt427);
		aClass6_388.method245(i + 1, i_29_ + 1, i_31_ - 2, anInt427);
		aClass6_388.method245(i + 2, i_29_ + 2, i_31_ - 4, anInt428);
		aClass6_388.method244(i, i_29_ + i_31_ - 1, i_30_, anInt430);
		aClass6_388.method244(i + 1, i_29_ + i_31_ - 2, i_30_ - 2, anInt430);
		aClass6_388.method244(i + 2, i_29_ + i_31_ - 3, i_30_ - 4, anInt429);
		aClass6_388.method245(i + i_30_ - 1, i_29_, i_31_, anInt430);
		aClass6_388.method245(i + i_30_ - 2, i_29_ + 1, i_31_ - 2, anInt430);
		aClass6_388.method245(i + i_30_ - 3, i_29_ + 2, i_31_ - 4, anInt429);
		aClass6_388.resetClip();
	}

	public void method322(int i, int i_34_, int i_35_, int i_36_) {
		aClass6_388.method242(i, i_34_, i_35_, i_36_, 0);
		aClass6_388.method243(i, i_34_, i_35_, i_36_, anInt424);
		aClass6_388.method243(i + 1, i_34_ + 1, i_35_ - 2, i_36_ - 2, anInt425);
		aClass6_388.method243(i + 2, i_34_ + 2, i_35_ - 4, i_36_ - 4, anInt426);
		aClass6_388.method255(i, i_34_, 2 + anInt434);
		aClass6_388.method255(i + i_35_ - 7, i_34_, 3 + anInt434);
		aClass6_388.method255(i, i_34_ + i_36_ - 7, 4 + anInt434);
		aClass6_388.method255(i + i_35_ - 7, i_34_ + i_36_ - 7, 5 + anInt434);
	}

	protected void method323(int i, int i_37_, int i_38_) {
		aClass6_388.method255(i, i_37_, i_38_);
	}

	protected void method324(int i, int i_39_, int i_40_) {
		aClass6_388.method244(i, i_39_, i_40_, 0);
	}

	protected void method325(int i, int i_41_, int i_42_, int i_43_, int i_44_, int i_45_, String[] strings, int i_46_, int i_47_) {
		int i_48_ = i_44_ / aClass6_388.method271(i_45_);
		if (i_47_ > i_46_ - i_48_)
			i_47_ = i_46_ - i_48_;
		if (i_47_ < 0)
			i_47_ = 0;
		anIntArray396[i] = i_47_;
		if (i_48_ < i_46_) {
			int i_49_ = i_41_ + i_43_ - 12;
			int i_50_ = (i_44_ - 27) * i_48_ / i_46_;
			if (i_50_ < 6)
				i_50_ = 6;
			int i_51_ = (i_44_ - 27 - i_50_) * i_47_ / (i_46_ - i_48_);
			if (anInt413 == 1 && mouseX >= i_49_ && mouseX <= i_49_ + 12) {
				if (mouseY > i_42_ && mouseY < i_42_ + 12 && i_47_ > 0)
					i_47_--;
				if (mouseY > i_42_ + i_44_ - 12 && mouseY < i_42_ + i_44_ && i_47_ < i_46_ - i_48_)
					i_47_++;
				anIntArray396[i] = i_47_;
				aBooleanArray395[i] = true;
			}
			if (anInt413 == 1 && (mouseX >= i_49_ && mouseX <= i_49_ + 12 || (mouseX >= i_49_ - 12 && mouseX <= i_49_ + 24 && aBooleanArray392[i]))) {
				if (mouseY > i_42_ + 12 && mouseY < i_42_ + i_44_ - 12) {
					aBooleanArray392[i] = true;
					int i_52_ = mouseY - i_42_ - 12 - i_50_ / 2;
					i_47_ = i_52_ * i_46_ / (i_44_ - 24);
					if (i_47_ > i_46_ - i_48_)
						i_47_ = i_46_ - i_48_;
					if (i_47_ < 0)
						i_47_ = 0;
					anIntArray396[i] = i_47_;
					aBooleanArray395[i] = true;
				}
			} else
				aBooleanArray392[i] = false;
			if (aBooleanArray395[i]) {
				method314(i_41_, i_42_, i_41_ + i_43_, i_42_ + i_44_);
				aBooleanArray395[i] = false;
			}
			i_51_ = (i_44_ - 27 - i_50_) * i_47_ / (i_46_ - i_48_);
			method326(i_41_, i_42_, i_43_, i_44_, i_51_, i_50_);
		} else {
			i_47_ = 0;
			anIntArray396[i] = 0;
		}
		if (aBooleanArray395[i]) {
			method314(i_41_, i_42_, i_41_ + i_43_, i_42_ + i_44_);
			aBooleanArray395[i] = false;
		}
		int i_53_ = i_44_ - i_48_ * aClass6_388.method271(i_45_);
		int i_54_ = i_42_ + aClass6_388.method271(i_45_) * 5 / 6 + i_53_ / 2;
		for (int i_55_ = i_47_; i_55_ < i_46_; i_55_++) {
			method319(i, i_41_ + 2, i_54_, strings[i_55_], i_45_);
			i_54_ += aClass6_388.method271(i_45_) - anInt438;
			if (i_54_ >= i_42_ + i_44_)
				break;
		}
	}

	protected void method326(int i, int i_56_, int i_57_, int i_58_, int i_59_, int i_60_) {
		int i_61_ = i + i_57_ - 12;
		aClass6_388.method243(i_61_, i_56_, 12, i_58_, 0);
		aClass6_388.method255(i_61_ + 1, i_56_ + 1, anInt434);
		aClass6_388.method255(i_61_ + 1, i_56_ + i_58_ - 12, 1 + anInt434);
		aClass6_388.method244(i_61_, i_56_ + 13, 12, 0);
		aClass6_388.method244(i_61_, i_56_ + i_58_ - 13, 12, 0);
		aClass6_388.method241(i_61_ + 1, i_56_ + 14, 11, i_58_ - 27, anInt419, anInt420);
		aClass6_388.method242(i_61_ + 3, i_59_ + i_56_ + 14, 7, i_60_, anInt422);
		aClass6_388.method245(i_61_ + 2, i_59_ + i_56_ + 14, i_60_, anInt421);
		aClass6_388.method245(i_61_ + 2 + 8, i_59_ + i_56_ + 14, i_60_, anInt423);
	}

	protected void method327(int i, int i_62_, int i_63_, int i_64_, String[] strings) {
		int i_65_ = 0;
		int i_66_ = strings.length;
		for (int i_67_ = 0; i_67_ < i_66_; i_67_++) {
			i_65_ += aClass6_388.method272(strings[i_67_], i_64_);
			if (i_67_ < i_66_ - 1)
				i_65_ += aClass6_388.method272("  ", i_64_);
		}
		int i_68_ = i_62_ - i_65_ / 2;
		int i_69_ = i_63_ + aClass6_388.method271(i_64_) / 3;
		for (int i_70_ = 0; i_70_ < i_66_; i_70_++) {
			int i_71_;
			if (componentUseSecondaryColor[i])
				i_71_ = 16777215;
			else
				i_71_ = 0;
			if (mouseX >= i_68_ && mouseX <= i_68_ + aClass6_388.method272(strings[i_70_], i_64_) && mouseY <= i_69_ && mouseY > i_69_ - aClass6_388.method271(i_64_)) {
				if (componentUseSecondaryColor[i])
					i_71_ = 8421504;
				else
					i_71_ = 16777215;
				if (anInt412 == 1) {
					componentSelectedIndex[i] = i_70_;
					componentClicked[i] = true;
				}
			}
			if (componentSelectedIndex[i] == i_70_) {
				if (componentUseSecondaryColor[i])
					i_71_ = 16711680;
				else
					i_71_ = 12582912;
			}
			aClass6_388.method268(strings[i_70_], i_68_, i_69_, i_64_, i_71_);
			i_68_ += aClass6_388.method272(strings[i_70_] + "  ", i_64_);
		}
	}

	protected void method328(int i, int i_72_, int i_73_, int i_74_, String[] strings) {
		int i_75_ = strings.length;
		int i_76_ = i_73_ - aClass6_388.method271(i_74_) * (i_75_ - 1) / 2;
		for (int i_77_ = 0; i_77_ < i_75_; i_77_++) {
			int i_78_;
			if (componentUseSecondaryColor[i])
				i_78_ = 16777215;
			else
				i_78_ = 0;
			int i_79_ = aClass6_388.method272(strings[i_77_], i_74_);
			if (mouseX >= i_72_ - i_79_ / 2 && mouseX <= i_72_ + i_79_ / 2 && mouseY - 2 <= i_76_ && mouseY - 2 > i_76_ - aClass6_388.method271(i_74_)) {
				if (componentUseSecondaryColor[i])
					i_78_ = 8421504;
				else
					i_78_ = 16777215;
				if (anInt412 == 1) {
					componentSelectedIndex[i] = i_77_;
					componentClicked[i] = true;
				}
			}
			if (componentSelectedIndex[i] == i_77_) {
				if (componentUseSecondaryColor[i])
					i_78_ = 16711680;
				else
					i_78_ = 12582912;
			}
			aClass6_388.method268(strings[i_77_], i_72_ - i_79_ / 2, i_76_, i_74_, i_78_);
			i_76_ += aClass6_388.method271(i_74_);
		}
	}

	protected void method329(int i, int i_80_, int i_81_, int i_82_, String[] strings, int i_83_) {
		int i_84_ = strings.length;
		int i_85_ = (i_84_ + 1) / 2;
		int i_86_ = i_81_ - aClass6_388.method271(i_82_) * (i_85_ - 1) / 2;
		for (int i_87_ = 0; i_87_ < i_84_; i_87_++) {
			if (i_87_ == i_85_)
				i_86_ = i_81_ - aClass6_388.method271(i_82_) * (i_85_ - 1) / 2;
			int i_88_;
			if (i_87_ < i_85_)
				i_88_ = i_80_ - i_83_;
			else
				i_88_ = i_80_ + i_83_;
			int i_89_;
			if (componentUseSecondaryColor[i])
				i_89_ = 16777215;
			else
				i_89_ = 0;
			int i_90_ = aClass6_388.method272(strings[i_87_], i_82_);
			if (mouseX >= i_88_ - i_90_ / 2 && mouseX <= i_88_ + i_90_ / 2 && mouseY - 2 <= i_86_ && mouseY - 2 > i_86_ - aClass6_388.method271(i_82_)) {
				if (componentUseSecondaryColor[i])
					i_89_ = 8421504;
				else
					i_89_ = 16777215;
				if (anInt412 == 1) {
					componentSelectedIndex[i] = i_87_;
					componentClicked[i] = true;
				}
			}
			if (componentSelectedIndex[i] == i_87_) {
				if (componentUseSecondaryColor[i])
					i_89_ = 16711680;
				else
					i_89_ = 12582912;
			}
			aClass6_388.method268(strings[i_87_], i_88_ - i_90_ / 2, i_86_, i_82_, i_89_);
			i_86_ += aClass6_388.method271(i_82_);
		}
	}

	protected void method330(int i, int i_91_, int i_92_, int i_93_, int i_94_, int i_95_, String[] strings, int i_96_, int i_97_) {
		int i_98_ = i_94_ / aClass6_388.method271(i_95_);
		if (i_98_ < i_96_) {
			int i_99_ = i_91_ + i_93_ - 12;
			int i_100_ = (i_94_ - 27) * i_98_ / i_96_;
			if (i_100_ < 6)
				i_100_ = 6;
			int i_101_ = (i_94_ - 27 - i_100_) * i_97_ / (i_96_ - i_98_);
			if (anInt413 == 1 && mouseX >= i_99_ && mouseX <= i_99_ + 12) {
				if (mouseY > i_92_ && mouseY < i_92_ + 12 && i_97_ > 0)
					i_97_--;
				if (mouseY > i_92_ + i_94_ - 12 && mouseY < i_92_ + i_94_ && i_97_ < i_96_ - i_98_)
					i_97_++;
				anIntArray396[i] = i_97_;
				aBooleanArray395[i] = true;
			}
			if (anInt413 == 1 && (mouseX >= i_99_ && mouseX <= i_99_ + 12 || (mouseX >= i_99_ - 12 && mouseX <= i_99_ + 24 && aBooleanArray392[i]))) {
				if (mouseY > i_92_ + 12 && mouseY < i_92_ + i_94_ - 12) {
					aBooleanArray392[i] = true;
					int i_102_ = mouseY - i_92_ - 12 - i_100_ / 2;
					i_97_ = i_102_ * i_96_ / (i_94_ - 24);
					if (i_97_ < 0)
						i_97_ = 0;
					if (i_97_ > i_96_ - i_98_)
						i_97_ = i_96_ - i_98_;
					anIntArray396[i] = i_97_;
					aBooleanArray395[i] = true;
				}
			} else
				aBooleanArray392[i] = false;
			if (aBooleanArray395[i]) {
				method314(i_91_, i_92_, i_91_ + i_93_, i_92_ + i_94_);
				aBooleanArray395[i] = false;
			}
			i_101_ = (i_94_ - 27 - i_100_) * i_97_ / (i_96_ - i_98_);
			method326(i_91_, i_92_, i_93_, i_94_, i_101_, i_100_);
		} else {
			i_97_ = 0;
			anIntArray396[i] = 0;
		}
		if (aBooleanArray395[i]) {
			method314(i_91_, i_92_, i_91_ + i_93_, i_92_ + i_94_);
			aBooleanArray395[i] = false;
		}
		anIntArray399[i] = -1;
		int i_103_ = i_94_ - i_98_ * aClass6_388.method271(i_95_);
		int i_104_ = i_92_ + aClass6_388.method271(i_95_) * 5 / 6 + i_103_ / 2;
		for (int i_105_ = i_97_; i_105_ < i_96_; i_105_++) {
			int i_106_;
			if (componentUseSecondaryColor[i])
				i_106_ = 16777215;
			else
				i_106_ = 0;
			if (mouseX >= i_91_ + 2 && (mouseX <= i_91_ + 2 + aClass6_388.method272(strings[i_105_], i_95_)) && mouseY - 2 <= i_104_ && mouseY - 2 > i_104_ - aClass6_388.method271(i_95_)) {
				if (componentUseSecondaryColor[i])
					i_106_ = 8421504;
				else
					i_106_ = 16777215;
				anIntArray399[i] = i_105_;
				if (anInt412 == 1) {
					componentSelectedIndex[i] = i_105_;
					componentClicked[i] = true;
				}
			}
			if (componentSelectedIndex[i] == i_105_ && aBoolean431)
				i_106_ = 16711680;
			aClass6_388.method268(strings[i_105_], i_91_ + 2, i_104_, i_95_, i_106_);
			i_104_ += aClass6_388.method271(i_95_);
			if (i_104_ >= i_92_ + i_94_)
				break;
		}
	}

	public int method331(int i, int i_107_, String string, int i_108_, boolean bool) {
		componentType[componentCount] = 0;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentFont[componentCount] = i_108_;
		componentUseSecondaryColor[componentCount] = bool;
		componentX[componentCount] = i;
		componentY[componentCount] = i_107_;
		componentText[componentCount] = string;
		return componentCount++;
	}

	public int createText(int x, int y, String text, int font, boolean secondaryColor) {
		componentType[componentCount] = 1;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentFont[componentCount] = font;
		componentUseSecondaryColor[componentCount] = secondaryColor;
		componentX[componentCount] = x;
		componentY[componentCount] = y;
		componentText[componentCount] = text;
		return componentCount++;
	}

	public void setText(int i, String string) {
		componentText[i] = string;
		aBooleanArray395[i] = true;
	}

	public String getText(int i) {
		if (componentText[i] == null)
			return "null";
		return componentText[i];
	}

	public int createButton(int x, int y, int width, int height) {
		componentType[componentCount] = 2;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentX[componentCount] = x;
		componentY[componentCount] = y;
		componentWidth[componentCount] = width;
		componentHeight[componentCount] = height;
		return componentCount++;
	}

	public int createCenteredButton(int x, int y, int width, int height) {
		componentType[componentCount] = 2;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentX[componentCount] = x - width / 2;
		componentY[componentCount] = y - height / 2;
		componentWidth[componentCount] = width;
		componentHeight[componentCount] = height;
		return componentCount++;
	}

	public int createRectangle(int x, int y, int width, int height) {
		componentType[componentCount] = 11;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentX[componentCount] = x;
		componentY[componentCount] = y;
		componentWidth[componentCount] = width;
		componentHeight[componentCount] = height;
		return componentCount++;
	}

	public int createCenteredRectangle(int x, int y, int width, int height) {
		componentType[componentCount] = 11;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentX[componentCount] = x - width / 2;
		componentY[componentCount] = y - height / 2;
		componentWidth[componentCount] = width;
		componentHeight[componentCount] = height;
		return componentCount++;
	}

	public int createHorizontalLine(int x, int y, int width) {
		componentType[componentCount] = 3;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentX[componentCount] = x;
		componentY[componentCount] = y;
		componentWidth[componentCount] = width;
		return componentCount++;
	}

	public int method340(int i, int i_125_, int i_126_, int i_127_, int i_128_, int i_129_, boolean bool) {
		componentType[componentCount] = 4;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentX[componentCount] = i;
		componentY[componentCount] = i_125_;
		componentWidth[componentCount] = i_126_;
		componentHeight[componentCount] = i_127_;
		componentUseSecondaryColor[componentCount] = bool;
		componentFont[componentCount] = i_128_;
		componentMaxCharacters[componentCount] = i_129_;
		anIntArray397[componentCount] = 0;
		anIntArray396[componentCount] = 0;
		aStringArrayArray409[componentCount] = new String[i_129_];
		return componentCount++;
	}

	public void method341(int i, String string, boolean bool) {
		int i_130_ = anIntArray397[i]++;
		if (i_130_ >= componentMaxCharacters[i]) {
			i_130_--;
			anIntArray397[i]--;
			for (int i_131_ = 0; i_131_ < i_130_; i_131_++)
				aStringArrayArray409[i][i_131_] = aStringArrayArray409[i][i_131_ + 1];
		}
		aStringArrayArray409[i][i_130_] = string;
		if (bool)
			anIntArray396[i] = 999999;
		aBooleanArray395[i] = true;
	}

	public int method342(int i, int i_132_, int i_133_, int i_134_, int i_135_, int i_136_, boolean bool, boolean bool_137_) {
		componentType[componentCount] = 5;
		componentEnabled[componentCount] = true;
		componentTextCensored[componentCount] = bool;
		componentClicked[componentCount] = false;
		componentFont[componentCount] = i_135_;
		componentUseSecondaryColor[componentCount] = bool_137_;
		componentX[componentCount] = i;
		componentY[componentCount] = i_132_;
		componentWidth[componentCount] = i_133_;
		componentHeight[componentCount] = i_134_;
		componentMaxCharacters[componentCount] = i_136_;
		componentText[componentCount] = "";
		return componentCount++;
	}

	public int createInputBox(int x, int y, int width, int height, int font, int maxLength, boolean textCensored, boolean secondaryColor) {
		componentType[componentCount] = 6;
		componentEnabled[componentCount] = true;
		componentTextCensored[componentCount] = textCensored;
		componentClicked[componentCount] = false;
		componentFont[componentCount] = font;
		componentUseSecondaryColor[componentCount] = secondaryColor;
		componentX[componentCount] = x;
		componentY[componentCount] = y;
		componentWidth[componentCount] = width;
		componentHeight[componentCount] = height;
		componentMaxCharacters[componentCount] = maxLength;
		componentText[componentCount] = "";
		return componentCount++;
	}

	public int method344(int i, int i_144_, String[] strings, int i_145_, boolean bool) {
		componentType[componentCount] = 7;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentFont[componentCount] = i_145_;
		componentUseSecondaryColor[componentCount] = bool;
		componentX[componentCount] = i;
		componentY[componentCount] = i_144_;
		aStringArrayArray409[componentCount] = strings;
		componentSelectedIndex[componentCount] = 0;
		return componentCount++;
	}

	public int method345(int i, int i_146_, String[] strings, int i_147_, boolean bool) {
		componentType[componentCount] = 8;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentFont[componentCount] = i_147_;
		componentUseSecondaryColor[componentCount] = bool;
		componentX[componentCount] = i;
		componentY[componentCount] = i_146_;
		aStringArrayArray409[componentCount] = strings;
		componentSelectedIndex[componentCount] = 0;
		return componentCount++;
	}

	public int method346(int i, int i_148_, String[] strings, int i_149_, boolean bool, int i_150_) {
		componentType[componentCount] = 14;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentFont[componentCount] = i_149_;
		componentUseSecondaryColor[componentCount] = bool;
		componentX[componentCount] = i;
		componentY[componentCount] = i_148_;
		aStringArrayArray409[componentCount] = strings;
		componentSelectedIndex[componentCount] = 0;
		componentWidth[componentCount] = i_150_;
		return componentCount++;
	}

	public int method347(int i, int i_151_, int i_152_, int i_153_, int i_154_, int i_155_, boolean bool) {
		componentType[componentCount] = 9;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentFont[componentCount] = i_154_;
		componentUseSecondaryColor[componentCount] = bool;
		componentX[componentCount] = i;
		componentY[componentCount] = i_151_;
		componentWidth[componentCount] = i_152_;
		componentHeight[componentCount] = i_153_;
		componentMaxCharacters[componentCount] = i_155_;
		aStringArrayArray409[componentCount] = new String[i_155_];
		anIntArray397[componentCount] = 0;
		anIntArray396[componentCount] = 0;
		componentSelectedIndex[componentCount] = -1;
		anIntArray399[componentCount] = -1;
		return componentCount++;
	}

	public void method348(int i) {
		anIntArray397[i] = 0;
		aBooleanArray395[i] = true;
	}

	public void method349(int i, int i_156_, String string) {
		aStringArrayArray409[i][i_156_] = string;
		if (i_156_ + 1 > anIntArray397[i])
			anIntArray397[i] = i_156_ + 1;
		aBooleanArray395[i] = true;
	}

	public int createClickableBox(int x, int y, int width, int height) {
		componentType[componentCount] = 10;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentX[componentCount] = x;
		componentY[componentCount] = y;
		componentWidth[componentCount] = width;
		componentHeight[componentCount] = height;
		return componentCount++;
	}

	public int createCenteredClickableBox(int x, int y, int width, int height) {
		componentType[componentCount] = 10;
		componentEnabled[componentCount] = true;
		componentClicked[componentCount] = false;
		componentX[componentCount] = x - width / 2;
		componentY[componentCount] = y - height / 2;
		componentWidth[componentCount] = width;
		componentHeight[componentCount] = height;
		return componentCount++;
	}
}
