package jagex;

import java.applet.Applet;
import java.awt.Component;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class tools {
	public static Component component;
	public static Applet applet;
	public static String loadingText = "Loading";
	public static String unpackingText = "Unpacking";

	public static void provideComponent(Component component_, boolean isApplet) {
		component = component_;
		if (isApplet)
			applet = (Applet) component_;
		else
			applet = null;
	}

	public static InputStream openInputStream(String string) throws IOException {
		InputStream inputStream;
		if (applet == null)
			inputStream = new FileInputStream(string);
		else {
			URL url = new URL(applet.getCodeBase(), string);
			inputStream = url.openStream();
		}
		return inputStream;
	}

	public static int unsigned(byte i) {
		return i & 0xff;
	}

	public static int g2(byte[] is, int i) {
		return unsigned(is[i]) * 256 + unsigned(is[i + 1]);
	}

	public static int g3(byte[] is, int i) {
		return (unsigned(is[i]) * 16777216 + unsigned(is[i + 1]) * 65536 + unsigned(is[i + 2]) * 256 + unsigned(is[i + 3]));
	}

	public static String makeString(String input, int inputLength) {
		String output = "";
		for (int id = 0; id < inputLength; id++) {
			if (id >= input.length())
				output += " ";
			else {
				char c = input.charAt(id);
				if (c >= 'a' && c <= 'z')
					output += c;
				else if (c >= 'A' && c <= 'Z')
					output += c;
				else if (c >= '0' && c <= '9')
					output += c;
				else
					output += '_';
			}
		}
		return output;
	}

	public static byte[] getFile(String fileName, String fileLoadtext, int percent, gameshell gameshell) throws IOException {
		if (gameshell != null)
			gameshell.drawLoadingBar2(percent, loadingText + " " + fileLoadtext + " - 0%");
		InputStream inputStream = openInputStream(fileName);
		DataInputStream dataInputStream = new DataInputStream(inputStream);
		byte[] headerBuffer = new byte[6];
		dataInputStream.readFully(headerBuffer, 0, 6);
		int unpackedSize = ((headerBuffer[0] & 0xff) << 16) + ((headerBuffer[1] & 0xff) << 8) + (headerBuffer[2] & 0xff);
		int size = ((headerBuffer[3] & 0xff) << 16) + ((headerBuffer[4] & 0xff) << 8) + (headerBuffer[5] & 0xff);
		if (gameshell != null)
			gameshell.drawLoadingBar2(percent, loadingText + " " + fileLoadtext + " - 5%");
		int offset = 0;
		byte[] buffer = new byte[size];
		while (offset < size) {
			int length = size - offset;
			if (length > 1000)
				length = 1000;
			dataInputStream.readFully(buffer, offset, length);
			offset += length;
			if (gameshell != null)
				gameshell.drawLoadingBar2(percent, (loadingText + " " + fileLoadtext + " - " + (5 + offset * 95 / size) + "%"));
		}
		dataInputStream.close();
		if (gameshell != null)
			gameshell.drawLoadingBar2(percent, unpackingText + " " + fileLoadtext);
		if (size != unpackedSize) {
			byte[] unpackedBuffer = new byte[unpackedSize];
			bzip2.unpack(unpackedBuffer, unpackedSize, buffer, size, 0);
			return unpackedBuffer;
		}
		return buffer;
	}

	public static byte[] getFile(String fileName) throws IOException {
		return getFile(fileName, "", 0, null);
	}

	public static int findFile(String fileName, byte[] buffer) {
		int fileAmount = buffer[0] * 256 + buffer[1];
		int expectedHash = 0;
		fileName = fileName.toUpperCase();
		for (int id = 0; id < fileName.length(); id++)
			expectedHash = expectedHash * 61 + fileName.charAt(id) - 32;
		int bufferIndex = 2 + fileAmount * 10;
		for (int fileId = 0; fileId < fileAmount; fileId++) {
			int fileHash = (buffer[fileId * 10 + 2] & 0xff) * 16777216 + (buffer[fileId * 10 + 3] & 0xff) * 65536 + (buffer[fileId * 10 + 4] & 0xff) * 256 + (buffer[fileId * 10 + 5] & 0xff);
			int fileSize = (buffer[fileId * 10 + 9] & 0xff) * 65536 + (buffer[fileId * 10 + 10] & 0xff) * 256 + (buffer[fileId * 10 + 11] & 0xff);
			if (fileHash == expectedHash)
				return bufferIndex;
			bufferIndex += fileSize;
		}
		System.out.println("Warning file not found: " + fileName);
		return 0;
	}

	public static byte[] getFile(String fileName, byte[] inputBuffer, byte[] outputBuffer) {
		int fileAmount = inputBuffer[0] * 256 + inputBuffer[1];
		int expectedHash = 0;
		fileName = fileName.toUpperCase();
		for (int id = 0; id < fileName.length(); id++)
			expectedHash = expectedHash * 61 + fileName.charAt(id) - 32;
		int bufferIndex = 2 + fileAmount * 10;
		for (int fileId = 0; fileId < fileAmount; fileId++) {
			int fileHash = (inputBuffer[fileId * 10 + 2] & 0xff) * 16777216 + (inputBuffer[fileId * 10 + 3] & 0xff) * 65536 + (inputBuffer[fileId * 10 + 4] & 0xff) * 256 + (inputBuffer[fileId * 10 + 5] & 0xff);
			int unpackedFileSize = (inputBuffer[fileId * 10 + 6] & 0xff) * 65536 + (inputBuffer[fileId * 10 + 7] & 0xff) * 256 + (inputBuffer[fileId * 10 + 8] & 0xff);
			int fileSize = (inputBuffer[fileId * 10 + 9] & 0xff) * 65536 + (inputBuffer[fileId * 10 + 10] & 0xff) * 256 + (inputBuffer[fileId * 10 + 11] & 0xff);
			if (fileHash == expectedHash) {
				if (unpackedFileSize != fileSize)
					bzip2.unpack(outputBuffer, unpackedFileSize, inputBuffer, fileSize, bufferIndex);
				else {
					for (int id = 0; id < unpackedFileSize; id++)
						outputBuffer[id] = inputBuffer[bufferIndex + id];
				}
				return outputBuffer;
			}
			bufferIndex += fileSize;
		}
		System.out.println("Warning file not found: " + fileName);
		return null;
	}

	public static byte[] getFile(String fileName, int extraSize, byte[] inputBuffer) {
		int fileAmount = inputBuffer[0] * 256 + inputBuffer[1];
		int expectedHash = 0;
		fileName = fileName.toUpperCase();
		for (int id = 0; id < fileName.length(); id++)
			expectedHash = expectedHash * 61 + fileName.charAt(id) - 32;
		int bufferIndex = 2 + fileAmount * 10;
		for (int fileId = 0; fileId < fileAmount; fileId++) {
			int fileHash = ((inputBuffer[fileId * 10 + 2] & 0xff) * 16777216 + (inputBuffer[fileId * 10 + 3] & 0xff) * 65536 + (inputBuffer[fileId * 10 + 4] & 0xff) * 256 + (inputBuffer[fileId * 10 + 5] & 0xff));
			int unpackedFileSize = ((inputBuffer[fileId * 10 + 6] & 0xff) * 65536 + (inputBuffer[fileId * 10 + 7] & 0xff) * 256 + (inputBuffer[fileId * 10 + 8] & 0xff));
			int fileSize = ((inputBuffer[fileId * 10 + 9] & 0xff) * 65536 + (inputBuffer[fileId * 10 + 10] & 0xff) * 256 + (inputBuffer[fileId * 10 + 11] & 0xff));
			if (fileHash == expectedHash) {
				byte[] outputBuffer = new byte[unpackedFileSize + extraSize];
				if (unpackedFileSize != fileSize)
					bzip2.unpack(outputBuffer, unpackedFileSize, inputBuffer, fileSize, bufferIndex);
				else {
					for (int id = 0; id < unpackedFileSize; id++)
						outputBuffer[id] = inputBuffer[bufferIndex + id];
				}
				return outputBuffer;
			}
			bufferIndex += fileSize;
		}
		System.out.println("Warning file not found: " + fileName);
		return null;
	}
}
