package jagex;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Event;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;

public class gameshell extends Applet implements Runnable {
	private int width = 512;
	private int height = 384;
	private Thread thread;
	private int delayTime = 20;
	private int maxCycles = 1000;
	private long[] oldTimes = new long[10];
	private viewbox viewbox;
	private boolean usingApplet;
	private int stopState;
	private int mainLoopLag;
	public int mouseOffsetY;
	public int idleCycle;
	public int paintState = 1;
	public String headerLoadingText;
	private boolean hideJagex = false;
	private int loadingPercent;
	private String loadingText = "Loading";
	private Font timesRoman15 = new Font("TimesRoman", 0, 15);
	private Font helvetica13Bold = new Font("Helvetica", 1, 13);
	private Font helvetica12 = new Font("Helvetica", 0, 12);
	private Image logoImage;
	private Graphics graphics;
	public boolean curlyBracket1KeyDown = false;
	public boolean curlyBracket2KeyDown = false;
	public boolean leftArrowKeyDown = false;
	public boolean rightArrowKeyDown = false;
	public boolean upArrowKeyDown = false;
	public boolean downArrowKeyDown = false;
	public boolean spaceKeyDown = false;
	public boolean nmKeyDown = false;
	public int minDelay = 1;
	public int mouseX;
	public int mouseY;
	public int mouseDown;
	public int mouseClick;
	public int keyDown;
	public int lastKeyDown;
	public boolean lowMemory = false;
	public String string16Chars = "";
	public String string16Chars2 = "";
	public String string80Chars = "";
	public String string80Chars2 = "";
	public int fps;

	public void mainInit() {
		/* empty */
	}

	public synchronized void mainLoop() {
		/* empty */
	}

	public void mainDestroy() {
		/* empty */
	}

	public synchronized void mainRedraw() {
		/* empty */
	}

	public void mainPaint() {
		/* empty */
	}

	public final void startApplication(int w, int h, String title, boolean resizable) {
		usingApplet = false;
		System.out.println("Started application");
		width = w;
		height = h;
		viewbox = new viewbox(this, w, h, title, resizable, false);
		paintState = 1;
		tools.provideComponent(viewbox, false);
		thread = new Thread(this);
		thread.start();
		thread.setPriority(1);
	}

	public final boolean isUsingApplet() {
		return usingApplet;
	}

	public final void createViewBox(int w, int h, String title, boolean resizable) {
		if (viewbox == null) {
			width = w;
			height = h;
			viewbox = new viewbox(this, w, h, title, resizable, usingApplet);
			tools.provideComponent(viewbox, false);
		}
	}

	public final void resizeWrapper(int w, int h) {
		if (viewbox != null) {
			viewbox.resize(w, h);
			width = w;
			height = h;
		}
	}

	public final void setIconImage(Image image) {
		if (viewbox != null) {
			viewbox.setIconImage(image);
		}
	}

	public void mainResize(int w, int h) {
		resizeWrapper(w, h);
	}

	public final Graphics getGraphics() {
		if (viewbox == null)
			return super.getGraphics();
		return viewbox.getGraphics();
	}

	public final int width() {
		return width;
	}

	public final int height() {
		return height;
	}

	public final Image createImage(int width, int height) {
		if (viewbox == null)
			return super.createImage(width, height);
		return viewbox.createImage(width, height);
	}

	public Frame getViewbox() {
		return viewbox;
	}

	public final void setFps(int fps) {
		delayTime = 1000 / fps;
	}

	public final void setMaxCycles(int cycles) {
		maxCycles = cycles;
	}

	public final void resetOldTimes() {
		for (int id = 0; id < 10; id++) {
			oldTimes[id] = 0L;
		}
	}

	public synchronized boolean keyDown(Event event, int key) {
		mainKeyDown(key);
		keyDown = key;
		lastKeyDown = key;
		idleCycle = 0;
		if (key == 1006)
			leftArrowKeyDown = true;
		if (key == 1007)
			rightArrowKeyDown = true;
		if (key == 1004)
			upArrowKeyDown = true;
		if (key == 1005)
			downArrowKeyDown = true;
		if ((char) key == ' ')
			spaceKeyDown = true;
		if ((char) key == 'n' || (char) key == 'm')
			nmKeyDown = true;
		if ((char) key == 'N' || (char) key == 'M')
			nmKeyDown = true;
		if ((char) key == '{')
			curlyBracket1KeyDown = true;
		if ((char) key == '}')
			curlyBracket2KeyDown = true;
		if ((char) key == '\u03f0')//f1
			lowMemory = !lowMemory;
		if ((key >= 97 && key <= 122 || key >= 65 && key <= 90 || key >= 48 && key <= 57 || key == 32) && string16Chars.length() < 16)
			string16Chars += (char) key;
		if (key >= 32 && key <= 122 && string80Chars.length() < 80)
			string80Chars += (char) key;
		if (key == 8 && string16Chars.length() > 0)
			string16Chars = string16Chars.substring(0, string16Chars.length() - 1);
		if (key == 8 && string80Chars.length() > 0)
			string80Chars = string80Chars.substring(0, string80Chars.length() - 1);
		if (key == 10 || key == 13) {
			string16Chars2 = string16Chars;
			string80Chars2 = string80Chars;
		}
		return true;
	}

	public void mainKeyDown(int i) {
		/* empty */
	}

	public synchronized boolean keyUp(Event event, int key) {
		keyDown = 0;
		if (key == 1006)
			leftArrowKeyDown = false;
		if (key == 1007)
			rightArrowKeyDown = false;
		if (key == 1004)
			upArrowKeyDown = false;
		if (key == 1005)
			downArrowKeyDown = false;
		if ((char) key == ' ')
			spaceKeyDown = false;
		if ((char) key == 'n' || (char) key == 'm')
			nmKeyDown = false;
		if ((char) key == 'N' || (char) key == 'M')
			nmKeyDown = false;
		if ((char) key == '{')
			curlyBracket1KeyDown = false;
		if ((char) key == '}')
			curlyBracket2KeyDown = false;
		return true;
	}

	public synchronized boolean mouseMove(Event event, int x, int y) {
		mouseX = x;
		mouseY = y + mouseOffsetY;
		mouseDown = 0;
		idleCycle = 0;
		return true;
	}

	public synchronized boolean mouseUp(Event event, int x, int y) {
		mouseX = x;
		mouseY = y + mouseOffsetY;
		mouseDown = 0;
		return true;
	}

	public synchronized boolean mouseDown(Event event, int x, int y) {
		mouseX = x;
		mouseY = y + mouseOffsetY;
		if (event.metaDown())
			mouseDown = 2;
		else
			mouseDown = 1;
		mouseClick = mouseDown;
		idleCycle = 0;
		return true;
	}

	public synchronized boolean mouseDrag(Event event, int x, int y) {
		mouseX = x;
		mouseY = y + mouseOffsetY;
		if (event.metaDown())
			mouseDown = 2;
		else
			mouseDown = 1;
		return true;
	}

	public final void init() {
		usingApplet = true;
		System.out.println("Started applet");
		width = this.size().width;
		height = this.size().height;
		paintState = 1;
		tools.provideComponent(this, true);
		thread = new Thread(this);
		thread.start();
	}

	public final void start() {
		if (stopState >= 0)
			stopState = 0;
	}

	public final void stop() {
		if (stopState >= 0)
			stopState = 4000 / delayTime;
	}

	public final void destroy() {
		stopState = -1;
		try {
			Thread.sleep(5000L);
		} catch (Exception exception) {
			/* empty */
		}
		if (stopState == -1) {
			System.out.println("5 seconds expired, forcing kill");
			closeProgram();
			if (thread != null) {
				thread.stop();
				thread = null;
			}
		}
	}

	public final void closeProgram() {
		stopState = -2;
		System.out.println("Closing program");
		mainDestroy();
		try {
			Thread.sleep(1000L);
		} catch (Exception exception) {
			/* empty */
		}
		if (viewbox != null)
			viewbox.dispose();
		if (!usingApplet)
			System.exit(0);
	}

	public final void run() {
		if (paintState == 1) {
			paintState = 2;
			graphics = getGraphics();
			loadJagex();
			drawLoadingBar(0, "Loading...");
			mainInit();
			paintState = 0;
		}
		int oldTimesPos = 0;
		int ratio = 256;
		int delay = 1;
		int count = 0;
		for (int id = 0; id < 10; id++)
			oldTimes[id] = System.currentTimeMillis();
		long newTime = System.currentTimeMillis();
		while (stopState >= 0) {
			if (stopState > 0) {
				stopState--;
				if (stopState == 0) {
					closeProgram();
					thread = null;
					return;
				}
			}
			int oldRatio = ratio;
			int oldDelay = delay;
			ratio = 300;
			delay = 1;
			newTime = System.currentTimeMillis();
			if (oldTimes[oldTimesPos] == 0L) {
				ratio = oldRatio;
				delay = oldDelay;
			} else if (newTime > oldTimes[oldTimesPos])
				ratio = (int) ((long) (2560 * delayTime) / (newTime - oldTimes[oldTimesPos]));
			if (ratio < 25)
				ratio = 25;
			if (ratio > 256) {
				ratio = 256;
				delay = (int) ((long) delayTime - (newTime - oldTimes[oldTimesPos]) / 10L);
				if (delay < minDelay)
					delay = minDelay;
			}
			try {
				Thread.sleep((long) delay);
			} catch (InterruptedException interruptedexception) {
				/* empty */
			}
			oldTimes[oldTimesPos] = newTime;
			oldTimesPos = (oldTimesPos + 1) % 10;
			if (delay > 1) {
				for (int id = 0; id < 10; id++) {
					if (oldTimes[id] != 0L)
						oldTimes[id] += (long) delay;
				}
			}
			int cyclesPassed = 0;
			while (count < 256) {
				mainLoop();
				count += ratio;
				if (++cyclesPassed > maxCycles) {
					count = 0;
					mainLoopLag += 6;
					if (mainLoopLag > 25) {
						mainLoopLag = 0;
						lowMemory = true;
					}
					break;
				}
			}
			mainLoopLag--;
			count &= 0xff;
			mainRedraw();
			fps = 1000 * ratio / (delayTime * 256);
			if (usingApplet && oldTimesPos == 0)
				this.showStatus("Fps:" + fps + "Del:" + delay);
			if (viewbox != null && (viewbox.getWidth() != width || viewbox.getHeight() != height))
				mainResize(viewbox.getWidth(), viewbox.getHeight());
		}
		if (stopState == -1)
			closeProgram();
		thread = null;
	}

	public final void update(Graphics graphics) {
		paint(graphics);
	}

	public final void paint(Graphics graphics) {
		if (paintState == 2 && logoImage != null)
			drawLoadingBar(loadingPercent, loadingText);
		else if (paintState == 0)
			mainPaint();
	}

	public void loadJagex() {
		try {
			byte[] jagexJagFile = tools.getFile("jagex.jag");
			byte[] logoBuffer = tools.getFile("logo.tga", 0, jagexJagFile);
			logoImage = tgaToImage(logoBuffer);
			pixmap.appendJagexFont(tools.getFile("h11p.jf", 0, jagexJagFile));
			pixmap.appendJagexFont(tools.getFile("h12b.jf", 0, jagexJagFile));
			pixmap.appendJagexFont(tools.getFile("h12p.jf", 0, jagexJagFile));
			pixmap.appendJagexFont(tools.getFile("h13b.jf", 0, jagexJagFile));
			pixmap.appendJagexFont(tools.getFile("h14b.jf", 0, jagexJagFile));
			pixmap.appendJagexFont(tools.getFile("h16b.jf", 0, jagexJagFile));
			pixmap.appendJagexFont(tools.getFile("h20b.jf", 0, jagexJagFile));
			pixmap.appendJagexFont(tools.getFile("h24b.jf", 0, jagexJagFile));
		} catch (java.io.IOException ioexception) {
			System.out.println("Error loading jagex.dat");
		}
	}

	public void drawLoadingBar(int percent, String text) {
		int x = (width - 281) / 2;
		int y = (height - 148) / 2;
		graphics.setColor(Color.black);
		graphics.fillRect(0, 0, width, height);
		if (!hideJagex)
			graphics.drawImage(logoImage, x, y, this);
		x += 2;
		y += 90;
		loadingPercent = percent;
		loadingText = text;
		graphics.setColor(new Color(132, 132, 132));
		if (hideJagex)
			graphics.setColor(new Color(220, 0, 0));
		graphics.drawRect(x - 2, y - 2, 280, 23);
		graphics.fillRect(x, y, 277 * percent / 100, 20);
		graphics.setColor(new Color(198, 198, 198));
		if (hideJagex)
			graphics.setColor(new Color(255, 255, 255));
		drawString(graphics, text, timesRoman15, x + 138, y + 10);
		if (!hideJagex) {
			drawString(graphics, "Created by JAGeX - visit www.jagex.com", helvetica13Bold, x + 138, y + 30);
			drawString(graphics, "Copyright \u00a92000 Andrew Gower", helvetica13Bold, x + 138, y + 44);
		} else {
			graphics.setColor(new Color(132, 132, 152));
			drawString(graphics, "Copyright \u00a92000 Andrew Gower", helvetica12, x + 138, height - 20);
		}
		if (headerLoadingText != null) {
			graphics.setColor(Color.white);
			drawString(graphics, headerLoadingText, helvetica13Bold, x + 138, y - 120);
		}
	}

	public void drawLoadingBar2(int percent, String text) {
		int x = (width - 281) / 2;
		int y = (height - 148) / 2;
		x += 2;
		y += 90;
		loadingPercent = percent;
		loadingText = text;
		int ratio = 277 * percent / 100;
		graphics.setColor(new Color(132, 132, 132));
		if (hideJagex)
			graphics.setColor(new Color(220, 0, 0));
		graphics.fillRect(x, y, ratio, 20);
		graphics.setColor(Color.black);
		graphics.fillRect(x + ratio, y, 277 - ratio, 20);
		graphics.setColor(new Color(198, 198, 198));
		if (hideJagex)
			graphics.setColor(new Color(255, 255, 255));
		drawString(graphics, text, timesRoman15, x + 138, y + 10);
	}

	public void drawString(Graphics graphics, String string, Font font, int x, int y) {
		FontMetrics fontMetrics = tools.component.getFontMetrics(font);
		fontMetrics.stringWidth(string);
		graphics.setFont(font);
		graphics.drawString(string, x - fontMetrics.stringWidth(string) / 2, y + fontMetrics.getHeight() / 4);
	}

	public Image tgaToImage(byte[] inputBuffer) {
		int width = tools.unsigned(inputBuffer[13]) * 256 + tools.unsigned(inputBuffer[12]);
		int height = tools.unsigned(inputBuffer[15]) * 256 + tools.unsigned(inputBuffer[14]);
		byte[] redColorComponents = new byte[256];
		byte[] greenColorComponents = new byte[256];
		byte[] blueColorComponents = new byte[256];
		for (int id = 0; id < 256; id++) {
			redColorComponents[id] = inputBuffer[20 + id * 3];
			greenColorComponents[id] = inputBuffer[19 + id * 3];
			blueColorComponents[id] = inputBuffer[18 + id * 3];
		}
		IndexColorModel indexcolormodel = new IndexColorModel(8, 256, redColorComponents, greenColorComponents, blueColorComponents);
		byte[] pixels = new byte[width * height];
		int pixelsPos = 0;
		for (int y = height - 1; y >= 0; y--) {
			for (int x = 0; x < width; x++)
				pixels[pixelsPos++] = inputBuffer[786 + x + y * width];
		}
		MemoryImageSource memoryimagesource = new MemoryImageSource(width, height, indexcolormodel, pixels, 0, width);
		Image image = this.createImage(memoryimagesource);
		return image;
	}
}
