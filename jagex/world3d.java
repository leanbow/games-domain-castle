package jagex;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.io.File;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;

public class world3d {
	int anInt456 = 50;
	int[] anIntArray457 = new int[anInt456];
	int[][] anIntArrayArray458 = new int[anInt456][256];
	int[] anIntArray459;
	public int anInt460;
	public static String texturesLoadingText = "textures";
	public int minRenderZ = 5;
	public int maxRenderZ = 1000;
	public int anInt464 = 1000;
	public int anInt465 = 20;
	public int anInt466 = 10;
	public static int[] sinCosLookupTable1024 = new int[2048];
	private static int[] sinCosLookupTable256 = new int[512];
	public boolean aBoolean469 = false;
	public double aDouble470 = 1.1;
	public int anInt471 = 1;
	private boolean aBoolean472 = false;
	private int anInt473;
	private int anInt474;
	private int anInt475;
	private int anInt476;
	private int anInt477;
	private int anInt478;
	private object3d aClass7_479;
	private object3d aClass7_480;
	private int anInt481;
	private int anInt482;
	private int anInt483 = 512;
	private int centerX = 256;
	private int centerY = 192;
	private int anInt486 = 256;
	private int anInt487 = 256;
	private int screenWidth = 8;
	private int anInt489 = 4;
	private int cameraX;
	private int cameraY;
	private int cameraZ;
	private int cameraPitch;
	private int cameraYaw;
	private int cameraRoll;
	private int objectsPos;
	private int maxObjectCount;
	private object3d[] objects;
	private int[] anIntArray499;
	private int polygonsPos;
	private polygon3d[] polygons;
	private int anInt502;
	private int anInt503;
	private int[] anIntArray504;
	private int[] anIntArray505;
	private int[] anIntArray506;
	private int[] anIntArray507;
	private int[] anIntArray508;
	private int[] anIntArray509;
	private int[] anIntArray510;
	public object3d aClass7_511;
	private static final int anInt512 = 16;
	private static final int anInt513 = 4;
	private static final int anInt514 = 5;
	private static final int anInt515 = 12345678;
	int texturesCount;
	String[] texturesName;
	int[] texturesColor;
	int[] texturesSize;
	long[] aLongArray520;
	boolean[] aBooleanArray521;
	int[][] texturesPixels;
	private static long aLong523;
	byte[] texturesArchiveBuffer;
	int[][] anIntArrayArray525;
	int[][] anIntArrayArray526;
	private static byte[] aByteArray527;
	private static int[] anIntArray528 = new int[256];
	pixmap pixmap;
	public int[] pixels;
	scanline[] aClass4Array531;
	int anInt532;
	int anInt533;
	int[] anIntArray534 = new int[40];
	int[] anIntArray535 = new int[40];
	int[] anIntArray536 = new int[40];
	int[] anIntArray537 = new int[40];
	int[] anIntArray538 = new int[40];
	int[] anIntArray539 = new int[40];
	boolean lowMemory = false;
	static int anInt541;
	static int anInt542;
	static int anInt543;
	static int anInt544;
	static int anInt545;
	static int anInt546;
	int anInt547;
	int anInt548;

	public world3d(pixmap class6, int i, int i_0_, int i_1_) {
		pixmap = class6;
		centerX = class6.width / 2;
		centerY = class6.height / 2;
		pixels = class6.pixels;
		objectsPos = 0;
		maxObjectCount = i;
		objects = new object3d[maxObjectCount];
		anIntArray499 = new int[maxObjectCount];
		polygonsPos = 0;
		polygons = new polygon3d[i_0_];
		for (int i_2_ = 0; i_2_ < i_0_; i_2_++)
			polygons[i_2_] = new polygon3d();
		anInt503 = 0;
		aClass7_511 = new object3d(i_1_ * 2, i_1_);
		anIntArray504 = new int[i_1_];
		anIntArray508 = new int[i_1_];
		anIntArray509 = new int[i_1_];
		anIntArray505 = new int[i_1_];
		anIntArray506 = new int[i_1_];
		anIntArray507 = new int[i_1_];
		anIntArray510 = new int[i_1_];
		if (aByteArray527 == null)
			aByteArray527 = new byte[17691];
		cameraX = 0;
		cameraY = 0;
		cameraZ = 0;
		cameraPitch = 0;
		cameraYaw = 0;
		cameraRoll = 0;
		for (int id = 0; id < 256; id++) {
			sinCosLookupTable256[id] = (int) (Math.sin((double) id * 2 * Math.PI / 256) * 32768.0);
			sinCosLookupTable256[id + 256] = (int) (Math.cos((double) id * 2 * Math.PI / 256) * 32768.0);
		}
		for (int id = 0; id < 1024; id++) {
			sinCosLookupTable1024[id] = (int) (Math.sin((double) id * 2 * Math.PI / 1024) * 32768.0);
			sinCosLookupTable1024[id + 1024] = (int) (Math.cos((double) id * 2 * Math.PI / 1024) * 32768.0);
		}
	}

	public void addObject(object3d object3d) {
		if (objectsPos < maxObjectCount) {
			anIntArray499[objectsPos] = 0;
			objects[objectsPos++] = object3d;
		}
	}

	public void clearObjects() {
		clear();
		for (int id = 0; id < objectsPos; id++)
			objects[id] = null;
		objectsPos = 0;
	}

	public void clear() {
		anInt503 = 0;
		aClass7_511.clear();
	}

	public int method355(int i, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_) {
		anIntArray504[anInt503] = i;//sprite,texture id?
		anIntArray505[anInt503] = i_5_;//x
		anIntArray506[anInt503] = i_6_;//y
		anIntArray507[anInt503] = i_7_;//z
		anIntArray508[anInt503] = i_8_;//screen/size x
		anIntArray509[anInt503] = i_9_;//screen/size y
		anIntArray510[anInt503] = 0;
		int i_10_ = aClass7_511.addVertex2(i_5_, i_6_, i_7_);
		int i_11_ = aClass7_511.addVertex2(i_5_, i_6_ - i_9_, i_7_);
		int[] is = { i_10_, i_11_ };
		aClass7_511.addPolygon(2, is, 0, 0);
		aClass7_511.aByteArray335[anInt503++] = (byte) 0;
		return anInt503 - 1;
	}

	public int method356(int i, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_) {
		anIntArray504[anInt503] = i;
		anIntArray505[anInt503] = i_12_;
		anIntArray506[anInt503] = i_13_;
		anIntArray507[anInt503] = i_14_;
		anIntArray508[anInt503] = i_15_;
		anIntArray509[anInt503] = i_16_;
		anIntArray510[anInt503] = 0;
		int i_18_ = aClass7_511.addVertex2(i_12_, i_13_, i_14_);
		int i_19_ = aClass7_511.addVertex2(i_12_, i_13_ - i_16_, i_14_);
		int[] is = { i_18_, i_19_ };
		aClass7_511.addPolygon(2, is, 0, 0);
		aClass7_511.anIntArray334[anInt503] = i_17_;
		aClass7_511.aByteArray335[anInt503++] = (byte) 0;
		return anInt503 - 1;
	}

	public void method357(int i, int i_20_) {
		method359(i, i_20_, -12345678, 12345678, -12345678, 12345678);
	}

	public void method358(int i, int i_21_, int i_22_, int i_23_) {
		method359(i, i_21_, i_22_, i_23_, i_22_, i_23_);
	}

	public void method359(int i, int i_24_, int i_25_, int i_26_, int i_27_, int i_28_) {
		anInt473 = i - anInt486;
		anInt474 = i_24_;
		anInt475 = i_25_;
		anInt476 = i_26_;
		anInt477 = i_27_;
		anInt478 = i_28_;
		aBoolean472 = true;
		anInt481 = -1;
		anInt482 = -1;
		aClass7_479 = null;
		aClass7_480 = null;
	}

	public int method360() {
		return anInt482;
	}

	public object3d method361() {
		return aClass7_480;
	}

	public void method362(int i, int i_29_, int i_30_, int i_31_, int i_32_, int i_33_) {
		centerX = i_30_;
		centerY = i_31_;
		anInt486 = i;
		anInt487 = i_29_;
		anInt483 = i_32_;
		screenWidth = i_33_;
		aClass4Array531 = new scanline[i_31_ + i_29_];
		for (int i_34_ = 0; i_34_ < i_31_ + i_29_; i_34_++)
			aClass4Array531[i_34_] = new scanline();
	}

	private void method363(polygon3d[] class9s, int i, int i_35_) {
		if (i < i_35_) {
			int i_36_ = i - 1;
			int i_37_ = i_35_ + 1;
			int i_38_ = (i + i_35_) / 2;
			polygon3d class9 = class9s[i_38_];
			class9s[i_38_] = class9s[i];
			class9s[i] = class9;
			int i_39_ = class9.averageZ;
			while (i_36_ < i_37_) {
				do
					i_37_--;
				while (class9s[i_37_].averageZ < i_39_);
				do
					i_36_++;
				while (class9s[i_36_].averageZ > i_39_);
				if (i_36_ < i_37_) {
					polygon3d class9_40_ = class9s[i_36_];
					class9s[i_36_] = class9s[i_37_];
					class9s[i_37_] = class9_40_;
				}
			}
			method363(class9s, i, i_37_);
			method363(class9s, i_37_ + 1, i_35_);
		}
	}

	public void method364(int i, polygon3d[] class9s, int i_41_) {
		for (int i_42_ = 0; i_42_ <= i_41_; i_42_++) {
			class9s[i_42_].aBoolean453 = false;
			class9s[i_42_].anInt454 = i_42_;
			class9s[i_42_].anInt455 = -1;
		}
		int i_43_ = 0;
		for (;;) {
			if (class9s[i_43_].aBoolean453)
				i_43_++;
			else {
				if (i_43_ == i_41_)
					break;
				polygon3d class9 = class9s[i_43_];
				class9.aBoolean453 = true;
				int i_44_ = i_43_;
				int i_45_ = i_43_ + i;
				if (i_45_ >= i_41_)
					i_45_ = i_41_ - 1;
				for (int i_46_ = i_45_; i_46_ >= i_44_ + 1; i_46_--) {
					polygon3d class9_47_ = class9s[i_46_];
					if (class9.anInt439 < class9_47_.anInt441 && class9_47_.anInt439 < class9.anInt441 && class9.anInt440 < class9_47_.anInt442 && class9_47_.anInt440 < class9.anInt442 && class9.anInt454 != class9_47_.anInt455 && !method382(class9, class9_47_) && method383(class9_47_, class9)) {
						method365(class9s, i_44_, i_46_);
						if (class9s[i_46_] != class9_47_)
							i_46_++;
						i_44_ = anInt547;
						class9_47_.anInt455 = class9.anInt454;
					}
				}
			}
		}
	}

	public boolean method365(polygon3d[] class9s, int i, int i_48_) {
		for (;;) {
			polygon3d class9 = class9s[i];
			for (int i_49_ = i + 1; i_49_ <= i_48_; i_49_++) {
				polygon3d class9_50_ = class9s[i_49_];
				if (!method382(class9_50_, class9))
					break;
				class9s[i] = class9_50_;
				class9s[i_49_] = class9;
				i = i_49_;
				if (i == i_48_) {
					anInt547 = i;
					anInt548 = i - 1;
					return true;
				}
			}
			polygon3d class9_51_ = class9s[i_48_];
			for (int i_52_ = i_48_ - 1; i_52_ >= i; i_52_--) {
				polygon3d class9_53_ = class9s[i_52_];
				if (!method382(class9_51_, class9_53_))
					break;
				class9s[i_48_] = class9_53_;
				class9s[i_52_] = class9_51_;
				i_48_ = i_52_;
				if (i == i_48_) {
					anInt547 = i_48_ + 1;
					anInt548 = i_48_;
					return true;
				}
			}
			if (i + 1 >= i_48_) {
				anInt547 = i;
				anInt548 = i_48_;
				return false;
			}
			if (!method365(class9s, i + 1, i_48_)) {
				anInt547 = i;
				return false;
			}
			i_48_ = anInt548;
		}
	}

	public void method366(int i, int i_54_, int i_55_) {
		int i_56_ = 1024 - cameraPitch & 0x3ff;
		int i_57_ = 1024 - cameraYaw & 0x3ff;
		int i_58_ = 1024 - cameraRoll & 0x3ff;
		if (i_58_ != 0) {
			int i_59_ = sinCosLookupTable1024[i_58_];
			int i_60_ = sinCosLookupTable1024[i_58_ + 1024];
			int i_61_ = i_54_ * i_59_ + i * i_60_ >> 15;
			i_54_ = i_54_ * i_60_ - i * i_59_ >> 15;
			i = i_61_;
		}
		if (i_56_ != 0) {
			int i_62_ = sinCosLookupTable1024[i_56_];
			int i_63_ = sinCosLookupTable1024[i_56_ + 1024];
			int i_64_ = i_54_ * i_63_ - i_55_ * i_62_ >> 15;
			i_55_ = i_54_ * i_62_ + i_55_ * i_63_ >> 15;
			i_54_ = i_64_;
		}
		if (i_57_ != 0) {
			int i_65_ = sinCosLookupTable1024[i_57_];
			int i_66_ = sinCosLookupTable1024[i_57_ + 1024];
			int i_67_ = i_55_ * i_65_ + i * i_66_ >> 15;
			i_55_ = i_55_ * i_66_ - i * i_65_ >> 15;
			i = i_67_;
		}
		if (i < anInt541)
			anInt541 = i;
		if (i > anInt542)
			anInt542 = i;
		if (i_54_ < anInt543)
			anInt543 = i_54_;
		if (i_54_ > anInt544)
			anInt544 = i_54_;
		if (i_55_ < anInt545)
			anInt545 = i_55_;
		if (i_55_ > anInt546)
			anInt546 = i_55_;
	}

	public void method367() {
		lowMemory = pixmap.lowMemory;
		int i = centerX * maxRenderZ >> screenWidth;
		int i_68_ = centerY * maxRenderZ >> screenWidth;
		anInt541 = 0;
		anInt542 = 0;
		anInt543 = 0;
		anInt544 = 0;
		anInt545 = 0;
		anInt546 = 0;
		method366(-i, -i_68_, maxRenderZ);
		method366(-i, i_68_, maxRenderZ);
		method366(i, -i_68_, maxRenderZ);
		method366(i, i_68_, maxRenderZ);
		method366(-centerX, -centerY, 0);
		method366(-centerX, centerY, 0);
		method366(centerX, -centerY, 0);
		method366(centerX, centerY, 0);
		anInt541 += cameraX;
		anInt542 += cameraX;
		anInt543 += cameraY;
		anInt544 += cameraY;
		anInt545 += cameraZ;
		anInt546 += cameraZ;
		objects[objectsPos] = aClass7_511;
		aClass7_511.updateNeeded = 2;
		for (int id = 0; id < objectsPos; id++)
			objects[id].update(cameraX, cameraY, cameraZ, cameraPitch, cameraYaw, cameraRoll, screenWidth, minRenderZ);
		objects[objectsPos].update(cameraX, cameraY, cameraZ, cameraPitch, cameraYaw, cameraRoll, screenWidth, minRenderZ);
		polygonsPos = 0;
		for (int objectId = 0; objectId < objectsPos; objectId++) {
			object3d object = objects[objectId];
			if (object.onScreen) {
				for (int polyId = 0; polyId < object.polygonCount; polyId++) {
					int polyPointsCount = object.polygonPointsCount[polyId];
					int[] points = object.polygonPoints[polyId];
					boolean inBounds = false;
					for (int polyPointsId = 0; polyPointsId < polyPointsCount; polyPointsId++) {
						int z = object.cameraModifiedVerticesZ[points[polyPointsId]];
						if (z > minRenderZ && z < maxRenderZ) {
							inBounds = true;
							break;
						}
					}
					if (inBounds) {
						int i_75_ = 0;
						for (int i_76_ = 0; i_76_ < polyPointsCount; i_76_++) {
							int x = object.screenX[points[i_76_]];
							if (x > -centerX)
								i_75_ |= 0x1;
							if (x < centerX)
								i_75_ |= 0x2;
							if (i_75_ == 3)
								break;
						}
						if (i_75_ == 3) {
							i_75_ = 0;
							for (int i_78_ = 0; i_78_ < polyPointsCount; i_78_++) {
								int y = object.screenY[points[i_78_]];
								if (y > -centerY)
									i_75_ |= 0x1;
								if (y < centerY)
									i_75_ |= 0x2;
								if (i_75_ == 3)
									break;
							}
							if (i_75_ == 3) {
								polygon3d polygon = polygons[polygonsPos];
								polygon.object = object;
								polygon.polygonId = polyId;
								method380(polygonsPos);
								int color;
								if (polygon.anInt451 < 0)
									color = object.polygonColors1[polyId];
								else
									color = object.polygonColors2[polyId];
								if (color != 12345678) {
									int sumZ = 0;
									for (int id = 0; id < polyPointsCount; id++)
										sumZ += object.cameraModifiedVerticesZ[points[id]];
									polygon.averageZ = sumZ / polyPointsCount + object.zOffset;
									polygon.color = color;
									polygonsPos++;
								}
							}
						}
					}
				}
			}
		}
		object3d class7 = aClass7_511;
		if (class7.onScreen) {
			for (int i_84_ = 0; i_84_ < class7.polygonCount; i_84_++) {
				int[] is = class7.polygonPoints[i_84_];
				int i_85_ = is[0];
				int i_86_ = class7.screenX[i_85_];
				int i_87_ = class7.screenY[i_85_];
				int i_88_ = class7.cameraModifiedVerticesZ[i_85_];
				if (i_88_ > minRenderZ && i_88_ < anInt464) {
					int i_89_ = (anIntArray508[i_84_] << screenWidth) / i_88_;
					int i_90_ = (anIntArray509[i_84_] << screenWidth) / i_88_;
					if (i_86_ - i_89_ / 2 <= centerX && i_86_ + i_89_ / 2 >= -centerX && i_87_ - i_90_ <= centerY && i_87_ >= -centerY) {
						polygon3d class9 = polygons[polygonsPos];
						class9.object = class7;
						class9.polygonId = i_84_;
						method381(polygonsPos);
						class9.averageZ = (i_88_ + class7.cameraModifiedVerticesZ[is[1]]) / 2;
						polygonsPos++;
					}
				}
			}
		}
		if (polygonsPos != 0) {
			anInt460 = polygonsPos;
			method363(polygons, 0, polygonsPos - 1);
			method364(100, polygons, polygonsPos);
			for (int i_91_ = 0; i_91_ < polygonsPos; i_91_++) {
				polygon3d class9 = polygons[i_91_];
				class7 = class9.object;
				int i_92_ = class9.polygonId;
				if (class7 == aClass7_511) {
					int[] is = class7.polygonPoints[i_92_];
					int i_93_ = is[0];
					int i_94_ = class7.screenX[i_93_];
					int i_95_ = class7.screenY[i_93_];
					int i_96_ = class7.cameraModifiedVerticesZ[i_93_];
					int i_97_ = (anIntArray508[i_92_] << screenWidth) / i_96_;
					int i_98_ = (anIntArray509[i_92_] << screenWidth) / i_96_;
					int i_99_ = i_95_ - class7.screenY[is[1]];
					int i_100_ = ((class7.screenX[is[1]] - i_94_) * i_99_ / i_98_);
					i_100_ = class7.screenX[is[1]] - i_94_;
					int i_101_ = i_94_ - i_97_ / 2;
					int i_102_ = anInt487 + i_95_ - i_98_;
					pixmap.method265(i_101_ + anInt486, i_102_, i_97_, i_98_, anIntArray504[i_92_], i_100_, (256 << screenWidth) / i_96_);
					if (aBoolean472) {
						i_101_ += (anIntArray510[i_92_] << screenWidth) / i_96_;
						if (anInt474 >= i_102_ && anInt474 <= i_102_ + i_98_ && anInt473 >= i_101_ && anInt473 <= i_101_ + i_97_ && class7.aByteArray335[i_92_] == 0) {
							if (class7.anIntArray334[i_92_] >= anInt475 && class7.anIntArray334[i_92_] <= anInt476) {
								aClass7_479 = class7;
								anInt481 = i_92_;
							}
							if (class7.anIntArray334[i_92_] >= anInt477 && class7.anIntArray334[i_92_] <= anInt478) {
								aClass7_480 = class7;
								anInt482 = i_92_;
							}
						}
					}
				} else {
					int i_103_ = 0;
					int i_104_ = 0;
					int i_105_ = class7.polygonPointsCount[i_92_];
					int[] is = class7.polygonPoints[i_92_];
					if (class7.anIntArray316[i_92_] != 12345678) {
						if (class9.anInt451 < 0)
							i_104_ = (class7.anInt384 - class7.anIntArray316[i_92_]);
						else
							i_104_ = (class7.anInt384 + class7.anIntArray316[i_92_]);
					}
					for (int i_106_ = 0; i_106_ < i_105_; i_106_++) {
						int i_107_ = is[i_106_];
						anIntArray537[i_106_] = class7.cameraModifiedVerticesX[i_107_];
						anIntArray538[i_106_] = class7.cameraModifiedVerticesY[i_107_];
						anIntArray539[i_106_] = class7.cameraModifiedVerticesZ[i_107_];
						if (class7.anIntArray316[i_92_] == 12345678) {
							if (class9.anInt451 < 0)
								i_104_ = (class7.anInt384 - class7.anIntArray307[i_107_] + class7.aByteArray308[i_107_]);
							else
								i_104_ = (class7.anInt384 + class7.anIntArray307[i_107_] + class7.aByteArray308[i_107_]);
						}
						if (class7.cameraModifiedVerticesZ[i_107_] >= minRenderZ) {
							anIntArray534[i_103_] = class7.screenX[i_107_];
							anIntArray535[i_103_] = class7.screenY[i_107_];
							anIntArray536[i_103_] = i_104_;
							if (class7.cameraModifiedVerticesZ[i_107_] > anInt466)
								anIntArray536[i_103_] += (class7.cameraModifiedVerticesZ[i_107_] - anInt466) / anInt465;
							i_103_++;
						} else {
							int i_108_;
							if (i_106_ == 0)
								i_108_ = is[i_105_ - 1];
							else
								i_108_ = is[i_106_ - 1];
							if (class7.cameraModifiedVerticesZ[i_108_] >= minRenderZ) {
								int i_109_ = (class7.cameraModifiedVerticesZ[i_107_] - class7.cameraModifiedVerticesZ[i_108_]);
								int i_110_ = (class7.cameraModifiedVerticesX[i_107_] - ((class7.cameraModifiedVerticesX[i_107_] - class7.cameraModifiedVerticesX[i_108_]) * (class7.cameraModifiedVerticesZ[i_107_] - minRenderZ) / i_109_));
								int i_111_ = (class7.cameraModifiedVerticesY[i_107_] - ((class7.cameraModifiedVerticesY[i_107_] - class7.cameraModifiedVerticesY[i_108_]) * (class7.cameraModifiedVerticesZ[i_107_] - minRenderZ) / i_109_));
								anIntArray534[i_103_] = (i_110_ << screenWidth) / minRenderZ;
								anIntArray535[i_103_] = (i_111_ << screenWidth) / minRenderZ;
								anIntArray536[i_103_] = i_104_;
								i_103_++;
							}
							if (i_106_ == i_105_ - 1)
								i_108_ = is[0];
							else
								i_108_ = is[i_106_ + 1];
							if (class7.cameraModifiedVerticesZ[i_108_] >= minRenderZ) {
								int i_112_ = (class7.cameraModifiedVerticesZ[i_107_] - class7.cameraModifiedVerticesZ[i_108_]);
								int i_113_ = (class7.cameraModifiedVerticesX[i_107_] - ((class7.cameraModifiedVerticesX[i_107_] - class7.cameraModifiedVerticesX[i_108_]) * (class7.cameraModifiedVerticesZ[i_107_] - minRenderZ) / i_112_));
								int i_114_ = (class7.cameraModifiedVerticesY[i_107_] - ((class7.cameraModifiedVerticesY[i_107_] - class7.cameraModifiedVerticesY[i_108_]) * (class7.cameraModifiedVerticesZ[i_107_] - minRenderZ) / i_112_));
								anIntArray534[i_103_] = (i_113_ << screenWidth) / minRenderZ;
								anIntArray535[i_103_] = (i_114_ << screenWidth) / minRenderZ;
								anIntArray536[i_103_] = i_104_;
								i_103_++;
							}
						}
					}
					for (int i_115_ = 0; i_115_ < i_105_; i_115_++) {
						if (anIntArray536[i_115_] < 0)
							anIntArray536[i_115_] = 0;
						else if (anIntArray536[i_115_] > 255)
							anIntArray536[i_115_] = 255;
						if (class9.color >= 0) {
							if (texturesSize[class9.color] == 1)
								anIntArray536[i_115_] <<= 9;
							else
								anIntArray536[i_115_] <<= 6;
						}
					}
					method368(0, 0, 0, 0, i_103_, anIntArray534, anIntArray535, anIntArray536, class7, i_92_);
					if (anInt533 > anInt532)
						method369(0, 0, i_105_, anIntArray537, anIntArray538, anIntArray539, class9.color, class7);
				}
			}
			aBoolean472 = false;
		}
	}

	private void method368(int i, int i_116_, int i_117_, int i_118_, int i_119_, int[] is, int[] is_120_, int[] is_121_, object3d class7, int i_122_) {
		if (i_119_ == 3) {
			int i_123_ = is_120_[0] + anInt487;
			int i_124_ = is_120_[1] + anInt487;
			int i_125_ = is_120_[2] + anInt487;
			int i_126_ = is[0];
			int i_127_ = is[1];
			int i_128_ = is[2];
			int i_129_ = is_121_[0];
			int i_130_ = is_121_[1];
			int i_131_ = is_121_[2];
			int i_132_ = anInt487 + centerY - 1;
			int i_133_ = 0;
			int i_134_ = 0;
			int i_135_ = 0;
			int i_136_ = 0;
			int i_137_ = 12345678;
			int i_138_ = -12345678;
			if (i_125_ != i_123_) {
				i_134_ = (i_128_ - i_126_ << 8) / (i_125_ - i_123_);
				i_136_ = (i_131_ - i_129_ << 8) / (i_125_ - i_123_);
				if (i_123_ < i_125_) {
					i_133_ = i_126_ << 8;
					i_135_ = i_129_ << 8;
					i_137_ = i_123_;
					i_138_ = i_125_;
				} else {
					i_133_ = i_128_ << 8;
					i_135_ = i_131_ << 8;
					i_137_ = i_125_;
					i_138_ = i_123_;
				}
				if (i_137_ < 0) {
					i_133_ -= i_134_ * i_137_;
					i_135_ -= i_136_ * i_137_;
					i_137_ = 0;
				}
				if (i_138_ > i_132_)
					i_138_ = i_132_;
			}
			int i_139_ = 0;
			int i_140_ = 0;
			int i_141_ = 0;
			int i_142_ = 0;
			int i_143_ = 12345678;
			int i_144_ = -12345678;
			if (i_124_ != i_123_) {
				i_140_ = (i_127_ - i_126_ << 8) / (i_124_ - i_123_);
				i_142_ = (i_130_ - i_129_ << 8) / (i_124_ - i_123_);
				if (i_123_ < i_124_) {
					i_139_ = i_126_ << 8;
					i_141_ = i_129_ << 8;
					i_143_ = i_123_;
					i_144_ = i_124_;
				} else {
					i_139_ = i_127_ << 8;
					i_141_ = i_130_ << 8;
					i_143_ = i_124_;
					i_144_ = i_123_;
				}
				if (i_143_ < 0) {
					i_139_ -= i_140_ * i_143_;
					i_141_ -= i_142_ * i_143_;
					i_143_ = 0;
				}
				if (i_144_ > i_132_)
					i_144_ = i_132_;
			}
			int i_145_ = 0;
			int i_146_ = 0;
			int i_147_ = 0;
			int i_148_ = 0;
			int i_149_ = 12345678;
			int i_150_ = -12345678;
			if (i_125_ != i_124_) {
				i_146_ = (i_128_ - i_127_ << 8) / (i_125_ - i_124_);
				i_148_ = (i_131_ - i_130_ << 8) / (i_125_ - i_124_);
				if (i_124_ < i_125_) {
					i_145_ = i_127_ << 8;
					i_147_ = i_130_ << 8;
					i_149_ = i_124_;
					i_150_ = i_125_;
				} else {
					i_145_ = i_128_ << 8;
					i_147_ = i_131_ << 8;
					i_149_ = i_125_;
					i_150_ = i_124_;
				}
				if (i_149_ < 0) {
					i_145_ -= i_146_ * i_149_;
					i_147_ -= i_148_ * i_149_;
					i_149_ = 0;
				}
				if (i_150_ > i_132_)
					i_150_ = i_132_;
			}
			anInt532 = i_137_;
			if (i_143_ < anInt532)
				anInt532 = i_143_;
			if (i_149_ < anInt532)
				anInt532 = i_149_;
			anInt533 = i_138_;
			if (i_144_ > anInt533)
				anInt533 = i_144_;
			if (i_150_ > anInt533)
				anInt533 = i_150_;
			int i_151_ = 0;
			for (i_117_ = anInt532; i_117_ < anInt533; i_117_++) {
				if (i_117_ >= i_137_ && i_117_ < i_138_) {
					i = i_116_ = i_133_;
					i_118_ = i_151_ = i_135_;
					i_133_ += i_134_;
					i_135_ += i_136_;
				} else {
					i = 655360;
					i_116_ = -655360;
				}
				if (i_117_ >= i_143_ && i_117_ < i_144_) {
					if (i_139_ < i) {
						i = i_139_;
						i_118_ = i_141_;
					}
					if (i_139_ > i_116_) {
						i_116_ = i_139_;
						i_151_ = i_141_;
					}
					i_139_ += i_140_;
					i_141_ += i_142_;
				}
				if (i_117_ >= i_149_ && i_117_ < i_150_) {
					if (i_145_ < i) {
						i = i_145_;
						i_118_ = i_147_;
					}
					if (i_145_ > i_116_) {
						i_116_ = i_145_;
						i_151_ = i_147_;
					}
					i_145_ += i_146_;
					i_147_ += i_148_;
				}
				scanline class4 = aClass4Array531[i_117_];
				class4.anInt233 = i;
				class4.anInt234 = i_116_;
				class4.anInt235 = i_118_;
				class4.anInt236 = i_151_;
			}
			if (anInt532 < anInt487 - centerY)
				anInt532 = anInt487 - centerY;
		} else if (i_119_ == 4) {
			int i_152_ = is_120_[0] + anInt487;
			int i_153_ = is_120_[1] + anInt487;
			int i_154_ = is_120_[2] + anInt487;
			int i_155_ = is_120_[3] + anInt487;
			int i_156_ = is[0];
			int i_157_ = is[1];
			int i_158_ = is[2];
			int i_159_ = is[3];
			int i_160_ = is_121_[0];
			int i_161_ = is_121_[1];
			int i_162_ = is_121_[2];
			int i_163_ = is_121_[3];
			int i_164_ = anInt487 + centerY - 1;
			int i_165_ = 0;
			int i_166_ = 0;
			int i_167_ = 0;
			int i_168_ = 0;
			int i_169_ = 12345678;
			int i_170_ = -12345678;
			if (i_155_ != i_152_) {
				i_166_ = (i_159_ - i_156_ << 8) / (i_155_ - i_152_);
				i_168_ = (i_163_ - i_160_ << 8) / (i_155_ - i_152_);
				if (i_152_ < i_155_) {
					i_165_ = i_156_ << 8;
					i_167_ = i_160_ << 8;
					i_169_ = i_152_;
					i_170_ = i_155_;
				} else {
					i_165_ = i_159_ << 8;
					i_167_ = i_163_ << 8;
					i_169_ = i_155_;
					i_170_ = i_152_;
				}
				if (i_169_ < 0) {
					i_165_ -= i_166_ * i_169_;
					i_167_ -= i_168_ * i_169_;
					i_169_ = 0;
				}
				if (i_170_ > i_164_)
					i_170_ = i_164_;
			}
			int i_171_ = 0;
			int i_172_ = 0;
			int i_173_ = 0;
			int i_174_ = 0;
			int i_175_ = 12345678;
			int i_176_ = -12345678;
			if (i_153_ != i_152_) {
				i_172_ = (i_157_ - i_156_ << 8) / (i_153_ - i_152_);
				i_174_ = (i_161_ - i_160_ << 8) / (i_153_ - i_152_);
				if (i_152_ < i_153_) {
					i_171_ = i_156_ << 8;
					i_173_ = i_160_ << 8;
					i_175_ = i_152_;
					i_176_ = i_153_;
				} else {
					i_171_ = i_157_ << 8;
					i_173_ = i_161_ << 8;
					i_175_ = i_153_;
					i_176_ = i_152_;
				}
				if (i_175_ < 0) {
					i_171_ -= i_172_ * i_175_;
					i_173_ -= i_174_ * i_175_;
					i_175_ = 0;
				}
				if (i_176_ > i_164_)
					i_176_ = i_164_;
			}
			int i_177_ = 0;
			int i_178_ = 0;
			int i_179_ = 0;
			int i_180_ = 0;
			int i_181_ = 12345678;
			int i_182_ = -12345678;
			if (i_154_ != i_153_) {
				i_178_ = (i_158_ - i_157_ << 8) / (i_154_ - i_153_);
				i_180_ = (i_162_ - i_161_ << 8) / (i_154_ - i_153_);
				if (i_153_ < i_154_) {
					i_177_ = i_157_ << 8;
					i_179_ = i_161_ << 8;
					i_181_ = i_153_;
					i_182_ = i_154_;
				} else {
					i_177_ = i_158_ << 8;
					i_179_ = i_162_ << 8;
					i_181_ = i_154_;
					i_182_ = i_153_;
				}
				if (i_181_ < 0) {
					i_177_ -= i_178_ * i_181_;
					i_179_ -= i_180_ * i_181_;
					i_181_ = 0;
				}
				if (i_182_ > i_164_)
					i_182_ = i_164_;
			}
			int i_183_ = 0;
			int i_184_ = 0;
			int i_185_ = 0;
			int i_186_ = 0;
			int i_187_ = 12345678;
			int i_188_ = -12345678;
			if (i_155_ != i_154_) {
				i_184_ = (i_159_ - i_158_ << 8) / (i_155_ - i_154_);
				i_186_ = (i_163_ - i_162_ << 8) / (i_155_ - i_154_);
				if (i_154_ < i_155_) {
					i_183_ = i_158_ << 8;
					i_185_ = i_162_ << 8;
					i_187_ = i_154_;
					i_188_ = i_155_;
				} else {
					i_183_ = i_159_ << 8;
					i_185_ = i_163_ << 8;
					i_187_ = i_155_;
					i_188_ = i_154_;
				}
				if (i_187_ < 0) {
					i_183_ -= i_184_ * i_187_;
					i_185_ -= i_186_ * i_187_;
					i_187_ = 0;
				}
				if (i_188_ > i_164_)
					i_188_ = i_164_;
			}
			anInt532 = i_169_;
			if (i_175_ < anInt532)
				anInt532 = i_175_;
			if (i_181_ < anInt532)
				anInt532 = i_181_;
			if (i_187_ < anInt532)
				anInt532 = i_187_;
			anInt533 = i_170_;
			if (i_176_ > anInt533)
				anInt533 = i_176_;
			if (i_182_ > anInt533)
				anInt533 = i_182_;
			if (i_188_ > anInt533)
				anInt533 = i_188_;
			int i_189_ = 0;
			for (i_117_ = anInt532; i_117_ < anInt533; i_117_++) {
				if (i_117_ >= i_169_ && i_117_ < i_170_) {
					i = i_116_ = i_165_;
					i_118_ = i_189_ = i_167_;
					i_165_ += i_166_;
					i_167_ += i_168_;
				} else {
					i = 655360;
					i_116_ = -655360;
				}
				if (i_117_ >= i_175_ && i_117_ < i_176_) {
					if (i_171_ < i) {
						i = i_171_;
						i_118_ = i_173_;
					}
					if (i_171_ > i_116_) {
						i_116_ = i_171_;
						i_189_ = i_173_;
					}
					i_171_ += i_172_;
					i_173_ += i_174_;
				}
				if (i_117_ >= i_181_ && i_117_ < i_182_) {
					if (i_177_ < i) {
						i = i_177_;
						i_118_ = i_179_;
					}
					if (i_177_ > i_116_) {
						i_116_ = i_177_;
						i_189_ = i_179_;
					}
					i_177_ += i_178_;
					i_179_ += i_180_;
				}
				if (i_117_ >= i_187_ && i_117_ < i_188_) {
					if (i_183_ < i) {
						i = i_183_;
						i_118_ = i_185_;
					}
					if (i_183_ > i_116_) {
						i_116_ = i_183_;
						i_189_ = i_185_;
					}
					i_183_ += i_184_;
					i_185_ += i_186_;
				}
				scanline class4 = aClass4Array531[i_117_];
				class4.anInt233 = i;
				class4.anInt234 = i_116_;
				class4.anInt235 = i_118_;
				class4.anInt236 = i_189_;
			}
			if (anInt532 < anInt487 - centerY)
				anInt532 = anInt487 - centerY;
		} else {
			anInt533 = anInt532 = is_120_[0] += anInt487;
			for (i_117_ = 1; i_117_ < i_119_; i_117_++) {
				int i_190_;
				if ((i_190_ = is_120_[i_117_] += anInt487) < anInt532)
					anInt532 = i_190_;
				else if (i_190_ > anInt533)
					anInt533 = i_190_;
			}
			if (anInt532 < anInt487 - centerY)
				anInt532 = anInt487 - centerY;
			if (anInt533 >= anInt487 + centerY)
				anInt533 = anInt487 + centerY - 1;
			if (anInt532 >= anInt533)
				return;
			for (i_117_ = anInt532; i_117_ < anInt533; i_117_++) {
				scanline class4 = aClass4Array531[i_117_];
				class4.anInt233 = 655360;
				class4.anInt234 = -655360;
			}
			int i_191_ = i_119_ - 1;
			int i_192_ = is_120_[0];
			int i_193_ = is_120_[i_191_];
			if (i_192_ < i_193_) {
				int i_194_ = is[0] << 8;
				int i_195_ = (is[i_191_] - is[0] << 8) / (i_193_ - i_192_);
				int i_196_ = is_121_[0] << 8;
				int i_197_ = (is_121_[i_191_] - is_121_[0] << 8) / (i_193_ - i_192_);
				if (i_192_ < 0) {
					i_194_ -= i_195_ * i_192_;
					i_196_ -= i_197_ * i_192_;
					i_192_ = 0;
				}
				if (i_193_ > anInt533)
					i_193_ = anInt533;
				for (i_117_ = i_192_; i_117_ <= i_193_; i_117_++) {
					scanline class4 = aClass4Array531[i_117_];
					class4.anInt233 = class4.anInt234 = i_194_;
					class4.anInt235 = class4.anInt236 = i_196_;
					i_194_ += i_195_;
					i_196_ += i_197_;
				}
			} else if (i_192_ > i_193_) {
				int i_198_ = is[i_191_] << 8;
				int i_199_ = (is[0] - is[i_191_] << 8) / (i_192_ - i_193_);
				int i_200_ = is_121_[i_191_] << 8;
				int i_201_ = (is_121_[0] - is_121_[i_191_] << 8) / (i_192_ - i_193_);
				if (i_193_ < 0) {
					i_198_ -= i_199_ * i_193_;
					i_200_ -= i_201_ * i_193_;
					i_193_ = 0;
				}
				if (i_192_ > anInt533)
					i_192_ = anInt533;
				for (i_117_ = i_193_; i_117_ <= i_192_; i_117_++) {
					scanline class4 = aClass4Array531[i_117_];
					class4.anInt233 = class4.anInt234 = i_198_;
					class4.anInt235 = class4.anInt236 = i_200_;
					i_198_ += i_199_;
					i_200_ += i_201_;
				}
			}
			for (i_117_ = 0; i_117_ < i_191_; i_117_++) {
				int i_202_ = i_117_ + 1;
				i_192_ = is_120_[i_117_];
				i_193_ = is_120_[i_202_];
				if (i_192_ < i_193_) {
					int i_203_ = is[i_117_] << 8;
					int i_204_ = (is[i_202_] - is[i_117_] << 8) / (i_193_ - i_192_);
					int i_205_ = is_121_[i_117_] << 8;
					int i_206_ = ((is_121_[i_202_] - is_121_[i_117_] << 8) / (i_193_ - i_192_));
					if (i_192_ < 0) {
						i_203_ -= i_204_ * i_192_;
						i_205_ -= i_206_ * i_192_;
						i_192_ = 0;
					}
					if (i_193_ > anInt533)
						i_193_ = anInt533;
					for (int i_207_ = i_192_; i_207_ <= i_193_; i_207_++) {
						scanline class4 = aClass4Array531[i_207_];
						if (i_203_ < class4.anInt233) {
							class4.anInt233 = i_203_;
							class4.anInt235 = i_205_;
						}
						if (i_203_ > class4.anInt234) {
							class4.anInt234 = i_203_;
							class4.anInt236 = i_205_;
						}
						i_203_ += i_204_;
						i_205_ += i_206_;
					}
				} else if (i_192_ > i_193_) {
					int i_208_ = is[i_202_] << 8;
					int i_209_ = (is[i_117_] - is[i_202_] << 8) / (i_192_ - i_193_);
					int i_210_ = is_121_[i_202_] << 8;
					int i_211_ = ((is_121_[i_117_] - is_121_[i_202_] << 8) / (i_192_ - i_193_));
					if (i_193_ < 0) {
						i_208_ -= i_209_ * i_193_;
						i_210_ -= i_211_ * i_193_;
						i_193_ = 0;
					}
					if (i_192_ > anInt533)
						i_192_ = anInt533;
					for (int i_212_ = i_193_; i_212_ <= i_192_; i_212_++) {
						scanline class4 = aClass4Array531[i_212_];
						if (i_208_ < class4.anInt233) {
							class4.anInt233 = i_208_;
							class4.anInt235 = i_210_;
						}
						if (i_208_ > class4.anInt234) {
							class4.anInt234 = i_208_;
							class4.anInt236 = i_210_;
						}
						i_208_ += i_209_;
						i_210_ += i_211_;
					}
				}
			}
			if (anInt532 < anInt487 - centerY)
				anInt532 = anInt487 - centerY;
		}
		if (aBoolean472 && anInt474 >= anInt532 && anInt474 < anInt533) {
			scanline class4 = aClass4Array531[anInt474];
			if (anInt473 >= class4.anInt233 >> 8 && anInt473 <= class4.anInt234 >> 8 && class4.anInt233 <= class4.anInt234 && !class7.aBoolean339 && class7.aByteArray335[i_122_] == 0) {
				if (class7.anIntArray334[i_122_] >= anInt475 && class7.anIntArray334[i_122_] <= anInt476) {
					aClass7_479 = class7;
					anInt481 = i_122_;
				}
				if (class7.anIntArray334[i_122_] >= anInt477 && class7.anIntArray334[i_122_] <= anInt478) {
					aClass7_480 = class7;
					anInt482 = i_122_;
				}
			}
		}
	}

	private void method369(int i, int i_213_, int i_214_, int[] is, int[] is_215_, int[] is_216_, int i_217_, object3d class7) {
		if (i_217_ >= 0) {
			if (i_217_ >= texturesCount)
				i_217_ = 0;
			method387(i_217_);
			int i_218_ = is[0];
			int i_219_ = is_215_[0];
			int i_220_ = is_216_[0];
			int i_221_ = i_218_ - is[1];
			int i_222_ = i_219_ - is_215_[1];
			int i_223_ = i_220_ - is_216_[1];
			i_214_--;
			int i_224_ = is[i_214_] - i_218_;
			int i_225_ = is_215_[i_214_] - i_219_;
			int i_226_ = is_216_[i_214_] - i_220_;
			if (texturesSize[i_217_] == 1) {
				int i_227_ = i_224_ * i_219_ - i_225_ * i_218_ << 12;
				int i_228_ = (i_225_ * i_220_ - i_226_ * i_219_ << 5 - screenWidth + 7 + 4);
				int i_229_ = i_226_ * i_218_ - i_224_ * i_220_ << 5 - screenWidth + 7;
				int i_230_ = i_221_ * i_219_ - i_222_ * i_218_ << 12;
				int i_231_ = (i_222_ * i_220_ - i_223_ * i_219_ << 5 - screenWidth + 7 + 4);
				int i_232_ = i_223_ * i_218_ - i_221_ * i_220_ << 5 - screenWidth + 7;
				int i_233_ = i_222_ * i_224_ - i_221_ * i_225_ << 5;
				int i_234_ = i_223_ * i_225_ - i_222_ * i_226_ << 5 - screenWidth + 4;
				int i_235_ = i_221_ * i_226_ - i_223_ * i_224_ >> screenWidth - 5;
				int i_236_ = i_228_ >> 4;
				int i_237_ = i_231_ >> 4;
				int i_238_ = i_234_ >> 4;
				int i_239_ = anInt532 - anInt487;
				int i_240_ = anInt483;
				int i_241_ = anInt486 + anInt532 * i_240_;
				int i_242_ = 1;
				i_227_ += i_229_ * i_239_;
				i_230_ += i_232_ * i_239_;
				i_233_ += i_235_ * i_239_;
				if (lowMemory == true) {
					if ((anInt532 & 0x1) == 1) {
						anInt532++;
						i_227_ += i_229_;
						i_230_ += i_232_;
						i_233_ += i_235_;
						i_241_ += i_240_;
					}
					i_229_ <<= 1;
					i_232_ <<= 1;
					i_235_ <<= 1;
					i_240_ <<= 1;
					i_242_ = 2;
				}
				if (class7.aBoolean331) {
					for (i = anInt532; i < anInt533; i += i_242_) {
						scanline class4 = aClass4Array531[i];
						i_213_ = class4.anInt233 >> 8;
						int i_243_ = class4.anInt234 >> 8;
						int i_244_ = i_243_ - i_213_;
						if (i_244_ <= 0) {
							i_227_ += i_229_;
							i_230_ += i_232_;
							i_233_ += i_235_;
							i_241_ += i_240_;
						} else {
							int i_245_ = class4.anInt235;
							int i_246_ = (class4.anInt236 - i_245_) / i_244_;
							if (i_213_ < -centerX) {
								i_245_ += (-centerX - i_213_) * i_246_;
								i_213_ = -centerX;
								i_244_ = i_243_ - i_213_;
							}
							if (i_243_ > centerX) {
								i_243_ = centerX;
								i_244_ = i_243_ - i_213_;
							}
							method371(pixels, texturesPixels[i_217_], 0, 0, i_227_ + i_236_ * i_213_, i_230_ + i_237_ * i_213_, i_233_ + i_238_ * i_213_, i_228_, i_231_, i_234_, i_244_, i_241_ + i_213_, i_245_, i_246_ << 2);
							i_227_ += i_229_;
							i_230_ += i_232_;
							i_233_ += i_235_;
							i_241_ += i_240_;
						}
					}
				} else if (!aBooleanArray521[i_217_]) {
					for (i = anInt532; i < anInt533; i += i_242_) {
						scanline class4 = aClass4Array531[i];
						i_213_ = class4.anInt233 >> 8;
						int i_247_ = class4.anInt234 >> 8;
						int i_248_ = i_247_ - i_213_;
						if (i_248_ <= 0) {
							i_227_ += i_229_;
							i_230_ += i_232_;
							i_233_ += i_235_;
							i_241_ += i_240_;
						} else {
							int i_249_ = class4.anInt235;
							int i_250_ = (class4.anInt236 - i_249_) / i_248_;
							if (i_213_ < -centerX) {
								i_249_ += (-centerX - i_213_) * i_250_;
								i_213_ = -centerX;
								i_248_ = i_247_ - i_213_;
							}
							if (i_247_ > centerX) {
								i_247_ = centerX;
								i_248_ = i_247_ - i_213_;
							}
							method370(pixels, texturesPixels[i_217_], 0, 0, i_227_ + i_236_ * i_213_, i_230_ + i_237_ * i_213_, i_233_ + i_238_ * i_213_, i_228_, i_231_, i_234_, i_248_, i_241_ + i_213_, i_249_, i_250_ << 2);
							i_227_ += i_229_;
							i_230_ += i_232_;
							i_233_ += i_235_;
							i_241_ += i_240_;
						}
					}
				} else {
					for (i = anInt532; i < anInt533; i += i_242_) {
						scanline class4 = aClass4Array531[i];
						i_213_ = class4.anInt233 >> 8;
						int i_251_ = class4.anInt234 >> 8;
						int i_252_ = i_251_ - i_213_;
						if (i_252_ <= 0) {
							i_227_ += i_229_;
							i_230_ += i_232_;
							i_233_ += i_235_;
							i_241_ += i_240_;
						} else {
							int i_253_ = class4.anInt235;
							int i_254_ = (class4.anInt236 - i_253_) / i_252_;
							if (i_213_ < -centerX) {
								i_253_ += (-centerX - i_213_) * i_254_;
								i_213_ = -centerX;
								i_252_ = i_251_ - i_213_;
							}
							if (i_251_ > centerX) {
								i_251_ = centerX;
								i_252_ = i_251_ - i_213_;
							}
							method372(pixels, 0, 0, 0, texturesPixels[i_217_], i_227_ + i_236_ * i_213_, i_230_ + i_237_ * i_213_, i_233_ + i_238_ * i_213_, i_228_, i_231_, i_234_, i_252_, i_241_ + i_213_, i_253_, i_254_);
							i_227_ += i_229_;
							i_230_ += i_232_;
							i_233_ += i_235_;
							i_241_ += i_240_;
						}
					}
				}
			} else {
				int i_255_ = i_224_ * i_219_ - i_225_ * i_218_ << 11;
				int i_256_ = (i_225_ * i_220_ - i_226_ * i_219_ << 5 - screenWidth + 6 + 4);
				int i_257_ = i_226_ * i_218_ - i_224_ * i_220_ << 5 - screenWidth + 6;
				int i_258_ = i_221_ * i_219_ - i_222_ * i_218_ << 11;
				int i_259_ = (i_222_ * i_220_ - i_223_ * i_219_ << 5 - screenWidth + 6 + 4);
				int i_260_ = i_223_ * i_218_ - i_221_ * i_220_ << 5 - screenWidth + 6;
				int i_261_ = i_222_ * i_224_ - i_221_ * i_225_ << 5;
				int i_262_ = i_223_ * i_225_ - i_222_ * i_226_ << 5 - screenWidth + 4;
				int i_263_ = i_221_ * i_226_ - i_223_ * i_224_ >> screenWidth - 5;
				int i_264_ = i_256_ >> 4;
				int i_265_ = i_259_ >> 4;
				int i_266_ = i_262_ >> 4;
				int i_267_ = anInt532 - anInt487;
				int i_268_ = anInt483;
				int i_269_ = anInt486 + anInt532 * i_268_;
				int i_270_ = 1;
				i_255_ += i_257_ * i_267_;
				i_258_ += i_260_ * i_267_;
				i_261_ += i_263_ * i_267_;
				if (lowMemory == true) {
					if ((anInt532 & 0x1) == 1) {
						anInt532++;
						i_255_ += i_257_;
						i_258_ += i_260_;
						i_261_ += i_263_;
						i_269_ += i_268_;
					}
					i_257_ <<= 1;
					i_260_ <<= 1;
					i_263_ <<= 1;
					i_268_ <<= 1;
					i_270_ = 2;
				}
				if (class7.aBoolean331) {
					for (i = anInt532; i < anInt533; i += i_270_) {
						scanline class4 = aClass4Array531[i];
						i_213_ = class4.anInt233 >> 8;
						int i_271_ = class4.anInt234 >> 8;
						int i_272_ = i_271_ - i_213_;
						if (i_272_ <= 0) {
							i_255_ += i_257_;
							i_258_ += i_260_;
							i_261_ += i_263_;
							i_269_ += i_268_;
						} else {
							int i_273_ = class4.anInt235;
							int i_274_ = (class4.anInt236 - i_273_) / i_272_;
							if (i_213_ < -centerX) {
								i_273_ += (-centerX - i_213_) * i_274_;
								i_213_ = -centerX;
								i_272_ = i_271_ - i_213_;
							}
							if (i_271_ > centerX) {
								i_271_ = centerX;
								i_272_ = i_271_ - i_213_;
							}
							method374(pixels, texturesPixels[i_217_], 0, 0, i_255_ + i_264_ * i_213_, i_258_ + i_265_ * i_213_, i_261_ + i_266_ * i_213_, i_256_, i_259_, i_262_, i_272_, i_269_ + i_213_, i_273_, i_274_);
							i_255_ += i_257_;
							i_258_ += i_260_;
							i_261_ += i_263_;
							i_269_ += i_268_;
						}
					}
				} else if (!aBooleanArray521[i_217_]) {
					for (i = anInt532; i < anInt533; i += i_270_) {
						scanline class4 = aClass4Array531[i];
						i_213_ = class4.anInt233 >> 8;
						int i_275_ = class4.anInt234 >> 8;
						int i_276_ = i_275_ - i_213_;
						if (i_276_ <= 0) {
							i_255_ += i_257_;
							i_258_ += i_260_;
							i_261_ += i_263_;
							i_269_ += i_268_;
						} else {
							int i_277_ = class4.anInt235;
							int i_278_ = (class4.anInt236 - i_277_) / i_276_;
							if (i_213_ < -centerX) {
								i_277_ += (-centerX - i_213_) * i_278_;
								i_213_ = -centerX;
								i_276_ = i_275_ - i_213_;
							}
							if (i_275_ > centerX) {
								i_275_ = centerX;
								i_276_ = i_275_ - i_213_;
							}
							method373(pixels, texturesPixels[i_217_], 0, 0, i_255_ + i_264_ * i_213_, i_258_ + i_265_ * i_213_, i_261_ + i_266_ * i_213_, i_256_, i_259_, i_262_, i_276_, i_269_ + i_213_, i_277_, i_278_);
							i_255_ += i_257_;
							i_258_ += i_260_;
							i_261_ += i_263_;
							i_269_ += i_268_;
						}
					}
				} else {
					for (i = anInt532; i < anInt533; i += i_270_) {
						scanline class4 = aClass4Array531[i];
						i_213_ = class4.anInt233 >> 8;
						int i_279_ = class4.anInt234 >> 8;
						int i_280_ = i_279_ - i_213_;
						if (i_280_ <= 0) {
							i_255_ += i_257_;
							i_258_ += i_260_;
							i_261_ += i_263_;
							i_269_ += i_268_;
						} else {
							int i_281_ = class4.anInt235;
							int i_282_ = (class4.anInt236 - i_281_) / i_280_;
							if (i_213_ < -centerX) {
								i_281_ += (-centerX - i_213_) * i_282_;
								i_213_ = -centerX;
								i_280_ = i_279_ - i_213_;
							}
							if (i_279_ > centerX) {
								i_279_ = centerX;
								i_280_ = i_279_ - i_213_;
							}
							method375(pixels, 0, 0, 0, texturesPixels[i_217_], i_255_ + i_264_ * i_213_, i_258_ + i_265_ * i_213_, i_261_ + i_266_ * i_213_, i_256_, i_259_, i_262_, i_280_, i_269_ + i_213_, i_281_, i_282_);
							i_255_ += i_257_;
							i_258_ += i_260_;
							i_261_ += i_263_;
							i_269_ += i_268_;
						}
					}
				}
			}
		} else {
			for (int i_283_ = 0; i_283_ < anInt456; i_283_++) {
				if (anIntArray457[i_283_] == i_217_) {
					anIntArray459 = anIntArrayArray458[i_283_];
					break;
				}
				if (i_283_ == anInt456 - 1) {
					int i_284_ = (int) (Math.random() * (double) anInt456);
					anIntArray457[i_284_] = i_217_;
					i_217_ = -1 - i_217_;
					int i_285_ = (i_217_ >> 10 & 0x1f) * 8;
					int i_286_ = (i_217_ >> 5 & 0x1f) * 8;
					int i_287_ = (i_217_ & 0x1f) * 8;
					for (int i_288_ = 0; i_288_ < 256; i_288_++) {
						int i_289_ = i_288_ * i_288_;
						int i_290_ = i_285_ * i_289_ / 65536;
						int i_291_ = i_286_ * i_289_ / 65536;
						int i_292_ = i_287_ * i_289_ / 65536;
						anIntArrayArray458[i_284_][255 - i_288_] = (i_290_ << 16) + (i_291_ << 8) + i_292_;
					}
					anIntArray459 = anIntArrayArray458[i_284_];
				}
			}
			int i_293_ = anInt483;
			int i_294_ = anInt486 + anInt532 * i_293_;
			int i_295_ = 1;
			if (lowMemory == true) {
				if ((anInt532 & 0x1) == 1) {
					anInt532++;
					i_294_ += i_293_;
				}
				i_293_ <<= 1;
				i_295_ = 2;
			}
			if (class7.aBoolean332) {
				for (i = anInt532; i < anInt533; i += i_295_) {
					scanline class4 = aClass4Array531[i];
					i_213_ = class4.anInt233 >> 8;
					int i_296_ = class4.anInt234 >> 8;
					int i_297_ = i_296_ - i_213_;
					if (i_297_ <= 0)
						i_294_ += i_293_;
					else {
						int i_298_ = class4.anInt235;
						int i_299_ = (class4.anInt236 - i_298_) / i_297_;
						if (i_213_ < -centerX) {
							i_298_ += (-centerX - i_213_) * i_299_;
							i_213_ = -centerX;
							i_297_ = i_296_ - i_213_;
						}
						if (i_296_ > centerX) {
							i_296_ = centerX;
							i_297_ = i_296_ - i_213_;
						}
						method377(pixels, -i_297_, i_294_ + i_213_, 0, anIntArray459, i_298_, i_299_);
						i_294_ += i_293_;
					}
				}
			} else if (aBoolean469) {
				for (i = anInt532; i < anInt533; i += i_295_) {
					scanline class4 = aClass4Array531[i];
					i_213_ = class4.anInt233 >> 8;
					int i_300_ = class4.anInt234 >> 8;
					int i_301_ = i_300_ - i_213_;
					if (i_301_ <= 0)
						i_294_ += i_293_;
					else {
						int i_302_ = class4.anInt235;
						int i_303_ = (class4.anInt236 - i_302_) / i_301_;
						if (i_213_ < -centerX) {
							i_302_ += (-centerX - i_213_) * i_303_;
							i_213_ = -centerX;
							i_301_ = i_300_ - i_213_;
						}
						if (i_300_ > centerX) {
							i_300_ = centerX;
							i_301_ = i_300_ - i_213_;
						}
						method376(pixels, -i_301_, i_294_ + i_213_, 0, anIntArray459, i_302_, i_303_);
						i_294_ += i_293_;
					}
				}
			} else {
				for (i = anInt532; i < anInt533; i += i_295_) {
					scanline class4 = aClass4Array531[i];
					i_213_ = class4.anInt233 >> 8;
					int i_304_ = class4.anInt234 >> 8;
					int i_305_ = i_304_ - i_213_;
					if (i_305_ <= 0)
						i_294_ += i_293_;
					else {
						int i_306_ = class4.anInt235;
						int i_307_ = (class4.anInt236 - i_306_) / i_305_;
						if (i_213_ < -centerX) {
							i_306_ += (-centerX - i_213_) * i_307_;
							i_213_ = -centerX;
							i_305_ = i_304_ - i_213_;
						}
						if (i_304_ > centerX) {
							i_304_ = centerX;
							i_305_ = i_304_ - i_213_;
						}
						method378(pixels, -i_305_, i_294_ + i_213_, 0, anIntArray459, i_306_, i_307_);
						i_294_ += i_293_;
					}
				}
			}
		}
	}

	private static void method370(int[] is, int[] is_308_, int i, int i_309_, int i_310_, int i_311_, int i_312_, int i_313_, int i_314_, int i_315_, int i_316_, int i_317_, int i_318_, int i_319_) {
		if (i_316_ > 0) {
			int i_320_ = 0;
			int i_321_ = 0;
			int i_322_ = 0;
			if (i_312_ != 0) {
				i = i_310_ / i_312_ << 7;
				i_309_ = i_311_ / i_312_ << 7;
			}
			if (i < 0)
				i = 0;
			else if (i > 16256)
				i = 16256;
			i_310_ += i_313_;
			i_311_ += i_314_;
			i_312_ += i_315_;
			if (i_312_ != 0) {
				i_320_ = i_310_ / i_312_ << 7;
				i_321_ = i_311_ / i_312_ << 7;
			}
			if (i_320_ < 0)
				i_320_ = 0;
			else if (i_320_ > 16256)
				i_320_ = 16256;
			int i_323_ = i_320_ - i >> 4;
			int i_324_ = i_321_ - i_309_ >> 4;
			for (int i_325_ = i_316_ >> 4; i_325_ > 0; i_325_--) {
				i += i_318_ & 0x600000;
				i_322_ = i_318_ >> 23;
				i_318_ += i_319_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				i = (i & 0x3fff) + (i_318_ & 0x600000);
				i_322_ = i_318_ >> 23;
				i_318_ += i_319_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				i = (i & 0x3fff) + (i_318_ & 0x600000);
				i_322_ = i_318_ >> 23;
				i_318_ += i_319_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				i = (i & 0x3fff) + (i_318_ & 0x600000);
				i_322_ = i_318_ >> 23;
				i_318_ += i_319_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i = i_320_;
				i_309_ = i_321_;
				i_310_ += i_313_;
				i_311_ += i_314_;
				i_312_ += i_315_;
				if (i_312_ != 0) {
					i_320_ = i_310_ / i_312_ << 7;
					i_321_ = i_311_ / i_312_ << 7;
				}
				if (i_320_ < 0)
					i_320_ = 0;
				else if (i_320_ > 16256)
					i_320_ = 16256;
				i_323_ = i_320_ - i >> 4;
				i_324_ = i_321_ - i_309_ >> 4;
			}
			for (int i_326_ = 0; i_326_ < (i_316_ & 0xf); i_326_++) {
				if ((i_326_ & 0x3) == 0) {
					i = (i & 0x3fff) + (i_318_ & 0x600000);
					i_322_ = i_318_ >> 23;
					i_318_ += i_319_;
				}
				is[i_317_++] = is_308_[(i_309_ & 0x3f80) + (i >> 7)] >>> i_322_;
				i += i_323_;
				i_309_ += i_324_;
			}
		}
	}

	private static void method371(int[] is, int[] is_327_, int i, int i_328_, int i_329_, int i_330_, int i_331_, int i_332_, int i_333_, int i_334_, int i_335_, int i_336_, int i_337_, int i_338_) {
		if (i_335_ > 0) {
			int i_339_ = 0;
			int i_340_ = 0;
			int i_341_ = 0;
			if (i_331_ != 0) {
				i = i_329_ / i_331_ << 7;
				i_328_ = i_330_ / i_331_ << 7;
			}
			if (i < 0)
				i = 0;
			else if (i > 16256)
				i = 16256;
			i_329_ += i_332_;
			i_330_ += i_333_;
			i_331_ += i_334_;
			if (i_331_ != 0) {
				i_339_ = i_329_ / i_331_ << 7;
				i_340_ = i_330_ / i_331_ << 7;
			}
			if (i_339_ < 0)
				i_339_ = 0;
			else if (i_339_ > 16256)
				i_339_ = 16256;
			int i_342_ = i_339_ - i >> 4;
			int i_343_ = i_340_ - i_328_ >> 4;
			for (int i_344_ = i_335_ >> 4; i_344_ > 0; i_344_--) {
				i += i_337_ & 0x600000;
				i_341_ = i_337_ >> 23;
				i_337_ += i_338_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				i = (i & 0x3fff) + (i_337_ & 0x600000);
				i_341_ = i_337_ >> 23;
				i_337_ += i_338_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				i = (i & 0x3fff) + (i_337_ & 0x600000);
				i_341_ = i_337_ >> 23;
				i_337_ += i_338_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				i = (i & 0x3fff) + (i_337_ & 0x600000);
				i_341_ = i_337_ >> 23;
				i_337_ += i_338_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i = i_339_;
				i_328_ = i_340_;
				i_329_ += i_332_;
				i_330_ += i_333_;
				i_331_ += i_334_;
				if (i_331_ != 0) {
					i_339_ = i_329_ / i_331_ << 7;
					i_340_ = i_330_ / i_331_ << 7;
				}
				if (i_339_ < 0)
					i_339_ = 0;
				else if (i_339_ > 16256)
					i_339_ = 16256;
				i_342_ = i_339_ - i >> 4;
				i_343_ = i_340_ - i_328_ >> 4;
			}
			for (int i_345_ = 0; i_345_ < (i_335_ & 0xf); i_345_++) {
				if ((i_345_ & 0x3) == 0) {
					i = (i & 0x3fff) + (i_337_ & 0x600000);
					i_341_ = i_337_ >> 23;
					i_337_ += i_338_;
				}
				is[i_336_++] = (is_327_[(i_328_ & 0x3f80) + (i >> 7)] >>> i_341_) + (is[i_336_] >> 1 & 0x7f7f7f);
				i += i_342_;
				i_328_ += i_343_;
			}
		}
	}

	private static void method372(int[] is, int i, int i_346_, int i_347_, int[] is_348_, int i_349_, int i_350_, int i_351_, int i_352_, int i_353_, int i_354_, int i_355_, int i_356_, int i_357_, int i_358_) {
		if (i_355_ > 0) {
			int i_359_ = 0;
			int i_360_ = 0;
			i_358_ <<= 2;
			if (i_351_ != 0) {
				i_359_ = i_349_ / i_351_ << 7;
				i_360_ = i_350_ / i_351_ << 7;
			}
			if (i_359_ < 0)
				i_359_ = 0;
			else if (i_359_ > 16256)
				i_359_ = 16256;
			for (int i_361_ = i_355_; i_361_ > 0; i_361_ -= 16) {
				i_349_ += i_352_;
				i_350_ += i_353_;
				i_351_ += i_354_;
				i_346_ = i_359_;
				i_347_ = i_360_;
				if (i_351_ != 0) {
					i_359_ = i_349_ / i_351_ << 7;
					i_360_ = i_350_ / i_351_ << 7;
				}
				if (i_359_ < 0)
					i_359_ = 0;
				else if (i_359_ > 16256)
					i_359_ = 16256;
				int i_362_ = i_359_ - i_346_ >> 4;
				int i_363_ = i_360_ - i_347_ >> 4;
				int i_364_ = i_357_ >> 23;
				i_346_ += i_357_ & 0x600000;
				i_357_ += i_358_;
				if (i_361_ < 16) {
					for (int i_365_ = 0; i_365_ < i_361_; i_365_++) {
						if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
							is[i_356_] = i;
						i_356_++;
						i_346_ += i_362_;
						i_347_ += i_363_;
						if ((i_365_ & 0x3) == 3) {
							i_346_ = (i_346_ & 0x3fff) + (i_357_ & 0x600000);
							i_364_ = i_357_ >> 23;
							i_357_ += i_358_;
						}
					}
				} else {
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					i_346_ = (i_346_ & 0x3fff) + (i_357_ & 0x600000);
					i_364_ = i_357_ >> 23;
					i_357_ += i_358_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					i_346_ = (i_346_ & 0x3fff) + (i_357_ & 0x600000);
					i_364_ = i_357_ >> 23;
					i_357_ += i_358_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					i_346_ = (i_346_ & 0x3fff) + (i_357_ & 0x600000);
					i_364_ = i_357_ >> 23;
					i_357_ += i_358_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
					i_346_ += i_362_;
					i_347_ += i_363_;
					if ((i = (is_348_[(i_347_ & 0x3f80) + (i_346_ >> 7)] >>> i_364_)) != 0)
						is[i_356_] = i;
					i_356_++;
				}
			}
		}
	}

	private static void method373(int[] is, int[] is_366_, int i, int i_367_, int i_368_, int i_369_, int i_370_, int i_371_, int i_372_, int i_373_, int i_374_, int i_375_, int i_376_, int i_377_) {
		if (i_374_ > 0) {
			int i_378_ = 0;
			int i_379_ = 0;
			i_377_ <<= 2;
			if (i_370_ != 0) {
				i_378_ = i_368_ / i_370_ << 6;
				i_379_ = i_369_ / i_370_ << 6;
			}
			if (i_378_ < 0)
				i_378_ = 0;
			else if (i_378_ > 4032)
				i_378_ = 4032;
			for (int i_380_ = i_374_; i_380_ > 0; i_380_ -= 16) {
				i_368_ += i_371_;
				i_369_ += i_372_;
				i_370_ += i_373_;
				i = i_378_;
				i_367_ = i_379_;
				if (i_370_ != 0) {
					i_378_ = i_368_ / i_370_ << 6;
					i_379_ = i_369_ / i_370_ << 6;
				}
				if (i_378_ < 0)
					i_378_ = 0;
				else if (i_378_ > 4032)
					i_378_ = 4032;
				int i_381_ = i_378_ - i >> 4;
				int i_382_ = i_379_ - i_367_ >> 4;
				int i_383_ = i_376_ >> 20;
				i += i_376_ & 0xc0000;
				i_376_ += i_377_;
				if (i_380_ < 16) {
					for (int i_384_ = 0; i_384_ < i_380_; i_384_++) {
						is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
						i += i_381_;
						i_367_ += i_382_;
						if ((i_384_ & 0x3) == 3) {
							i = (i & 0xfff) + (i_376_ & 0xc0000);
							i_383_ = i_376_ >> 20;
							i_376_ += i_377_;
						}
					}
				} else {
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					i = (i & 0xfff) + (i_376_ & 0xc0000);
					i_383_ = i_376_ >> 20;
					i_376_ += i_377_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					i = (i & 0xfff) + (i_376_ & 0xc0000);
					i_383_ = i_376_ >> 20;
					i_376_ += i_377_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					i = (i & 0xfff) + (i_376_ & 0xc0000);
					i_383_ = i_376_ >> 20;
					i_376_ += i_377_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
					i += i_381_;
					i_367_ += i_382_;
					is[i_375_++] = is_366_[(i_367_ & 0xfc0) + (i >> 6)] >>> i_383_;
				}
			}
		}
	}

	private static void method374(int[] is, int[] is_385_, int i, int i_386_, int i_387_, int i_388_, int i_389_, int i_390_, int i_391_, int i_392_, int i_393_, int i_394_, int i_395_, int i_396_) {
		if (i_393_ > 0) {
			int i_397_ = 0;
			int i_398_ = 0;
			i_396_ <<= 2;
			if (i_389_ != 0) {
				i_397_ = i_387_ / i_389_ << 6;
				i_398_ = i_388_ / i_389_ << 6;
			}
			if (i_397_ < 0)
				i_397_ = 0;
			else if (i_397_ > 4032)
				i_397_ = 4032;
			for (int i_399_ = i_393_; i_399_ > 0; i_399_ -= 16) {
				i_387_ += i_390_;
				i_388_ += i_391_;
				i_389_ += i_392_;
				i = i_397_;
				i_386_ = i_398_;
				if (i_389_ != 0) {
					i_397_ = i_387_ / i_389_ << 6;
					i_398_ = i_388_ / i_389_ << 6;
				}
				if (i_397_ < 0)
					i_397_ = 0;
				else if (i_397_ > 4032)
					i_397_ = 4032;
				int i_400_ = i_397_ - i >> 4;
				int i_401_ = i_398_ - i_386_ >> 4;
				int i_402_ = i_395_ >> 20;
				i += i_395_ & 0xc0000;
				i_395_ += i_396_;
				if (i_399_ < 16) {
					for (int i_403_ = 0; i_403_ < i_399_; i_403_++) {
						is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
						i += i_400_;
						i_386_ += i_401_;
						if ((i_403_ & 0x3) == 3) {
							i = (i & 0xfff) + (i_395_ & 0xc0000);
							i_402_ = i_395_ >> 20;
							i_395_ += i_396_;
						}
					}
				} else {
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					i = (i & 0xfff) + (i_395_ & 0xc0000);
					i_402_ = i_395_ >> 20;
					i_395_ += i_396_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					i = (i & 0xfff) + (i_395_ & 0xc0000);
					i_402_ = i_395_ >> 20;
					i_395_ += i_396_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					i = (i & 0xfff) + (i_395_ & 0xc0000);
					i_402_ = i_395_ >> 20;
					i_395_ += i_396_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
					i += i_400_;
					i_386_ += i_401_;
					is[i_394_++] = (is_385_[(i_386_ & 0xfc0) + (i >> 6)] >>> i_402_) + (is[i_394_] >> 1 & 0x7f7f7f);
				}
			}
		}
	}

	private static void method375(int[] is, int i, int i_404_, int i_405_, int[] is_406_, int i_407_, int i_408_, int i_409_, int i_410_, int i_411_, int i_412_, int i_413_, int i_414_, int i_415_, int i_416_) {
		if (i_413_ > 0) {
			int i_417_ = 0;
			int i_418_ = 0;
			i_416_ <<= 2;
			if (i_409_ != 0) {
				i_417_ = i_407_ / i_409_ << 6;
				i_418_ = i_408_ / i_409_ << 6;
			}
			if (i_417_ < 0)
				i_417_ = 0;
			else if (i_417_ > 4032)
				i_417_ = 4032;
			for (int i_419_ = i_413_; i_419_ > 0; i_419_ -= 16) {
				i_407_ += i_410_;
				i_408_ += i_411_;
				i_409_ += i_412_;
				i_404_ = i_417_;
				i_405_ = i_418_;
				if (i_409_ != 0) {
					i_417_ = i_407_ / i_409_ << 6;
					i_418_ = i_408_ / i_409_ << 6;
				}
				if (i_417_ < 0)
					i_417_ = 0;
				else if (i_417_ > 4032)
					i_417_ = 4032;
				int i_420_ = i_417_ - i_404_ >> 4;
				int i_421_ = i_418_ - i_405_ >> 4;
				int i_422_ = i_415_ >> 20;
				i_404_ += i_415_ & 0xc0000;
				i_415_ += i_416_;
				if (i_419_ < 16) {
					for (int i_423_ = 0; i_423_ < i_419_; i_423_++) {
						if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
							is[i_414_] = i;
						i_414_++;
						i_404_ += i_420_;
						i_405_ += i_421_;
						if ((i_423_ & 0x3) == 3) {
							i_404_ = (i_404_ & 0xfff) + (i_415_ & 0xc0000);
							i_422_ = i_415_ >> 20;
							i_415_ += i_416_;
						}
					}
				} else {
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					i_404_ = (i_404_ & 0xfff) + (i_415_ & 0xc0000);
					i_422_ = i_415_ >> 20;
					i_415_ += i_416_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					i_404_ = (i_404_ & 0xfff) + (i_415_ & 0xc0000);
					i_422_ = i_415_ >> 20;
					i_415_ += i_416_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					i_404_ = (i_404_ & 0xfff) + (i_415_ & 0xc0000);
					i_422_ = i_415_ >> 20;
					i_415_ += i_416_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
					i_404_ += i_420_;
					i_405_ += i_421_;
					if ((i = (is_406_[(i_405_ & 0xfc0) + (i_404_ >> 6)] >>> i_422_)) != 0)
						is[i_414_] = i;
					i_414_++;
				}
			}
		}
	}

	private static void method376(int[] is, int i, int i_424_, int i_425_, int[] is_426_, int i_427_, int i_428_) {
		if (i < 0) {
			i_428_ <<= 1;
			i_425_ = is_426_[i_427_ >> 8 & 0xff];
			i_427_ += i_428_;
			int i_429_ = i / 8;
			for (int i_430_ = i_429_; i_430_ < 0; i_430_++) {
				is[i_424_++] = i_425_;
				is[i_424_++] = i_425_;
				i_425_ = is_426_[i_427_ >> 8 & 0xff];
				i_427_ += i_428_;
				is[i_424_++] = i_425_;
				is[i_424_++] = i_425_;
				i_425_ = is_426_[i_427_ >> 8 & 0xff];
				i_427_ += i_428_;
				is[i_424_++] = i_425_;
				is[i_424_++] = i_425_;
				i_425_ = is_426_[i_427_ >> 8 & 0xff];
				i_427_ += i_428_;
				is[i_424_++] = i_425_;
				is[i_424_++] = i_425_;
				i_425_ = is_426_[i_427_ >> 8 & 0xff];
				i_427_ += i_428_;
			}
			i_429_ = -(i % 8);
			for (int i_431_ = 0; i_431_ < i_429_; i_431_++) {
				is[i_424_++] = i_425_;
				if ((i_431_ & 0x1) == 1) {
					i_425_ = is_426_[i_427_ >> 8 & 0xff];
					i_427_ += i_428_;
				}
			}
		}
	}

	private static void method377(int[] is, int i, int i_432_, int i_433_, int[] is_434_, int i_435_, int i_436_) {
		if (i < 0) {
			i_436_ <<= 2;
			i_433_ = is_434_[i_435_ >> 8 & 0xff];
			i_435_ += i_436_;
			int i_437_ = i / 16;
			for (int i_438_ = i_437_; i_438_ < 0; i_438_++) {
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				i_433_ = is_434_[i_435_ >> 8 & 0xff];
				i_435_ += i_436_;
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				i_433_ = is_434_[i_435_ >> 8 & 0xff];
				i_435_ += i_436_;
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				i_433_ = is_434_[i_435_ >> 8 & 0xff];
				i_435_ += i_436_;
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				i_433_ = is_434_[i_435_ >> 8 & 0xff];
				i_435_ += i_436_;
			}
			i_437_ = -(i % 16);
			for (int i_439_ = 0; i_439_ < i_437_; i_439_++) {
				is[i_432_++] = i_433_ + (is[i_432_] >> 1 & 0x7f7f7f);
				if ((i_439_ & 0x3) == 3) {
					i_433_ = is_434_[i_435_ >> 8 & 0xff];
					i_435_ += i_436_;
					i_435_ += i_436_;
				}
			}
		}
	}

	private static void method378(int[] is, int i, int i_440_, int i_441_, int[] is_442_, int i_443_, int i_444_) {
		if (i < 0) {
			i_444_ <<= 2;
			i_441_ = is_442_[i_443_ >> 8 & 0xff];
			i_443_ += i_444_;
			int i_445_ = i / 16;
			for (int i_446_ = i_445_; i_446_ < 0; i_446_++) {
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				i_441_ = is_442_[i_443_ >> 8 & 0xff];
				i_443_ += i_444_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				i_441_ = is_442_[i_443_ >> 8 & 0xff];
				i_443_ += i_444_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				i_441_ = is_442_[i_443_ >> 8 & 0xff];
				i_443_ += i_444_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				is[i_440_++] = i_441_;
				i_441_ = is_442_[i_443_ >> 8 & 0xff];
				i_443_ += i_444_;
			}
			i_445_ = -(i % 16);
			for (int i_447_ = 0; i_447_ < i_445_; i_447_++) {
				is[i_440_++] = i_441_;
				if ((i_447_ & 0x3) == 3) {
					i_441_ = is_442_[i_443_ >> 8 & 0xff];
					i_443_ += i_444_;
				}
			}
		}
	}

	public void setCamera(int camX, int camY, int camZ, int camPitch, int camYaw, int camRoll, int zoom) {
		cameraPitch = 1024 - camPitch & 0x3ff;
		cameraYaw = 1024 - camYaw & 0x3ff;
		cameraRoll = 1024 - camRoll & 0x3ff;
		int xOff = 0;
		int yOff = 0;
		int zOff = zoom;
		if (camPitch != 0) {
			int pitchSin = sinCosLookupTable1024[camPitch];
			int pitchCos = sinCosLookupTable1024[camPitch + 1024];
			int y = yOff * pitchCos - zOff * pitchSin >> 15;
			zOff = yOff * pitchSin + zOff * pitchCos >> 15;
			yOff = y;
		}
		if (camYaw != 0) {
			int yawSin = sinCosLookupTable1024[camYaw];
			int yawCos = sinCosLookupTable1024[camYaw + 1024];
			int x = zOff * yawSin + xOff * yawCos >> 15;
			zOff = zOff * yawCos - xOff * yawSin >> 15;
			xOff = x;
		}
		if (camRoll != 0) {
			int rollSin = sinCosLookupTable1024[camRoll];
			int rollCos = sinCosLookupTable1024[camRoll + 1024];
			int x = yOff * rollSin + xOff * rollCos >> 15;
			yOff = yOff * rollCos - xOff * rollSin >> 15;
			xOff = x;
		}
		cameraX = camX - xOff;
		cameraY = camY - yOff;
		cameraZ = camZ - zOff;
	}

	private void method380(int i) {
		polygon3d class9 = polygons[i];
		object3d class7 = class9.object;
		int i_466_ = class9.polygonId;
		int[] is = class7.polygonPoints[i_466_];
		int i_467_ = class7.polygonPointsCount[i_466_];
		int i_468_ = class7.anIntArray315[i_466_];
		int i_469_ = class7.cameraModifiedVerticesX[is[0]];
		int i_470_ = class7.cameraModifiedVerticesY[is[0]];
		int i_471_ = class7.cameraModifiedVerticesZ[is[0]];
		int i_472_ = class7.cameraModifiedVerticesX[is[1]] - i_469_;
		int i_473_ = class7.cameraModifiedVerticesY[is[1]] - i_470_;
		int i_474_ = class7.cameraModifiedVerticesZ[is[1]] - i_471_;
		int i_475_ = class7.cameraModifiedVerticesX[is[2]] - i_469_;
		int i_476_ = class7.cameraModifiedVerticesY[is[2]] - i_470_;
		int i_477_ = class7.cameraModifiedVerticesZ[is[2]] - i_471_;
		int i_478_ = i_473_ * i_477_ - i_476_ * i_474_;
		int i_479_ = i_474_ * i_475_ - i_477_ * i_472_;
		int i_480_ = i_472_ * i_476_ - i_475_ * i_473_;
		if (i_468_ == -1) {
			i_468_ = 0;
			for (/**/; (i_478_ > 25000 || i_479_ > 25000 || i_480_ > 25000 || i_478_ < -25000 || i_479_ < -25000 || i_480_ < -25000); i_480_ >>= 1) {
				i_468_++;
				i_478_ >>= 1;
				i_479_ >>= 1;
			}
			class7.anIntArray315[i_466_] = i_468_;
			class7.anIntArray314[i_466_] = (int) ((double) anInt489 * Math.sqrt((double) (i_478_ * i_478_ + i_479_ * i_479_ + i_480_ * i_480_)));
		} else {
			i_478_ >>= i_468_;
			i_479_ >>= i_468_;
			i_480_ >>= i_468_;
		}
		class9.anInt451 = i_469_ * i_478_ + i_470_ * i_479_ + i_471_ * i_480_;
		class9.anInt448 = i_478_;
		class9.anInt449 = i_479_;
		class9.anInt450 = i_480_;
		int i_481_ = class7.cameraModifiedVerticesZ[is[0]];
		int i_482_ = i_481_;
		int i_483_ = class7.screenX[is[0]];
		int i_484_ = i_483_;
		int i_485_ = class7.screenY[is[0]];
		int i_486_ = i_485_;
		for (int i_487_ = 1; i_487_ < i_467_; i_487_++) {
			int i_488_ = class7.cameraModifiedVerticesZ[is[i_487_]];
			if (i_488_ > i_482_)
				i_482_ = i_488_;
			else if (i_488_ < i_481_)
				i_481_ = i_488_;
			i_488_ = class7.screenX[is[i_487_]];
			if (i_488_ > i_484_)
				i_484_ = i_488_;
			else if (i_488_ < i_483_)
				i_483_ = i_488_;
			i_488_ = class7.screenY[is[i_487_]];
			if (i_488_ > i_486_)
				i_486_ = i_488_;
			else if (i_488_ < i_485_)
				i_485_ = i_488_;
		}
		class9.anInt443 = i_481_;
		class9.anInt444 = i_482_;
		class9.anInt439 = i_483_;
		class9.anInt441 = i_484_;
		class9.anInt440 = i_485_;
		class9.anInt442 = i_486_;
	}

	private void method381(int i) {
		polygon3d class9 = polygons[i];
		object3d class7 = class9.object;
		int i_489_ = class9.polygonId;
		int[] is = class7.polygonPoints[i_489_];
		int i_490_ = 0;
		int i_491_ = 0;
		int i_492_ = 1;
		int i_493_ = class7.cameraModifiedVerticesX[is[0]];
		int i_494_ = class7.cameraModifiedVerticesY[is[0]];
		int i_495_ = class7.cameraModifiedVerticesZ[is[0]];
		class7.anIntArray314[i_489_] = 1;
		class7.anIntArray315[i_489_] = 0;
		class9.anInt451 = i_493_ * i_490_ + i_494_ * i_491_ + i_495_ * i_492_;
		class9.anInt448 = i_490_;
		class9.anInt449 = i_491_;
		class9.anInt450 = i_492_;
		int i_496_ = class7.cameraModifiedVerticesZ[is[0]];
		int i_497_ = i_496_;
		int i_498_ = class7.screenX[is[0]];
		int i_499_ = i_498_;
		if (class7.screenX[is[1]] < i_498_)
			i_498_ = class7.screenX[is[1]];
		else
			i_499_ = class7.screenX[is[1]];
		int i_500_ = class7.screenY[is[1]];
		int i_501_ = class7.screenY[is[0]];
		int i_502_ = class7.cameraModifiedVerticesZ[is[1]];
		if (i_502_ > i_497_)
			i_497_ = i_502_;
		else if (i_502_ < i_496_)
			i_496_ = i_502_;
		i_502_ = class7.screenX[is[1]];
		if (i_502_ > i_499_)
			i_499_ = i_502_;
		else if (i_502_ < i_498_)
			i_498_ = i_502_;
		i_502_ = class7.screenY[is[1]];
		if (i_502_ > i_501_)
			i_501_ = i_502_;
		else if (i_502_ < i_500_)
			i_500_ = i_502_;
		class9.anInt443 = i_496_;
		class9.anInt444 = i_497_;
		class9.anInt439 = i_498_ - 20;
		class9.anInt441 = i_499_ + 20;
		class9.anInt440 = i_500_;
		class9.anInt442 = i_501_;
	}

	private boolean method382(polygon3d class9, polygon3d class9_503_) {
		if (class9.anInt439 >= class9_503_.anInt441)
			return true;
		if (class9_503_.anInt439 >= class9.anInt441)
			return true;
		if (class9.anInt440 >= class9_503_.anInt442)
			return true;
		if (class9_503_.anInt440 >= class9.anInt442)
			return true;
		if (class9.anInt443 >= class9_503_.anInt444)
			return true;
		if (class9_503_.anInt443 > class9.anInt444)
			return false;
		object3d class7 = class9.object;
		object3d class7_504_ = class9_503_.object;
		int i = class9.polygonId;
		int i_505_ = class9_503_.polygonId;
		int[] is = class7.polygonPoints[i];
		int[] is_506_ = class7_504_.polygonPoints[i_505_];
		int i_507_ = class7.polygonPointsCount[i];
		int i_508_ = class7_504_.polygonPointsCount[i_505_];
		int i_509_ = class7_504_.cameraModifiedVerticesX[is_506_[0]];
		int i_510_ = class7_504_.cameraModifiedVerticesY[is_506_[0]];
		int i_511_ = class7_504_.cameraModifiedVerticesZ[is_506_[0]];
		int i_512_ = class9_503_.anInt448;
		int i_513_ = class9_503_.anInt449;
		int i_514_ = class9_503_.anInt450;
		int i_515_ = class7_504_.anIntArray314[i_505_];
		int i_516_ = class9_503_.anInt451;
		boolean bool = false;
		for (int i_517_ = 0; i_517_ < i_507_; i_517_++) {
			int i_518_ = is[i_517_];
			int i_519_ = ((i_509_ - class7.cameraModifiedVerticesX[i_518_]) * i_512_ + (i_510_ - class7.cameraModifiedVerticesY[i_518_]) * i_513_ + (i_511_ - class7.cameraModifiedVerticesZ[i_518_]) * i_514_);
			if (i_519_ < -i_515_ && i_516_ < 0 || i_519_ > i_515_ && i_516_ > 0) {
				bool = true;
				break;
			}
		}
		if (!bool)
			return true;
		i_509_ = class7.cameraModifiedVerticesX[is[0]];
		i_510_ = class7.cameraModifiedVerticesY[is[0]];
		i_511_ = class7.cameraModifiedVerticesZ[is[0]];
		i_512_ = class9.anInt448;
		i_513_ = class9.anInt449;
		i_514_ = class9.anInt450;
		i_515_ = class7.anIntArray314[i];
		i_516_ = class9.anInt451;
		bool = false;
		for (int i_520_ = 0; i_520_ < i_508_; i_520_++) {
			int i_521_ = is_506_[i_520_];
			int i_522_ = ((i_509_ - class7_504_.cameraModifiedVerticesX[i_521_]) * i_512_ + (i_510_ - class7_504_.cameraModifiedVerticesY[i_521_]) * i_513_ + (i_511_ - class7_504_.cameraModifiedVerticesZ[i_521_]) * i_514_);
			if (i_522_ < -i_515_ && i_516_ > 0 || i_522_ > i_515_ && i_516_ < 0) {
				bool = true;
				break;
			}
		}
		if (!bool)
			return true;
		int[] is_523_;
		int[] is_524_;
		if (i_507_ == 2) {
			is_523_ = new int[4];
			is_524_ = new int[4];
			int i_525_ = is[0];
			int i_526_ = is[1];
			is_523_[0] = class7.screenX[i_525_] - 20;
			is_523_[1] = class7.screenX[i_526_] - 20;
			is_523_[2] = class7.screenX[i_526_] + 20;
			is_523_[3] = class7.screenX[i_525_] + 20;
			is_524_[0] = is_524_[3] = class7.screenY[i_525_];
			is_524_[1] = is_524_[2] = class7.screenY[i_526_];
		} else {
			is_523_ = new int[i_507_];
			is_524_ = new int[i_507_];
			for (int i_527_ = 0; i_527_ < i_507_; i_527_++) {
				int i_528_ = is[i_527_];
				is_523_[i_527_] = class7.screenX[i_528_];
				is_524_[i_527_] = class7.screenY[i_528_];
			}
		}
		int[] is_529_;
		int[] is_530_;
		if (i_508_ == 2) {
			is_529_ = new int[4];
			is_530_ = new int[4];
			int i_531_ = is_506_[0];
			int i_532_ = is_506_[1];
			is_529_[0] = class7_504_.screenX[i_531_] - 20;
			is_529_[1] = class7_504_.screenX[i_532_] - 20;
			is_529_[2] = class7_504_.screenX[i_532_] + 20;
			is_529_[3] = class7_504_.screenX[i_531_] + 20;
			is_530_[0] = is_530_[3] = class7_504_.screenY[i_531_];
			is_530_[1] = is_530_[2] = class7_504_.screenY[i_532_];
		} else {
			is_529_ = new int[i_508_];
			is_530_ = new int[i_508_];
			for (int i_533_ = 0; i_533_ < i_508_; i_533_++) {
				int i_534_ = is_506_[i_533_];
				is_529_[i_533_] = class7_504_.screenX[i_534_];
				is_530_[i_533_] = class7_504_.screenY[i_534_];
			}
		}
		if (method393(is_523_, is_524_, is_529_, is_530_))
			return false;
		return true;
	}

	private boolean method383(polygon3d class9, polygon3d class9_535_) {
		object3d class7 = class9.object;
		object3d class7_536_ = class9_535_.object;
		int i = class9.polygonId;
		int i_537_ = class9_535_.polygonId;
		int[] is = class7.polygonPoints[i];
		int[] is_538_ = class7_536_.polygonPoints[i_537_];
		int i_539_ = class7.polygonPointsCount[i];
		int i_540_ = class7_536_.polygonPointsCount[i_537_];
		int i_541_ = class7_536_.cameraModifiedVerticesX[is_538_[0]];
		int i_542_ = class7_536_.cameraModifiedVerticesY[is_538_[0]];
		int i_543_ = class7_536_.cameraModifiedVerticesZ[is_538_[0]];
		int i_544_ = class9_535_.anInt448;
		int i_545_ = class9_535_.anInt449;
		int i_546_ = class9_535_.anInt450;
		int i_547_ = class7_536_.anIntArray314[i_537_];
		int i_548_ = class9_535_.anInt451;
		boolean bool = false;
		for (int i_549_ = 0; i_549_ < i_539_; i_549_++) {
			int i_550_ = is[i_549_];
			int i_551_ = ((i_541_ - class7.cameraModifiedVerticesX[i_550_]) * i_544_ + (i_542_ - class7.cameraModifiedVerticesY[i_550_]) * i_545_ + (i_543_ - class7.cameraModifiedVerticesZ[i_550_]) * i_546_);
			if (i_551_ < -i_547_ && i_548_ < 0 || i_551_ > i_547_ && i_548_ > 0) {
				bool = true;
				break;
			}
		}
		if (!bool)
			return true;
		i_541_ = class7.cameraModifiedVerticesX[is[0]];
		i_542_ = class7.cameraModifiedVerticesY[is[0]];
		i_543_ = class7.cameraModifiedVerticesZ[is[0]];
		i_544_ = class9.anInt448;
		i_545_ = class9.anInt449;
		i_546_ = class9.anInt450;
		i_547_ = class7.anIntArray314[i];
		i_548_ = class9.anInt451;
		bool = false;
		for (int i_552_ = 0; i_552_ < i_540_; i_552_++) {
			int i_553_ = is_538_[i_552_];
			int i_554_ = ((i_541_ - class7_536_.cameraModifiedVerticesX[i_553_]) * i_544_ + (i_542_ - class7_536_.cameraModifiedVerticesY[i_553_]) * i_545_ + (i_543_ - class7_536_.cameraModifiedVerticesZ[i_553_]) * i_546_);
			if (i_554_ < -i_547_ && i_548_ > 0 || i_554_ > i_547_ && i_548_ < 0) {
				bool = true;
				break;
			}
		}
		if (!bool)
			return true;
		return false;
	}

	public void copyTextureData(world3d class10_555_) {
		texturesCount = class10_555_.texturesCount;
		texturesName = class10_555_.texturesName;
		texturesPixels = class10_555_.texturesPixels;
		texturesColor = class10_555_.texturesColor;
		texturesSize = class10_555_.texturesSize;
		aLongArray520 = class10_555_.aLongArray520;
		aBooleanArray521 = class10_555_.aBooleanArray521;
		texturesArchiveBuffer = class10_555_.texturesArchiveBuffer;
		anIntArrayArray525 = class10_555_.anIntArrayArray525;
		anIntArrayArray526 = class10_555_.anIntArrayArray526;
	}

	public void method385(world3d class10_556_) {
		copyTextureData(class10_556_);
		method362(class10_556_.anInt486, class10_556_.anInt487, class10_556_.centerX, class10_556_.centerY, class10_556_.anInt483, class10_556_.screenWidth);
		maxRenderZ = class10_556_.maxRenderZ;
		anInt464 = class10_556_.anInt464;
		anInt465 = class10_556_.anInt465;
		anInt466 = class10_556_.anInt466;
	}

	public void loadTextures(String archiveName, int i, int i_557_, int loadingPercent, gameshell gameShell) {
		try {
			texturesArchiveBuffer = tools.getFile(archiveName, texturesLoadingText, loadingPercent, gameShell);
			byte[] texturesFileBuffer = tools.getFile("textures.txt", 0, texturesArchiveBuffer);
			megastream megastream = new megastream(texturesFileBuffer);
			megastream.skipToEquals();
			texturesCount = megastream.getBase10Value();
			texturesName = new String[texturesCount];
			aBooleanArray521 = new boolean[texturesCount];
			aLongArray520 = new long[texturesCount];
			texturesColor = new int[texturesCount];
			texturesSize = new int[texturesCount];
			texturesPixels = new int[texturesCount][];
			for (int id = 0; id < texturesCount; id++) {
				megastream.skipToEquals();
				texturesName[id] = megastream.getStringValue();
				texturesColor[id] = megastream.getBase16Value();
				texturesSize[id] = megastream.getBase10Value();
				aBooleanArray521[id] = false;
				texturesPixels[id] = null;
				aLongArray520[id] = 0L;
			}
			aLong523 = 0L;
			anIntArrayArray525 = new int[i][];
			anIntArrayArray526 = new int[i_557_][];
			for (int i_560_ = 0; i_560_ < texturesCount; i_560_++)
				method387(i_560_);
		} catch (java.io.IOException ioexception) {
			System.out.println("Error loading texture set");
		}
	}
	
	public void method387(int i) {
		if (i >= 0) {
			aLongArray520[i] = aLong523++;
			if (texturesPixels[i] == null) {
				if (texturesSize[i] == 0) {
					for (int i_561_ = 0; i_561_ < anIntArrayArray525.length; i_561_++) {
						if (anIntArrayArray525[i_561_] == null) {
							anIntArrayArray525[i_561_] = new int[256 * 64];
							texturesPixels[i] = anIntArrayArray525[i_561_];
							tools.getFile(texturesName[i] + ".tga", texturesArchiveBuffer, aByteArray527);
							method388(i);
							return;
						}
					}
					long l = 1L << 30;
					int i_562_ = 0;
					for (int i_563_ = 0; i_563_ < texturesCount; i_563_++) {
						if (i_563_ != i && texturesSize[i_563_] == 0 && texturesPixels[i_563_] != null && aLongArray520[i_563_] < l) {
							l = aLongArray520[i_563_];
							i_562_ = i_563_;
						}
					}
					texturesPixels[i] = texturesPixels[i_562_];
					texturesPixels[i_562_] = null;
					tools.getFile(texturesName[i] + ".tga", texturesArchiveBuffer, aByteArray527);
					method388(i);
				} else {
					for (int i_564_ = 0; i_564_ < anIntArrayArray526.length; i_564_++) {
						if (anIntArrayArray526[i_564_] == null) {
							anIntArrayArray526[i_564_] = new int[65536];
							texturesPixels[i] = anIntArrayArray526[i_564_];
							tools.getFile(texturesName[i] + ".tga", texturesArchiveBuffer, aByteArray527);
							method388(i);
							return;
						}
					}
					long l = 1L << 30;
					int i_565_ = 0;
					for (int i_566_ = 0; i_566_ < texturesCount; i_566_++) {
						if (i_566_ != i && texturesSize[i_566_] == 1 && texturesPixels[i_566_] != null && aLongArray520[i_566_] < l) {
							l = aLongArray520[i_566_];
							i_565_ = i_566_;
						}
					}
					texturesPixels[i] = texturesPixels[i_565_];
					texturesPixels[i_565_] = null;
					tools.getFile(texturesName[i] + ".tga", texturesArchiveBuffer, aByteArray527);
					method388(i);
				}
			}
		}
	}

	private void method388(int i) {
		int i_567_;
		if (texturesSize[i] == 0)
			i_567_ = 64;
		else
			i_567_ = 128;
		int[] is = texturesPixels[i];
		int i_568_ = 0;
		for (int i_569_ = 0; i_569_ < 256; i_569_++)
			anIntArray528[i_569_] = (((aByteArray527[20 + i_569_ * 3] & 0xff) << 16) + ((aByteArray527[19 + i_569_ * 3] & 0xff) << 8) + (aByteArray527[18 + i_569_ * 3] & 0xff));
		for (int i_570_ = i_567_ - 1; i_570_ >= 0; i_570_--) {
			for (int i_571_ = 0; i_571_ < i_567_; i_571_++) {
				int i_572_ = (anIntArray528[aByteArray527[786 + i_571_ + i_570_ * i_567_] & 0xff]);
				if (i_572_ != 0xff00ff && texturesColor[i] != 0) {
					int i_573_ = i_572_ >> 16 & 0xff;
					int i_574_ = i_572_ >> 8 & 0xff;
					int i_575_ = i_572_ & 0xff;
					if (i_573_ == i_574_ && i_574_ == i_575_) {
						int i_576_ = texturesColor[i] >> 16 & 0xff;
						int i_577_ = texturesColor[i] >> 8 & 0xff;
						int i_578_ = texturesColor[i] & 0xff;
						i_572_ = ((i_573_ * i_576_ >> 8 << 16) + (i_574_ * i_577_ >> 8 << 8) + (i_575_ * i_578_ >> 8));
					}
				}
				i_572_ &= 0xf8f8ff;
				if (i_572_ == 0)
					i_572_ = 1;
				else if (i_572_ == 16253183) {
					i_572_ = 0;
					aBooleanArray521[i] = true;
				}
				is[i_568_++] = i_572_;
			}
		}
		for (int i_579_ = 0; i_579_ < i_568_; i_579_++) {
			int i_580_ = is[i_579_];
			is[i_568_ + i_579_] = i_580_ - (i_580_ >>> 3) & 0xf8f8ff;
			is[i_568_ * 2 + i_579_] = i_580_ - (i_580_ >>> 2) & 0xf8f8ff;
			is[i_568_ * 3 + i_579_] = i_580_ - (i_580_ >>> 2) - (i_580_ >>> 3) & 0xf8f8ff;
		}
	}

	public static int method389(int i, int i_581_, int i_582_) {
		return -1 - i / 8 * 1024 - i_581_ / 8 * 32 - i_582_ / 8;
	}

	public int method390(int i, int i_583_, int i_584_, int i_585_, int i_586_) {
		if (i_585_ == i_583_)
			return i;
		return i + (i_584_ - i) * (i_586_ - i_583_) / (i_585_ - i_583_);
	}

	public boolean method391(int i, int i_587_, int i_588_, int i_589_, boolean bool) {
		if (bool && i <= i_588_ || i < i_588_) {
			if (i > i_589_)
				return true;
			if (i_587_ > i_588_)
				return true;
			if (i_587_ > i_589_)
				return true;
			if (bool)
				return false;
			return true;
		}
		if (i < i_589_)
			return true;
		if (i_587_ < i_588_)
			return true;
		if (i_587_ < i_589_)
			return true;
		return bool;
	}

	public boolean method392(int i, int i_590_, int i_591_, boolean bool) {
		if (bool && i <= i_591_ || i < i_591_) {
			if (i_590_ > i_591_)
				return true;
			if (bool)
				return false;
			return true;
		}
		if (i_590_ < i_591_)
			return true;
		return bool;
	}

	public boolean method393(int[] is, int[] is_592_, int[] is_593_, int[] is_594_) {
		int i = is.length;
		int i_595_ = is_593_.length;
		int i_596_ = 0;
		int i_598_;
		int i_597_ = i_598_ = is_592_[0];
		int i_599_ = 0;
		int i_601_;
		int i_600_ = i_601_ = is_594_[0];
		int i_602_ = 0;
		for (int i_603_ = 1; i_603_ < i; i_603_++) {
			if (is_592_[i_603_] < i_598_) {
				i_598_ = is_592_[i_603_];
				i_599_ = i_603_;
			} else if (is_592_[i_603_] > i_597_)
				i_597_ = is_592_[i_603_];
		}
		for (int i_604_ = 1; i_604_ < i_595_; i_604_++) {
			if (is_594_[i_604_] < i_601_) {
				i_601_ = is_594_[i_604_];
				i_602_ = i_604_;
			} else if (is_594_[i_604_] > i_600_)
				i_600_ = is_594_[i_604_];
		}
		if (i_601_ >= i_597_)
			return false;
		if (i_598_ >= i_600_)
			return false;
		int i_605_;
		boolean bool;
		int i_606_;
		if (is_592_[i_599_] < is_594_[i_602_]) {
			for (i_606_ = i_599_; is_592_[i_606_] < is_594_[i_602_]; i_606_ = (i_606_ + 1) % i) {
				/* empty */
			}
			for (/**/; is_592_[i_599_] < is_594_[i_602_]; i_599_ = (i_599_ - 1 + i) % i) {
				/* empty */
			}
			int i_607_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_602_]);
			int i_608_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_602_]);
			int i_609_ = is_593_[i_602_];
			bool = i_607_ < i_609_ | i_608_ < i_609_;
			if (method392(i_607_, i_608_, i_609_, bool))
				return true;
			i_605_ = (i_602_ + 1) % i_595_;
			i_602_ = (i_602_ - 1 + i_595_) % i_595_;
			if (i_599_ == i_606_)
				i_596_ = 1;
		} else {
			for (i_605_ = i_602_; is_594_[i_605_] < is_592_[i_599_]; i_605_ = (i_605_ + 1) % i_595_) {
				/* empty */
			}
			for (/**/; is_594_[i_602_] < is_592_[i_599_]; i_602_ = (i_602_ - 1 + i_595_) % i_595_) {
				/* empty */
			}
			int i_610_ = is[i_599_];
			int i_611_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_592_[i_599_]);
			int i_612_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_592_[i_599_]);
			bool = i_610_ < i_611_ | i_610_ < i_612_;
			if (method392(i_611_, i_612_, i_610_, !bool))
				return true;
			i_606_ = (i_599_ + 1) % i;
			i_599_ = (i_599_ - 1 + i) % i;
			if (i_602_ == i_605_)
				i_596_ = 2;
		}
		while (i_596_ == 0) {
			if (is_592_[i_599_] < is_592_[i_606_]) {
				if (is_592_[i_599_] < is_594_[i_602_]) {
					if (is_592_[i_599_] < is_594_[i_605_]) {
						int i_613_ = is[i_599_];
						int i_614_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_592_[i_599_]);
						int i_615_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_592_[i_599_]);
						int i_616_ = method390((is_593_[(i_605_ - 1 + i_595_) % i_595_]), (is_594_[(i_605_ - 1 + i_595_) % i_595_]), is_593_[i_605_], is_594_[i_605_], is_592_[i_599_]);
						if (method391(i_613_, i_614_, i_615_, i_616_, bool))
							return true;
						i_599_ = (i_599_ - 1 + i) % i;
						if (i_599_ == i_606_)
							i_596_ = 1;
					} else {
						int i_617_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_605_]);
						int i_618_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_605_]);
						int i_619_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_594_[i_605_]);
						int i_620_ = is_593_[i_605_];
						if (method391(i_617_, i_618_, i_619_, i_620_, bool))
							return true;
						i_605_ = (i_605_ + 1) % i_595_;
						if (i_602_ == i_605_)
							i_596_ = 2;
					}
				} else if (is_594_[i_602_] < is_594_[i_605_]) {
					int i_621_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_602_]);
					int i_622_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_602_]);
					int i_623_ = is_593_[i_602_];
					int i_624_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_594_[i_602_]);
					if (method391(i_621_, i_622_, i_623_, i_624_, bool))
						return true;
					i_602_ = (i_602_ - 1 + i_595_) % i_595_;
					if (i_602_ == i_605_)
						i_596_ = 2;
				} else {
					int i_625_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_605_]);
					int i_626_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_605_]);
					int i_627_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_594_[i_605_]);
					int i_628_ = is_593_[i_605_];
					if (method391(i_625_, i_626_, i_627_, i_628_, bool))
						return true;
					i_605_ = (i_605_ + 1) % i_595_;
					if (i_602_ == i_605_)
						i_596_ = 2;
				}
			} else if (is_592_[i_606_] < is_594_[i_602_]) {
				if (is_592_[i_606_] < is_594_[i_605_]) {
					int i_629_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_592_[i_606_]);
					int i_630_ = is[i_606_];
					int i_631_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_592_[i_606_]);
					int i_632_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_592_[i_606_]);
					if (method391(i_629_, i_630_, i_631_, i_632_, bool))
						return true;
					i_606_ = (i_606_ + 1) % i;
					if (i_599_ == i_606_)
						i_596_ = 1;
				} else {
					int i_633_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_605_]);
					int i_634_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_605_]);
					int i_635_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_594_[i_605_]);
					int i_636_ = is_593_[i_605_];
					if (method391(i_633_, i_634_, i_635_, i_636_, bool))
						return true;
					i_605_ = (i_605_ + 1) % i_595_;
					if (i_602_ == i_605_)
						i_596_ = 2;
				}
			} else if (is_594_[i_602_] < is_594_[i_605_]) {
				int i_637_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_602_]);
				int i_638_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_602_]);
				int i_639_ = is_593_[i_602_];
				int i_640_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_594_[i_602_]);
				if (method391(i_637_, i_638_, i_639_, i_640_, bool))
					return true;
				i_602_ = (i_602_ - 1 + i_595_) % i_595_;
				if (i_602_ == i_605_)
					i_596_ = 2;
			} else {
				int i_641_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_605_]);
				int i_642_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_605_]);
				int i_643_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_594_[i_605_]);
				int i_644_ = is_593_[i_605_];
				if (method391(i_641_, i_642_, i_643_, i_644_, bool))
					return true;
				i_605_ = (i_605_ + 1) % i_595_;
				if (i_602_ == i_605_)
					i_596_ = 2;
			}
		}
		while (i_596_ == 1) {
			if (is_592_[i_599_] < is_594_[i_602_]) {
				if (is_592_[i_599_] < is_594_[i_605_]) {
					int i_645_ = is[i_599_];
					int i_646_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_592_[i_599_]);
					int i_647_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_592_[i_599_]);
					if (method392(i_646_, i_647_, i_645_, !bool))
						return true;
					return false;
				}
				int i_648_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_605_]);
				int i_649_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_605_]);
				int i_650_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_594_[i_605_]);
				int i_651_ = is_593_[i_605_];
				if (method391(i_648_, i_649_, i_650_, i_651_, bool))
					return true;
				i_605_ = (i_605_ + 1) % i_595_;
				if (i_602_ == i_605_)
					i_596_ = 0;
			} else if (is_594_[i_602_] < is_594_[i_605_]) {
				int i_652_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_602_]);
				int i_653_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_602_]);
				int i_654_ = is_593_[i_602_];
				int i_655_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_594_[i_602_]);
				if (method391(i_652_, i_653_, i_654_, i_655_, bool))
					return true;
				i_602_ = (i_602_ - 1 + i_595_) % i_595_;
				if (i_602_ == i_605_)
					i_596_ = 0;
			} else {
				int i_656_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_605_]);
				int i_657_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_605_]);
				int i_658_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_594_[i_605_]);
				int i_659_ = is_593_[i_605_];
				if (method391(i_656_, i_657_, i_658_, i_659_, bool))
					return true;
				i_605_ = (i_605_ + 1) % i_595_;
				if (i_602_ == i_605_)
					i_596_ = 0;
			}
		}
		while (i_596_ == 2) {
			if (is_594_[i_602_] < is_592_[i_599_]) {
				if (is_594_[i_602_] < is_592_[i_606_]) {
					int i_660_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_602_]);
					int i_661_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_602_]);
					int i_662_ = is_593_[i_602_];
					if (method392(i_660_, i_661_, i_662_, bool))
						return true;
					return false;
				}
				int i_663_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_592_[i_606_]);
				int i_664_ = is[i_606_];
				int i_665_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_592_[i_606_]);
				int i_666_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_592_[i_606_]);
				if (method391(i_663_, i_664_, i_665_, i_666_, bool))
					return true;
				i_606_ = (i_606_ + 1) % i;
				if (i_599_ == i_606_)
					i_596_ = 0;
			} else if (is_592_[i_599_] < is_592_[i_606_]) {
				int i_667_ = is[i_599_];
				int i_668_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_592_[i_599_]);
				int i_669_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_592_[i_599_]);
				int i_670_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_592_[i_599_]);
				if (method391(i_667_, i_668_, i_669_, i_670_, bool))
					return true;
				i_599_ = (i_599_ - 1 + i) % i;
				if (i_599_ == i_606_)
					i_596_ = 0;
			} else {
				int i_671_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_592_[i_606_]);
				int i_672_ = is[i_606_];
				int i_673_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_592_[i_606_]);
				int i_674_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_592_[i_606_]);
				if (method391(i_671_, i_672_, i_673_, i_674_, bool))
					return true;
				i_606_ = (i_606_ + 1) % i;
				if (i_599_ == i_606_)
					i_596_ = 0;
			}
		}
		if (is_592_[i_599_] < is_594_[i_602_]) {
			int i_675_ = is[i_599_];
			int i_676_ = method390(is_593_[(i_602_ + 1) % i_595_], is_594_[(i_602_ + 1) % i_595_], is_593_[i_602_], is_594_[i_602_], is_592_[i_599_]);
			int i_677_ = method390(is_593_[(i_605_ - 1 + i_595_) % i_595_], is_594_[(i_605_ - 1 + i_595_) % i_595_], is_593_[i_605_], is_594_[i_605_], is_592_[i_599_]);
			if (method392(i_676_, i_677_, i_675_, !bool))
				return true;
			return false;
		}
		int i_678_ = method390(is[(i_599_ + 1) % i], is_592_[(i_599_ + 1) % i], is[i_599_], is_592_[i_599_], is_594_[i_602_]);
		int i_679_ = method390(is[(i_606_ - 1 + i) % i], is_592_[(i_606_ - 1 + i) % i], is[i_606_], is_592_[i_606_], is_594_[i_602_]);
		int i_680_ = is_593_[i_602_];
		if (method392(i_678_, i_679_, i_680_, bool))
			return true;
		return false;
	}
}
