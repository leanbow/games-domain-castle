package jagex;

public class bzip2 {
	static final int anInt549 = 1;
	static final int anInt550 = 2;
	static final int anInt551 = 10;
	static final int anInt552 = 14;
	static final int anInt553 = 0;
	static final int anInt554 = 4;
	static final int anInt555 = 4096;
	static final int anInt556 = 16;
	static final int anInt557 = 258;
	static final int anInt558 = 23;
	static final int anInt559 = 0;
	static final int anInt560 = 1;
	static final int anInt561 = 6;
	static final int anInt562 = 50;
	static final int anInt563 = 4;
	static final int anInt564 = 18002;
	public static int[] anIntArray218;

	public static int unpack(byte[] is, int i, byte[] is_0_, int i_1_, int i_2_) {
		bzip2s class3 = new bzip2s();
		class3.aByteArray194 = is_0_;
		class3.anInt195 = i_2_;
		class3.aByteArray199 = is;
		class3.anInt200 = 0;
		class3.anInt196 = i_1_;
		class3.anInt201 = i;
		class3.anInt208 = 0;
		class3.anInt207 = 0;
		class3.anInt197 = 0;
		class3.anInt198 = 0;
		class3.anInt202 = 0;
		class3.anInt203 = 0;
		class3.anInt210 = 0;
		method396(class3);
		i -= class3.anInt201;
		return i;
	}

	private static void method395(bzip2s class3) {
		byte i = class3.aByte204;
		int i_3_ = class3.anInt205;
		int i_4_ = class3.anInt215;
		int i_5_ = class3.anInt213;
		int[] is = anIntArray218;
		int i_6_ = class3.anInt212;
		byte[] is_7_ = class3.aByteArray199;
		int i_8_ = class3.anInt200;
		int i_9_ = class3.anInt201;
		int i_10_ = i_9_;
		int i_11_ = class3.anInt232 + 1;
		while_5_: for (;;) {
			if (i_3_ > 0) {
				for (;;) {
					if (i_9_ == 0)
						break while_5_;
					if (i_3_ == 1)
						break;
					is_7_[i_8_] = i;
					i_3_--;
					i_8_++;
					i_9_--;
				}
				if (i_9_ == 0) {
					i_3_ = 1;
					break;
				}
				is_7_[i_8_] = i;
				i_8_++;
				i_9_--;
			}
			boolean bool = true;
			while (bool) {
				bool = false;
				if (i_4_ == i_11_) {
					i_3_ = 0;
					break while_5_;
				}
				i = (byte) i_5_;
				i_6_ = is[i_6_];
				int i_12_ = (byte) (i_6_ & 0xff);
				i_6_ >>= 8;
				i_4_++;
				if (i_12_ != i_5_) {
					i_5_ = i_12_;
					if (i_9_ == 0) {
						i_3_ = 1;
						break while_5_;
					}
					is_7_[i_8_] = i;
					i_8_++;
					i_9_--;
					bool = true;
				} else if (i_4_ == i_11_) {
					if (i_9_ == 0) {
						i_3_ = 1;
						break while_5_;
					}
					is_7_[i_8_] = i;
					i_8_++;
					i_9_--;
					bool = true;
				}
			}
			i_3_ = 2;
			i_6_ = is[i_6_];
			int i_13_ = (byte) (i_6_ & 0xff);
			i_6_ >>= 8;
			if (++i_4_ != i_11_) {
				if (i_13_ != i_5_)
					i_5_ = i_13_;
				else {
					i_3_ = 3;
					i_6_ = is[i_6_];
					i_13_ = (byte) (i_6_ & 0xff);
					i_6_ >>= 8;
					if (++i_4_ != i_11_) {
						if (i_13_ != i_5_)
							i_5_ = i_13_;
						else {
							i_6_ = is[i_6_];
							i_13_ = (byte) (i_6_ & 0xff);
							i_6_ >>= 8;
							i_4_++;
							i_3_ = (i_13_ & 0xff) + 4;
							i_6_ = is[i_6_];
							i_5_ = (byte) (i_6_ & 0xff);
							i_6_ >>= 8;
							i_4_++;
						}
					}
				}
			}
		}
		int i_14_ = class3.anInt202;
		class3.anInt202 += i_10_ - i_9_;
		if (class3.anInt202 < i_14_)
			class3.anInt203++;
		class3.aByte204 = i;
		class3.anInt205 = i_3_;
		class3.anInt215 = i_4_;
		class3.anInt213 = i_5_;
		anIntArray218 = is;
		class3.anInt212 = i_6_;
		class3.aByteArray199 = is_7_;
		class3.anInt200 = i_8_;
		class3.anInt201 = i_9_;
	}

	private static void method396(bzip2s class3) {
		int i = 0;
		int[] is = null;
		int[] is_33_ = null;
		int[] is_34_ = null;
		class3.anInt209 = 1;
		if (anIntArray218 == null)
			anIntArray218 = new int[100000 * class3.anInt209];
		boolean bool_35_ = true;
		while (bool_35_) {
			byte i_36_ = method397(class3);
			if (i_36_ == 23)
				break;
			i_36_ = method397(class3);
			i_36_ = method397(class3);
			i_36_ = method397(class3);
			i_36_ = method397(class3);
			i_36_ = method397(class3);
			class3.anInt210++;
			i_36_ = method397(class3);
			i_36_ = method397(class3);
			i_36_ = method397(class3);
			i_36_ = method397(class3);
			i_36_ = method398(class3);
			if (i_36_ != 0)
				class3.aBoolean206 = true;
			else
				class3.aBoolean206 = false;
			if (class3.aBoolean206)
				System.out.println("PANIC! RANDOMISED BLOCK!");
			class3.anInt211 = 0;
			int i_37_ = method397(class3);
			class3.anInt211 = class3.anInt211 << 8 | i_37_ & 0xff;
			i_37_ = method397(class3);
			class3.anInt211 = class3.anInt211 << 8 | i_37_ & 0xff;
			i_37_ = method397(class3);
			class3.anInt211 = class3.anInt211 << 8 | i_37_ & 0xff;
			for (int i_38_ = 0; i_38_ < 16; i_38_++) {
				i_36_ = method398(class3);
				if (i_36_ == 1)
					class3.aBooleanArray221[i_38_] = true;
				else
					class3.aBooleanArray221[i_38_] = false;
			}
			for (int i_39_ = 0; i_39_ < 256; i_39_++)
				class3.aBooleanArray220[i_39_] = false;
			for (int i_40_ = 0; i_40_ < 16; i_40_++) {
				if (class3.aBooleanArray221[i_40_]) {
					for (int i_41_ = 0; i_41_ < 16; i_41_++) {
						i_36_ = method398(class3);
						if (i_36_ == 1)
							class3.aBooleanArray220[i_40_ * 16 + i_41_] = true;
					}
				}
			}
			method401(class3);
			int i_42_ = class3.anInt219 + 2;
			int i_43_ = method399(3, class3);
			int i_44_ = method399(15, class3);
			for (int i_45_ = 0; i_45_ < i_44_; i_45_++) {
				int i_46_ = 0;
				for (;;) {
					i_36_ = method398(class3);
					if (i_36_ == 0)
						break;
					i_46_++;
				}
				class3.aByteArray226[i_45_] = (byte) i_46_;
			}
			byte[] is_47_ = new byte[6];
			for (byte i_48_ = 0; i_48_ < i_43_; i_48_++)
				is_47_[i_48_] = i_48_;
			for (int i_49_ = 0; i_49_ < i_44_; i_49_++) {
				byte i_50_ = class3.aByteArray226[i_49_];
				byte i_51_ = is_47_[i_50_];
				for (/**/; i_50_ > 0; i_50_--)
					is_47_[i_50_] = is_47_[i_50_ - 1];
				is_47_[0] = i_51_;
				class3.aByteArray225[i_49_] = i_51_;
			}
			for (int i_52_ = 0; i_52_ < i_43_; i_52_++) {
				int i_53_ = method399(5, class3);
				for (int i_54_ = 0; i_54_ < i_42_; i_54_++) {
					for (;;) {
						i_36_ = method398(class3);
						if (i_36_ == 0)
							break;
						i_36_ = method398(class3);
						if (i_36_ == 0)
							i_53_++;
						else
							i_53_--;
					}
					class3.aByteArrayArray227[i_52_][i_54_] = (byte) i_53_;
				}
			}
			for (int i_55_ = 0; i_55_ < i_43_; i_55_++) {
				int i_56_ = 32;
				byte i_57_ = 0;
				for (int i_58_ = 0; i_58_ < i_42_; i_58_++) {
					if (class3.aByteArrayArray227[i_55_][i_58_] > i_57_)
						i_57_ = class3.aByteArrayArray227[i_55_][i_58_];
					if (class3.aByteArrayArray227[i_55_][i_58_] < i_56_)
						i_56_ = class3.aByteArrayArray227[i_55_][i_58_];
				}
				method402(class3.anIntArrayArray228[i_55_], class3.anIntArrayArray229[i_55_], class3.anIntArrayArray230[i_55_], class3.aByteArrayArray227[i_55_], i_56_, i_57_, i_42_);
				class3.anIntArray231[i_55_] = i_56_;
			}
			int i_59_ = class3.anInt219 + 1;
			int i_61_ = -1;
			int i_62_ = 0;
			for (int i_63_ = 0; i_63_ <= 255; i_63_++)
				class3.anIntArray214[i_63_] = 0;
			int i_64_ = 4095;
			for (int i_65_ = 15; i_65_ >= 0; i_65_--) {
				for (int i_66_ = 15; i_66_ >= 0; i_66_--) {
					class3.aByteArray223[i_64_] = (byte) (i_65_ * 16 + i_66_);
					i_64_--;
				}
				class3.anIntArray224[i_65_] = i_64_ + 1;
			}
			int i_67_ = 0;
			if (i_62_ == 0) {
				i_61_++;
				i_62_ = 50;
				byte i_68_ = class3.aByteArray225[i_61_];
				i = class3.anIntArray231[i_68_];
				is = class3.anIntArrayArray228[i_68_];
				is_34_ = class3.anIntArrayArray230[i_68_];
				is_33_ = class3.anIntArrayArray229[i_68_];
			}
			i_62_--;
			int i_69_ = i;
			int i_70_;
			int i_71_;
			for (i_71_ = method399(i_69_, class3); i_71_ > is[i_69_]; i_71_ = i_71_ << 1 | i_70_) {
				i_69_++;
				i_70_ = method398(class3);
			}
			int i_72_ = is_34_[i_71_ - is_33_[i_69_]];
			while (i_72_ != i_59_) {
				if (i_72_ == 0 || i_72_ == 1) {
					int i_73_ = -1;
					int i_74_ = 1;
					do {
						if (i_72_ == 0)
							i_73_ += i_74_;
						else if (i_72_ == 1)
							i_73_ += 2 * i_74_;
						i_74_ *= 2;
						if (i_62_ == 0) {
							i_61_++;
							i_62_ = 50;
							byte i_75_ = class3.aByteArray225[i_61_];
							i = class3.anIntArray231[i_75_];
							is = class3.anIntArrayArray228[i_75_];
							is_34_ = class3.anIntArrayArray230[i_75_];
							is_33_ = class3.anIntArrayArray229[i_75_];
						}
						i_62_--;
						i_69_ = i;
						for (i_71_ = method399(i_69_, class3); i_71_ > is[i_69_]; i_71_ = i_71_ << 1 | i_70_) {
							i_69_++;
							i_70_ = method398(class3);
						}
						i_72_ = is_34_[i_71_ - is_33_[i_69_]];
					} while (i_72_ == 0 || i_72_ == 1);
					i_73_++;
					i_37_ = (class3.aByteArray222[(class3.aByteArray223[class3.anIntArray224[0]] & 0xff)]);
					class3.anIntArray214[i_37_ & 0xff] += i_73_;
					for (/**/; i_73_ > 0; i_73_--) {
						anIntArray218[i_67_] = i_37_ & 0xff;
						i_67_++;
					}
				} else {
					int i_76_ = i_72_ - 1;
					if (i_76_ < 16) {
						int i_77_ = class3.anIntArray224[0];
						i_36_ = class3.aByteArray223[i_77_ + i_76_];
						for (/**/; i_76_ > 3; i_76_ -= 4) {
							int i_78_ = i_77_ + i_76_;
							class3.aByteArray223[i_78_] = class3.aByteArray223[i_78_ - 1];
							class3.aByteArray223[i_78_ - 1] = class3.aByteArray223[i_78_ - 2];
							class3.aByteArray223[i_78_ - 2] = class3.aByteArray223[i_78_ - 3];
							class3.aByteArray223[i_78_ - 3] = class3.aByteArray223[i_78_ - 4];
						}
						for (/**/; i_76_ > 0; i_76_--)
							class3.aByteArray223[i_77_ + i_76_] = class3.aByteArray223[i_77_ + i_76_ - 1];
						class3.aByteArray223[i_77_] = i_36_;
					} else {
						int i_79_ = i_76_ / 16;
						int i_80_ = i_76_ % 16;
						int i_81_ = class3.anIntArray224[i_79_] + i_80_;
						i_36_ = class3.aByteArray223[i_81_];
						for (/**/; i_81_ > class3.anIntArray224[i_79_]; i_81_--)
							class3.aByteArray223[i_81_] = class3.aByteArray223[i_81_ - 1];
						class3.anIntArray224[i_79_]++;
						for (/**/; i_79_ > 0; i_79_--) {
							class3.anIntArray224[i_79_]--;
							class3.aByteArray223[class3.anIntArray224[i_79_]] = (class3.aByteArray223[class3.anIntArray224[i_79_ - 1] + 16 - 1]);
						}
						class3.anIntArray224[0]--;
						class3.aByteArray223[class3.anIntArray224[0]] = i_36_;
						if (class3.anIntArray224[0] == 0) {
							int i_82_ = 4095;
							for (int i_83_ = 15; i_83_ >= 0; i_83_--) {
								for (int i_84_ = 15; i_84_ >= 0; i_84_--) {
									class3.aByteArray223[i_82_] = (class3.aByteArray223[(class3.anIntArray224[i_83_] + i_84_)]);
									i_82_--;
								}
								class3.anIntArray224[i_83_] = i_82_ + 1;
							}
						}
					}
					class3.anIntArray214[(class3.aByteArray222[i_36_ & 0xff] & 0xff)]++;
					anIntArray218[i_67_] = class3.aByteArray222[i_36_ & 0xff] & 0xff;
					i_67_++;
					if (i_62_ == 0) {
						i_61_++;
						i_62_ = 50;
						byte i_85_ = class3.aByteArray225[i_61_];
						i = class3.anIntArray231[i_85_];
						is = class3.anIntArrayArray228[i_85_];
						is_34_ = class3.anIntArrayArray230[i_85_];
						is_33_ = class3.anIntArrayArray229[i_85_];
					}
					i_62_--;
					i_69_ = i;
					for (i_71_ = method399(i_69_, class3); i_71_ > is[i_69_]; i_71_ = i_71_ << 1 | i_70_) {
						i_69_++;
						i_70_ = method398(class3);
					}
					i_72_ = is_34_[i_71_ - is_33_[i_69_]];
				}
			}
			class3.anInt205 = 0;
			class3.aByte204 = (byte) 0;
			class3.anIntArray216[0] = 0;
			for (int i_86_ = 1; i_86_ <= 256; i_86_++)
				class3.anIntArray216[i_86_] = class3.anIntArray214[i_86_ - 1];
			for (int i_87_ = 1; i_87_ <= 256; i_87_++)
				class3.anIntArray216[i_87_] += class3.anIntArray216[i_87_ - 1];
			for (int i_88_ = 0; i_88_ < i_67_; i_88_++) {
				i_37_ = (byte) (anIntArray218[i_88_] & 0xff);
				anIntArray218[class3.anIntArray216[i_37_ & 0xff]] |= i_88_ << 8;
				class3.anIntArray216[i_37_ & 0xff]++;
			}
			class3.anInt212 = anIntArray218[class3.anInt211] >> 8;
			class3.anInt215 = 0;
			class3.anInt212 = anIntArray218[class3.anInt212];
			class3.anInt213 = (byte) (class3.anInt212 & 0xff);
			class3.anInt212 >>= 8;
			class3.anInt215++;
			class3.anInt232 = i_67_;
			method395(class3);
			if (class3.anInt215 == class3.anInt232 + 1 && class3.anInt205 == 0)
				bool_35_ = true;
			else
				bool_35_ = false;
		}
	}

	private static byte method397(bzip2s class3) {
		return (byte) method399(8, class3);
	}

	private static byte method398(bzip2s class3) {
		return (byte) method399(1, class3);
	}

	private static int method399(int i, bzip2s class3) {
		int i_89_;
		for (;;) {
			if (class3.anInt208 >= i) {
				int i_90_ = class3.anInt207 >> class3.anInt208 - i & (1 << i) - 1;
				class3.anInt208 -= i;
				i_89_ = i_90_;
				break;
			}
			class3.anInt207 = (class3.anInt207 << 8 | class3.aByteArray194[class3.anInt195] & 0xff);
			class3.anInt208 += 8;
			class3.anInt195++;
			class3.anInt196--;
			class3.anInt197++;
			if (class3.anInt197 == 0)
				class3.anInt198++;
		}
		return i_89_;
	}

	private static void method401(bzip2s class3) {
		class3.anInt219 = 0;
		for (int i = 0; i < 256; i++) {
			if (class3.aBooleanArray220[i]) {
				class3.aByteArray222[class3.anInt219] = (byte) i;
				class3.anInt219++;
			}
		}
	}

	private static void method402(int[] is, int[] is_94_, int[] is_95_, byte[] is_96_, int i, int i_97_, int i_98_) {
		int i_99_ = 0;
		for (int i_100_ = i; i_100_ <= i_97_; i_100_++) {
			for (int i_101_ = 0; i_101_ < i_98_; i_101_++) {
				if (is_96_[i_101_] == i_100_) {
					is_95_[i_99_] = i_101_;
					i_99_++;
				}
			}
		}
		for (int i_102_ = 0; i_102_ < 23; i_102_++)
			is_94_[i_102_] = 0;
		for (int i_103_ = 0; i_103_ < i_98_; i_103_++)
			is_94_[is_96_[i_103_] + 1]++;
		for (int i_104_ = 1; i_104_ < 23; i_104_++)
			is_94_[i_104_] += is_94_[i_104_ - 1];
		for (int i_105_ = 0; i_105_ < 23; i_105_++)
			is[i_105_] = 0;
		int i_106_ = 0;
		for (int i_107_ = i; i_107_ <= i_97_; i_107_++) {
			i_106_ += is_94_[i_107_ + 1] - is_94_[i_107_];
			is[i_107_] = i_106_ - 1;
			i_106_ <<= 1;
		}
		for (int i_108_ = i + 1; i_108_ <= i_97_; i_108_++)
			is_94_[i_108_] = (is[i_108_ - 1] + 1 << 1) - is_94_[i_108_];
	}

	public static class bzip2s {
		final int anInt184 = 4096;
		final int anInt185 = 16;
		final int anInt186 = 258;
		final int anInt187 = 23;
		final int anInt188 = 0;
		final int anInt189 = 1;
		final int anInt190 = 6;
		final int anInt191 = 50;
		final int anInt192 = 4;
		final int anInt193 = 18002;
		byte[] aByteArray194;
		int anInt195;
		int anInt196;
		int anInt197;
		int anInt198;
		byte[] aByteArray199;
		int anInt200;
		int anInt201;
		int anInt202;
		int anInt203;
		byte aByte204;
		int anInt205;
		boolean aBoolean206;
		int anInt207;
		int anInt208;
		int anInt209;
		int anInt210;
		int anInt211;
		int anInt212;
		int anInt213;
		int[] anIntArray214 = new int[256];
		int anInt215;
		int[] anIntArray216 = new int[257];
		int[] anIntArray217 = new int[257];
		int anInt219;
		boolean[] aBooleanArray220 = new boolean[256];
		boolean[] aBooleanArray221 = new boolean[16];
		byte[] aByteArray222 = new byte[256];
		byte[] aByteArray223 = new byte[4096];
		int[] anIntArray224 = new int[16];
		byte[] aByteArray225 = new byte[18002];
		byte[] aByteArray226 = new byte[18002];
		byte[][] aByteArrayArray227 = new byte[6][258];
		int[][] anIntArrayArray228 = new int[6][258];
		int[][] anIntArrayArray229 = new int[6][258];
		int[][] anIntArrayArray230 = new int[6][258];
		int[] anIntArray231 = new int[6];
		int anInt232;
	}

}
