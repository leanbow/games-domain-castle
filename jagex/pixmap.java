package jagex;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

public class pixmap implements ImageProducer, ImageObserver {
	public int width;
	public int height;
	public int pixelAmount;
	public int maxWidth;
	public int maxHeight;
	ColorModel directColorModel;
	public int[] pixels;
	ImageConsumer imageConsumer;
	private Component component;
	public static String aString252 = "setpass";
	public Image image;
	private int[][] anIntArrayArray254;
	private byte[][] aByteArrayArray255;
	private int[][] anIntArrayArray256;
	public int[] anIntArray257;
	public int[] anIntArray258;
	public int[] anIntArray259;
	public int[] anIntArray260;
	public int[] anIntArray261;
	public int[] anIntArray262;
	public boolean[] aBooleanArray263;
	private boolean aBoolean264 = true;
	private int clipY;
	private int clipHeight;
	private int clipX;
	private int clipWidth;
	public boolean lowMemory = false;
	static byte[][] aByteArrayArray270 = new byte[50][];
	static int[] anIntArray271;
	static int anInt272;
	public boolean aBoolean273 = false;
	int[] anIntArray274;
	int[] anIntArray275;
	int[] anIntArray276;
	int[] anIntArray277;
	int[] anIntArray278;
	int[] anIntArray279;
	int[] anIntArray280;
	public static final int anInt281 = 0;
	public static final int anInt282 = 16777215;
	public static final int anInt283 = 16711680;
	public static final int anInt284 = 12582912;
	public static final int anInt285 = 65280;
	public static final int anInt286 = 255;
	public static final int anInt287 = 16776960;
	public static final int anInt288 = 65535;
	public static final int anInt289 = 16711935;
	public static final int anInt290 = 12632256;
	public static final int anInt291 = 8421504;
	public static final int anInt292 = 4210752;
	public static final int anInt293 = 0;
	public static final int anInt294 = 1;
	public static final int anInt295 = 2;
	public static final int anInt296 = 3;
	public static final int anInt297 = 4;
	public static final int anInt298 = 5;
	public static final int anInt299 = 6;
	public static final int anInt300 = 7;

	public pixmap(int w, int h, int i_1_, Component component) {
		this.component = component;
		clipHeight = h;
		clipWidth = w;
		maxWidth = width = w;
		maxHeight = height = h;
		pixelAmount = w * h;
		pixels = new int[w * h];
		anIntArrayArray254 = new int[i_1_][];
		aBooleanArray263 = new boolean[i_1_];
		aByteArrayArray255 = new byte[i_1_][];
		anIntArrayArray256 = new int[i_1_][];
		anIntArray257 = new int[i_1_];
		anIntArray258 = new int[i_1_];
		anIntArray261 = new int[i_1_];
		anIntArray262 = new int[i_1_];
		anIntArray259 = new int[i_1_];
		anIntArray260 = new int[i_1_];
		if (w > 1 && h > 1 && component != null) {
			directColorModel = new DirectColorModel(32, 16711680, 65280, 255);
			for (int id = 0; id < pixelAmount; id++)
				pixels[id] = 0;
			image = component.createImage(this);
			setPixels();
			component.prepareImage(image, component);
			setPixels();
			component.prepareImage(image, component);
			setPixels();
			component.prepareImage(image, component);
		}
	}

	public synchronized void method231(int i, int i_4_) {
		if (width > maxWidth)
			width = maxWidth;
		if (height > maxHeight)
			height = maxHeight;
		width = i;
		height = i_4_;
		pixelAmount = i * i_4_;
	}

	public synchronized void addConsumer(ImageConsumer imageconsumer) {
		imageConsumer = imageconsumer;
		imageconsumer.setDimensions(width, height);
		imageconsumer.setProperties(null);
		imageconsumer.setColorModel(directColorModel);
		imageconsumer.setHints(14);
	}

	public synchronized boolean isConsumer(ImageConsumer imageconsumer) {
		if (imageConsumer == imageconsumer)
			return true;
		return false;
	}

	public synchronized void removeConsumer(ImageConsumer imageconsumer) {
		if (imageConsumer == imageconsumer)
			imageConsumer = null;
	}

	public void startProduction(ImageConsumer imageconsumer) {
		addConsumer(imageconsumer);
	}

	public void requestTopDownLeftRightResend(ImageConsumer imageconsumer) {
		System.out.println("TDLR");
	}

	public synchronized void setPixels() {
		if (imageConsumer != null) {
			imageConsumer.setPixels(0, 0, width, height, directColorModel, pixels, 0, width);
			imageConsumer.imageComplete(2);
		}
	}

	public synchronized void setPixels(int x, int y, int width, int height) {
		if (imageConsumer != null) {
			imageConsumer.setPixels(x, y, width, height, directColorModel, pixels, x + y * width, width);
			imageConsumer.imageComplete(2);
		}
	}

	public void setClip(int x, int y, int w, int h) {
		if (x < 0)
			x = 0;
		if (y < 0)
			y = 0;
		if (w > width)
			w = width;
		if (h > height)
			h = height;
		clipX = x;
		clipY = y;
		clipWidth = w;
		clipHeight = h;
	}

	public void resetClip() {
		clipX = 0;
		clipY = 0;
		clipWidth = width;
		clipHeight = height;
	}

	public void drawImage(Graphics graphics, int x, int y, int width, int height) {
		setPixels(x, y, width, height);
		graphics.drawImage(image, 0, 0, this);
	}

	public void drawImage(Graphics graphics, int x, int y) {
		setPixels();
		graphics.drawImage(image, x, y, this);
	}

	public void clear() {
		if (!lowMemory) {
			for (int id = 0; id < pixelAmount; id++) {
				pixels[id] = 0;
			}
		} else {
			int pixelPos = 0;
			for (int h = -height; h < 0; h += 2) {
				for (int w = -width; w < 0; w++) {
					pixels[pixelPos++] = 0;
				}
				pixelPos += width;
			}
		}
	}

	public void method239(int i, int i_19_, int i_20_, int i_21_, int i_22_) {
		int i_23_ = 256 - i_22_;
		int i_24_ = (i_21_ >> 16 & 0xff) * i_22_;
		int i_25_ = (i_21_ >> 8 & 0xff) * i_22_;
		int i_26_ = (i_21_ & 0xff) * i_22_;
		int i_27_ = i_19_ - i_20_;
		if (i_27_ < 0)
			i_27_ = 0;
		int i_28_ = i_19_ + i_20_;
		if (i_28_ >= height)
			i_28_ = height - 1;
		int step = 1;
		if (lowMemory == true) {
			step = 2;
			if ((i_27_ & 0x1) != 0)
				i_27_++;
		}
		for (int i_30_ = i_27_; i_30_ <= i_28_; i_30_ += step) {
			int i_31_ = i_30_ - i_19_;
			int i_32_ = (int) Math.sqrt((double) (i_20_ * i_20_ - i_31_ * i_31_));
			int i_33_ = i - i_32_;
			if (i_33_ < 0)
				i_33_ = 0;
			int i_34_ = i + i_32_;
			if (i_34_ >= width)
				i_34_ = width - 1;
			int i_35_ = i_33_ + i_30_ * width;
			for (int i_36_ = i_33_; i_36_ <= i_34_; i_36_++) {
				int i_37_ = (pixels[i_35_] >> 16 & 0xff) * i_23_;
				int i_38_ = (pixels[i_35_] >> 8 & 0xff) * i_23_;
				int i_39_ = (pixels[i_35_] & 0xff) * i_23_;
				int i_40_ = ((i_24_ + i_37_ >> 8 << 16) + (i_25_ + i_38_ >> 8 << 8) + (i_26_ + i_39_ >> 8));
				pixels[i_35_++] = i_40_;
			}
		}
	}

	public void method240(int i, int i_41_, int i_42_, int i_43_, int i_44_, int i_45_) {
		if (i < clipX) {
			i_42_ -= clipX - i;
			i = clipX;
		}
		if (i_41_ < clipY) {
			i_43_ -= clipY - i_41_;
			i_41_ = clipY;
		}
		if (i + i_42_ > clipWidth)
			i_42_ = clipWidth - i;
		if (i_41_ + i_43_ > clipHeight)
			i_43_ = clipHeight - i_41_;
		int i_46_ = 256 - i_45_;
		int i_47_ = (i_44_ >> 16 & 0xff) * i_45_;
		int i_48_ = (i_44_ >> 8 & 0xff) * i_45_;
		int i_49_ = (i_44_ & 0xff) * i_45_;
		int i_50_ = width - i_42_;
		int i_51_ = 1;
		if (lowMemory == true) {
			i_51_ = 2;
			i_50_ += width;
			if ((i_41_ & 0x1) != 0) {
				i_41_++;
				i_43_--;
			}
		}
		int i_52_ = i + i_41_ * width;
		for (int i_53_ = 0; i_53_ < i_43_; i_53_ += i_51_) {
			for (int i_54_ = -i_42_; i_54_ < 0; i_54_++) {
				int i_55_ = (pixels[i_52_] >> 16 & 0xff) * i_46_;
				int i_56_ = (pixels[i_52_] >> 8 & 0xff) * i_46_;
				int i_57_ = (pixels[i_52_] & 0xff) * i_46_;
				int i_58_ = ((i_47_ + i_55_ >> 8 << 16) + (i_48_ + i_56_ >> 8 << 8) + (i_49_ + i_57_ >> 8));
				pixels[i_52_++] = i_58_;
			}
			i_52_ += i_50_;
		}
	}

	public void method241(int i, int i_59_, int i_60_, int i_61_, int i_62_, int i_63_) {
		if (i < clipX) {
			i_60_ -= clipX - i;
			i = clipX;
		}
		if (i + i_60_ > clipWidth)
			i_60_ = clipWidth - i;
		int i_64_ = i_63_ >> 16 & 0xff;
		int i_65_ = i_63_ >> 8 & 0xff;
		int i_66_ = i_63_ & 0xff;
		int i_67_ = i_62_ >> 16 & 0xff;
		int i_68_ = i_62_ >> 8 & 0xff;
		int i_69_ = i_62_ & 0xff;
		int i_70_ = width - i_60_;
		int i_71_ = 1;
		if (lowMemory == true) {
			i_71_ = 2;
			i_70_ += width;
			if ((i_59_ & 0x1) != 0) {
				i_59_++;
				i_61_--;
			}
		}
		int i_72_ = i + i_59_ * width;
		for (int i_73_ = 0; i_73_ < i_61_; i_73_ += i_71_) {
			if (i_73_ + i_59_ >= clipY && i_73_ + i_59_ < clipHeight) {
				int i_74_ = (((i_64_ * i_73_ + i_67_ * (i_61_ - i_73_)) / i_61_ << 16) + ((i_65_ * i_73_ + i_68_ * (i_61_ - i_73_)) / i_61_ << 8) + (i_66_ * i_73_ + i_69_ * (i_61_ - i_73_)) / i_61_);
				for (int i_75_ = -i_60_; i_75_ < 0; i_75_++)
					pixels[i_72_++] = i_74_;
				i_72_ += i_70_;
			} else
				i_72_ += width;
		}
	}

	public void method242(int i, int i_76_, int i_77_, int i_78_, int i_79_) {
		if (i < clipX) {
			i_77_ -= clipX - i;
			i = clipX;
		}
		if (i_76_ < clipY) {
			i_78_ -= clipY - i_76_;
			i_76_ = clipY;
		}
		if (i + i_77_ > clipWidth)
			i_77_ = clipWidth - i;
		if (i_76_ + i_78_ > clipHeight)
			i_78_ = clipHeight - i_76_;
		int i_80_ = width - i_77_;
		int i_81_ = 1;
		if (lowMemory == true) {
			i_81_ = 2;
			i_80_ += width;
			if ((i_76_ & 0x1) != 0) {
				i_76_++;
				i_78_--;
			}
		}
		int i_82_ = i + i_76_ * width;
		for (int i_83_ = -i_78_; i_83_ < 0; i_83_ += i_81_) {
			for (int i_84_ = -i_77_; i_84_ < 0; i_84_++)
				pixels[i_82_++] = i_79_;
			i_82_ += i_80_;
		}
	}

	public void method243(int i, int i_85_, int i_86_, int i_87_, int i_88_) {
		method244(i, i_85_, i_86_, i_88_);
		method244(i, i_85_ + i_87_ - 1, i_86_, i_88_);
		method245(i, i_85_, i_87_, i_88_);
		method245(i + i_86_ - 1, i_85_, i_87_, i_88_);
	}

	public void method244(int i, int i_89_, int i_90_, int i_91_) {
		if (i_89_ >= clipY && i_89_ < clipHeight) {
			if (i < clipX) {
				i_90_ -= clipX - i;
				i = clipX;
			}
			if (i + i_90_ > clipWidth)
				i_90_ = clipWidth - i;
			int i_92_ = i + i_89_ * width;
			for (int i_93_ = 0; i_93_ < i_90_; i_93_++)
				pixels[i_92_ + i_93_] = i_91_;
		}
	}

	public void method245(int i, int i_94_, int i_95_, int i_96_) {
		if (i >= clipX && i < clipWidth) {
			if (i_94_ < clipY) {
				i_95_ -= clipY - i_94_;
				i_94_ = clipY;
			}
			if (i_94_ + i_95_ > clipWidth)
				i_95_ = clipHeight - i_94_;
			int i_97_ = i + i_94_ * width;
			for (int i_98_ = 0; i_98_ < i_95_; i_98_++)
				pixels[i_97_ + i_98_ * width] = i_96_;
		}
	}

	public void method246() {
		int i = width * height;
		for (int i_99_ = 0; i_99_ < i; i_99_++) {
			int i_100_ = pixels[i_99_] & 0xffffff;
			pixels[i_99_] = ((i_100_ >>> 1 & 0x7f7f7f) + (i_100_ >>> 2 & 0x3f3f3f) + (i_100_ >>> 3 & 0x1f1f1f) + (i_100_ >>> 4 & 0xf0f0f));
		}
	}

	public static int method247(int i, int i_101_, int i_102_) {
		return (i << 16) + (i_101_ << 8) + i_102_;
	}

	public void method248() {
		for (int i = 0; i < anIntArrayArray254.length; i++) {
			anIntArrayArray254[i] = null;
			anIntArray257[i] = 0;
			anIntArray258[i] = 0;
			aByteArrayArray255[i] = null;
			anIntArrayArray256[i] = null;
		}
	}

	public void method249(byte[] is, int i, int i_103_, boolean bool, int i_104_, boolean bool_105_) {
		method250(is, i, i_103_, bool, i_104_, 1, bool_105_);
	}

	public void method250(byte[] is, int bufferOff, int i_106_, boolean bool, int xGrids, int yGrids, boolean bool_109_) {
		int imageWidth = (is[13 + bufferOff] & 0xff) * 256 + (is[12 + bufferOff] & 0xff);
		int imageHeight = (is[15 + bufferOff] & 0xff) * 256 + (is[14 + bufferOff] & 0xff);
		int transparentPaletteId = -1;
		int[] palette = new int[256];
		for (int paletteId = 0; paletteId < 256; paletteId++) {
			palette[paletteId] = (-16777216 + ((is[bufferOff + 20 + paletteId * 3] & 0xff) << 16) + ((is[bufferOff + 19 + paletteId * 3] & 0xff) << 8) + (is[bufferOff + 18 + paletteId * 3] & 0xff));
			if (palette[paletteId] == -65281)
				transparentPaletteId = paletteId;
		}
		if (transparentPaletteId == -1)
			bool = false;
		if (bool_109_ && bool)
			palette[transparentPaletteId] = palette[0];
		int spriteWidth = imageWidth / xGrids;
		int spriteHeight = imageHeight / yGrids;
		int[] pixels = new int[spriteWidth * spriteHeight];
		for (int yGrid = 0; yGrid < yGrids; yGrid++) {
			for (int xGrid = 0; xGrid < xGrids; xGrid++) {
				int pixelsPos = 0;
				for (int y = spriteHeight * yGrid; y < spriteHeight * (yGrid + 1); y++) {
					for (int x = spriteWidth * xGrid; x < spriteWidth * (xGrid + 1); x++) {
						if (bool_109_)
							pixels[pixelsPos++] = is[bufferOff + 786 + x + ((imageHeight - y - 1) * imageWidth)] & 0xff;
						else
							pixels[pixelsPos++] = palette[is[(bufferOff + 786 + x + ((imageHeight - y - 1) * imageWidth))] & 0xff];
					}
				}
				if (bool_109_)
					method251(pixels, spriteWidth, spriteHeight, i_106_++, bool, palette, transparentPaletteId);
				else
					method251(pixels, spriteWidth, spriteHeight, i_106_++, bool, null, -65281);
			}
		}
	}

	private void method251(int[] is, int i, int i_123_, int i_124_, boolean bool, int[] is_125_, int i_126_) {
		int i_127_ = 0;
		int i_128_ = 0;
		int i_129_ = i;
		int i_130_ = i_123_;
		if (bool && aBoolean264) {
			while_1_: for (int i_131_ = 0; i_131_ < i_123_; i_131_++) {
				for (int i_132_ = 0; i_132_ < i; i_132_++) {
					int i_133_ = is[i_132_ + i_131_ * i];
					if (i_133_ != i_126_) {
						i_128_ = i_131_;
						break while_1_;
					}
				}
			}
			while_2_: for (int i_134_ = 0; i_134_ < i; i_134_++) {
				for (int i_135_ = 0; i_135_ < i_123_; i_135_++) {
					int i_136_ = is[i_134_ + i_135_ * i];
					if (i_136_ != i_126_) {
						i_127_ = i_134_;
						break while_2_;
					}
				}
			}
			while_3_: for (int i_137_ = i_123_ - 1; i_137_ >= 0; i_137_--) {
				for (int i_138_ = 0; i_138_ < i; i_138_++) {
					int i_139_ = is[i_138_ + i_137_ * i];
					if (i_139_ != i_126_) {
						i_130_ = i_137_ + 1;
						break while_3_;
					}
				}
			}
			while_4_: for (int i_140_ = i - 1; i_140_ >= 0; i_140_--) {
				for (int i_141_ = 0; i_141_ < i_123_; i_141_++) {
					int i_142_ = is[i_140_ + i_141_ * i];
					if (i_142_ != i_126_) {
						i_129_ = i_140_ + 1;
						break while_4_;
					}
				}
			}
		}
		anIntArray257[i_124_] = i_129_ - i_127_;
		anIntArray258[i_124_] = i_130_ - i_128_;
		aBooleanArray263[i_124_] = bool;
		anIntArray259[i_124_] = i_127_;
		anIntArray260[i_124_] = i_128_;
		anIntArray261[i_124_] = i;
		anIntArray262[i_124_] = i_123_;
		if (is_125_ == null) {
			anIntArrayArray254[i_124_] = new int[(i_129_ - i_127_) * (i_130_ - i_128_)];
			int i_143_ = 0;
			for (int i_144_ = i_128_; i_144_ < i_130_; i_144_++) {
				for (int i_145_ = i_127_; i_145_ < i_129_; i_145_++) {
					int i_146_ = is[i_145_ + i_144_ * i];
					if (bool) {
						if (i_146_ == i_126_)
							i_146_ = 0;
						if (i_146_ == -16777216)
							i_146_ = -16711423;
					}
					anIntArrayArray254[i_124_][i_143_++] = i_146_ & 0xffffff;
				}
			}
		} else {
			aByteArrayArray255[i_124_] = new byte[(i_129_ - i_127_) * (i_130_ - i_128_)];
			anIntArrayArray256[i_124_] = is_125_;
			int i_147_ = 0;
			for (int i_148_ = i_128_; i_148_ < i_130_; i_148_++) {
				for (int i_149_ = i_127_; i_149_ < i_129_; i_149_++) {
					int i_150_ = is[i_149_ + i_148_ * i];
					if (bool) {
						if (i_150_ == i_126_)
							i_150_ = 0;
						else if (i_150_ == 0)
							i_150_ = i_126_;
					}
					aByteArrayArray255[i_124_][i_147_++] = (byte) i_150_;
				}
			}
		}
	}

	public void method252(int i, int i_151_) {
		anIntArray257[i_151_] = anIntArray257[i];
		anIntArray258[i_151_] = anIntArray258[i];
		anIntArray259[i_151_] = anIntArray259[i];
		anIntArray260[i_151_] = anIntArray260[i];
		aBooleanArray263[i_151_] = aBooleanArray263[i];
		anIntArray261[i_151_] = anIntArray261[i];
		anIntArray262[i_151_] = anIntArray262[i];
		int i_152_ = anIntArray257[i] * anIntArray258[i];
		anIntArrayArray254[i_151_] = new int[i_152_];
		for (int i_153_ = 0; i_153_ < i_152_; i_153_++)
			anIntArrayArray254[i_151_][i_153_] = anIntArrayArray254[i][i_153_];
	}

	public void method253(int i, int i_154_, int i_155_) {
		int[] is = anIntArrayArray254[i];
		int i_156_ = anIntArray257[i] * anIntArray258[i];
		for (int i_157_ = 0; i_157_ < i_156_; i_157_++) {
			if (is[i_157_] == i_154_)
				is[i_157_] = i_155_;
		}
	}

	public void method254(int i, int i_158_) {
		int[] is = anIntArrayArray254[i];
		int i_159_ = anIntArray257[i];
		int i_160_ = anIntArray258[i];
		int[] is_161_ = anIntArrayArray254[i_158_] = new int[i_159_ * i_160_];
		for (int i_162_ = 0; i_162_ < i_159_; i_162_++) {
			for (int i_163_ = 0; i_163_ < i_160_; i_163_++)
				is_161_[i_163_ + i_162_ * i_160_] = is[i_159_ - i_162_ - 1 + i_163_ * i_159_];
		}
		anIntArray257[i_158_] = i_160_;
		anIntArray258[i_158_] = i_159_;
		anIntArray261[i_158_] = anIntArray261[i];
		anIntArray262[i_158_] = anIntArray262[i];
		anIntArray260[i_158_] = anIntArray261[i] - anIntArray259[i] - anIntArray257[i];
		anIntArray259[i_158_] = anIntArray260[i];
		aBooleanArray263[i_158_] = aBooleanArray263[i];
	}

	public void method255(int i, int i_164_, int i_165_) {
		if (aBooleanArray263[i_165_]) {
			i += anIntArray259[i_165_];
			i_164_ += anIntArray260[i_165_];
		}
		int i_166_ = i + i_164_ * width;
		int i_167_ = 0;
		int i_168_ = anIntArray258[i_165_];
		int i_169_ = anIntArray257[i_165_];
		int i_170_ = width - i_169_;
		int i_171_ = 0;
		if (i_164_ < clipY) {
			int i_172_ = clipY - i_164_;
			i_168_ -= i_172_;
			i_164_ = clipY;
			i_167_ += i_172_ * i_169_;
			i_166_ += i_172_ * width;
		}
		if (i_164_ + i_168_ >= clipHeight)
			i_168_ -= i_164_ + i_168_ - clipHeight + 1;
		if (i < clipX) {
			int i_173_ = clipX - i;
			i_169_ -= i_173_;
			i = clipX;
			i_167_ += i_173_;
			i_166_ += i_173_;
			i_171_ += i_173_;
			i_170_ += i_173_;
		}
		if (i + i_169_ >= clipWidth) {
			int i_174_ = i + i_169_ - clipWidth + 1;
			i_169_ -= i_174_;
			i_171_ += i_174_;
			i_170_ += i_174_;
		}
		if (i_169_ > 0 && i_168_ > 0) {
			int i_175_ = 1;
			if (lowMemory == true) {
				i_175_ = 2;
				i_170_ += width;
				i_171_ += anIntArray257[i_165_];
				if ((i_164_ & 0x1) != 0) {
					i_166_ += width;
					i_168_--;
				}
			}
			if (aBooleanArray263[i_165_])
				method257(pixels, anIntArrayArray254[i_165_], 0, i_167_, i_166_, i_169_, i_168_, i_170_, i_171_, i_175_);
			else
				method256(pixels, anIntArrayArray254[i_165_], i_167_, i_166_, i_169_, i_168_, i_170_, i_171_, i_175_);
		}
	}

	private void method256(int[] is, int[] is_176_, int i, int i_177_, int i_178_, int i_179_, int i_180_, int i_181_, int i_182_) {
		int i_183_ = -(i_178_ >> 2);
		i_178_ = -(i_178_ & 0x3);
		for (int i_184_ = -i_179_; i_184_ < 0; i_184_ += i_182_) {
			for (int i_185_ = i_183_; i_185_ < 0; i_185_++) {
				is[i_177_++] = is_176_[i++];
				is[i_177_++] = is_176_[i++];
				is[i_177_++] = is_176_[i++];
				is[i_177_++] = is_176_[i++];
			}
			for (int i_186_ = i_178_; i_186_ < 0; i_186_++)
				is[i_177_++] = is_176_[i++];
			i_177_ += i_180_;
			i += i_181_;
		}
	}

	private void method257(int[] is, int[] is_187_, int i, int i_188_, int i_189_, int i_190_, int i_191_, int i_192_, int i_193_, int i_194_) {
		int i_195_ = -(i_190_ >> 2);
		i_190_ = -(i_190_ & 0x3);
		for (int i_196_ = -i_191_; i_196_ < 0; i_196_ += i_194_) {
			for (int i_197_ = i_195_; i_197_ < 0; i_197_++) {
				i = is_187_[i_188_++];
				if (i != 0)
					is[i_189_++] = i;
				else
					i_189_++;
				i = is_187_[i_188_++];
				if (i != 0)
					is[i_189_++] = i;
				else
					i_189_++;
				i = is_187_[i_188_++];
				if (i != 0)
					is[i_189_++] = i;
				else
					i_189_++;
				i = is_187_[i_188_++];
				if (i != 0)
					is[i_189_++] = i;
				else
					i_189_++;
			}
			for (int i_198_ = i_190_; i_198_ < 0; i_198_++) {
				i = is_187_[i_188_++];
				if (i != 0)
					is[i_189_++] = i;
				else
					i_189_++;
			}
			i_189_ += i_192_;
			i_188_ += i_193_;
		}
	}

	public void method258(int i, int i_199_, int i_200_) {
		if (aBooleanArray263[i_200_]) {
			i += anIntArray259[i_200_];
			i_199_ += anIntArray260[i_200_];
		}
		int i_201_ = i + i_199_ * width;
		int i_202_ = 0;
		int i_203_ = anIntArray258[i_200_];
		int i_204_ = anIntArray257[i_200_];
		int i_205_ = width - i_204_;
		int i_206_ = 0;
		if (i_199_ < clipY) {
			int i_207_ = clipY - i_199_;
			i_203_ -= i_207_;
			i_199_ = clipY;
			i_202_ += i_207_ * i_204_;
			i_201_ += i_207_ * width;
		}
		if (i_199_ + i_203_ >= clipHeight)
			i_203_ -= i_199_ + i_203_ - clipHeight + 1;
		if (i < clipX) {
			int i_208_ = clipX - i;
			i_204_ -= i_208_;
			i = clipX;
			i_202_ += i_208_;
			i_201_ += i_208_;
			i_206_ += i_208_;
			i_205_ += i_208_;
		}
		if (i + i_204_ >= clipWidth) {
			int i_209_ = i + i_204_ - clipWidth + 1;
			i_204_ -= i_209_;
			i_206_ += i_209_;
			i_205_ += i_209_;
		}
		if (i_204_ > 0 && i_203_ > 0) {
			int i_210_ = 1;
			if (lowMemory == true) {
				i_210_ = 2;
				i_205_ += width;
				i_206_ += anIntArray257[i_200_];
				if ((i_199_ & 0x1) != 0) {
					i_201_ += width;
					i_203_--;
				}
			}
			method259(pixels, anIntArrayArray254[i_200_], 0, i_202_, i_201_, i_204_, i_203_, i_205_, i_206_, i_210_);
		}
	}

	private void method259(int[] is, int[] is_211_, int i, int i_212_, int i_213_, int i_214_, int i_215_, int i_216_, int i_217_, int i_218_) {
		int i_219_ = -(i_214_ >> 2);
		i_214_ = -(i_214_ & 0x3);
		for (int i_220_ = -i_215_; i_220_ < 0; i_220_ += i_218_) {
			for (int i_221_ = i_219_; i_221_ < 0; i_221_++) {
				i = is_211_[i_212_++];
				if (i != 0)
					is[i_213_++] = (i >> 1 & 0x7f7f7f) + (is[i_213_] >> 1 & 0x7f7f7f);
				else
					i_213_++;
				i = is_211_[i_212_++];
				if (i != 0)
					is[i_213_++] = (i >> 1 & 0x7f7f7f) + (is[i_213_] >> 1 & 0x7f7f7f);
				else
					i_213_++;
				i = is_211_[i_212_++];
				if (i != 0)
					is[i_213_++] = (i >> 1 & 0x7f7f7f) + (is[i_213_] >> 1 & 0x7f7f7f);
				else
					i_213_++;
				i = is_211_[i_212_++];
				if (i != 0)
					is[i_213_++] = (i >> 1 & 0x7f7f7f) + (is[i_213_] >> 1 & 0x7f7f7f);
				else
					i_213_++;
			}
			for (int i_222_ = i_214_; i_222_ < 0; i_222_++) {
				i = is_211_[i_212_++];
				if (i != 0)
					is[i_213_++] = (i >> 1 & 0x7f7f7f) + (is[i_213_] >> 1 & 0x7f7f7f);
				else
					i_213_++;
			}
			i_213_ += i_216_;
			i_212_ += i_217_;
		}
	}

	public void method260(int i, int i_223_, int i_224_, int i_225_, int i_226_, int i_227_) {
		try {
			int i_228_ = anIntArray257[i_226_];
			int i_229_ = anIntArray258[i_226_];
			int i_230_ = 0;
			int i_231_ = 0;
			int i_232_ = (i_228_ << 16) / i_224_;
			int i_233_ = (i_229_ << 16) / i_225_;
			if (aBooleanArray263[i_226_]) {
				int i_234_ = anIntArray261[i_226_];
				int i_235_ = anIntArray262[i_226_];
				i_232_ = (i_234_ << 16) / i_224_;
				i_233_ = (i_235_ << 16) / i_225_;
				i += (anIntArray259[i_226_] * i_224_ + i_234_ - 1) / i_234_;
				i_223_ += (anIntArray260[i_226_] * i_225_ + i_235_ - 1) / i_235_;
				if (anIntArray259[i_226_] * i_224_ % i_234_ != 0)
					i_230_ = (i_234_ - anIntArray259[i_226_] * i_224_ % i_234_ << 16) / i_224_;
				if (anIntArray260[i_226_] * i_225_ % i_235_ != 0)
					i_231_ = (i_235_ - anIntArray260[i_226_] * i_225_ % i_235_ << 16) / i_225_;
				i_224_ = (i_224_ * (anIntArray257[i_226_] - (i_230_ >> 16)) / i_234_);
				i_225_ = (i_225_ * (anIntArray258[i_226_] - (i_231_ >> 16)) / i_235_);
			}
			int i_236_ = i + i_223_ * width;
			int i_237_ = width - i_224_;
			if (i_223_ < clipY) {
				int i_238_ = clipY - i_223_;
				i_225_ -= i_238_;
				i_223_ = 0;
				i_236_ += i_238_ * width;
				i_231_ += i_233_ * i_238_;
			}
			if (i_223_ + i_225_ >= clipHeight)
				i_225_ -= i_223_ + i_225_ - clipHeight + 1;
			if (i < clipX) {
				int i_239_ = clipX - i;
				i_224_ -= i_239_;
				i = 0;
				i_236_ += i_239_;
				i_230_ += i_232_ * i_239_;
				i_237_ += i_239_;
			}
			if (i + i_224_ >= clipWidth) {
				int i_240_ = i + i_224_ - clipWidth + 1;
				i_224_ -= i_240_;
				i_237_ += i_240_;
			}
			int i_241_ = 1;
			if (lowMemory == true) {
				i_241_ = 2;
				i_237_ += width;
				i_233_ += i_233_;
				if ((i_223_ & 0x1) != 0) {
					i_236_ += width;
					i_225_--;
				}
			}
			method261(pixels, anIntArrayArray254[i_226_], 0, i_230_, i_231_, i_236_, i_237_, i_224_, i_225_, i_232_, i_233_, i_228_, i_241_, i_227_);
		} catch (Exception exception) {
			System.out.println("error in sprite clipping routine");
		}
	}

	private void method261(int[] is, int[] is_242_, int i, int i_243_, int i_244_, int i_245_, int i_246_, int i_247_, int i_248_, int i_249_, int i_250_, int i_251_, int i_252_, int i_253_) {
		int i_254_ = i_253_ >> 16 & 0xff;
		int i_255_ = i_253_ >> 8 & 0xff;
		int i_256_ = i_253_ & 0xff;
		try {
			int i_257_ = i_243_;
			for (int i_258_ = -i_248_; i_258_ < 0; i_258_ += i_252_) {
				int i_259_ = (i_244_ >> 16) * i_251_;
				for (int i_260_ = -i_247_; i_260_ < 0; i_260_++) {
					i = is_242_[(i_243_ >> 16) + i_259_];
					if (i != 0) {
						int i_261_ = i >> 16 & 0xff;
						int i_262_ = i >> 8 & 0xff;
						int i_263_ = i & 0xff;
						if (i_261_ == i_262_ && i_262_ == i_263_)
							is[i_245_++] = ((i_261_ * i_254_ >> 8 << 16) + (i_262_ * i_255_ >> 8 << 8) + (i_263_ * i_256_ >> 8));
						else
							is[i_245_++] = i;
					} else
						i_245_++;
					i_243_ += i_249_;
				}
				i_244_ += i_250_;
				i_243_ = i_257_;
				i_245_ += i_246_;
			}
		} catch (Exception exception) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	public void method262(int i, int i_264_, int i_265_, int i_266_, int i_267_) {
		try {
			int i_268_ = anIntArray257[i_267_];
			int i_269_ = anIntArray258[i_267_];
			int i_270_ = 0;
			int i_271_ = 0;
			int i_272_ = (i_268_ << 16) / i_265_;
			int i_273_ = (i_269_ << 16) / i_266_;
			if (aBooleanArray263[i_267_]) {
				int i_274_ = anIntArray261[i_267_];
				int i_275_ = anIntArray262[i_267_];
				i_272_ = (i_274_ << 16) / i_265_;
				i_273_ = (i_275_ << 16) / i_266_;
				i += (anIntArray259[i_267_] * i_265_ + i_274_ - 1) / i_274_;
				i_264_ += (anIntArray260[i_267_] * i_266_ + i_275_ - 1) / i_275_;
				if (anIntArray259[i_267_] * i_265_ % i_274_ != 0)
					i_270_ = (i_274_ - anIntArray259[i_267_] * i_265_ % i_274_ << 16) / i_265_;
				if (anIntArray260[i_267_] * i_266_ % i_275_ != 0)
					i_271_ = (i_275_ - anIntArray260[i_267_] * i_266_ % i_275_ << 16) / i_266_;
				i_265_ = (i_265_ * (anIntArray257[i_267_] - (i_270_ >> 16)) / i_274_);
				i_266_ = (i_266_ * (anIntArray258[i_267_] - (i_271_ >> 16)) / i_275_);
			}
			int i_276_ = i + i_264_ * width;
			int i_277_ = width - i_265_;
			if (i_264_ < clipY) {
				int i_278_ = clipY - i_264_;
				i_266_ -= i_278_;
				i_264_ = 0;
				i_276_ += i_278_ * width;
				i_271_ += i_273_ * i_278_;
			}
			if (i_264_ + i_266_ >= clipHeight)
				i_266_ -= i_264_ + i_266_ - clipHeight + 1;
			if (i < clipX) {
				int i_279_ = clipX - i;
				i_265_ -= i_279_;
				i = 0;
				i_276_ += i_279_;
				i_270_ += i_272_ * i_279_;
				i_277_ += i_279_;
			}
			if (i + i_265_ >= clipWidth) {
				int i_280_ = i + i_265_ - clipWidth + 1;
				i_265_ -= i_280_;
				i_277_ += i_280_;
			}
			int i_281_ = 1;
			if (lowMemory == true) {
				i_281_ = 2;
				i_277_ += width;
				i_273_ += i_273_;
				if ((i_264_ & 0x1) != 0) {
					i_276_ += width;
					i_266_--;
				}
			}
			if (aBooleanArray263[i_267_])
				method264(pixels, anIntArrayArray254[i_267_], 0, i_270_, i_271_, i_276_, i_277_, i_265_, i_266_, i_272_, i_273_, i_268_, i_281_);
			else
				method263(pixels, anIntArrayArray254[i_267_], i_276_, i_270_, i_271_, i_277_, i_265_, i_266_, i_272_, i_273_, i_268_, i_281_);
		} catch (Exception exception) {
			System.out.println("error in sprite clipping routine");
		}
	}

	private void method263(int[] is, int[] is_282_, int i, int i_283_, int i_284_, int i_285_, int i_286_, int i_287_, int i_288_, int i_289_, int i_290_, int i_291_) {
		try {
			int i_292_ = i_283_;
			for (int i_293_ = -i_287_; i_293_ < 0; i_293_ += i_291_) {
				int i_294_ = (i_284_ >> 16) * i_290_;
				for (int i_295_ = -i_286_; i_295_ < 0; i_295_++) {
					is[i++] = is_282_[(i_283_ >> 16) + i_294_];
					i_283_ += i_288_;
				}
				i_284_ += i_289_;
				i_283_ = i_292_;
				i += i_285_;
			}
		} catch (Exception exception) {
			System.out.println("error in sprite plot routine");
		}
	}

	private void method264(int[] is, int[] is_296_, int i, int i_297_, int i_298_, int i_299_, int i_300_, int i_301_, int i_302_, int i_303_, int i_304_, int i_305_, int i_306_) {
		try {
			int i_307_ = i_297_;
			for (int i_308_ = -i_302_; i_308_ < 0; i_308_ += i_306_) {
				int i_309_ = (i_298_ >> 16) * i_305_;
				for (int i_310_ = -i_301_; i_310_ < 0; i_310_++) {
					i = is_296_[(i_297_ >> 16) + i_309_];
					if (i != 0)
						is[i_299_++] = i;
					else
						i_299_++;
					i_297_ += i_303_;
				}
				i_298_ += i_304_;
				i_297_ = i_307_;
				i_299_ += i_300_;
			}
		} catch (Exception exception) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	public void method265(int i, int i_311_, int i_312_, int i_313_, int i_314_, int i_315_, int i_316_) {
		method262(i, i_311_, i_312_, i_313_, i_314_);
	}

	public static int appendJagexFont(byte[] is) {
		aByteArrayArray270[anInt272] = is;
		return anInt272++;
	}

	public void method267(String string, int i, int i_317_, int i_318_, int i_319_) {
		method268(string, i - method272(string, i_318_) / 2, i_317_, i_318_, i_319_);
	}

	public void method268(String string, int i, int i_320_, int i_321_, int i_322_) {
		byte[] is = aByteArrayArray270[i_321_];
		boolean bool = false;
		if (string.toLowerCase().startsWith(aString252))
			bool = true;
		for (int i_323_ = 0; i_323_ < string.length(); i_323_++) {
			if (string.charAt(i_323_) == '@' && i_323_ + 4 < string.length() && string.charAt(i_323_ + 4) == '@') {
				if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("red"))
					i_322_ = anInt283;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("yel"))
					i_322_ = anInt287;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("gre"))
					i_322_ = anInt285;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("blu"))
					i_322_ = anInt286;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("cya"))
					i_322_ = anInt288;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("mag"))
					i_322_ = anInt289;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("whi"))
					i_322_ = anInt282;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("bla"))
					i_322_ = anInt281;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("dre"))
					i_322_ = anInt284;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("ora"))
					i_322_ = 16748608;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("ran"))
					i_322_ = (int) (Math.random() * 1.6777215E7);
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("or1"))
					i_322_ = 16756736;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("or2"))
					i_322_ = 16740352;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("or3"))
					i_322_ = 16723968;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("gr1"))
					i_322_ = 12648192;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("gr2"))
					i_322_ = 8453888;
				else if (string.substring(i_323_ + 1, i_323_ + 4).equalsIgnoreCase("gr3"))
					i_322_ = 4259584;
				i_323_ += 4;
			} else if (string.charAt(i_323_) == '~' && i_323_ + 4 < string.length() && string.charAt(i_323_ + 4) == '~') {
				char c = string.charAt(i_323_ + 1);
				char c_324_ = string.charAt(i_323_ + 2);
				char c_325_ = string.charAt(i_323_ + 3);
				if (c >= '0' && c <= '9' && c_324_ >= '0' && c_324_ <= '9' && c_325_ >= '0' && c_325_ <= '9')
					i = Integer.parseInt(string.substring(i_323_ + 1, i_323_ + 4));
				i_323_ += 4;
			} else {
				int i_326_ = anIntArray271[string.charAt(i_323_)];
				if (bool && i_323_ > aString252.length() && string.charAt(i_323_) != '*')
					i_326_ = anIntArray271[88];
				if (aBoolean273 && i_322_ != 0)
					method269(i_326_, i + 1, i_320_, 0, is);
				if (aBoolean273 && i_322_ != 0)
					method269(i_326_, i, i_320_ + 1, 0, is);
				method269(i_326_, i, i_320_, i_322_, is);
				i += is[i_326_ + 7];
			}
		}
	}

	private void method269(int i, int i_327_, int i_328_, int i_329_, byte[] is) {
		int i_330_ = i_327_ + is[i + 5];
		int i_331_ = i_328_ - is[i + 6];
		int i_332_ = is[i + 3];
		int i_333_ = is[i + 4];
		int i_334_ = is[i] * 16384 + is[i + 1] * 128 + is[i + 2];
		int i_335_ = i_330_ + i_331_ * width;
		int i_336_ = width - i_332_;
		int i_337_ = 0;
		if (i_331_ < clipY) {
			int i_338_ = clipY - i_331_;
			i_333_ -= i_338_;
			i_331_ = clipY;
			i_334_ += i_338_ * i_332_;
			i_335_ += i_338_ * width;
		}
		if (i_331_ + i_333_ >= clipHeight)
			i_333_ -= i_331_ + i_333_ - clipHeight + 1;
		if (i_330_ < clipX) {
			int i_339_ = clipX - i_330_;
			i_332_ -= i_339_;
			i_330_ = clipX;
			i_334_ += i_339_;
			i_335_ += i_339_;
			i_337_ += i_339_;
			i_336_ += i_339_;
		}
		if (i_330_ + i_332_ >= clipWidth) {
			int i_340_ = i_330_ + i_332_ - clipWidth + 1;
			i_332_ -= i_340_;
			i_337_ += i_340_;
			i_336_ += i_340_;
		}
		if (i_332_ > 0 && i_333_ > 0)
			method270(pixels, is, i_329_, i_334_, i_335_, i_332_, i_333_, i_336_, i_337_);
	}

	private void method270(int[] is, byte[] is_341_, int i, int i_342_, int i_343_, int i_344_, int i_345_, int i_346_, int i_347_) {
		int i_348_ = -(i_344_ >> 2);
		i_344_ = -(i_344_ & 0x3);
		for (int i_349_ = -i_345_; i_349_ < 0; i_349_++) {
			for (int i_350_ = i_348_; i_350_ < 0; i_350_++) {
				if (is_341_[i_342_++] != 0)
					is[i_343_++] = i;
				else
					i_343_++;
				if (is_341_[i_342_++] != 0)
					is[i_343_++] = i;
				else
					i_343_++;
				if (is_341_[i_342_++] != 0)
					is[i_343_++] = i;
				else
					i_343_++;
				if (is_341_[i_342_++] != 0)
					is[i_343_++] = i;
				else
					i_343_++;
			}
			for (int i_351_ = i_344_; i_351_ < 0; i_351_++) {
				if (is_341_[i_342_++] != 0)
					is[i_343_++] = i;
				else
					i_343_++;
			}
			i_343_ += i_346_;
			i_342_ += i_347_;
		}
	}

	public int method271(int i) {
		if (i == 0)
			return aByteArrayArray270[i][8] - 2;
		return aByteArrayArray270[i][8] - 1;
	}

	public int method272(String string, int i) {
		int i_352_ = 0;
		byte[] is = aByteArrayArray270[i];
		for (int i_353_ = 0; i_353_ < string.length(); i_353_++) {
			if (string.charAt(i_353_) == '@' && i_353_ + 4 < string.length() && string.charAt(i_353_ + 4) == '@')
				i_353_ += 4;
			else if (string.charAt(i_353_) == '~' && i_353_ + 4 < string.length() && string.charAt(i_353_ + 4) == '~')
				i_353_ += 4;
			else
				i_352_ += is[anIntArray271[string.charAt(i_353_)] + 7];
		}
		return i_352_;
	}

	public boolean imageUpdate(Image image, int i, int i_354_, int i_355_, int i_356_, int i_357_) {
		return true;
	}

	static {
		String string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| ";
		anIntArray271 = new int[256];
		for (int i = 0; i < 256; i++) {
			int i_358_ = string.indexOf(i);
			if (i_358_ == -1)
				i_358_ = 74;
			anIntArray271[i] = i_358_ * 9;
		}
	}
}
