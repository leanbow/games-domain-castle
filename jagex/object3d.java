package jagex;

public class object3d {
	public int vertexCount;
	public int[] cameraModifiedVerticesX;
	public int[] cameraModifiedVerticesY;
	public int[] cameraModifiedVerticesZ;
	public int[] screenX;
	public int[] screenY;
	public int[] anIntArray307;
	public byte[] aByteArray308;
	public int polygonCount;
	public int[] polygonPointsCount;
	public int[][] polygonPoints;
	public int[] polygonColors1;
	public int[] polygonColors2;
	public int[] anIntArray314;
	public int[] anIntArray315;
	public int[] anIntArray316;
	public int[] verticesNormalX;
	public int[] verticesNormalY;
	public int[] verticesNormalZ;
	public int anInt320;
	public int zOffset;
	public int updateNeeded = 1;
	public boolean onScreen = true;
	public int minX;
	public int maxX;
	public int minY;
	public int maxY;
	public int minZ;
	public int maxZ;
	public boolean aBoolean330 = true;
	public boolean aBoolean331 = false;
	public boolean aBoolean332 = false;
	public int anInt333 = -1;
	public int[] anIntArray334;
	public byte[] aByteArray335;
	private boolean aBoolean336 = false;
	public boolean aBoolean337 = false;
	public boolean aBoolean338 = false;
	public boolean aBoolean339 = false;
	public boolean aBoolean340 = false;
	private static int[] sinCosLookupTable256 = new int[512];
	private static int[] sinCosLookupTable1024 = new int[2048];
	private static byte[] aByteArray343 = new byte[64];
	private static int[] anIntArray344 = new int[256];
	private int anInt345 = 4;
	private int anInt346 = 12345678;
	public int vertexLimit;
	public int[] verticesX;
	public int[] verticesY;
	public int[] verticesZ;
	public int[] transformedVerticesX;
	public int[] transformedVerticesY;
	public int[] transformedVerticesZ;
	private int polygonLimit;
	private int[][] anIntArrayArray355;
	private int[] polygonsMinX;
	private int[] polygonsMaxX;
	private int[] polygonsMinY;
	private int[] polygonsMaxY;
	private int[] polygonsMinZ;
	private int[] polygonsMaxZ;
	private int translateX;
	private int translateY;
	private int translateZ;
	private int rotatePitch;
	private int rotateYaw;
	private int rotateRoll;
	private int scaleX;
	private int scaleY;
	private int scaleZ;
	private int scaleXFromY;
	private int scaleZFromY;
	private int scaleXFromZ;
	private int scaleYFromZ;
	private int scaleZFromX;
	private int scaleYFromX;
	private int transformType;
	private int highestLength = 12345678;
	private int anInt379 = 180;
	private int anInt380 = 155;
	private int anInt381 = 95;
	private int anInt382 = 256;
	protected int anInt383 = 512;
	protected int anInt384 = 32;
	private byte[] buffer;
	private int bufferPos;

	public object3d(int i, int i_0_) {
		init(i, i_0_);
		anIntArrayArray355 = new int[i_0_][1];
		for (int i_1_ = 0; i_1_ < i_0_; i_1_++)
			anIntArrayArray355[i_1_][0] = i_1_;
	}

	private void init(int i, int i_2_) {
		verticesX = new int[i];
		verticesY = new int[i];
		verticesZ = new int[i];
		anIntArray307 = new int[i];
		aByteArray308 = new byte[i];
		polygonPointsCount = new int[i_2_];
		polygonPoints = new int[i_2_][];
		polygonColors1 = new int[i_2_];
		polygonColors2 = new int[i_2_];
		anIntArray316 = new int[i_2_];
		anIntArray315 = new int[i_2_];
		anIntArray314 = new int[i_2_];
		if (!aBoolean340) {
			cameraModifiedVerticesX = new int[i];
			cameraModifiedVerticesY = new int[i];
			cameraModifiedVerticesZ = new int[i];
			screenX = new int[i];
			screenY = new int[i];
		}
		if (!aBoolean339) {
			aByteArray335 = new byte[i_2_];
			anIntArray334 = new int[i_2_];
		}
		if (aBoolean336) {
			transformedVerticesX = verticesX;
			transformedVerticesY = verticesY;
			transformedVerticesZ = verticesZ;
		} else {
			transformedVerticesX = new int[i];
			transformedVerticesY = new int[i];
			transformedVerticesZ = new int[i];
		}
		if (!aBoolean338 || !aBoolean337) {
			verticesNormalX = new int[i_2_];
			verticesNormalY = new int[i_2_];
			verticesNormalZ = new int[i_2_];
		}
		if (!aBoolean337) {
			polygonsMinX = new int[i_2_];
			polygonsMaxX = new int[i_2_];
			polygonsMinY = new int[i_2_];
			polygonsMaxY = new int[i_2_];
			polygonsMinZ = new int[i_2_];
			polygonsMaxZ = new int[i_2_];
		}
		polygonCount = 0;
		vertexCount = 0;
		vertexLimit = i;
		polygonLimit = i_2_;
		translateX = translateY = translateZ = 0;
		rotatePitch = rotateYaw = rotateRoll = 0;
		scaleX = scaleY = scaleZ = 256;
		scaleXFromY = scaleZFromY = scaleXFromZ = scaleYFromZ = scaleZFromX = scaleYFromX = 256;
		transformType = 0;
	}

	public void clear() {
		polygonCount = 0;
		vertexCount = 0;
	}

	public object3d(byte[] is, int i) {
		buffer = is;
		bufferPos = i;
		read(buffer);//file length
		int vertexCount = read(buffer);
		int polygonCount = read(buffer);
		init(vertexCount, polygonCount);
		anIntArrayArray355 = new int[polygonCount][];
		for (int id = 0; id < vertexCount; id++) {
			int x = read(buffer);
			int y = read(buffer);
			int z = read(buffer);
			addVertex(x, y, z);
		}
		for (int id = 0; id < polygonCount; id++) {
			int polygonPointsCount = read(buffer);
			int polygonColor1 = read(buffer);
			int polygonColor2 = read(buffer);
			int length2 = read(buffer);
			anInt383 = read(buffer);
			anInt384 = read(buffer);
			int i_14_ = read(buffer);
			int[] polygonPoints = new int[polygonPointsCount];
			for (int i_16_ = 0; i_16_ < polygonPointsCount; i_16_++)
				polygonPoints[i_16_] = read(buffer);
			int[] is_17_ = new int[length2];
			for (int i_18_ = 0; i_18_ < length2; i_18_++)
				is_17_[i_18_] = read(buffer);
			int i_19_ = addPolygon(polygonPointsCount, polygonPoints, polygonColor1, polygonColor2);
			anIntArrayArray355[id] = is_17_;
			if (i_14_ == 0)
				anIntArray316[i_19_] = 0;
			else
				anIntArray316[i_19_] = anInt346;
		}
		updateNeeded = 1;
	}

	public object3d(object3d[] class7s, int i) {
		method275(class7s, i, true);
	}

	public void method275(object3d[] class7s, int i, boolean bool) {
		int i_20_ = 0;
		int i_21_ = 0;
		for (int i_22_ = 0; i_22_ < i; i_22_++) {
			i_20_ += class7s[i_22_].polygonCount;
			i_21_ += class7s[i_22_].vertexCount;
		}
		init(i_21_, i_20_);
		if (bool)
			anIntArrayArray355 = new int[i_20_][];
		for (int i_23_ = 0; i_23_ < i; i_23_++) {
			object3d class7_24_ = class7s[i_23_];
			class7_24_.applyChanges();
			anInt384 = class7_24_.anInt384;
			anInt383 = class7_24_.anInt383;
			anInt379 = class7_24_.anInt379;
			anInt380 = class7_24_.anInt380;
			anInt381 = class7_24_.anInt381;
			anInt382 = class7_24_.anInt382;
			for (int i_25_ = 0; i_25_ < class7_24_.polygonCount; i_25_++) {
				int[] is = new int[class7_24_.polygonPointsCount[i_25_]];
				int[] is_26_ = class7_24_.polygonPoints[i_25_];
				for (int i_27_ = 0; i_27_ < class7_24_.polygonPointsCount[i_25_]; i_27_++)
					is[i_27_] = addVertex(class7_24_.verticesX[is_26_[i_27_]], class7_24_.verticesY[is_26_[i_27_]], class7_24_.verticesZ[is_26_[i_27_]]);
				int i_28_ = addPolygon(class7_24_.polygonPointsCount[i_25_], is, class7_24_.polygonColors1[i_25_], class7_24_.polygonColors2[i_25_]);
				anIntArray316[i_28_] = class7_24_.anIntArray316[i_25_];
				anIntArray315[i_28_] = class7_24_.anIntArray315[i_25_];
				anIntArray314[i_28_] = class7_24_.anIntArray314[i_25_];
				if (bool) {
					if (i > 1) {
						anIntArrayArray355[i_28_] = new int[(class7_24_.anIntArrayArray355[i_25_]).length + 1];
						anIntArrayArray355[i_28_][0] = i_23_;
						for (int i_29_ = 0; (i_29_ < class7_24_.anIntArrayArray355[i_25_].length); i_29_++)
							anIntArrayArray355[i_28_][i_29_ + 1] = class7_24_.anIntArrayArray355[i_25_][i_29_];
					} else {
						anIntArrayArray355[i_28_] = (new int[class7_24_.anIntArrayArray355[i_25_].length]);
						for (int i_30_ = 0; (i_30_ < class7_24_.anIntArrayArray355[i_25_].length); i_30_++)
							anIntArrayArray355[i_28_][i_30_] = class7_24_.anIntArrayArray355[i_25_][i_30_];
					}
				}
			}
		}
		updateNeeded = 1;
	}

	public int addVertex(int x, int y, int z) {
		for (int id = 0; id < vertexCount; id++) {
			if (verticesX[id] == x && verticesY[id] == y && verticesZ[id] == z)
				return id;
		}
		if (vertexCount >= vertexLimit)
			return -1;
		verticesX[vertexCount] = x;
		verticesY[vertexCount] = y;
		verticesZ[vertexCount] = z;
		return vertexCount++;
	}

	public int addVertex2(int x, int y, int z) {
		if (vertexCount >= vertexLimit)
			return -1;
		verticesX[vertexCount] = x;
		verticesY[vertexCount] = y;
		verticesZ[vertexCount] = z;
		return vertexCount++;
	}

	public int addPolygon(int pointsCount, int[] points, int color1, int color2) {
		if (polygonCount >= polygonLimit)
			return -1;
		polygonPointsCount[polygonCount] = pointsCount;
		polygonPoints[polygonCount] = points;
		polygonColors1[polygonCount] = color1;
		polygonColors2[polygonCount] = color2;
		updateNeeded = 1;
		return polygonCount++;
	}

	public void method279(int i) {
		if (!aBoolean339) {
			for (int i_38_ = 0; i_38_ < anIntArray334.length; i_38_++)
				anIntArray334[i_38_] = i;
		}
	}

	public void method280(boolean bool, int i, int i_39_, int i_40_, int i_41_, int i_42_) {
		anInt384 = 256 - i * 4;
		anInt383 = (64 - i_39_) * 16 + 128;
		if (!aBoolean338) {
			for (int i_43_ = 0; i_43_ < polygonCount; i_43_++) {
				if (bool)
					anIntArray316[i_43_] = anInt346;
				else
					anIntArray316[i_43_] = 0;
			}
			anInt379 = i_40_;
			anInt380 = i_41_;
			anInt381 = i_42_;
			anInt382 = (int) Math.sqrt((double) (i_40_ * i_40_ + i_41_ * i_41_ + i_42_ * i_42_));
			method298();
		}
	}

	public void method281(int i, int i_44_, int i_45_, int i_46_, int i_47_) {
		anInt384 = 256 - i * 4;
		anInt383 = (64 - i_44_) * 16 + 128;
		if (!aBoolean338) {
			anInt379 = i_45_;
			anInt380 = i_46_;
			anInt381 = i_47_;
			anInt382 = (int) Math.sqrt((double) (i_45_ * i_45_ + i_46_ * i_46_ + i_47_ * i_47_));
			method298();
		}
	}

	public void method282(int i, int i_48_, int i_49_, int i_50_) {
		if (i_48_ != anInt346)
			verticesX[i] = i_48_;
		if (i_49_ != anInt346)
			verticesY[i] = i_49_;
		if (i_50_ != anInt346)
			verticesZ[i] = i_50_;
		updateNeeded = 1;
	}

	public void method283(int i, int i_51_) {
		polygonColors1[i] = i_51_;
	}

	public void method284(int i, int i_52_) {
		polygonColors2[i] = i_52_;
	}

	public void requestAdjustRotate(int pitch, int yaw, int roll) {
		rotatePitch = rotatePitch + pitch & 0xff;
		rotateYaw = rotateYaw + yaw & 0xff;
		rotateRoll = rotateRoll + roll & 0xff;
		determinateTransformType();
		updateNeeded = 1;
	}

	public void requestSetRotate(int pitch, int yaw, int roll) {
		rotatePitch = pitch & 0xff;
		rotateYaw = yaw & 0xff;
		rotateRoll = roll & 0xff;
		determinateTransformType();
		updateNeeded = 1;
	}

	public void requestAdjustTranslate(int x, int y, int z) {
		translateX += x;
		translateY += y;
		translateZ += z;
		determinateTransformType();
		updateNeeded = 1;
	}

	public void requestSetTranslate(int x, int y, int z) {
		translateX = x;
		translateY = y;
		translateZ = z;
		determinateTransformType();
		updateNeeded = 1;
	}

	public int getTranslateX() {
		return translateX;
	}

	public void requestScale(int x, int y, int z) {
		scaleX = x;
		scaleY = y;
		scaleZ = z;
		determinateTransformType();
		updateNeeded = 1;
	}

	public void requestScale(int xy, int zy, int xz, int yz, int zx, int yx) {
		scaleXFromY = xy;
		scaleZFromY = zy;
		scaleXFromZ = xz;
		scaleYFromZ = yz;
		scaleZFromX = zx;
		scaleYFromX = yx;
		determinateTransformType();
		updateNeeded = 1;
	}

	private void determinateTransformType() {
		if (scaleXFromY != 256 || scaleZFromY != 256 || scaleXFromZ != 256 || scaleYFromZ != 256 || scaleZFromX != 256 || scaleYFromX != 256)
			transformType = 4;
		else if (scaleX != 256 || scaleY != 256 || scaleZ != 256)
			transformType = 3;
		else if (rotatePitch != 0 || rotateYaw != 0 || rotateRoll != 0)
			transformType = 2;
		else if (translateX != 0 || translateY != 0 || translateZ != 0)
			transformType = 1;
		else
			transformType = 0;
	}

	private void translate(int x, int y, int z) {
		for (int id = 0; id < vertexCount; id++) {
			transformedVerticesX[id] += x;
			transformedVerticesY[id] += y;
			transformedVerticesZ[id] += z;
		}
	}

	private void rotate(int pitch, int yaw, int roll) {
		for (int id = 0; id < vertexCount; id++) {
			if (roll != 0) {
				int rollSin = sinCosLookupTable256[roll];
				int rollCos = sinCosLookupTable256[roll + 256];
				int x = ((transformedVerticesY[id] * rollSin + transformedVerticesX[id] * rollCos) >> 15);
				transformedVerticesY[id] = (transformedVerticesY[id] * rollCos - transformedVerticesX[id] * rollSin) >> 15;
				transformedVerticesX[id] = x;
			}
			if (pitch != 0) {
				int pitchSin = sinCosLookupTable256[pitch];
				int pitchCos = sinCosLookupTable256[pitch + 256];
				int y = ((transformedVerticesY[id] * pitchCos - transformedVerticesZ[id] * pitchSin) >> 15);
				transformedVerticesZ[id] = (transformedVerticesY[id] * pitchSin + transformedVerticesZ[id] * pitchCos) >> 15;
				transformedVerticesY[id] = y;
			}
			if (yaw != 0) {
				int yawSin = sinCosLookupTable256[yaw];
				int yawCos = sinCosLookupTable256[yaw + 256];
				int x = ((transformedVerticesZ[id] * yawSin + transformedVerticesX[id] * yawCos) >> 15);
				transformedVerticesZ[id] = (transformedVerticesZ[id] * yawCos - transformedVerticesX[id] * yawSin) >> 15;
				transformedVerticesX[id] = x;
			}
		}
	}

	private void scale(int i, int i_83_, int i_84_, int i_85_, int i_86_, int i_87_) {
		for (int id = 0; id < vertexCount; id++) {
			if (i != 0)
				transformedVerticesX[id] += transformedVerticesY[id] * i >> 8;
			if (i_83_ != 0)
				transformedVerticesZ[id] += transformedVerticesY[id] * i_83_ >> 8;
			if (i_84_ != 0)
				transformedVerticesX[id] += transformedVerticesZ[id] * i_84_ >> 8;
			if (i_85_ != 0)
				transformedVerticesY[id] += transformedVerticesZ[id] * i_85_ >> 8;
			if (i_86_ != 0)
				transformedVerticesZ[id] += transformedVerticesX[id] * i_86_ >> 8;
			if (i_87_ != 0)
				transformedVerticesY[id] += transformedVerticesX[id] * i_87_ >> 8;
		}
	}

	private void scale(int x, int y, int z) {
		for (int id = 0; id < vertexCount; id++) {
			transformedVerticesX[id] = transformedVerticesX[id] * x >> 8;
			transformedVerticesY[id] = transformedVerticesY[id] * y >> 8;
			transformedVerticesZ[id] = transformedVerticesZ[id] * z >> 8;
		}
	}

	private void calculateBounds() {
		minX = minY = minZ = 999999;
		highestLength = maxX = maxY = maxZ = -999999;
		for (int polygonId = 0; polygonId < polygonCount; polygonId++) {
			int[] polyPoints = polygonPoints[polygonId];
			int polyPoint = polyPoints[0];
			int polyPointCount = polygonPointsCount[polygonId];
			int lowestX;
			int highestX = lowestX = transformedVerticesX[polyPoint];
			int lowestY;
			int highestY = lowestY = transformedVerticesY[polyPoint];
			int lowestZ;
			int highestZ = lowestZ = transformedVerticesZ[polyPoint];
			for (int pointId = 0; pointId < polyPointCount; pointId++) {
				polyPoint = polyPoints[pointId];
				if (transformedVerticesX[polyPoint] < lowestX)
					lowestX = transformedVerticesX[polyPoint];
				else if (transformedVerticesX[polyPoint] > highestX)
					highestX = transformedVerticesX[polyPoint];
				if (transformedVerticesY[polyPoint] < lowestY)
					lowestY = transformedVerticesY[polyPoint];
				else if (transformedVerticesY[polyPoint] > highestY)
					highestY = transformedVerticesY[polyPoint];
				if (transformedVerticesZ[polyPoint] < lowestZ)
					lowestZ = transformedVerticesZ[polyPoint];
				else if (transformedVerticesZ[polyPoint] > highestZ)
					highestZ = transformedVerticesZ[polyPoint];
			}
			if (!aBoolean337) {
				polygonsMinX[polygonId] = lowestX;
				polygonsMaxX[polygonId] = highestX;
				polygonsMinY[polygonId] = lowestY;
				polygonsMaxY[polygonId] = highestY;
				polygonsMinZ[polygonId] = lowestZ;
				polygonsMaxZ[polygonId] = highestZ;
			}
			if (highestX - lowestX > highestLength)
				highestLength = highestX - lowestX;
			if (highestY - lowestY > highestLength)
				highestLength = highestY - lowestY;
			if (highestZ - lowestZ > highestLength)
				highestLength = highestZ - lowestZ;
			if (lowestX < minX)
				minX = lowestX;
			if (highestX > maxX)
				maxX = highestX;
			if (lowestY < minY)
				minY = lowestY;
			if (highestY > maxY)
				maxY = highestY;
			if (lowestZ < minZ)
				minZ = lowestZ;
			if (highestZ > maxZ)
				maxZ = highestZ;
		}
	}

	public void method298() {
		if (!aBoolean338) {
			int i = anInt383 * anInt382 >> 8;
			for (int i_101_ = 0; i_101_ < polygonCount; i_101_++) {
				if (anIntArray316[i_101_] != anInt346)
					anIntArray316[i_101_] = (verticesNormalX[i_101_] * anInt379 + verticesNormalY[i_101_] * anInt380 + verticesNormalZ[i_101_] * anInt381) / i;
			}
			int[] is = new int[vertexCount];
			int[] is_102_ = new int[vertexCount];
			int[] is_103_ = new int[vertexCount];
			int[] is_104_ = new int[vertexCount];
			for (int i_105_ = 0; i_105_ < vertexCount; i_105_++) {
				is[i_105_] = 0;
				is_102_[i_105_] = 0;
				is_103_[i_105_] = 0;
				is_104_[i_105_] = 0;
			}
			for (int i_106_ = 0; i_106_ < polygonCount; i_106_++) {
				if (anIntArray316[i_106_] == anInt346) {
					for (int i_107_ = 0; i_107_ < polygonPointsCount[i_106_]; i_107_++) {
						int i_108_ = polygonPoints[i_106_][i_107_];
						is[i_108_] += verticesNormalX[i_106_];
						is_102_[i_108_] += verticesNormalY[i_106_];
						is_103_[i_108_] += verticesNormalZ[i_106_];
						is_104_[i_108_]++;
					}
				}
			}
			for (int i_109_ = 0; i_109_ < vertexCount; i_109_++) {
				if (is_104_[i_109_] > 0)
					anIntArray307[i_109_] = ((is[i_109_] * anInt379 + is_102_[i_109_] * anInt380 + is_103_[i_109_] * anInt381) / (i * is_104_[i_109_]));
			}
		}
	}

	public void calculateNormals() {
		if (!aBoolean338 || !aBoolean337) {
			for (int polygonId = 0; polygonId < polygonCount; polygonId++) {
				int[] points = polygonPoints[polygonId];
				int point1X = transformedVerticesX[points[0]];
				int point1Y = transformedVerticesY[points[0]];
				int point1Z = transformedVerticesZ[points[0]];
				int x1 = transformedVerticesX[points[1]] - point1X;
				int y1 = transformedVerticesY[points[1]] - point1Y;
				int z1 = transformedVerticesZ[points[1]] - point1Z;
				int x2 = transformedVerticesX[points[2]] - point1X;
				int y2 = transformedVerticesY[points[2]] - point1Y;
				int z2 = transformedVerticesZ[points[2]] - point1Z;
				int normalX = y1 * z2 - y2 * z1;
				int normalY = z1 * x2 - z2 * x1;
				int normalZ = x1 * y2 - x2 * y1;
				for (/**/; (normalX > 8192 || normalY > 8192 || normalZ > 8192 || normalX < -8192 || normalY < -8192 || normalZ < -8192); /**/) {
					normalX >>= 1;
					normalY >>= 1;
					normalZ >>= 1;
				}
				int normalLen = (int) (256.0 * Math.sqrt((double) (normalX * normalX + normalY * normalY + normalZ * normalZ)));
				if (normalLen <= 0)
					normalLen = 1;
				verticesNormalX[polygonId] = normalX * 65536 / normalLen;
				verticesNormalY[polygonId] = normalY * 65536 / normalLen;
				verticesNormalZ[polygonId] = normalZ * 65536 / normalLen;
				anIntArray315[polygonId] = -1;
			}
			method298();
		}
	}

	public void update() {
		if (updateNeeded == 2) {
			updateNeeded = 0;
			for (int id = 0; id < vertexCount; id++) {
				transformedVerticesX[id] = verticesX[id];
				transformedVerticesY[id] = verticesY[id];
				transformedVerticesZ[id] = verticesZ[id];
			}
			minX = minY = minZ = -9999999;
			highestLength = maxX = maxY = maxZ = 9999999;
		} else if (updateNeeded == 1) {
			updateNeeded = 0;
			for (int id = 0; id < vertexCount; id++) {
				transformedVerticesX[id] = verticesX[id];
				transformedVerticesY[id] = verticesY[id];
				transformedVerticesZ[id] = verticesZ[id];
			}
			if (transformType >= 2)
				rotate(rotatePitch, rotateYaw, rotateRoll);
			if (transformType >= 3)
				scale(scaleX, scaleY, scaleZ);
			if (transformType >= 4)
				scale(scaleXFromY, scaleZFromY, scaleXFromZ, scaleYFromZ, scaleZFromX, scaleYFromX);
			if (transformType >= 1)
				translate(translateX, translateY, translateZ);
			calculateBounds();
			calculateNormals();
		}
	}

	public void update(int cameraX, int cameraY, int cameraZ, int cameraPitch, int cameraYaw, int cameraRoll, int screenWidth, int minRenderZ) {
		update();
		if (minZ > world3d.anInt546 || maxZ < world3d.anInt545 || minX > world3d.anInt542 || maxX < world3d.anInt541 || minY > world3d.anInt544 || maxY < world3d.anInt543)
			onScreen = false;
		else {
			onScreen = true;
			int rollSin = 0;
			int rollCos = 0;
			int pitchSin = 0;
			int pitchCos = 0;
			int yawSin = 0;
			int yawCos = 0;
			if (cameraRoll != 0) {
				rollSin = sinCosLookupTable1024[cameraRoll];
				rollCos = sinCosLookupTable1024[cameraRoll + 1024];
			}
			if (cameraYaw != 0) {
				yawSin = sinCosLookupTable1024[cameraYaw];
				yawCos = sinCosLookupTable1024[cameraYaw + 1024];
			}
			if (cameraPitch != 0) {
				pitchSin = sinCosLookupTable1024[cameraPitch];
				pitchCos = sinCosLookupTable1024[cameraPitch + 1024];
			}
			for (int id = 0; id < vertexCount; id++) {
				int x = transformedVerticesX[id] - cameraX;
				int y = transformedVerticesY[id] - cameraY;
				int z = transformedVerticesZ[id] - cameraZ;
				if (cameraRoll != 0) {
					int oldX = y * rollSin + x * rollCos >> 15;
					y = y * rollCos - x * rollSin >> 15;
					x = oldX;
				}
				if (cameraYaw != 0) {
					int oldX = z * yawSin + x * yawCos >> 15;
					z = z * yawCos - x * yawSin >> 15;
					x = oldX;
				}
				if (cameraPitch != 0) {
					int oldY = y * pitchCos - z * pitchSin >> 15;
					z = y * pitchSin + z * pitchCos >> 15;
					y = oldY;
				}
				if (z >= minRenderZ)
					screenX[id] = (x << screenWidth) / z;
				else
					screenX[id] = x << screenWidth;
				if (z >= minRenderZ)
					screenY[id] = (y << screenWidth) / z;
				else
					screenY[id] = y << screenWidth;
				cameraModifiedVerticesX[id] = x;
				cameraModifiedVerticesY[id] = y;
				cameraModifiedVerticesZ[id] = z;
			}
		}
	}

	public void applyChanges() {
		update();
		for (int i = 0; i < vertexCount; i++) {
			verticesX[i] = transformedVerticesX[i];
			verticesY[i] = transformedVerticesY[i];
			verticesZ[i] = transformedVerticesZ[i];
		}
		translateX = translateY = translateZ = 0;
		rotatePitch = rotateYaw = rotateRoll = 0;
		scaleX = scaleY = scaleZ = 256;
		scaleXFromY = scaleZFromY = scaleXFromZ = scaleYFromZ = scaleZFromX = scaleYFromX = 256;
		transformType = 0;
	}

	public object3d copy() {
		object3d[] class7s = new object3d[1];
		class7s[0] = this;
		object3d class7_143_ = new object3d(class7s, 1);
		class7_143_.zOffset = zOffset;
		return class7_143_;
	}

	public int read(byte[] buf) {
		for (/**/; buf[bufferPos] == 10 || buf[bufferPos] == 13; bufferPos++) {
			/* empty */
		}
		int firstValue = anIntArray344[buf[bufferPos++] & 0xff];
		int secondValue = anIntArray344[buf[bufferPos++] & 0xff];
		int thirdValue = anIntArray344[buf[bufferPos++] & 0xff];
		int finalValue = (firstValue << 12) | (secondValue << 6) | thirdValue;
		finalValue -= 0x20000;
		if (finalValue == 123456)
			finalValue = anInt346;
		return finalValue;
	}

	static {
		for (int id = 0; id < 256; id++) {
			sinCosLookupTable256[id] = (int) (Math.sin((double) 2 * Math.PI * id / 256) * 32768.0);
			sinCosLookupTable256[id + 256] = (int) (Math.cos((double) 2 * Math.PI * id / 256) * 32768.0);
		}
		for (int id = 0; id < 1024; id++) {
			sinCosLookupTable1024[id] = (int) (Math.sin((double) 2 * Math.PI * id / 1024) * 32768.0);
			sinCosLookupTable1024[id + 1024] = (int) (Math.cos((double) 2 * Math.PI * id / 1024) * 32768.0);
		}
		for (int i = 0; i < 10; i++)
			aByteArray343[i] = (byte) (48 + i);
		for (int i = 0; i < 26; i++)
			aByteArray343[i + 10] = (byte) (65 + i);
		for (int i = 0; i < 26; i++)
			aByteArray343[i + 36] = (byte) (97 + i);
		aByteArray343[62] = (byte) -93;
		aByteArray343[63] = (byte) 36;
		for (int i = 0; i < 10; i++)
			anIntArray344[48 + i] = i;
		for (int i = 0; i < 26; i++)
			anIntArray344[65 + i] = i + 10;
		for (int i = 0; i < 26; i++)
			anIntArray344[97 + i] = i + 36;
		anIntArray344[163] = 62;
		anIntArray344[36] = 63;
	}
}
