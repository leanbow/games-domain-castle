import jagex.pixmap;
import jagex.tools;
import jagex.world3d;

public class lobby2 extends module {
	gameclient anApplet_Sub1_Sub1_Sub1_747;
	pixmap pixelMap;
	int[] anIntArray749 = new int[50];
	boolean aBoolean750 = false;
	int anInt751;
	int anInt752;
	int[] anIntArray753 = { 900, 1200, 1200, 1200, 1200, 1130, 1400, 1200 };
	int[] anIntArray754 = { 0, 0, 0, 0, 256, 256, 0, 0 };
	int anInt755 = -1;
	int anInt756 = -2;
	boolean[] aBooleanArray757 = new boolean[20];
	int[] anIntArray758 = new int[20];
	int[] anIntArray759 = new int[20];
	String[] aStringArray760 = new String[20];
	String[] aStringArray761 = new String[20];
	int anInt762;
	byte[] aByteArray763 = new byte[5000];
	byte[] aByteArray764 = new byte[1000];
	int[] anIntArray765 = { 85, 255, 425, 171, 341 };
	int[] anIntArray766 = { 40, 40, 40, 173, 173 };
	int[] anIntArray767 = { 192, 320, 448, 64, 192, 320, 448 };
	int[] anIntArray768 = { 54, 0, 54, 153, 182, 128, 182 };
	int[] anIntArray769 = { 64, 192, 320, 448, 64, 192, 320, 448 };
	int[] anIntArray770 = { 25, 37, 0, 37, 158, 170, 133, 170 };

	public void onInit(gameclient gameclient, int i) {
		anApplet_Sub1_Sub1_Sub1_747 = gameclient;
		anInt751 = i;
		pixelMap = gameclient.pixelMap;
	}

	public void onModuleChange() {
		anInt756 = -2;
		aBoolean750 = false;
		for (int i = 0; i < gameclient.gameCount; i++)
			aBooleanArray757[i] = false;
		anInt762 = 0;
	}

	public void onLoop() {
		int[] is = null;
		int[] is_0_ = null;
		int i = 60;
		int i_1_ = 50;
		if (gameclient.gameCount == 5) {
			is = anIntArray765;
			is_0_ = anIntArray766;
			i = 75;
			i_1_ = 55;
		}
		if (gameclient.gameCount == 7) {
			is = anIntArray767;
			is_0_ = anIntArray768;
		}
		if (gameclient.gameCount == 8) {
			is = anIntArray769;
			is_0_ = anIntArray770;
		}
		for (int i_2_ = 0; i_2_ < gameclient.gameCount; i_2_++) {
			int i_3_ = is[i_2_];
			int i_4_ = is_0_[i_2_] + 55;
			if (i_2_ >= 0 && anApplet_Sub1_Sub1_Sub1_747.mouseClick == 1 && anApplet_Sub1_Sub1_Sub1_747.mouseX > i_3_ - i && anApplet_Sub1_Sub1_Sub1_747.mouseY > i_4_ - i_1_ && anApplet_Sub1_Sub1_Sub1_747.mouseX < i_3_ + i && anApplet_Sub1_Sub1_Sub1_747.mouseY < i_4_ + i_1_) {
				anApplet_Sub1_Sub1_Sub1_747.mouseClick = 0;
				anApplet_Sub1_Sub1_Sub1_747.method77(i_2_);
				return;
			}
		}
		anInt752 = anInt752 + 8 & 0x3ff;
		anApplet_Sub1_Sub1_Sub1_747.mouseClick = 0;
		for (int i_5_ = 0; i_5_ < gameclient.gameCount; i_5_++) {
			if (anIntArray758[i_5_] > 0) {
				anApplet_Sub1_Sub1_Sub1_747.modules[i_5_ + 4].aBoolean142 = false;
				anApplet_Sub1_Sub1_Sub1_747.modules[i_5_ + 4].onLoop();
				anIntArray758[i_5_]--;
			}
		}
		for (int i_6_ = 0; i_6_ < gameclient.gameCount; i_6_++) {
			if (anIntArray759[i_6_] > 0) {
				anIntArray759[i_6_]--;
				if (anIntArray759[i_6_] == 0)
					method149(i_6_ + 1);
			}
		}
		int i_7_ = 0;
		while (i_7_ < anInt762) {
			int i_8_ = ((aByteArray763[i_7_] & 0xff) * 256 + (aByteArray763[i_7_ + 1] & 0xff));
			int i_9_ = aByteArray763[i_7_ + 2] & 0xff;
			int i_10_ = aByteArray763[i_7_ + 3] & 0xff;
			if (anApplet_Sub1_Sub1_Sub1_747.modules[i_9_ + 3].method114()) {
				for (int i_11_ = 0; i_11_ < i_8_; i_11_++)
					aByteArray764[i_11_] = aByteArray763[i_11_ + i_7_ + 4];
				anApplet_Sub1_Sub1_Sub1_747.modules[i_9_ + 3].method103(i_10_, i_8_, aByteArray764);
				anInt762 -= 4 + i_8_;
				for (int i_12_ = i_7_; i_12_ < anInt762; i_12_++)
					aByteArray763[i_12_] = aByteArray763[i_12_ + 4 + i_8_];
			} else
				i_7_ += 4 + i_8_;
		}
	}

	public void method103(int i, int i_13_, byte[] is) {
		if (i == 200) {
			aBoolean750 = true;
			for (int i_14_ = 0; i_14_ < gameclient.gameCount; i_14_++)
				anIntArray749[i_14_] = tools.g2(is, i_14_ * 2 + 1);
			anApplet_Sub1_Sub1_Sub1_747.aClass2_582.putPacketId(24);
			for (int i_15_ = 0; i_15_ < gameclient.gameCount; i_15_++) {
				if (!aBooleanArray757[i_15_])
					anApplet_Sub1_Sub1_Sub1_747.aClass2_582.p1(i_15_);
			}
			anApplet_Sub1_Sub1_Sub1_747.aClass2_582.flushNoException();
		}
	}

	public void method118(int i, int i_16_, byte[] is, int i_17_) {
		if (i_17_ != 0) {
			aBooleanArray757[i_17_ - 1] = true;
			anIntArray758[i_17_ - 1] = 150;
			if (anApplet_Sub1_Sub1_Sub1_747.modules[i_17_ + 3].method114())
				anApplet_Sub1_Sub1_Sub1_747.modules[i_17_ + 3].method103(i, i_16_, is);
			else if (anInt762 + i_16_ + 4 < 5000) {
				aByteArray763[anInt762++] = (byte) (i_16_ / 256);
				aByteArray763[anInt762++] = (byte) (i_16_ & 0xff);
				aByteArray763[anInt762++] = (byte) i_17_;
				aByteArray763[anInt762++] = (byte) i;
				for (int i_18_ = 0; i_18_ < i_16_; i_18_++)
					aByteArray763[anInt762++] = is[i_18_];
			}
		}
	}

	public void method119(int i) {
		if (i != 0 && aBooleanArray757[i - 1] != false) {
			if (anIntArray759[i - 1] == 0)
				anIntArray759[i - 1] = 150;
		}
	}

	public void method120(int i, String string, String string_19_) {
		if (i != 0) {
			aStringArray760[i - 1] = string.trim();
			aStringArray761[i - 1] = string_19_.trim();
		}
	}

	public void method149(int i) {
		aBooleanArray757[i - 1] = false;
		int i_20_ = 0;
		while (i_20_ < anInt762) {
			int i_21_ = ((aByteArray763[i_20_] & 0xff) * 256 + (aByteArray763[i_20_ + 1] & 0xff));
			int i_22_ = aByteArray763[i_20_ + 2] & 0xff;
			if (i == i_22_) {
				anInt762 -= 4 + i_21_;
				for (int i_23_ = i_20_; i_23_ < anInt762; i_23_++)
					aByteArray763[i_23_] = aByteArray763[i_23_ + 4 + i_21_];
			} else
				i_20_ += 4 + i_21_;
		}
	}

	public void onDraw() {
		int mouseX = anApplet_Sub1_Sub1_Sub1_747.mouseX;
		int mouseY = anApplet_Sub1_Sub1_Sub1_747.mouseY;
		boolean bool = false;
		int[] is = null;
		int[] is_25_ = null;
		int i_26_ = 60;
		int i_27_ = 50;
		if (gameclient.gameCount == 5) {
			is = anIntArray765;
			is_25_ = anIntArray766;
			i_26_ = 75;
			i_27_ = 55;
		}
		if (gameclient.gameCount == 7) {
			is = anIntArray767;
			is_25_ = anIntArray768;
		}
		if (gameclient.gameCount == 8) {
			is = anIntArray769;
			is_25_ = anIntArray770;
		}
		for (int i_28_ = 0; i_28_ < gameclient.gameCount; i_28_++) {
			int i_29_ = is[i_28_];
			int i_30_ = is_25_[i_28_] + 55;
			if (mouseX > i_29_ - i_26_ && mouseY > i_30_ - i_27_ && mouseX < i_29_ + i_26_ && mouseY < i_30_ + i_27_) {
				if (!aBooleanArray757[i_28_]) {
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method113(0);
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method117();
				} else
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method121();
				anApplet_Sub1_Sub1_Sub1_747.aClass10_1084.method362(i_29_, i_30_, i_26_, i_27_, 512, 7);
				anApplet_Sub1_Sub1_Sub1_747.aClass10_1085.method362(i_29_, i_30_, i_26_, i_27_, 512, 7);
				anApplet_Sub1_Sub1_Sub1_747.aClass10_1086.method362(i_29_, i_30_, i_26_, i_27_, 512, 7);
				anApplet_Sub1_Sub1_Sub1_747.aClass10_1087.method362(i_29_, i_30_, i_26_, i_27_, 512, 7);
				pixelMap.method241(i_29_ - i_26_, i_30_ - i_27_, i_26_ * 2, i_27_ * 2, 0, 4210752);
				if (anInt755 != i_28_)
					anInt752 = anIntArray754[i_28_];
				if (i_28_ == 0)
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method116(896 + anInt752 & 0x3ff, 512, anIntArray753[i_28_]);
				else
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method116(896, anInt752, anIntArray753[i_28_]);
				anInt755 = i_28_;
				bool = true;
				pixelMap.method243(i_29_ - i_26_, i_30_ - i_27_, i_26_ * 2, i_27_ * 2, 12632256);
			} else if (anInt756 == i_28_ || anInt756 == -2 || anIntArray758[i_28_] > 0) {
				if (!aBooleanArray757[i_28_]) {
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method113(0);
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method117();
				} else
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method121();
				anApplet_Sub1_Sub1_Sub1_747.aClass10_1084.method362(i_29_, i_30_, i_26_, i_27_, 512, 7);
				anApplet_Sub1_Sub1_Sub1_747.aClass10_1085.method362(i_29_, i_30_, i_26_, i_27_, 512, 7);
				anApplet_Sub1_Sub1_Sub1_747.aClass10_1086.method362(i_29_, i_30_, i_26_, i_27_, 512, 7);
				pixelMap.method241(i_29_ - i_26_, i_30_ - i_27_, i_26_ * 2, i_27_ * 2, 0, 4210752);
				if (i_28_ == 0)
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method116(896, 512, anIntArray753[i_28_]);
				else
					anApplet_Sub1_Sub1_Sub1_747.modules[i_28_ + 4].method116(896, anIntArray754[i_28_], anIntArray753[i_28_]);
				pixelMap.method243(i_29_ - i_26_, i_30_ - i_27_, i_26_ * 2, i_27_ * 2, 8421504);
			}
			pixelMap.method242(i_29_ - 64, i_30_ + i_27_, 128, 25, 0);
			if (aBoolean750)
				pixelMap.method267(((anApplet_Sub1_Sub1_Sub1_747.gameNameModules[i_28_]) + " - " + anIntArray749[i_28_] + " " + (anApplet_Sub1_Sub1_Sub1_747.chalroomModules[1])), i_29_, i_30_ + i_27_ + 12, 1, 16777215);
			else
				pixelMap.method267((anApplet_Sub1_Sub1_Sub1_747.gameNameModules[i_28_]), i_29_, i_30_ + i_27_ + 12, 1, 16777215);
			if (aBooleanArray757[i_28_])
				pixelMap.method267((aStringArray760[i_28_] + " " + (anApplet_Sub1_Sub1_Sub1_747.chalroomModules[38]) + " " + aStringArray761[i_28_]), i_29_, i_30_ + i_27_ + 23, 0, 16777215);
		}
		if (gameclient.gameCount == 5)
			pixelMap.method267((anApplet_Sub1_Sub1_Sub1_747.chalroomModules[36]), 256, 25, 6, 16777215);
		else if (gameclient.gameCount == 7)
			pixelMap.method268((anApplet_Sub1_Sub1_Sub1_747.chalroomModules[36]), 25, 40, 6, 16777215);
		else if (gameclient.gameCount == 8)
			pixelMap.method268((anApplet_Sub1_Sub1_Sub1_747.chalroomModules[36]), 25, 20, 6, 16777215);
		anInt756 = anInt755;
		
		if (!bool)
			anInt755 = -1;
	}
}
