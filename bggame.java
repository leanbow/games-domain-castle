import jagex.object3d;
import jagex.pixmap;
import jagex.tools;
import jagex.world3d;

public class bggame extends module {
	int anInt965 = 56;
	gameclient anApplet_Sub1_Sub1_Sub1_966;
	pixmap aClass6_967;
	world3d aClass10_968;
	world3d aClass10_969;
	world3d aClass10_970;
	int anInt971;
	object3d aClass7_972;
	object3d aClass7_973;
	object3d aClass7_974;
	object3d aClass7_975;
	object3d aClass7_976;
	object3d aClass7_977;
	object3d[] aClass7Array978 = new object3d[32];
	int anInt979;
	int[] anIntArray980 = new int[32];
	object3d[] aClass7Array981 = new object3d[32];
	int anInt982;
	int[] anIntArray983 = new int[32];
	object3d[] aClass7Array984 = new object3d[32];
	int anInt985;
	object3d[] aClass7Array986 = new object3d[32];
	int anInt987;
	int[] anIntArray988 = new int[32];
	int anInt989;
	int anInt990 = 896;
	int anInt991;
	int anInt992;
	int anInt993;
	int anInt994;
	int anInt995 = 1;
	int anInt996 = 1;
	int anInt997;
	int anInt998 = -1;
	boolean aBoolean999 = false;
	boolean aBoolean1000 = false;
	boolean aBoolean1001 = false;
	boolean aBoolean1002 = false;
	int anInt1003 = 100;
	int anInt1004;
	int anInt1005 = 60;
	int anInt1006;
	int anInt1007;
	boolean aBoolean1008 = true;
	int[] anIntArray1009 = new int[24];
	int[] anIntArray1010 = new int[2];
	int[] anIntArray1011 = new int[24];
	int[] anIntArray1012 = new int[2];
	int anInt1013;
	int[] anIntArray1014 = new int[24];
	int[] anIntArray1015 = new int[24];
	int[] anIntArray1016 = new int[24];
	int[] anIntArray1017 = new int[24];
	int[] anIntArray1018 = new int[24];
	boolean aBoolean1019 = false;
	int anInt1020 = -1;
	int anInt1021 = -1;
	int anInt1022 = -1;
	int anInt1023;
	int anInt1024 = 100;

	public void onInit(gameclient applet_sub1_sub1_sub1, int i) {
		anApplet_Sub1_Sub1_Sub1_966 = applet_sub1_sub1_sub1;
		anInt971 = i;
		aClass6_967 = applet_sub1_sub1_sub1.pixelMap;
		aClass10_968 = applet_sub1_sub1_sub1.aClass10_1086;
		aClass10_969 = applet_sub1_sub1_sub1.aClass10_1084;
		aClass10_970 = applet_sub1_sub1_sub1.aClass10_1085;
		aClass6_967.method249(applet_sub1_sub1_sub1.aByteArray1083, tools.findFile("dice.tga", applet_sub1_sub1_sub1.aByteArray1083), i, true, 6, false);
		for (int i_0_ = 0; i_0_ < 6; i_0_++) {
			aClass6_967.method252(i + i_0_, i + i_0_ + 10);
			aClass6_967.method253(i + i_0_ + 10, 16711680, 5790698);
			aClass6_967.method253(i + i_0_ + 10, 11603727, 2042071);
			aClass6_967.method253(i + i_0_ + 10, 8917522, 1188);
			aClass6_967.method253(i + i_0_ + 10, 15248556, 11320808);
			aClass6_967.method253(i + i_0_ + 10, 14780295, 7901908);
			aClass6_967.method260(0, 0, 22, 24, i + i_0_ + 10, 33023);
		}
		aClass7_972 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("bgammon1.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_975 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("bgammon2.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_973 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("whitebit.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_973.requestScale(128, 160, 128);
		aClass7_974 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("redbit.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_974.requestScale(128, 160, 128);
		aClass7_976 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("arrow.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_976.requestScale(128, 160, 128);
		aClass7_977 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("cross.ob2", (applet_sub1_sub1_sub1.aByteArray1082)));
		aClass7_977.requestScale(140, 160, 140);
		aClass7_972.method280(true, 52, 32, -50, -10, -50);
		aClass7_973.method280(true, 52, 32, -50, -10, -50);
		aClass7_974.method280(true, 52, 32, -50, -10, -50);
		aClass7_976.method280(true, 52, 32, -50, -10, -50);
		for (int i_1_ = 0; i_1_ < 32; i_1_++)
			aClass7Array978[i_1_] = aClass7_973.copy();
		for (int i_2_ = 0; i_2_ < 32; i_2_++)
			aClass7Array981[i_2_] = aClass7_974.copy();
		for (int i_3_ = 0; i_3_ < 32; i_3_++)
			aClass7Array984[i_3_] = aClass7_976.copy();
		for (int i_4_ = 0; i_4_ < 32; i_4_++)
			aClass7Array986[i_4_] = aClass7_977.copy();
	}

	public void method113(int i) {
		anInt1007 = i;
		anInt989 = 0;
		if (i == 0)
			anInt989 = 0;
		else
			anInt989 = 512;
		aClass10_968.clearObjects();
		aClass10_969.clearObjects();
		aClass10_968.method362(256, 152, 256, 152, 512, 9);
		aClass10_968.maxRenderZ = 3000;
		aClass10_968.anInt464 = 3000;
		aClass10_968.anInt465 = 20;
		aClass10_968.anInt466 = 5000;
		aClass10_969.method385(aClass10_968);
		aClass10_970.method385(aClass10_968);
		aClass10_968.clearObjects();
		aClass10_969.clearObjects();
		aClass10_970.clearObjects();
		aClass10_968.addObject(aClass7_972);
		aClass10_970.addObject(aClass7_975);
		for (int i_5_ = 0; i_5_ < 24; i_5_++)
			anIntArray1009[i_5_] = 0;
		anIntArray1009[0] = 52;
		anIntArray1009[5] = 5;
		anIntArray1009[7] = 3;
		anIntArray1009[11] = 55;
		anIntArray1009[12] = 5;
		anIntArray1009[16] = 53;
		anIntArray1009[18] = 55;
		anIntArray1009[23] = 2;
		anIntArray1010[0] = 0;
		anIntArray1010[1] = 0;
		anInt995 = 1;
		anInt996 = 1;
		anInt998 = -1;
		anInt1003 = 100;
		aBoolean1000 = false;
		aBoolean1008 = true;
		aBoolean1001 = false;
		aBoolean1002 = false;
	}

	public void method121() {
		aClass10_968.clearObjects();
		aClass10_969.clearObjects();
		aClass10_970.clearObjects();
		aClass10_968.addObject(aClass7_972);
		aClass10_970.addObject(aClass7_975);
	}

	public boolean method114() {
		if (aBoolean1000)
			return false;
		return true;
	}

	public void method103(int i, int i_6_, byte[] is) {
		if (i == 255) {
			anInt1004 = is[1] & 0xff;
			anInt1005 = is[2] & 0xff;
			anInt1003 = 100;
			anInt995 = is[3];
			anInt996 = is[4];
			anInt997 = is[5];
			if (anInt1004 != anInt1007) {
				aBoolean1008 = false;
				anInt1013 = 0;
			}
			if (anInt1004 == anInt1007 && anInt1013 == 0)
				method185(anInt1007, anInt995, anInt996, anIntArray1009, anIntArray1010, false);
		}
		if (i == 254) {
			int i_7_ = 1;
			for (int i_8_ = 0; i_8_ < 24; i_8_++)
				anIntArray1009[i_8_] = is[i_7_++];
			anInt1004 = is[i_7_++] & 0xff;
			anInt1005 = is[i_7_++] & 0xff;
			anInt1003 = 100;
			anInt995 = is[i_7_++];
			anInt996 = is[i_7_++];
			anInt997 = is[i_7_++];
			anIntArray1010[0] = is[i_7_++];
			anIntArray1010[1] = is[i_7_++];
			aBoolean1008 = false;
			anInt998 = -1;
			anInt1013 = 0;
		}
		if (i == 253 && (is[1] != anInt1020 || is[2] != anInt1021 || is[3] != anInt1022 || anInt1022 != anInt1007)) {
			aBoolean1000 = true;
			anInt1023 = 0;
			anInt1013 = 0;
			anInt998 = -1;
			anInt1020 = is[1];
			anInt1021 = is[2];
			anInt1022 = is[3];
			method183();
			if (anInt1020 == 50) {
				anIntArray1010[0]--;
				if (anIntArray1010[0] < 0)
					anIntArray1010[0] = 0;
			} else if (anInt1020 == 51) {
				anIntArray1010[1]--;
				if (anIntArray1010[1] < 0)
					anIntArray1010[1] = 0;
			} else {
				anIntArray1009[anInt1020]--;
				if (anIntArray1009[anInt1020] == 50 || anIntArray1009[anInt1020] < 0)
					anIntArray1009[anInt1020] = 0;
			}
		}
	}

	public void onLoop() {
		anInt1006++;
		if (anApplet_Sub1_Sub1_Sub1_966.mouseDown == 2 && !aBoolean999) {
			if (anInt993 == 0) {
				anInt993 = anApplet_Sub1_Sub1_Sub1_966.mouseDown;
				anInt991 = anApplet_Sub1_Sub1_Sub1_966.mouseX;
				anInt992 = anApplet_Sub1_Sub1_Sub1_966.mouseY;
			}
			int i = anApplet_Sub1_Sub1_Sub1_966.mouseX - anInt991;
			int i_9_ = anApplet_Sub1_Sub1_Sub1_966.mouseY - anInt992;
			anInt989 = anInt989 + i * 2 + 1024 & 0x3ff;
			anInt990 = anInt990 - i_9_ * 2 + 1024 & 0x3ff;
			if (anInt990 > 256 && anInt990 < 600)
				anInt990 = 600;
			if (anInt990 > 960 || anInt990 < 256)
				anInt990 = 960;
			anInt991 = anApplet_Sub1_Sub1_Sub1_966.mouseX;
			anInt992 = anApplet_Sub1_Sub1_Sub1_966.mouseY;
		} else
			anInt993 = 0;
		if (anApplet_Sub1_Sub1_Sub1_966.mouseClick == 1)
			anInt994 = 1;
		if (aBoolean1000) {
			anInt1023++;
			if (anInt1023 >= anInt1024) {
				if (anInt1021 != 100) {
					if (anIntArray1009[anInt1021] == 0)
						anIntArray1009[anInt1021] = anInt1022 * 50 + 1;
					else if (anIntArray1009[anInt1021] / 50 == anInt1022)
						anIntArray1009[anInt1021]++;
					else
						anIntArray1009[anInt1021] = anInt1022 * 50 + 1;
					if (aBoolean142)
						anApplet_Sub1_Sub1_Sub1_966.xmPlayer.method85(0, 0, 8000, 63);
				} else if (aBoolean142)
					anApplet_Sub1_Sub1_Sub1_966.xmPlayer.method85(0, 3, 8000, 63);
				aBoolean1000 = false;
				aBoolean1008 = true;
				anApplet_Sub1_Sub1_Sub1_966.aBoolean604 = false;
			}
		}
		anInt1003--;
		if (anInt1003 <= 0 && anInt1005 > 10) {
			anInt1003 = 50;
			anInt1005--;
		}
	}

	public void method116(int i, int i_10_, int i_11_) {
		method181();
		method182();
		if (aBoolean1000)
			method184();
		aClass10_968.setCamera(0, 100, 0, i, i_10_, 0, i_11_);
		aClass10_968.method367();
		aClass10_969.setCamera(0, 100, 0, i, i_10_, 0, i_11_);
		aClass10_969.method367();
		aClass10_970.setCamera(0, 100, 0, i, i_10_, 0, i_11_);
		aClass10_970.method367();
	}

	public void onDraw() {
		aClass6_967.method241(0, 0, 512, 384, 0, 4210752);
		if (aBoolean999) {
			method181();
			method182();
			if (aBoolean1000)
				method184();
			int i = 1600;
			int i_12_;
			if (anInt1007 == 0)
				i_12_ = 0;
			else
				i_12_ = 512;
			aClass10_969.method357(anApplet_Sub1_Sub1_Sub1_966.mouseX, anApplet_Sub1_Sub1_Sub1_966.mouseY);
			aClass10_970.method357(anApplet_Sub1_Sub1_Sub1_966.mouseX, anApplet_Sub1_Sub1_Sub1_966.mouseY);
			aClass10_968.setCamera(0, 100, 0, 768, i_12_, 0, i);
			aClass10_968.method367();
			aClass10_969.setCamera(0, 100, 0, 768, i_12_, 0, i);
			aClass10_969.method367();
			aClass10_970.setCamera(0, 100, 0, 768, i_12_, 0, i);
			aClass10_970.method367();
		} else {
			int i = 1300;
			aClass10_969.method357(anApplet_Sub1_Sub1_Sub1_966.mouseX, anApplet_Sub1_Sub1_Sub1_966.mouseY);
			aClass10_970.method357(anApplet_Sub1_Sub1_Sub1_966.mouseX, anApplet_Sub1_Sub1_Sub1_966.mouseY);
			method116(anInt990, anInt989, i);
		}
		if (anInt995 > 0)
			aClass6_967.method255(8, 25, anInt971 + anInt995 - 1);
		if (anInt996 > 0)
			aClass6_967.method255(33, 25, anInt971 + anInt996 + 9);
		if (aBoolean1000 || aBoolean1008)
			aClass6_967.method268((anApplet_Sub1_Sub1_Sub1_966.generalGameModules[0]), 7, 12, 1, 16777215);
		else if (anInt1004 == anInt1007) {
			if (anInt998 == -1) {
				String string = anApplet_Sub1_Sub1_Sub1_966.backgammonGameModules[1];
				if (anInt997 == 1)
					string = anApplet_Sub1_Sub1_Sub1_966.backgammonGameModules[2];
				if (anInt997 == 2)
					string = anApplet_Sub1_Sub1_Sub1_966.backgammonGameModules[3];
				if (anInt997 == 3)
					string = anApplet_Sub1_Sub1_Sub1_966.backgammonGameModules[4];
				aClass6_967.method268((anApplet_Sub1_Sub1_Sub1_966.generalGameModules[1] + " - " + anApplet_Sub1_Sub1_Sub1_966.backgammonGameModules[0] + string + " " + anApplet_Sub1_Sub1_Sub1_966.backgammonGameModules[5]), 7, 12, 1, 16777215);
			} else
				aClass6_967.method268(((anApplet_Sub1_Sub1_Sub1_966.generalGameModules[1]) + " - " + (anApplet_Sub1_Sub1_Sub1_966.backgammonGameModules[6])), 7, 12, 1, 16777215);
		} else
			aClass6_967.method268((anApplet_Sub1_Sub1_Sub1_966.generalGameModules[2]), 7, 12, 1, 16777215);
		aClass6_967.method268((anApplet_Sub1_Sub1_Sub1_966.generalGameModules[3] + anInt1005), 7, 23, 1, 16777215);
		int i = 460;
		int i_13_ = 4;
		anApplet_Sub1_Sub1_Sub1_966.aClass8_1094.method321(i, i_13_, 46, 28);
		aClass6_967.method267(anApplet_Sub1_Sub1_Sub1_966.generalGameModules[4], i + 22, i_13_ + 13, 1, 0);
		if (aBoolean999)
			aClass6_967.method267((anApplet_Sub1_Sub1_Sub1_966.generalGameModules[5]), i + 22, i_13_ + 23, 1, 0);
		else
			aClass6_967.method267((anApplet_Sub1_Sub1_Sub1_966.generalGameModules[6]), i + 22, i_13_ + 23, 1, 0);
		i = 460;
		i_13_ = 34;
		anApplet_Sub1_Sub1_Sub1_966.aClass8_1094.method321(i, i_13_, 46, 28);
		aClass6_967.method267(anApplet_Sub1_Sub1_Sub1_966.generalGameModules[7], i + 22, i_13_ + 18, 1, 0);
		i = 460;
		i_13_ = 64;
		anApplet_Sub1_Sub1_Sub1_966.aClass8_1094.method321(i, i_13_, 46, 28);
		if (anApplet_Sub1_Sub1_Sub1_966.aBoolean604)
			aClass6_967.method267((anApplet_Sub1_Sub1_Sub1_966.generalGameModules[8]), i + 22, i_13_ + 13, 1, 0);
		else
			aClass6_967.method267((anApplet_Sub1_Sub1_Sub1_966.generalGameModules[9]), i + 22, i_13_ + 13, 1, 0);
		aClass6_967.method267(anApplet_Sub1_Sub1_Sub1_966.generalGameModules[10], i + 22, i_13_ + 23, 1, 0);
		if (aBoolean1019 && anInt998 >= 0) {
			i = 456;
			i_13_ = 270;
			aClass6_967.method240(i, i_13_, 52, 34, 0, 128);
			aClass6_967.method242(i, i_13_, 46, 28, 16711680);
			aClass6_967.method267((anApplet_Sub1_Sub1_Sub1_966.backgammonGameModules[7]), i + 23, i_13_ + 14, 1, 16777215);
			aClass6_967.method267((anApplet_Sub1_Sub1_Sub1_966.backgammonGameModules[8]), i + 23, i_13_ + 24, 1, 16777215);
			if (anInt994 == 1 && anApplet_Sub1_Sub1_Sub1_966.mouseX > i && anApplet_Sub1_Sub1_Sub1_966.mouseY > i_13_ && anApplet_Sub1_Sub1_Sub1_966.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_966.mouseY < i_13_ + 29) {
				anInt994 = 0;
				aBoolean1000 = true;
				anInt1023 = 0;
				anInt1020 = anInt998;
				anInt1021 = 100;
				anInt1022 = anInt1007;
				method183();
				if (anInt1020 == 50) {
					anIntArray1010[0]--;
					if (anIntArray1010[0] < 0)
						anIntArray1010[0] = 0;
				} else if (anInt1020 == 51) {
					anIntArray1010[1]--;
					if (anIntArray1010[1] < 0)
						anIntArray1010[1] = 0;
				} else {
					anIntArray1009[anInt1020]--;
					if (anIntArray1009[anInt1020] == 50 || anIntArray1009[anInt1020] < 0)
						anIntArray1009[anInt1020] = 0;
				}
				anApplet_Sub1_Sub1_Sub1_966.aClass2_582.putPacketId(255);
				anApplet_Sub1_Sub1_Sub1_966.aClass2_582.p1(anInt1020);
				anApplet_Sub1_Sub1_Sub1_966.aClass2_582.p1(anInt1021);
				anApplet_Sub1_Sub1_Sub1_966.aClass2_582.flushNoException();
			}
		}
		if (aBoolean1001) {
			method179(anApplet_Sub1_Sub1_Sub1_966.generalGameModules[11], anApplet_Sub1_Sub1_Sub1_966.generalGameModules[12]);
			if (anApplet_Sub1_Sub1_Sub1_966.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_966.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_966.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_966.mouseY < 188 && anInt994 == 1) {
				anApplet_Sub1_Sub1_Sub1_966.resign();
				aBoolean1001 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_966.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_966.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_966.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_966.mouseY < 188 && anInt994 == 1)
				aBoolean1001 = false;
			anInt994 = 0;
		} else if (aBoolean1002) {
			if (anApplet_Sub1_Sub1_Sub1_966.aBoolean604)
				method179(anApplet_Sub1_Sub1_Sub1_966.generalGameModules[13], anApplet_Sub1_Sub1_Sub1_966.generalGameModules[14]);
			else
				method179(anApplet_Sub1_Sub1_Sub1_966.generalGameModules[15], anApplet_Sub1_Sub1_Sub1_966.generalGameModules[16]);
			if (anApplet_Sub1_Sub1_Sub1_966.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_966.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_966.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_966.mouseY < 188 && anInt994 == 1) {
				anApplet_Sub1_Sub1_Sub1_966.offerDraw();
				aBoolean1002 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_966.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_966.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_966.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_966.mouseY < 188 && anInt994 == 1)
				aBoolean1002 = false;
			anInt994 = 0;
		} else {
			i = 460;
			i_13_ = 4;
			if (anInt994 == 1 && anApplet_Sub1_Sub1_Sub1_966.mouseX > i && anApplet_Sub1_Sub1_Sub1_966.mouseY > i_13_ && anApplet_Sub1_Sub1_Sub1_966.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_966.mouseY < i_13_ + 29) {
				aBoolean999 = !aBoolean999;
				anInt994 = 0;
			}
			i = 460;
			i_13_ = 34;
			if (anInt994 == 1 && anApplet_Sub1_Sub1_Sub1_966.mouseX > i && anApplet_Sub1_Sub1_Sub1_966.mouseY > i_13_ && anApplet_Sub1_Sub1_Sub1_966.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_966.mouseY < i_13_ + 29) {
				aBoolean1001 = true;
				anInt994 = 0;
			}
			i = 460;
			i_13_ = 64;
			if (anInt994 == 1 && anApplet_Sub1_Sub1_Sub1_966.mouseX > i && anApplet_Sub1_Sub1_Sub1_966.mouseY > i_13_ && anApplet_Sub1_Sub1_Sub1_966.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_966.mouseY < i_13_ + 29) {
				aBoolean1002 = true;
				anInt994 = 0;
			}
			if ((aBoolean999 || !aBoolean999) && anInt994 == 1 && !aBoolean1000 && !aBoolean1008) {
				object3d class7;
				if (anInt998 == -1) {
					class7 = aClass10_969.method361();
					if (class7 == null)
						class7 = aClass10_970.method361();
				} else {
					class7 = aClass10_970.method361();
					if (class7 == null)
						class7 = aClass10_969.method361();
				}
				if (anInt998 == -1) {
					int i_14_;
					object3d[] class7s;
					int[] is;
					if (anInt1007 == 0) {
						i_14_ = anInt979;
						class7s = aClass7Array978;
						is = anIntArray980;
					} else {
						i_14_ = anInt982;
						class7s = aClass7Array981;
						is = anIntArray983;
					}
					for (int i_15_ = 0; i_15_ < i_14_; i_15_++) {
						if (class7 == class7s[i_15_]) {
							int i_16_ = is[i_15_];
							int i_17_ = -1;
							for (int i_18_ = 0; i_18_ < anInt1013; i_18_++) {
								if (anIntArray1014[i_18_] > 0 && anIntArray1016[i_18_] == i_16_) {
									i_17_ = i_18_;
									break;
								}
							}
							if (i_17_ >= 0)
								anInt998 = i_16_;
						}
					}
				} else if (anInt998 >= 0) {
					for (int i_19_ = 0; i_19_ < anInt987; i_19_++) {
						if (class7 == aClass7Array986[i_19_]) {
							int i_20_ = anIntArray988[i_19_];
							aBoolean1000 = true;
							anInt1023 = 0;
							anInt1020 = anIntArray1016[i_20_];
							anInt1021 = anIntArray1017[i_20_];
							anInt1022 = anInt1007;
							method183();
							if (anInt1020 == 50) {
								anIntArray1010[0]--;
								if (anIntArray1010[0] < 0)
									anIntArray1010[0] = 0;
							} else if (anInt1020 == 51) {
								anIntArray1010[1]--;
								if (anIntArray1010[1] < 0)
									anIntArray1010[1] = 0;
							} else {
								anIntArray1009[anInt1020]--;
								if (anIntArray1009[anInt1020] == 50 || anIntArray1009[anInt1020] < 0)
									anIntArray1009[anInt1020] = 0;
							}
							anApplet_Sub1_Sub1_Sub1_966.aClass2_582.putPacketId(255);
							anApplet_Sub1_Sub1_Sub1_966.aClass2_582.p1(anInt1020);
							anApplet_Sub1_Sub1_Sub1_966.aClass2_582.p1(anInt1021);
							anApplet_Sub1_Sub1_Sub1_966.aClass2_582.flushNoException();
							break;
						}
						if (i_19_ == anInt987 - 1)
							anInt998 = -1;
					}
					if (anInt987 == 0)
						anInt998 = -1;
				}
			}
			anInt994 = 0;
		}
	}

	public void method179(String string, String string_21_) {
		int i = 160;
		int i_22_ = 80;
		aClass6_967.method240(256 - i / 2 + 10, 150 - i_22_ / 2 + 10, i, i_22_, 0, 128);
		aClass6_967.method242(256 - i / 2, 150 - i_22_ / 2, i, i_22_, pixmap.method247(96, 96, 121));
		aClass6_967.method267(string, 256, 130, 4, 16777215);
		aClass6_967.method267(string_21_, 256, 150, 4, 16777215);
		int i_23_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_966.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_966.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_966.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_966.mouseY < 188)
			i_23_ = 16711680;
		aClass6_967.method267(anApplet_Sub1_Sub1_Sub1_966.generalGameModules[17], 226, 178, 4, i_23_);
		i_23_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_966.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_966.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_966.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_966.mouseY < 188)
			i_23_ = 16711680;
		aClass6_967.method267(anApplet_Sub1_Sub1_Sub1_966.generalGameModules[18], 286, 178, 4, i_23_);
	}

	public void method180() {
		/* empty */
	}

	public void method181() {
		aClass10_970.clearObjects();
		aClass10_970.addObject(aClass7_975);
		aClass10_969.clearObjects();
		anInt979 = 0;
		anInt982 = 0;
		for (int i = 0; i < 24; i++) {
			int i_24_ = 0;
			int i_25_ = 0;
			int i_26_ = 0;
			int i_27_ = 0;
			if (i < 6) {
				i_24_ = 96 + (5 - i) * 64;
				i_25_ = -352;
				i_26_ = anInt965;
			} else if (i < 12) {
				i_24_ = -96 - (i - 6) * 64;
				i_25_ = -352;
				i_26_ = anInt965;
			} else if (i < 18) {
				i_24_ = -96 - (17 - i) * 64;
				i_25_ = 352;
				i_26_ = -anInt965;
			} else if (i < 24) {
				i_24_ = 96 + (i - 18) * 64;
				i_25_ = 352;
				i_26_ = -anInt965;
			}
			for (int i_28_ = 0; i_28_ < anIntArray1009[i] % 50; i_28_++) {
				object3d class7;
				if (anIntArray1009[i] < 50) {
					anIntArray980[anInt979] = i;
					class7 = aClass7Array978[anInt979++];
				} else {
					anIntArray983[anInt982] = i;
					class7 = aClass7Array981[anInt982++];
				}
				class7.requestSetTranslate(i_24_, -(i_27_ / 6) * 32, i_25_ + i_27_ % 6 * i_26_);
				if (i_27_ < 6)
					aClass10_969.addObject(class7);
				else
					aClass10_970.addObject(class7);
				i_27_++;
			}
		}
		for (int i = 0; i < anIntArray1010[0]; i++) {
			anIntArray980[anInt979] = 50;
			object3d class7 = aClass7Array978[anInt979++];
			aClass10_970.addObject(class7);
			class7.requestSetTranslate(0, -32, -i * 64 - 32);
		}
		for (int i = 0; i < anIntArray1010[1]; i++) {
			anIntArray983[anInt982] = 51;
			object3d class7 = aClass7Array981[anInt982++];
			aClass10_970.addObject(class7);
			class7.requestSetTranslate(0, -32, i * 64 + 32);
		}
	}

	public void method182() {
		anInt985 = 0;
		anInt987 = 0;
		aBoolean1019 = false;
		if (!aBoolean1000 && !aBoolean1008 && anInt1004 == anInt1007) {
			if (anInt998 == -1 && anInt1006 % 16 < 8) {
				for (int i = 0; i < anInt1013; i++) {
					if (anIntArray1014[i] > 0) {
						int i_29_ = anIntArray1016[i];
						int i_30_ = 0;
						int i_31_ = 0;
						int i_32_ = 0;
						if (i_29_ < 6) {
							i_30_ = 96 + (5 - i_29_) * 64;
							i_31_ = -352;
							int i_33_ = anIntArray1009[i_29_] % 50 - 1;
							i_31_ += anInt965 * (i_33_ % 6);
							i_32_ = -32 - i_33_ / 6 * 32;
						} else if (i_29_ < 12) {
							i_30_ = -96 - (i_29_ - 6) * 64;
							i_31_ = -352;
							int i_34_ = anIntArray1009[i_29_] % 50 - 1;
							i_31_ += anInt965 * (i_34_ % 6);
							i_32_ = -32 - i_34_ / 6 * 32;
						} else if (i_29_ < 18) {
							i_30_ = -96 - (17 - i_29_) * 64;
							i_31_ = 352;
							int i_35_ = anIntArray1009[i_29_] % 50 - 1;
							i_31_ -= anInt965 * (i_35_ % 6);
							i_32_ = -32 - i_35_ / 6 * 32;
						} else if (i_29_ < 24) {
							i_30_ = 96 + (i_29_ - 18) * 64;
							i_31_ = 352;
							int i_36_ = anIntArray1009[i_29_] % 50 - 1;
							i_31_ -= anInt965 * (i_36_ % 6);
							i_32_ = -32 - i_36_ / 6 * 32;
						} else if (i_29_ == 50) {
							i_30_ = 0;
							i_31_ = -32 - (anIntArray1010[0] - 1) * 64;
							i_32_ = -64;
						} else if (i_29_ == 51) {
							i_30_ = 0;
							i_31_ = 32 + (anIntArray1010[1] - 1) * 64;
							i_32_ = -64;
						}
						object3d class7 = aClass7Array984[anInt985++];
						aClass10_970.addObject(class7);
						class7.requestSetTranslate(i_30_, i_32_, i_31_);
						if (i_29_ < 12 && anInt1007 == 0 || i_29_ >= 12 && anInt1007 == 1)
							class7.requestSetRotate(0, 0, 0);
						else
							class7.requestSetRotate(0, 128, 0);
					}
				}
			}
			if (anInt998 >= 0) {
				for (int i = 0; i < anInt1013; i++) {
					if (anIntArray1016[i] == anInt998 && anIntArray1014[i] > 0) {
						int i_37_ = anIntArray1017[i];
						int i_38_ = 0;
						int i_39_ = 0;
						int i_40_ = 0;
						if (i_37_ != 100) {
							if (i_37_ < 6) {
								i_38_ = 96 + (5 - i_37_) * 64;
								i_39_ = -352;
								int i_41_ = anIntArray1009[i_37_] % 50;
								i_39_ += anInt965 * (i_41_ % 6);
								i_40_ = -(i_41_ / 6) * 32;
							} else if (i_37_ < 12) {
								i_38_ = -96 - (i_37_ - 6) * 64;
								i_39_ = -352;
								int i_42_ = anIntArray1009[i_37_] % 50;
								i_39_ += anInt965 * (i_42_ % 6);
								i_40_ = -(i_42_ / 6) * 32;
							} else if (i_37_ < 18) {
								i_38_ = -96 - (17 - i_37_) * 64;
								i_39_ = 352;
								int i_43_ = anIntArray1009[i_37_] % 50;
								i_39_ -= anInt965 * (i_43_ % 6);
								i_40_ = -(i_43_ / 6) * 32;
							} else if (i_37_ < 24) {
								i_38_ = 96 + (i_37_ - 18) * 64;
								i_39_ = 352;
								int i_44_ = anIntArray1009[i_37_] % 50;
								i_39_ -= anInt965 * (i_44_ % 6);
								i_40_ = -(i_44_ / 6) * 32;
							}
							anIntArray988[anInt987] = i;
							object3d class7 = aClass7Array986[anInt987++];
							if (i_40_ <= -32)
								aClass10_970.addObject(class7);
							else
								aClass10_969.addObject(class7);
							class7.requestSetTranslate(i_38_, i_40_, i_39_);
							if (anInt1006 % 16 >= 8) {
								for (int i_45_ = 0; i_45_ < class7.polygonCount; i_45_++)
									class7.method283(i_45_, -16385);
							} else {
								for (int i_46_ = 0; i_46_ < class7.polygonCount; i_46_++)
									class7.method283(i_46_, -31745);
							}
						} else
							aBoolean1019 = true;
					}
				}
			}
		}
	}

	public void method183() {
		int i = anInt1020;
		int i_47_ = 0;
		int i_48_ = 0;
		int i_49_ = 0;
		if (i < 6) {
			i_47_ = 96 + (5 - i) * 64;
			i_48_ = -352;
			int i_50_ = anIntArray1009[i] % 50;
			i_48_ += anInt965 * (i_50_ % 6);
			i_49_ = -(i_50_ / 6) * 32;
		} else if (i < 12) {
			i_47_ = -96 - (i - 6) * 64;
			i_48_ = -352;
			int i_51_ = anIntArray1009[i] % 50;
			i_48_ += anInt965 * (i_51_ % 6);
			i_49_ = -(i_51_ / 6) * 32;
		} else if (i < 18) {
			i_47_ = -96 - (17 - i) * 64;
			i_48_ = 352;
			int i_52_ = anIntArray1009[i] % 50;
			i_48_ -= anInt965 * (i_52_ % 6);
			i_49_ = -(i_52_ / 6) * 32;
		} else if (i < 24) {
			i_47_ = 96 + (i - 18) * 64;
			i_48_ = 352;
			int i_53_ = anIntArray1009[i] % 50;
			i_48_ -= anInt965 * (i_53_ % 6);
			i_49_ = -(i_53_ / 6) * 32;
		} else if (i == 50) {
			i_47_ = 0;
			i_48_ = -32 - (anIntArray1010[0] - 1) * 64;
			i_49_ = -32;
		} else if (i == 51) {
			i_47_ = 0;
			i_48_ = 32 + (anIntArray1010[1] - 1) * 64;
			i_49_ = -32;
		}
		i = anInt1021;
		int i_54_ = 0;
		int i_55_ = 0;
		int i_57_ = 0;
		if (i == 100) {
			i_54_ = i_47_;
			i_55_ = i_48_;
			i_57_ = -600;
		} else if (i < 6) {
			i_54_ = 96 + (5 - i) * 64;
			i_55_ = -352;
			int i_58_ = anIntArray1009[i] % 50;
			i_55_ += anInt965 * (i_58_ % 6);
			i_57_ = -(i_58_ / 6) * 32;
		} else if (i < 12) {
			i_54_ = -96 - (i - 6) * 64;
			i_55_ = -352;
			int i_59_ = anIntArray1009[i] % 50;
			i_55_ += anInt965 * (i_59_ % 6);
			i_57_ = -(i_59_ / 6) * 32;
		} else if (i < 18) {
			i_54_ = -96 - (17 - i) * 64;
			i_55_ = 352;
			int i_60_ = anIntArray1009[i] % 50;
			i_55_ -= anInt965 * (i_60_ % 6);
			i_57_ = -(i_60_ / 6) * 32;
		} else if (i < 24) {
			i_54_ = 96 + (i - 18) * 64;
			i_55_ = 352;
			int i_61_ = anIntArray1009[i] % 50;
			i_55_ -= anInt965 * (i_61_ % 6);
			i_57_ = -(i_61_ / 6) * 32;
		}
		int i_62_ = i_54_ - i_47_;
		if (i_62_ < 0)
			i_62_ = -i_62_;
		int i_63_ = i_55_ - i_48_;
		if (i_63_ < 0)
			i_63_ = -i_63_;
		int i_64_ = i_57_ - i_49_;
		if (i_64_ < 0)
			i_64_ = -i_64_;
		if (i_63_ > i_62_)
			i_62_ = i_63_;
		if (i_64_ > i_62_)
			i_62_ = i_64_;
		anInt1024 = i_62_ / 6;
	}

	public void method184() {
		int i = anInt1020;
		int i_65_ = 0;
		int i_66_ = 0;
		int i_67_ = 0;
		if (i < 6) {
			i_65_ = 96 + (5 - i) * 64;
			i_66_ = -352;
			int i_68_ = anIntArray1009[i] % 50;
			i_66_ += anInt965 * (i_68_ % 6);
			i_67_ = -(i_68_ / 6) * 32;
		} else if (i < 12) {
			i_65_ = -96 - (i - 6) * 64;
			i_66_ = -352;
			int i_69_ = anIntArray1009[i] % 50;
			i_66_ += anInt965 * (i_69_ % 6);
			i_67_ = -(i_69_ / 6) * 32;
		} else if (i < 18) {
			i_65_ = -96 - (17 - i) * 64;
			i_66_ = 352;
			int i_70_ = anIntArray1009[i] % 50;
			i_66_ -= anInt965 * (i_70_ % 6);
			i_67_ = -(i_70_ / 6) * 32;
		} else if (i < 24) {
			i_65_ = 96 + (i - 18) * 64;
			i_66_ = 352;
			int i_71_ = anIntArray1009[i] % 50;
			i_66_ -= anInt965 * (i_71_ % 6);
			i_67_ = -(i_71_ / 6) * 32;
		} else if (i == 50) {
			i_65_ = 0;
			i_66_ = -32 - (anIntArray1010[0] - 1) * 64;
			i_67_ = -32;
		} else if (i == 51) {
			i_65_ = 0;
			i_66_ = 32 + (anIntArray1010[1] - 1) * 64;
			i_67_ = -32;
		}
		i = anInt1021;
		int i_72_ = 0;
		int i_73_ = 0;
		int i_75_ = 0;
		if (i == 100) {
			i_72_ = i_65_;
			i_73_ = i_66_;
			i_75_ = -600;
		} else if (i < 6) {
			i_72_ = 96 + (5 - i) * 64;
			i_73_ = -352;
			int i_76_ = anIntArray1009[i] % 50;
			i_73_ += anInt965 * (i_76_ % 6);
			i_75_ = -(i_76_ / 6) * 32;
		} else if (i < 12) {
			i_72_ = -96 - (i - 6) * 64;
			i_73_ = -352;
			int i_77_ = anIntArray1009[i] % 50;
			i_73_ += anInt965 * (i_77_ % 6);
			i_75_ = -(i_77_ / 6) * 32;
		} else if (i < 18) {
			i_72_ = -96 - (17 - i) * 64;
			i_73_ = 352;
			int i_78_ = anIntArray1009[i] % 50;
			i_73_ -= anInt965 * (i_78_ % 6);
			i_75_ = -(i_78_ / 6) * 32;
		} else if (i < 24) {
			i_72_ = 96 + (i - 18) * 64;
			i_73_ = 352;
			int i_79_ = anIntArray1009[i] % 50;
			i_73_ -= anInt965 * (i_79_ % 6);
			i_75_ = -(i_79_ / 6) * 32;
		}
		int i_80_ = ((i_65_ * (anInt1024 - anInt1023) + i_72_ * anInt1023) / anInt1024);
		int i_81_ = ((i_66_ * (anInt1024 - anInt1023) + i_73_ * anInt1023) / anInt1024);
		int i_82_ = ((i_67_ * (anInt1024 - anInt1023) + i_75_ * anInt1023) / anInt1024);
		int i_83_ = (int) (Math.sin((double) anInt1023 * 3.142 / (double) anInt1024) * (double) anInt1024);
		if (i == 100)
			i_83_ = 0;
		object3d class7;
		if (anInt1022 == 0)
			class7 = aClass7Array978[anInt979];
		else
			class7 = aClass7Array981[anInt982];
		aClass10_970.addObject(class7);
		class7.requestSetTranslate(i_80_, i_82_ - i_83_, i_81_);
	}

	public boolean method185(int i, int i_84_, int i_85_, int[] is, int[] is_86_, boolean bool) {
		if (!bool)
			anInt1013 = 0;
		if (i_85_ == i_84_)
			i_85_ = 0;
		if (is_86_[i] > 0) {
			for (int i_87_ = 0; i_87_ < 2; i_87_++) {
				int i_88_;
				if (i_87_ == 0)
					i_88_ = i_84_;
				else
					i_88_ = i_85_;
				if (i_88_ > 0) {
					if (i == 0) {
						int i_89_ = is[24 - i_88_];
						if (i_89_ <= 1 || i_89_ == 51 || i_89_ / 50 == i) {
							if (bool)
								return true;
							anIntArray1014[anInt1013] = 2;
							anIntArray1015[anInt1013] = i_87_;
							anIntArray1016[anInt1013] = 50;
							anIntArray1017[anInt1013] = 24 - i_88_;
							anIntArray1018[anInt1013++] = 1;
						}
					}
					if (i == 1) {
						int i_90_ = is[i_88_ - 1];
						if (i_90_ <= 1 || i_90_ == 51 || i_90_ / 50 == i) {
							if (bool)
								return true;
							anIntArray1014[anInt1013] = 2;
							anIntArray1015[anInt1013] = i_87_;
							anIntArray1016[anInt1013] = 51;
							anIntArray1017[anInt1013] = i_88_ - 1;
							anIntArray1018[anInt1013++] = 1;
						}
					}
				}
			}
		}
		if (is_86_[i] == 0) {
			for (int i_91_ = 0; i_91_ < 2; i_91_++) {
				int i_92_;
				if (i_91_ == 0)
					i_92_ = i_84_;
				else
					i_92_ = i_85_;
				if (i_92_ > 0) {
					int i_93_ = i_92_;
					if (i == 0)
						i_93_ = -i_93_;
					for (int i_94_ = 0; i_94_ < 24; i_94_++) {
						if (is[i_94_] != 0 && is[i_94_] / 50 == i && i_94_ + i_93_ >= 0 && i_94_ + i_93_ < 24) {
							int i_95_ = is[i_94_ + i_93_];
							if (i_95_ <= 1 || i_95_ == 51 || i_95_ / 50 == i) {
								if (bool)
									return true;
								anIntArray1014[anInt1013] = 1;
								anIntArray1015[anInt1013] = i_91_;
								anIntArray1016[anInt1013] = i_94_;
								anIntArray1017[anInt1013] = i_94_ + i_93_;
								anIntArray1018[anInt1013++] = 1;
							}
						}
					}
				}
			}
		}
		boolean bool_96_ = true;
		int i_97_ = anInt1013;
		for (int i_98_ = 0; i_98_ < 24; i_98_++) {
			if (is[i_98_] != 0 && is[i_98_] / 50 == i && (i_98_ > 5 && i == 0 || i_98_ < 18 && i == 1))
				bool_96_ = false;
		}
		if (bool_96_ && is_86_[i] == 0) {
			for (int i_99_ = 0; i_99_ < 2; i_99_++) {
				int i_100_;
				if (i_99_ == 0)
					i_100_ = i_84_;
				else
					i_100_ = i_85_;
				if (i_100_ > 0) {
					int i_101_ = i_100_ - 1;
					if (i == 1)
						i_101_ = 23 - i_101_;
					if (is[i_101_] > 0 && is[i_101_] / 50 == i) {
						if (bool)
							return true;
						anIntArray1014[anInt1013] = 3;
						anIntArray1015[anInt1013] = i_99_;
						anIntArray1016[anInt1013] = i_101_;
						anIntArray1017[anInt1013] = 100;
						anIntArray1018[anInt1013++] = 1;
					} else if (i_97_ == 0) {
						for (int i_102_ = 5; i_102_ >= 0; i_102_--) {
							i_101_ = i_102_;
							if (i == 1)
								i_101_ = 23 - i_101_;
							if (is[i_101_] > 0 && is[i_101_] / 50 == i) {
								if (bool)
									return true;
								anIntArray1014[anInt1013] = 3;
								anIntArray1015[anInt1013] = i_99_;
								anIntArray1016[anInt1013] = i_101_;
								anIntArray1017[anInt1013] = 100;
								anIntArray1018[anInt1013++] = 1;
								break;
							}
						}
					}
				}
			}
		}
		if (bool || i_84_ <= 0 || i_85_ <= 0)
			return false;
		int i_103_ = 0;
		for (int i_104_ = 0; i_104_ < anInt1013; i_104_++) {
			for (int i_105_ = 0; i_105_ < 24; i_105_++)
				anIntArray1011[i_105_] = is[i_105_];
			for (int i_106_ = 0; i_106_ < 2; i_106_++)
				anIntArray1012[i_106_] = is_86_[i_106_];
			int i_107_ = anIntArray1016[i_104_];
			int i_108_ = anIntArray1017[i_104_];
			int i_109_ = anIntArray1014[i_104_];
			if (i_109_ == 1) {
				anIntArray1011[i_107_]--;
				if (anIntArray1011[i_107_] == 50 || anIntArray1011[i_107_] < 0)
					anIntArray1011[i_107_] = 0;
				if (anIntArray1011[i_108_] / 50 == i)
					anIntArray1011[i_108_]++;
				else
					anIntArray1011[i_108_] = i * 50 + 1;
			}
			if (i_109_ == 2) {
				anIntArray1012[i]--;
				if (anIntArray1011[i_108_] / 50 == i)
					anIntArray1011[i_108_]++;
				else
					anIntArray1011[i_108_] = i * 50 + 1;
			}
			if (i_109_ == 3) {
				anIntArray1011[i_107_]--;
				if (anIntArray1011[i_107_] == 50 || anIntArray1011[i_107_] < 0)
					anIntArray1011[i_107_] = 0;
			}
			int i_110_ = i_84_;
			int i_111_ = i_85_;
			if (anIntArray1015[i_104_] == 0)
				i_110_ = 0;
			else
				i_111_ = 0;
			if (method185(i, i_110_, i_111_, anIntArray1011, anIntArray1012, true)) {
				anIntArray1018[i_104_] = 2;
				i_103_++;
			}
		}
		if (i_103_ > 0) {
			for (int i_112_ = 0; i_112_ < anInt1013; i_112_++) {
				if (anIntArray1018[i_112_] == 1)
					anIntArray1014[i_112_] = 0;
			}
		}
		if (i_103_ == 0) {
			int i_113_;
			if (i_84_ < i_85_)
				i_113_ = 0;
			else
				i_113_ = 1;
			for (int i_114_ = 0; i_114_ < anInt1013; i_114_++) {
				if (anIntArray1015[i_114_] != i_113_) {
					i_103_ = 1;
					break;
				}
			}
			if (i_103_ == 1) {
				for (int i_115_ = 0; i_115_ < anInt1013; i_115_++) {
					if (anIntArray1015[i_115_] == i_113_)
						anIntArray1014[i_115_] = 0;
				}
			}
		}
		return false;
	}
}
