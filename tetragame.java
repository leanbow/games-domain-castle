import jagex.object3d;
import jagex.pixmap;
import jagex.tools;
import jagex.world3d;

public class tetragame extends module {
	gameclient anApplet_Sub1_Sub1_Sub1_771;
	pixmap aClass6_772;
	world3d aClass10_773;
	world3d aClass10_774;
	world3d aClass10_775;
	int anInt776;
	object3d aClass7_777;
	object3d aClass7_778;
	object3d aClass7_779;
	object3d aClass7_780;
	object3d[] aClass7Array781 = new object3d[32];
	int anInt782;
	object3d[] aClass7Array783 = new object3d[32];
	int anInt784;
	int anInt785 = 512;
	int anInt786 = 896;
	int anInt787;
	int anInt788;
	int anInt789;
	int mouseClicked;
	boolean aBoolean791 = false;
	boolean packetSetPosition = false;
	int anInt793;
	int anInt794;
	int anInt795;
	int anInt796;
	int anInt797 = 128;
	boolean aBoolean798 = false;
	int anInt799;
	boolean aBoolean800 = false;
	boolean aBoolean801 = false;
	int opponentPlayerIndex;
	int timeLeft = 60;
	int anInt804;
	int selfPlayerIndex;
	int anInt806 = 100;
	boolean aBoolean807 = true;
	int[][] anIntArrayArray808 = new int[7][6];
	int[][] anIntArrayArray809 = new int[7][6];
	int[][] anIntArrayArray810 = new int[7][6];
	int anInt811;

	public void onInit(gameclient applet_sub1_sub1_sub1, int i) {
		anApplet_Sub1_Sub1_Sub1_771 = applet_sub1_sub1_sub1;
		anInt776 = i;
		aClass6_772 = applet_sub1_sub1_sub1.pixelMap;
		aClass10_773 = applet_sub1_sub1_sub1.aClass10_1086;
		aClass10_774 = applet_sub1_sub1_sub1.aClass10_1084;
		aClass10_775 = applet_sub1_sub1_sub1.aClass10_1085;
		aClass6_772.method249(applet_sub1_sub1_sub1.aByteArray1083, tools.findFile("2dtetra.tga", applet_sub1_sub1_sub1.aByteArray1083), i, true, 3, false);
		aClass7_778 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("redbit.ob2", applet_sub1_sub1_sub1.aByteArray1082));
		aClass7_779 = new object3d(applet_sub1_sub1_sub1.aByteArray1082, tools.findFile("bluebit.ob2", applet_sub1_sub1_sub1.aByteArray1082));
		aClass7_778.requestScale(224, 224, 224);
		aClass7_779.requestScale(224, 224, 224);
		aClass7_777 = new object3d(56, 42);
		for (int i_0_ = 0; i_0_ <= 6; i_0_++) {
			for (int i_1_ = 0; i_1_ <= 7; i_1_++)
				aClass7_777.addVertex(i_1_ * 96 - 336, 0, i_0_ * 96 - 288);
		}
		for (int i_2_ = 0; i_2_ < 6; i_2_++) {
			for (int i_3_ = 0; i_3_ < 7; i_3_++) {
				int[] is = new int[4];
				is[0] = i_2_ * 8 + i_3_;
				is[1] = i_2_ * 8 + i_3_ + 1;
				is[2] = i_2_ * 8 + i_3_ + 9;
				is[3] = i_2_ * 8 + i_3_ + 8;
				aClass7_777.addPolygon(4, is, 12, 12);
			}
		}
		aClass7_777.method279(50);
		aClass7_777.method280(true, 52, 32, -50, -10, -50);
		aClass7_778.method280(true, 52, 32, -50, -10, -50);
		aClass7_779.method280(true, 52, 32, -50, -10, -50);
		aClass7_780 = aClass7_777.copy();
		aClass7_777.requestAdjustTranslate(0, -32, 0);
		aClass7_780.requestAdjustTranslate(0, 4, 0);
		for (int i_4_ = 0; i_4_ < 32; i_4_++) {
			aClass7Array781[i_4_] = aClass7_778.copy();
			aClass7Array783[i_4_] = aClass7_779.copy();
		}
	}

	public void method113(int i) {
		selfPlayerIndex = i;
		anInt785 = 512;
		aClass10_773.clearObjects();
		aClass10_774.clearObjects();
		aClass10_775.clearObjects();
		aClass10_773.method362(256, 152, 256, 152, 512, 9);
		aClass10_773.maxRenderZ = 3000;
		aClass10_773.anInt464 = 3000;
		aClass10_773.anInt465 = 20;
		aClass10_773.anInt466 = 5000;
		aClass10_774.method385(aClass10_773);
		aClass10_775.method385(aClass10_773);
		aClass10_773.clearObjects();
		aClass10_774.clearObjects();
		aClass10_775.clearObjects();
		aClass10_773.addObject(aClass7_780);
		aClass10_775.addObject(aClass7_777);
		for (int i_5_ = 0; i_5_ < 7; i_5_++) {
			for (int i_6_ = 0; i_6_ < 6; i_6_++) {
				anIntArrayArray808[i_5_][i_6_] = 0;
				anIntArrayArray809[i_5_][i_6_] = 0;
			}
		}
		packetSetPosition = false;
		aBoolean807 = true;
		aBoolean800 = false;
		aBoolean801 = false;
		anInt806 = 100;
	}

	public void method121() {
		aClass10_773.clearObjects();
		aClass10_774.clearObjects();
		aClass10_775.clearObjects();
		aClass10_773.addObject(aClass7_780);
		aClass10_775.addObject(aClass7_777);
	}

	public void method117() {
		anIntArrayArray808[3][5] = 1;
		anIntArrayArray808[3][4] = 2;
		anIntArrayArray808[3][3] = 1;
		anIntArrayArray808[4][5] = 2;
		anIntArrayArray808[4][4] = 1;
		anIntArrayArray808[0][5] = 2;
	}

	public boolean method114() {
		if (packetSetPosition)
			return false;
		if (anInt811 > 0)
			return false;
		return true;
	}

	public void method103(int i, int i_7_, byte[] is) {
		if (i == 255) {
			opponentPlayerIndex = is[1] & 0xff;
			timeLeft = is[2] & 0xff;
			anInt806 = 100;
			System.out.println("packet 255 = " + opponentPlayerIndex+":"+selfPlayerIndex);
			if (opponentPlayerIndex != selfPlayerIndex)
				aBoolean807 = false;
		}
		if (i == 254) {
			int i_8_ = 1;
			for (int i_9_ = 0; i_9_ < 7; i_9_++) {
				for (int i_10_ = 0; i_10_ < 6; i_10_++)
					anIntArrayArray809[i_9_][i_10_] = anIntArrayArray808[i_9_][i_10_] = is[i_8_++];
			}
			opponentPlayerIndex = is[i_8_++] & 0xff;
			System.out.println("packet 254 = " + opponentPlayerIndex+":"+selfPlayerIndex);
			timeLeft = is[i_8_++] & 0xff;
			anInt806 = 100;
			aBoolean807 = false;
			anApplet_Sub1_Sub1_Sub1_771.aBoolean604 = false;
		}
		if (i == 253) {
			int i_11_ = is[1];
			int i_12_ = is[2];
			int i_13_ = is[3];
			if (i_11_ != anInt793 || i_12_ != anInt794 || i_13_ != anInt796) {
				packetSetPosition = true;
				anInt793 = i_11_;
				anInt794 = i_12_;
				anInt795 = i_12_ * 96 + 48;
				anInt796 = i_13_;
				anInt797 = 128;
			}
			anIntArrayArray809[i_11_][i_12_] = i_13_;
			if (!packetSetPosition) {
				for (int i_14_ = 0; i_14_ < 7; i_14_++) {
					for (int i_15_ = 0; i_15_ < 6; i_15_++) {
						if (anIntArrayArray809[i_14_][i_15_] != anIntArrayArray808[i_14_][i_15_])
							anIntArrayArray808[i_14_][i_15_] = anIntArrayArray809[i_14_][i_15_];
					}
				}
			}
			anApplet_Sub1_Sub1_Sub1_771.aBoolean604 = false;
		}
	}

	public boolean method150(int[][] is) {
		for (int i = 0; i < 7; i++) {
			for (int i_16_ = 0; i_16_ < 6; i_16_++)
				anIntArrayArray810[i][i_16_] = 0;
		}
		for (int i = 0; i < 6; i++) {
			for (int i_17_ = 0; i_17_ < 7; i_17_++) {
				int i_18_ = anIntArrayArray808[i_17_][i];
				if (i_18_ != 0) {
					if (i_17_ <= 3 && i >= 3) {
						for (int i_19_ = 1; i_19_ < 4; i_19_++) {
							if (anIntArrayArray808[i_17_ + i_19_][i - i_19_] != i_18_)
								break;
							if (i_19_ == 3) {
								for (int i_20_ = 0; i_20_ < 4; i_20_++)
									anIntArrayArray810[i_17_ + i_20_][i - i_20_] = 1;
								return true;
							}
						}
					}
					if (i_17_ <= 3) {
						for (int i_21_ = 1; i_21_ < 4; i_21_++) {
							if (anIntArrayArray808[i_17_ + i_21_][i] != i_18_)
								break;
							if (i_21_ == 3) {
								for (int i_22_ = 0; i_22_ < 4; i_22_++)
									anIntArrayArray810[i_17_ + i_22_][i] = 1;
								return true;
							}
						}
					}
					if (i_17_ <= 3 && i <= 2) {
						for (int i_23_ = 1; i_23_ < 4; i_23_++) {
							if (anIntArrayArray808[i_17_ + i_23_][i + i_23_] != i_18_)
								break;
							if (i_23_ == 3) {
								for (int i_24_ = 0; i_24_ < 4; i_24_++)
									anIntArrayArray810[i_17_ + i_24_][i + i_24_] = 1;
								return true;
							}
						}
					}
					if (i <= 2) {
						for (int i_25_ = 1; i_25_ < 4; i_25_++) {
							if (anIntArrayArray808[i_17_][i + i_25_] != i_18_)
								break;
							if (i_25_ == 3) {
								for (int i_26_ = 0; i_26_ < 4; i_26_++)
									anIntArrayArray810[i_17_][i + i_26_] = 1;
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	public void onLoop() {
		anInt804++;
		if (anApplet_Sub1_Sub1_Sub1_771.mouseDown == 2 && !aBoolean791) {
			if (anInt789 == 0) {
				anInt789 = anApplet_Sub1_Sub1_Sub1_771.mouseDown;
				anInt787 = anApplet_Sub1_Sub1_Sub1_771.mouseX;
				anInt788 = anApplet_Sub1_Sub1_Sub1_771.mouseY;
			}
			int i = anApplet_Sub1_Sub1_Sub1_771.mouseX - anInt787;
			int i_27_ = anApplet_Sub1_Sub1_Sub1_771.mouseY - anInt788;
			anInt785 = anInt785 + i * 2 + 1024 & 0x3ff;
			anInt786 = anInt786 - i_27_ * 2 + 1024 & 0x3ff;
			if (anInt786 > 256 && anInt786 < 600)
				anInt786 = 600;
			if (anInt786 > 960 || anInt786 < 256)
				anInt786 = 960;
			anInt787 = anApplet_Sub1_Sub1_Sub1_771.mouseX;
			anInt788 = anApplet_Sub1_Sub1_Sub1_771.mouseY;
		} else
			anInt789 = 0;
		if (anApplet_Sub1_Sub1_Sub1_771.mouseClick == 1)
			mouseClicked = 1;
		if (packetSetPosition && anInt795 > 0) {
			anInt795 -= anInt797 / 128;
			anInt797 += 40;
			if (anInt795 <= 0) {
				anInt795 = 0;
				packetSetPosition = false;
				anIntArrayArray808[anInt793][anInt794] = anInt796;
				if (method150(anIntArrayArray808))
					anInt811 = 128;
			}
			if (anInt795 <= 0 && aBoolean142)
				anApplet_Sub1_Sub1_Sub1_771.xmPlayer.method85(0, 1, 8000, 63);
		}
		if (anInt811 > 0)
			anInt811--;
		anInt806--;
		if (anInt806 <= 0 && timeLeft > 10) {
			anInt806 = 50;
			timeLeft--;
		}
	}

	public void method116(int i, int i_28_, int i_29_) {
		method153();
		method154();
		if (!packetSetPosition && aBoolean798 && opponentPlayerIndex == selfPlayerIndex && !aBoolean807)
			method155();
		aClass10_773.setCamera(0, 40, 0, i, i_28_, 0, i_29_);
		aClass10_773.method367();
		aClass10_774.setCamera(0, 40, 0, i, i_28_, 0, i_29_);
		aClass10_774.method367();
		aClass10_775.setCamera(0, 40, 0, i, i_28_, 0, i_29_);
		aClass10_775.method367();
	}

	public void onDraw() {
		aClass6_772.method241(0, 0, 512, 384, 0, 4210752);
		if (aBoolean791)
			method152();
		else {
			int i = 1000;
			aClass10_775.method358(anApplet_Sub1_Sub1_Sub1_771.mouseX, anApplet_Sub1_Sub1_Sub1_771.mouseY, 50, 50);
			method116(anInt786, anInt785, i);
		}
		if (packetSetPosition || aBoolean807)
			aClass6_772.method268((anApplet_Sub1_Sub1_Sub1_771.generalGameModules[0]), 7, 12, 1, 16777215);
		else if (opponentPlayerIndex == selfPlayerIndex)
			aClass6_772.method268((anApplet_Sub1_Sub1_Sub1_771.generalGameModules[1]), 7, 12, 1, 16777215);
		else
			aClass6_772.method268((anApplet_Sub1_Sub1_Sub1_771.generalGameModules[2]), 7, 12, 1, 16777215);
		aClass6_772.method268((anApplet_Sub1_Sub1_Sub1_771.generalGameModules[3] + timeLeft), 7, 23, 1, 16777215);
		int i = 460;
		int i_30_ = 4;
		anApplet_Sub1_Sub1_Sub1_771.aClass8_1094.method321(i, i_30_, 46, 28);
		aClass6_772.method267(anApplet_Sub1_Sub1_Sub1_771.generalGameModules[4], i + 22, i_30_ + 13, 1, 0);
		if (aBoolean791)
			aClass6_772.method267((anApplet_Sub1_Sub1_Sub1_771.generalGameModules[5]), i + 22, i_30_ + 23, 1, 0);
		else
			aClass6_772.method267((anApplet_Sub1_Sub1_Sub1_771.generalGameModules[6]), i + 22, i_30_ + 23, 1, 0);
		i = 460;
		i_30_ = 34;
		anApplet_Sub1_Sub1_Sub1_771.aClass8_1094.method321(i, i_30_, 46, 28);
		aClass6_772.method267(anApplet_Sub1_Sub1_Sub1_771.generalGameModules[7], i + 22, i_30_ + 18, 1, 0);
		i = 460;
		i_30_ = 64;
		anApplet_Sub1_Sub1_Sub1_771.aClass8_1094.method321(i, i_30_, 46, 28);
		if (anApplet_Sub1_Sub1_Sub1_771.aBoolean604)
			aClass6_772.method267((anApplet_Sub1_Sub1_Sub1_771.generalGameModules[8]), i + 22, i_30_ + 13, 1, 0);
		else
			aClass6_772.method267((anApplet_Sub1_Sub1_Sub1_771.generalGameModules[9]), i + 22, i_30_ + 13, 1, 0);
		aClass6_772.method267(anApplet_Sub1_Sub1_Sub1_771.generalGameModules[10], i + 22, i_30_ + 23, 1, 0);
		if (aBoolean800) {
			method151(anApplet_Sub1_Sub1_Sub1_771.generalGameModules[11], anApplet_Sub1_Sub1_Sub1_771.generalGameModules[12]);
			if (anApplet_Sub1_Sub1_Sub1_771.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_771.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_771.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_771.mouseY < 188 && mouseClicked == 1) {
				anApplet_Sub1_Sub1_Sub1_771.resign();
				aBoolean800 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_771.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_771.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_771.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_771.mouseY < 188 && mouseClicked == 1)
				aBoolean800 = false;
			aBoolean798 = false;
			mouseClicked = 0;
		} else if (aBoolean801) {
			if (anApplet_Sub1_Sub1_Sub1_771.aBoolean604)
				method151(anApplet_Sub1_Sub1_Sub1_771.generalGameModules[13], anApplet_Sub1_Sub1_Sub1_771.generalGameModules[14]);
			else
				method151(anApplet_Sub1_Sub1_Sub1_771.generalGameModules[15], anApplet_Sub1_Sub1_Sub1_771.generalGameModules[16]);
			if (anApplet_Sub1_Sub1_Sub1_771.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_771.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_771.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_771.mouseY < 188 && mouseClicked == 1) {
				anApplet_Sub1_Sub1_Sub1_771.offerDraw();
				aBoolean801 = false;
			}
			if (anApplet_Sub1_Sub1_Sub1_771.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_771.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_771.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_771.mouseY < 188 && mouseClicked == 1)
				aBoolean801 = false;
			aBoolean798 = false;
			mouseClicked = 0;
		} else {
			i = 460;
			i_30_ = 4;
			if (mouseClicked == 1 && anApplet_Sub1_Sub1_Sub1_771.mouseX > i && anApplet_Sub1_Sub1_Sub1_771.mouseY > i_30_ && anApplet_Sub1_Sub1_Sub1_771.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_771.mouseY < i_30_ + 29) {
				aBoolean791 = !aBoolean791;
				mouseClicked = 0;
			}
			i = 460;
			i_30_ = 34;
			if (mouseClicked == 1 && anApplet_Sub1_Sub1_Sub1_771.mouseX > i && anApplet_Sub1_Sub1_Sub1_771.mouseY > i_30_ && anApplet_Sub1_Sub1_Sub1_771.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_771.mouseY < i_30_ + 29) {
				aBoolean800 = true;
				mouseClicked = 0;
			}
			i = 460;
			i_30_ = 64;
			if (mouseClicked == 1 && anApplet_Sub1_Sub1_Sub1_771.mouseX > i && anApplet_Sub1_Sub1_Sub1_771.mouseY > i_30_ && anApplet_Sub1_Sub1_Sub1_771.mouseX < i + 46 && anApplet_Sub1_Sub1_Sub1_771.mouseY < i_30_ + 29) {
				aBoolean801 = true;
				mouseClicked = 0;
			}
			int i_31_ = -1;
			if (aBoolean791 && anApplet_Sub1_Sub1_Sub1_771.mouseX > 85 && anApplet_Sub1_Sub1_Sub1_771.mouseX < 428)
				i_31_ = (anApplet_Sub1_Sub1_Sub1_771.mouseX - 85) / 49;
			else {
				int i_32_ = aClass10_775.method360();
				if (i_32_ != -1)
					i_31_ = 6 - i_32_ % 7;
			}
			if (!packetSetPosition && opponentPlayerIndex == selfPlayerIndex && !aBoolean807 && i_31_ != -1) {
				if (anIntArrayArray808[i_31_][0] == 0) {
					anInt799 = i_31_;
					aBoolean798 = true;
				} else
					aBoolean798 = false;
			} else
				aBoolean798 = false;
			if (!packetSetPosition && mouseClicked == 1 && opponentPlayerIndex == selfPlayerIndex && !aBoolean807 && i_31_ != -1) {
				for (int i_33_ = 5; i_33_ >= 0; i_33_--) {
					if (anIntArrayArray808[i_31_][i_33_] == 0) {
						packetSetPosition = true;
						aBoolean807 = true;
						anInt793 = i_31_;
						anInt794 = i_33_;
						anInt795 = i_33_ * 96 + 48;
						anInt796 = selfPlayerIndex + 1;
						anInt797 = 128;
						anApplet_Sub1_Sub1_Sub1_771.aClass2_582.putPacketId(255);
						anApplet_Sub1_Sub1_Sub1_771.aClass2_582.p1(i_31_);
						anApplet_Sub1_Sub1_Sub1_771.aClass2_582.p1(i_33_);
						anApplet_Sub1_Sub1_Sub1_771.aClass2_582.flushNoException();
						break;
					}
				}
			}
			mouseClicked = 0;
		}
	}

	public void method151(String string, String string_34_) {
		int i = 160;
		int i_35_ = 80;
		aClass6_772.method240(256 - i / 2 + 10, 150 - i_35_ / 2 + 10, i, i_35_, 0, 128);
		aClass6_772.method242(256 - i / 2, 150 - i_35_ / 2, i, i_35_, pixmap.method247(96, 96, 121));
		aClass6_772.method267(string, 256, 130, 4, 16777215);
		aClass6_772.method267(string_34_, 256, 150, 4, 16777215);
		int i_36_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_771.mouseX > 206 && anApplet_Sub1_Sub1_Sub1_771.mouseX < 246 && anApplet_Sub1_Sub1_Sub1_771.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_771.mouseY < 188)
			i_36_ = 16711680;
		aClass6_772.method267(anApplet_Sub1_Sub1_Sub1_771.generalGameModules[17], 226, 178, 4, i_36_);
		i_36_ = 16777215;
		if (anApplet_Sub1_Sub1_Sub1_771.mouseX > 266 && anApplet_Sub1_Sub1_Sub1_771.mouseX < 306 && anApplet_Sub1_Sub1_Sub1_771.mouseY > 168 && anApplet_Sub1_Sub1_Sub1_771.mouseY < 188)
			i_36_ = 16711680;
		aClass6_772.method267(anApplet_Sub1_Sub1_Sub1_771.generalGameModules[18], 286, 178, 4, i_36_);
	}

	public void method152() {
		int i = 85;
		int i_37_ = 5;
		for (int i_38_ = 0; i_38_ < 7; i_38_++) {
			for (int i_39_ = 0; i_39_ < 6; i_39_++) {
				if (anIntArrayArray808[i_38_][i_39_] > 0 && (anInt811 == 0 || anIntArrayArray810[i_38_][i_39_] == 0 || (anInt804 & 0x10) < 8))
					aClass6_772.method255(i_38_ * 49 + i, i_39_ * 49 + i_37_, anInt776 + (anIntArrayArray808[i_38_][i_39_]));
			}
		}
		if (!packetSetPosition && aBoolean798 && opponentPlayerIndex == selfPlayerIndex && !aBoolean807)
			aClass6_772.method255(anInt799 * 49 + i, i_37_ - 24, anInt776 + selfPlayerIndex + 1);
		if (packetSetPosition) {
			int i_40_ = anInt793 * 49;
			int i_41_ = anInt794 * 49 - anInt795 * 49 / 96;
			aClass6_772.method255(i_40_ + i, i_41_ + i_37_, anInt776 + anInt796);
		}
		for (int i_42_ = 0; i_42_ < 7; i_42_++) {
			for (int i_43_ = 0; i_43_ < 6; i_43_++)
				aClass6_772.method255(i_42_ * 49 + i, i_43_ * 49 + i_37_, anInt776);
		}
	}

	public void method153() {
		aClass10_774.clearObjects();
		anInt782 = 0;
		anInt784 = 0;
		for (int i = 0; i < 7; i++) {
			for (int i_44_ = 0; i_44_ < 6; i_44_++) {
				if (anIntArrayArray808[i][i_44_] > 0) {
					object3d class7;
					if (anIntArrayArray808[i][i_44_] == 1)
						class7 = aClass7Array781[anInt782++];
					else
						class7 = aClass7Array783[anInt784++];
					class7.requestSetTranslate(-(i * 96 - 336 + 48), 0, i_44_ * 96 - 288 + 48);
					if (anInt811 == 0 || anIntArrayArray810[i][i_44_] == 0 || (anInt804 & 0x10) < 8)
						aClass10_774.addObject(class7);
				}
			}
		}
	}

	public void method154() {
		if (packetSetPosition) {
			int i = anInt793 * 96 - 336 + 48;
			int i_45_ = anInt794 * 96 - 288 + 48 - anInt795;
			object3d class7;
			if (anInt796 == 1)
				class7 = aClass7Array781[anInt782++];
			else
				class7 = aClass7Array783[anInt784++];
			class7.requestSetTranslate(-i, 0, i_45_);
			aClass10_774.addObject(class7);
		}
	}

	public void method155() {
		int i = anInt799 * 96 - 336 + 48;
		object3d class7;
		if (selfPlayerIndex == 0)
			class7 = aClass7Array781[anInt782++];
		else
			class7 = aClass7Array783[anInt784++];
		class7.requestSetTranslate(-i, 0, -288);
		aClass10_774.addObject(class7);
	}
}
